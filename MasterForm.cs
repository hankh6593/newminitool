﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class MasterForm : Form
    {
       
        public MasterForm()
        {
            InitializeComponent();
        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Configuration form = new Configuration();
            form.Show();
        }

        private void scryptoTRACEEvaluationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            TrialProductEvaluation form = new TrialProductEvaluation();
            form.Show();
        }

        private void regresionTestEvaluationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ReleasedProductEvaluation form = new ReleasedProductEvaluation();
            form.Show();
        }

        private void automationTestToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            AutomationTest form = new AutomationTest();
            form.Show();
        }

        private void jSONGeneratorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            JSONGenerator form = new JSONGenerator();
            form.Show();
        }

        private void renamecsvFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            FileRenaming form = new FileRenaming();
            form.Show();
        }

        private void mergecsvFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            FileMerging form = new FileMerging();
            form.Show();
        }

        private void honeywellRegressionTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            HoneywellRegressionTest form = new HoneywellRegressionTest();
            form.Show();
        }

        private void filteredComparisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            FilteredComparison form = new FilteredComparison();
            form.Show();
        }

        private void versionComparisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            VersionComparison form = new VersionComparison();
            form.Show();
        }

        private void evaluationHistoryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            EvaluationHistory form = new EvaluationHistory();
            form.Show();
        }

        private void cDSToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            CDxEvaluation form = new CDxEvaluation();
            form.Show();
        }

        private void MasterForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void replaceTextMessageHoneywellCompanyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ReplaceTextMessage form = new ReplaceTextMessage();
            form.Show();
        }
    }
}
