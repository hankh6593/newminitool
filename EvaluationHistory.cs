﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Version = MiniTool.Data.Version;

namespace MiniTool
{
    public partial class EvaluationHistory : MasterForm
    {
        DatabaseConnection db;
        List<ResultForUploading> listDrawChart;
        Product product;
        public EvaluationHistory():base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            progressExport.Visible = false;
        }

        private void cbbOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbOS.SelectedIndex == -1)
            {
                //cbbProduct.SelectedIndex = -1;
                //cbbVersion.SelectedIndex = -1;
                //cbbZone.SelectedIndex = -1;
                //cbbDevice.SelectedIndex = -1;
            }
            else
            {
               
                List<Version> listVersion = db.GetUploadedVersion(cbbOS.Text);
                var bindingVersion = new BindingSource();
                bindingVersion.DataSource = listVersion;

                cbbVersion.DataSource = bindingVersion.DataSource;
                cbbVersion.DisplayMember = "version";
                cbbVersion.ValueMember = "id";
               
            }
        }

        public void LoadDevice()
        {
            List<Device> listDevice = db.GetAllDevices(0);
            Dictionary<int, String> listDisplay = new Dictionary<int, string>();
            for (int i = 0; i < listDevice.Count; i++)
            {
                listDisplay.Add(listDevice[i].id, listDevice[i].model);
            }

            var bindingDevice = new BindingSource();
            bindingDevice.DataSource = listDisplay;

            cbbDevice.DataSource = new BindingSource(listDisplay, null);
            cbbDevice.ValueMember = "Key";
            cbbDevice.DisplayMember = "Value";
        }

        private void cbbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbProduct.SelectedItem != null & cbbProduct.SelectedIndex != -1)
            {
                int productID = -1;
                if(Int32.TryParse(cbbProduct.SelectedValue.ToString(),out productID)){
                    product = db.GetProductByID(productID);
                }
                else
                {
                    product = db.GetProductByID(((Product)(cbbProduct.SelectedValue)).id);
                }
                List<String> listZone = new List<String>();
                int countCDS = 0;
                for (int i = 0; i < product.listZone.Count; i++)
                {
                    if(countCDS == 0 && product.listZone[i].method.Equals("CDS"))
                    {
                        listZone.Add((i + 1) + " - " + product.listZone[i].method);
                        countCDS++;
                    }
                    else if(!product.listZone[i].method.Equals("CDS"))
                    {
                        listZone.Add((i + 1) + " - " + product.listZone[i].method);
                    }

                }
                var bindingZone = new BindingSource();
                bindingZone.DataSource = listZone;

                cbbZone.DataSource = bindingZone.DataSource;
                //   cbbZone.SelectedIndex = -1;
            }
        }

        private void cbbZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbZone.SelectedIndex != -1)
            {
                string type = product.listZone[Int32.Parse(cbbZone.Text.Trim().Split('-')[0]) - 1].method;
                cbbEdge.DataSource = null;
                if (type.Equals("STC"))
                {
                    List<sTCEvaluationDetail> listEvaluationDetail = GetEvaluationDetail((Version)cbbVersion.SelectedItem, (Product)cbbProduct.SelectedItem, cbbDevice.Text, Int32.Parse(cbbZone.Text.Trim().Split('-')[0]));
                    if (listEvaluationDetail != null)
                    {
                        if (listEvaluationDetail.Count != 0)
                        {
                            List<String> listEdge = new List<String>();
                            sTCEvaluationDetail edgeDetail = new sTCEvaluationDetail();
                            for (int i = 0; i < listEvaluationDetail.Count; i++)
                            {
                                if (listEvaluationDetail[i].listEdge.Count > 0)
                                {
                                    edgeDetail = listEvaluationDetail[i];
                                    i = listEvaluationDetail.Count;
                                }
                            }
                            for (int i = 0; i < edgeDetail.listEdge.Count; i++)
                            {
                                listEdge.Add("Edge " + (i + 1));
                            }
                            var bindingEdge = new BindingSource();
                            bindingEdge.DataSource = listEdge;

                            cbbEdge.DataSource = bindingEdge.DataSource;
                        }

                        List<ResultForUploading> listDrawChart = db.GetResultByFullInfo(((Version)cbbVersion.SelectedItem).id, ((Product)cbbProduct.SelectedItem).id, cbbDevice.SelectedValue.ToString(), Int32.Parse(cbbZone.Text.Trim().Split('-')[0]));
                        if (listDrawChart != null && listDrawChart.Count > 0)
                        {
                            ResultForUploading infoFileName = listDrawChart[0];
                            String fileName = db.GetProductByID(infoFileName.productID).project.Replace(" ", "-") + "_EVA_" + db.GetVersionByID(infoFileName.versionID).os + "_" + db.GetVersionByID(infoFileName.versionID).version + "_" + cbbDevice.Text + "_" + infoFileName.productName.Replace(" ", "-");
                            if (cbbZone.Items.Count > 1)
                            {
                                fileName += "_" + "Z" + cbbZone.Text.Substring(0, 1);
                            }
                            txtFileName.Text = fileName;
                        }


                    }
                }else if (type.Equals("Others"))
                {

                }
                else
                {
                    DrawOtherChart(type);
                    List<ResultForUploading> listDrawChart = db.GetResultByFullInfo(((Version)cbbVersion.SelectedItem).id, ((Product)cbbProduct.SelectedItem).id, cbbDevice.Text, Int32.Parse(cbbZone.Text.Trim().Split('-')[0]));
                    if (listDrawChart != null && listDrawChart.Count > 0)
                    {
                        ResultForUploading infoFileName = listDrawChart[0];
                        String fileName = db.GetProductByID(infoFileName.productID).project.Replace(" ", "-") + "_EVA_" + db.GetVersionByID(infoFileName.versionID).os + "_" + db.GetVersionByID(infoFileName.versionID).version + "_" + db.GetDeviceByID((int)cbbDevice.SelectedValue).name.Replace(" ", "-") + "_" + infoFileName.productName.Replace(" ", "-");
                        if (cbbZone.Items.Count > 1)
                        {
                            fileName += "_" + "Z" + cbbZone.Text.Trim().Split('-')[0];
                        }
                        txtFileName.Text = fileName;
                    }
                }

            }
        }

        public List<sTCEvaluationDetail> GetEvaluationDetail(Version version, Product product, String deviceModel, int zone)
        {
            List<sTCEvaluationDetail> listEvaluationDetail = new List<sTCEvaluationDetail>();
            if (cbbVersion.SelectedItem != null && cbbProduct.SelectedItem != null && cbbDevice.SelectedItem != null && cbbZone.Text.Length != 0)
            {
                List<ResultForUploading> listDrawChart = db.GetResultByFullInfo(version.id, product.id, deviceModel, zone);
                for (int i = 0; i < listDrawChart.Count; i++)
                {
                    sTCEvaluationDetail evaluationDetail = Ultility.GetsTCEvaluationDetail(listDrawChart[i].statisticID, i, listDrawChart[i].info, CountEdge(listDrawChart));
                    listEvaluationDetail.Add(evaluationDetail);

                }
                return listEvaluationDetail;
            }
            else
            {
                return null;
            }

        }

        public int CountEdge(List<ResultForUploading> listData)
        {
            int count = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                int countEdge = 0;
                String[] data = listData[i].info.Split(';');
                for (int k = 0; k < data.Length; k++)
                {
                    if (data[k].Contains("Sigma="))
                    {
                        countEdge++;
                    }
                }
                if (countEdge > count)
                {
                    count = countEdge;
                }
            }
            return count;
        }


        public void DrawChart(List<sTCEvaluationDetail> listEvaluationDetail)
        {
            chartEvaluation.ResetAutoValues();
            chartEvaluation.Series.Clear();
            chartEvaluation.Legends[0].Docking = Docking.Bottom;
            int position = cbbEdge.SelectedIndex;
            if (position != -1)
            {
                List<DataDrawChart> listChart = new List<DataDrawChart>();
                for (int i = listEvaluationDetail.Count - 1; i > 0; i--)
                {
                    DataDrawChart data = new DataDrawChart();
                    data.statisticID = listEvaluationDetail[i].statisticID;
                    data.cal = listEvaluationDetail[i].cal;
                    data.center = 0;
                    if (listEvaluationDetail[i].listEdge.Count != 0)
                    {
                        data.shiftingNegative = listEvaluationDetail[i].listEdge[position].shiftingNegative;
                        data.shiftingPositive = listEvaluationDetail[i].listEdge[position].shiftingPositive;
                        data.eTriggerNegative = listEvaluationDetail[i].listEdge[position].eTriggerNegative;
                        data.eTriggerPositive = listEvaluationDetail[i].listEdge[position].eTriggerPositive;
                        data.e = listEvaluationDetail[i].listEdge[position].e;
                    }
                    else
                    {
                        data.shiftingNegative = 0;
                        data.shiftingPositive = 0;
                        data.eTriggerNegative = 0;
                        data.eTriggerPositive = 0;
                        data.e = 0;
                    }

                    listChart.Add(data);
                }
                dgvData.DataSource = listChart;
                dgvData.Columns[0].Width = 50;
                dgvData.Columns[0].HeaderText = "Statistic ID";
                dgvData.Columns[1].Width = 50;
                dgvData.Columns[1].HeaderText = "Center";
                dgvData.Columns[2].Width = 50;
                dgvData.Columns[2].HeaderText = "Trigger 1";
                dgvData.Columns[3].Width = 50;
                dgvData.Columns[3].HeaderText = "Trigger 2";
                dgvData.Columns[4].Width = 50;
                dgvData.Columns[4].HeaderText = "Shifting 1";
                dgvData.Columns[5].Width = 50;
                dgvData.Columns[5].HeaderText = "Shifting 2";
                dgvData.Columns[6].Width = 50;
                dgvData.Columns[6].HeaderText = "Cal";
                dgvData.Columns[7].Width = 50;
                dgvData.Columns[7].HeaderText = "E" + (position + 1);
                var seriesCenter = new Series();
                seriesCenter.ChartType = SeriesChartType.Line;
                seriesCenter.Name = "Center";
                seriesCenter.BorderWidth = 3;
                seriesCenter.Color = Color.Gray;
                chartEvaluation.Series.Add(seriesCenter);
                chartEvaluation.MinimumSize = new Size(1578, 500);
                chartEvaluation.Width = listChart.Count * 10;
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chartEvaluation.Series[0].Points.AddXY(listChart[i].statisticID, listChart[i].center);
                }


                var seriesTrigger1 = new Series();
                seriesTrigger1.ChartType = SeriesChartType.Line;
                seriesTrigger1.Name = "Trigger1";
                seriesTrigger1.BorderWidth = 3;
                seriesTrigger1.Color = Color.Green;
                chartEvaluation.Series.Add(seriesTrigger1);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chartEvaluation.Series[1].Points.AddXY(listChart[i].statisticID, listChart[i].eTriggerPositive);
                }



                var seriesTrigger2 = new Series();
                seriesTrigger2.ChartType = SeriesChartType.Line;
                seriesTrigger2.Name = "Trigger2";
                seriesTrigger2.BorderWidth = 3;
                seriesTrigger2.Color = Color.Green;
                chartEvaluation.Series.Add(seriesTrigger2);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chartEvaluation.Series[2].Points.AddXY(listChart[i].statisticID, listChart[i].eTriggerNegative);
                }


                var seriesShifting1 = new Series();
                seriesShifting1.ChartType = SeriesChartType.Line;
                seriesShifting1.Name = "Shifting1";
                seriesShifting1.BorderWidth = 3;
                seriesShifting1.Color = Color.Yellow;
                chartEvaluation.Series.Add(seriesShifting1);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chartEvaluation.Series[3].Points.AddXY(listChart[i].statisticID, listChart[i].shiftingPositive);
                }


                var seriesShifting2 = new Series();
                seriesShifting2.ChartType = SeriesChartType.Line;
                seriesShifting2.Name = "Shifting2";
                seriesShifting2.BorderWidth = 3;
                seriesShifting2.Color = Color.Yellow;
                chartEvaluation.Series.Add(seriesShifting2);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chartEvaluation.Series[4].Points.AddXY(listChart[i].statisticID, listChart[i].shiftingNegative);
                }


                var seriesCal = new Series();
                seriesCal.ChartType = SeriesChartType.Line;
                seriesCal.Name = "Cal";
                seriesCal.BorderWidth = 3;
                seriesCal.Color = Color.Red;
                chartEvaluation.Series.Add(seriesCal);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chartEvaluation.Series[5].Points.AddXY(listChart[i].statisticID, listChart[i].cal);
                }


                var seriesE = new Series();
                seriesE.ChartType = SeriesChartType.Line;
                seriesE.Name = "E";
                seriesE.BorderWidth = 3;
                seriesE.Color = Color.DarkBlue;
                seriesE.IsValueShownAsLabel = true;
                seriesE.SmartLabelStyle.Enabled = true;
                chartEvaluation.Series.Add(seriesE);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chartEvaluation.Series[6].Points.AddXY(listChart[i].statisticID, listChart[i].e);
                }
            }


        }

        public void DrawOtherChart(String type)
        {

            List<ResultForUploading> listDrawChart = db.GetResultByFullInfo(((Version)cbbVersion.SelectedItem).id, ((Product)cbbProduct.SelectedItem).id, cbbDevice.Text, Int32.Parse(cbbZone.SelectedValue.ToString().Split('-')[0].TrimEnd()));
            if (listDrawChart != null && listDrawChart.Count > 0)
            {
                chartEvaluation.ResetAutoValues();
                chartEvaluation.Series.Clear();
                chartEvaluation.Legends[0].Docking = Docking.Bottom;
                dgvData.DataSource = null;

                int zone = Int32.Parse(cbbZone.Text.Trim().Split('-')[0]);


                dgvData.DataSource = null;
                if (type.Equals("CDS"))
                {
                    List<DataDrawChart> listChart = new List<DataDrawChart>();
                    List<String> listFineSigma = new List<String>();
                    for (int i = listDrawChart.Count - 1; i > 0; i--)
                    {
                        if (listDrawChart[i].zone == zone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.pointValue = Double.Parse(listDrawChart[i].value);
                            data.statisticID = listDrawChart[i].statisticID;
                            listChart.Add(data);
                            listFineSigma.Add(listDrawChart[i].info);
                        }

                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Statistic ID");
                    dt.Columns.Add("CDS Value");
                    for (int i = 0; i < listChart.Count; i++)
                    {
                        dt.Rows.Add();
                        dt.Rows[dt.Rows.Count - 1][0] = listChart[i].statisticID;
                        dt.Rows[dt.Rows.Count - 1][1] = listChart[i].pointValue;
                    }
                    dgvData.DataSource = dt;
                    Random random = new Random();
                    var series = new Series();
                    series.Name = "Zone " + zone;
                    series.ChartType = SeriesChartType.Point;
                    series.IsValueShownAsLabel = true;
                    series.MarkerSize = 8;
                    series.MarkerStyle = MarkerStyle.Circle;
                    series.Color = Color.GreenYellow;
                    chartEvaluation.Series.Add(series);
                    for (int j = 0; j < listChart.Count; j++)
                    {
                        chartEvaluation.Series[0].Points.AddXY(listChart[j].statisticID, listChart[j].pointValue);
                    }
                }
                if (type.Equals("Others"))
                {

                }
                else
                {
                    List<DataDrawChart> listChart = new List<DataDrawChart>();
                    for (int i = listDrawChart.Count - 1; i > 0; i--)
                    {
                        if (listDrawChart[i].zone == zone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.pointValue = Double.Parse(listDrawChart[i].value);
                            data.statisticID = listDrawChart[i].statisticID;
                            listChart.Add(data);
                        }

                    }
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Statistic ID");
                    dt.Columns.Add(type + " Value");
                    for (int i = 0; i < listChart.Count; i++)
                    {
                        dt.Rows.Add();
                        dt.Rows[dt.Rows.Count - 1][0] = listChart[i].statisticID;
                        dt.Rows[dt.Rows.Count - 1][1] = listChart[i].pointValue;

                    }
                    dgvData.DataSource = dt;
                    Random random = new Random();
                    var series = new Series();
                    series.Name = "Zone " + zone;
                    series.ChartType = SeriesChartType.Point;
                    series.IsValueShownAsLabel = true;
                    series.MarkerSize = 8;
                    series.MarkerStyle = MarkerStyle.Circle;
                    series.Color = Color.GreenYellow;
                    chartEvaluation.MinimumSize = new Size(1578, 500);
                    chartEvaluation.Width = listChart.Count * 10;
                    chartEvaluation.Series.Add(series);
                    for (int j = 0; j < listChart.Count; j++)
                    {
                        chartEvaluation.Series[0].Points.AddXY(listChart[j].statisticID, listChart[j].pointValue);
                    }
                }

            }

        }

        private void cbbEdge_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbEdge.SelectedIndex != -1)
            {
                int id = 0;

                if (Int32.TryParse(cbbDevice.SelectedValue.ToString(), out id))
                {
                    DrawChart(GetEvaluationDetail((Version)cbbVersion.SelectedItem, (Product)cbbProduct.SelectedItem, cbbDevice.Text, Int32.Parse(cbbZone.Text.Trim().Split('-')[0])));
                    this.listDrawChart = db.GetResultByFullInfo(((Version)cbbVersion.SelectedItem).id, ((Product)cbbProduct.SelectedItem).id, cbbDevice.Text, Int32.Parse(cbbZone.Text.Trim().Split('-')[0]));

                }
                else
                {
                    DrawChart(GetEvaluationDetail((Version)cbbVersion.SelectedItem, (Product)cbbProduct.SelectedItem, cbbDevice.Text, Int32.Parse(cbbZone.Text.Trim().Split('-')[0])));
                    this.listDrawChart = db.GetResultByFullInfo(((Version)cbbVersion.SelectedItem).id, ((Product)cbbProduct.SelectedItem).id, cbbDevice.Text, Int32.Parse(cbbZone.Text.Trim().Split('-')[0]));

                }
                progressExport.Visible = false;
            }
        }

        private void cbbVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = -1;
            List<String> listDevice = new List<String>();
            if (Int32.TryParse(cbbVersion.SelectedValue.ToString(), out id)){
                 listDevice = db.GetUploadedDevice(id);
            }
            else
            {
                listDevice = db.GetUploadedDevice(((Version)cbbVersion.SelectedValue).id);
            }
            var bindingDevice = new BindingSource();
            bindingDevice.DataSource = listDevice;
            cbbDevice.DataSource = bindingDevice.DataSource;
        }

        private void cbbDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = -1;
            if (Int32.TryParse(cbbVersion.SelectedValue.ToString(), out id)){
                id = Int32.Parse(cbbVersion.SelectedValue.ToString());
            }
            else
            {
                id = ((Version)(cbbVersion.SelectedValue)).id;
            }
            List<Product> listProduct = db.GetUploadedProduct(id, cbbDevice.Text);
            var bindingProduct = new BindingSource();
            bindingProduct.DataSource = listProduct;

            cbbProduct.DataSource = bindingProduct.DataSource;
            cbbProduct.DisplayMember = "productName";
            cbbProduct.ValueMember = "id";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cbbDevice.SelectedIndex = -1;
            cbbOS.SelectedIndex = -1;
            cbbProduct.SelectedIndex = -1;
            cbbEdge.SelectedIndex = -1;
            cbbZone.SelectedIndex = -1;
            cbbVersion.SelectedIndex = -1;
            txtFileName.Text = "";
            txtResultPath.Text = "";
        }

        private void btnBrowseResult_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.ShowDialog();
            txtResultPath.Text = folderBrowserDialog.SelectedPath;
        }

        private void Export_Click(object sender, EventArgs e)
        {
            List<ResultForUploading> listDrawChart = db.GetResultByFullInfo(((Version)cbbVersion.SelectedItem).id, ((Product)cbbProduct.SelectedItem).id, cbbDevice.Text, Int32.Parse(cbbZone.SelectedValue.ToString().Split('-')[0].TrimEnd()));
            List<ResultForEvaluation> listDataForEvaluation = new List<ResultForEvaluation>();
            foreach(ResultForUploading evaUpload in listDrawChart)
            {
                if (!evaUpload.method.Equals("Others"))
                {
                    ResultForEvaluation eva = new ResultForEvaluation();
                    eva.createdDate = evaUpload.createdDate;
                    eva.deviceCode = evaUpload.deviceModel;
                    eva.deviceEmail = evaUpload.deviceModel;
                    eva.deviceID = -1;
                    eva.deviceModel = evaUpload.deviceModel;
                    eva.id = evaUpload.id;
                    eva.info = evaUpload.info;
                    eva.method = evaUpload.method;
                    eva.productID = evaUpload.productID;
                    eva.productName = evaUpload.productName;
                    eva.resultText = evaUpload.resultText;
                    eva.statisticID = evaUpload.statisticID;
                    eva.value = evaUpload.value;
                    eva.version = evaUpload.version;
                    eva.versionID = evaUpload.versionID;
                    eva.zone = evaUpload.zone;
                    listDataForEvaluation.Add(eva);
                }
               
            }

            if (listDrawChart != null)
            {
                int countCDS = 0;
                int countOthers = 0;
                for (int i = 0; i < product.listZone.Count; i++)
                {
                    if (product.listZone[i].method.Equals("STC"))
                    {
                        progressExport.Value = 0;
                        progressExport.Visible = true;
                        List<ResultForEvaluation> listSave = Ultility.GetResultForEvaluationByZone(listDataForEvaluation, product.listZone[i].zoneNo);
                        int edgeQty = Ultility.GetEdgeQtyFromUploadedData(listSave);
                        String fileName = @"" + txtResultPath.Text + @"\" + txtFileName.Text + "_Z" + product.listZone[i].zoneNo;
                        Ultility.ExportSTCZoneEvaluation(progressExport, listSave, product.listZone[i].zoneNo, edgeQty, fileName);
                    }
                    else if (product.listZone[i].method.Equals("CDS"))
                    {
                        if (countCDS == 0)
                        {
                            progressExport.Value = 0;
                            progressExport.Visible = true;
                            List<ResultForEvaluation> listSave = Ultility.GetResultForEvaluationByZone(listDataForEvaluation, product.listZone[i].zoneNo);
                            String fileName = @"" + txtResultPath.Text + @"\" + txtFileName.Text;
                            Ultility.ExportCDSZoneEvaluation(progressExport, listSave, product.listZone[i].zoneNo, fileName);
                            countCDS++;
                        }
                    }
                    else if (product.listZone[i].method.Equals("Others"))
                    {
                        countOthers++;
                    }
                }
                MessageBox.Show("Evaluation file has been exported for " + (product.listZone.Count - countCDS - countOthers) + " zone(s).");
            }
        }
    }
}
