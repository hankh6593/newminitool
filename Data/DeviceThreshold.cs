﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class DeviceThreshold
    {
        [DisplayName("Device Model")]
        public string deviceModel { get; set; }

        [DisplayName("Threshold")]
        public double threshold { get; set; }

        [DisplayName("Blur Threshold")]
        public string quality_Blur_Threshold { get; set; }

        [DisplayName("Version")]
        public string version { get; set; }

        [DisplayName("Comment")]
        public string comment { get; set; }

        public DeviceThreshold(string deviceModel, double threshold, string quality_Blur_Threshold, string version, string comment)
        {
            this.deviceModel = deviceModel;
            this.threshold = threshold;
            this.quality_Blur_Threshold = quality_Blur_Threshold;
            this.version = version;
            this.comment = comment;
        }

        public DeviceThreshold(string deviceModel)
        {
            this.deviceModel = deviceModel;
            this.threshold = 0.0;
            this.quality_Blur_Threshold = "100";
            this.version = "1.0";
            this.comment = "";
        }

        public DeviceThreshold()
        {
            this.deviceModel = "";
            this.threshold = 0.0;
            this.quality_Blur_Threshold = "100";
            this.version = "1.0";
            this.comment = "";
        }
    }
}
