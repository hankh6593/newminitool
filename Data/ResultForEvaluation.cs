﻿using MiniTool.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class ResultForEvaluation
    {
        public int id { get; set; }
        public String statisticID { get; set; }
        public int versionID { get; set; }
        public String version { get; set; }
        public int deviceID { get; set; }
        public String deviceCode { get; set; }
        public String deviceName { get; set; }
        public String deviceModel { get; set; }
        public String deviceEmail { get; set; }
        public int productID { get; set; }
        public String productName { get; set; }
        public String createdDate { get; set; }
        public String resultText { get; set; }
        public int zone { get; set; }
        public String method { get; set; }
        public String value { get; set; }
        public String info { get; set; }

        public ResultForEvaluation()
        {
            this.statisticID = "";
            this.versionID = -1;
            this.deviceID = -1;
            this.productID = -1;
            this.resultText = "";
            this.zone = 0;
            this.method = "";
            this.value = "";
            this.info = "";
        }

        //public ResultForEvaluation(int id, string statisticID, int versionID, string version, int deviceID, string deviceCode, string deviceName, string deviceModel, string deviceEmail, int productID, string productName, string createdDate, string resultText, int zone, string method, string value, string info)
        //{
        //    this.id = id;
        //    this.statisticID = statisticID ?? throw new ArgumentNullException(nameof(statisticID));
        //    this.versionID = versionID;
        //    this.version = version ?? throw new ArgumentNullException(nameof(version));
        //    this.deviceID = deviceID;
        //    this.deviceCode = deviceCode ?? throw new ArgumentNullException(nameof(deviceCode));
        //    this.deviceName = deviceName ?? throw new ArgumentNullException(nameof(deviceName));
        //    this.deviceModel = deviceModel ?? throw new ArgumentNullException(nameof(deviceName));
        //    this.deviceEmail = deviceEmail ?? throw new ArgumentNullException(nameof(deviceEmail));
        //    this.productID = productID;
        //    this.productName = productName ?? throw new ArgumentNullException(nameof(productName));
        //    this.createdDate = createdDate;
        //    this.resultText = resultText ?? throw new ArgumentNullException(nameof(resultText));
        //    this.zone = zone;
        //    this.method = method ?? throw new ArgumentNullException(nameof(method));
        //    this.value = value ?? throw new ArgumentNullException(nameof(value));
        //    this.info = info ?? throw new ArgumentNullException(nameof(info));
        //}

        public ResultForEvaluation(string statisticID, int versionID, int deviceID, int productID, string resultText, int zone, string method, string value, string info)
        {
            DatabaseConnection db = new DatabaseConnection();
            this.statisticID = statisticID ?? throw new ArgumentNullException(nameof(statisticID));
            this.versionID = versionID;
            this.deviceID = deviceID;
            this.productID = productID;

            this.version = db.GetVersionByID(versionID).version;

            this.deviceCode = db.GetDeviceByID(deviceID).code;
            this.deviceName = db.GetDeviceByID(deviceID).name;
            this.deviceEmail = db.GetDeviceByID(deviceID).email;

            this.productName = db.GetProductByID(productID).productName;
            this.resultText = resultText ?? throw new ArgumentNullException(nameof(resultText));
            this.zone = zone;
            this.method = method ?? throw new ArgumentNullException(nameof(method));
            this.value = value ?? throw new ArgumentNullException(nameof(value));
            this.info = info ?? throw new ArgumentNullException(nameof(info));
        }

        public ResultForEvaluation(string statisticID, int versionID, Device device, int productID, string resultText)
        {
            DatabaseConnection db = new DatabaseConnection();
            this.statisticID = statisticID ?? throw new ArgumentNullException(nameof(statisticID));
            this.versionID = versionID;
            this.deviceID = device.id;
            this.productID = productID;

            this.version = db.GetVersionByID(versionID).version;

            this.deviceCode = device.code;
            this.deviceName = device.name;
            this.deviceEmail = device.email;

            this.productName = db.GetProductByID(productID).productName;

            this.resultText = resultText ?? throw new ArgumentNullException(nameof(resultText));
        }
    }
}
