﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class Preset
    {
        public int id { get; set; }
        public String name { get; set; }
        public int deviceID { get; set; }
        public String platformName { get; set; }
        public double platformVersion { get; set; }
        public int productID { get; set; }
        public bool isFlash { get; set; }
        public bool isManual { get; set; }
        public String appPackage { get; set; }
        public String appActivity { get; set; }
        public String appiumHost { get; set; }
        public Int64 appiumPort { get; set; }
        public int quantity { get; set; }
        public bool isImageSet { get; set; }

        public Preset(int id, string name, int deviceID, string platformName, double platformVersion, int productID, bool isFlash, bool isManual, string appPackage, string appActivity, string appiumHost, Int64 appiumPort, int quantity, bool isImageSet)
        {
            this.id = id;
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.deviceID = deviceID;
            this.platformName = platformName ?? throw new ArgumentNullException(nameof(platformName));
            this.platformVersion = platformVersion;
            this.productID = productID;
            this.isFlash = isFlash;
            this.isManual = isManual;
            this.appPackage = appPackage ?? throw new ArgumentNullException(nameof(appPackage));
            this.appActivity = appActivity ?? throw new ArgumentNullException(nameof(appActivity));
            this.appiumHost = appiumHost ?? throw new ArgumentNullException(nameof(appiumHost));
            this.appiumPort = appiumPort;
            this.quantity = quantity;
            this.isImageSet = isImageSet;
        }

        public Preset()
        {
            this.id = -1;
            this.name = "";
            this.deviceID = -1;
            this.platformName = "";
            this.platformVersion = 0.0;
            this.productID = -1;
            this.isFlash = false;
            this.isManual = false;
            this.appPackage = "";
            this.appActivity = "";
            this.appiumHost = "";
            this.appiumPort = 0;
            this.quantity = 0;
            this.isImageSet = false;
        }
    }
}
