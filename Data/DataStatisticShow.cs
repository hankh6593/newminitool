﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class DataStatisticShow
    {
        [DisplayName("Draw")]
        public bool DrawChart { get; set; }

        [DisplayName("AppVersion")]
        public String versionName { get; set; }
        [DisplayName("Product")]
        public String productName { get; set; }
        [DisplayName("Device")]
        public String deviceModel { get; set; }
        [DisplayName("Zone")]
        public String zone { get; set; }
        [DisplayName("Edge")]
        public String edgeNumber { get; set; }
    }
}
