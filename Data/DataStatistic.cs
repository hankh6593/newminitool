﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    class DataStatistic
    {
        public bool isShowed { get; set; }
        public int versionID { get; set; }
        public String versionName { get; set; }

        public int productID { get; set; }
        public String productName { get; set; }
        public String deviceModel { get; set; }

        public int zone { get; set; }
        public String method { get; set; }
        public String edgeNumber { get; set; }
    }
}
