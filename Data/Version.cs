﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class Version
    {
        [DisplayName("Version ID")]
        public int id { get; set; }

        [DisplayName("App Version")]
        public String version { get; set; }

        [DisplayName("OS")]
        public String os { get; set; }

        public Version()
        {
            this.id = -1;
            this.version = "";
            this.os = "";
        }

        public Version(int id, string version, string os)
        {
            this.id = id;
            this.version = version ?? throw new ArgumentNullException(nameof(version));
            this.os = os ?? throw new ArgumentNullException(nameof(os));
        }
    }
}
