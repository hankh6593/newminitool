﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class Product
    {
        [DisplayName("No.")]
        public int id { get; set; }

        [DisplayName("Project")]
        public String project { get; set; }

        [DisplayName("Company ID")]
        public int companyID { get; set; }

        [DisplayName("Company Name")]
        public String companyName { get; set; }

        [DisplayName("Product ID")]
        public int productID { get; set; }

        [DisplayName("Product Name")]
        public String productName { get; set; }
        public String result { get; set; }

        public String type { get; set; }

        public List<Zone> listZone { get; set; }

        public Product()
        {
            this.id = -1;
            this.project = "";
            this.companyID = -1;
            this.companyName = "";
            this.productID = -1;
            this.productName = "";
            this.result = "";
            this.type = "";
            this.listZone = new List<Zone>();
        }

        public Product(int id, string project, int companyID, string companyName, int productID, string productName, string result, string type)
        {
            this.id = id;
            this.project = project;
            this.companyID = companyID;
            this.companyName = companyName;
            this.productID = productID;
            this.productName = productName;
            this.result = result;
            this.type = type;
            this.listZone = new List<Zone>();
        }

        public Product(int id, string project, int companyID, string companyName, int productID, string productName, string result, string type, List<Zone> listZone)
        {
            this.id = id;
            this.project = project;
            this.companyID = companyID;
            this.companyName = companyName;
            this.productID = productID;
            this.productName = productName;
            this.result = result;
            this.type = type;
            this.listZone = listZone;
        }
    }
}
