﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class Zone
    {
        public int id { get; set; }
        public int productID { get; set; }
        public int zoneNo { get; set; }
        public String method { get; set; }
        public String code { get; set; }
        public String mov { get; set; }
        public Double threshold { get; set; }

        public Zone(int id, int productID, int zoneNo, string method, string code, string mov, Double threshold)
        {
            this.id = id;
            this.productID = productID;
            this.zoneNo = zoneNo;
            this.method = method;
            this.mov = mov;
            this.code = code;
            this.threshold = threshold;
        }

        public Zone()
        {
            this.id = -1;
            this.productID = -1;
            this.zoneNo = -1;
            this.method = "";
            this.mov = "";
            this.code = "";
            this.threshold = 0;
        }
    }
}
