﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class ThresholdImported
    {
        public String deviceID { get; set; }
        public int productID { get; set; }
        public String server { get; set; }
        public double threshold { get; set; }

        public ThresholdImported()
        {
            this.deviceID = "";
            this.productID = -1;
            this.server = "";
            this.threshold = 0;
        }

        public ThresholdImported(string deviceID, int productID, string server, double threshold)
        {
            this.deviceID = deviceID;
            this.productID = productID;
            this.server = server;
            this.threshold = threshold;
        }
    }
}
