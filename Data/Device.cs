﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class Device
    {
        [DisplayName("No.")]
        public int id { get; set; }

        [DisplayName("Device Model")]
        public String model { get; set; }

        [DisplayName("Device Code")]
        public String code { get; set; }


        [DisplayName("Device Name")]
        public String name { get; set; }

        [DisplayName("Device User")]
        public String email { get; set; }


        [DisplayName("Location")]
        public String location { get; set; }

        [DisplayName("Type")]
        public String type { get; set; }

        public String UDID { get; set; }

        public bool isAnonymous { get; set; }

        public Device()
        {
            this.id = -1;
            this.model = "";
            this.code = "";
            this.name = "";
            this.email = "";
            this.location = "";
            this.type = "";
            this.UDID = "";
            this.isAnonymous = false;
        }

        public Device(int id, string model, string code, string email, string name,  string location, string type, string UDID, bool isAnonymous)
        {
            this.id = id;
            this.model = model;
            this.code = code;
            this.name = name;
            this.email = email;
            this.location = location;
            this.type = type;
            this.UDID = UDID;
            this.isAnonymous = isAnonymous;
        }
    }
}
