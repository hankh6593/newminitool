﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class sTCEvaluationDetail
    {
        public String statisticID { get; set; }
        public int zone { get; set; }
        public int cal { get; set; }
        public int stdDev { get; set; }
        public int noRL { get; set; }
        public int noVL { get; set; }
        public int noPL { get; set; }
        public int noTL { get; set; }
        public double blurScore { get; set; }
        public double errorOnFit { get; set; }
        public double angle { get; set; }
        public double shiftThreshold { get; set; }
        public double mitigation { get; set; }
        public string v { get; set; }
        public List<EdgeDetail> listEdge { get; set; }

        public bool isValid { get; set; }

        public sTCEvaluationDetail()
        {
            this.statisticID = "";
            this.zone = 0;
            this.cal = 0;
            this.stdDev = 0;
            this.noRL = 0;
            this.noVL = 0;
            this.noPL = 0;
            this.noTL = 0;
            this.blurScore = 0;
            this.errorOnFit = 0;
            this.angle = 0;
            this.shiftThreshold = 0;
            this.mitigation = 0;
            this.v = "";
            this.listEdge = new List<EdgeDetail>();
            this.isValid = true;
        }

        public sTCEvaluationDetail(string statisticID, int zone, int cal, int stdDev, int noRL, int noVL, int noPL, int noTL, double blurScore, double errorOnFit, double angle, double shiftThreshold, double mitigation, string v, List<EdgeDetail> listEdge)
        {
            this.statisticID = statisticID ?? throw new ArgumentNullException(nameof(statisticID));
            this.zone = zone;
            this.cal = cal;
            this.stdDev = stdDev;
            this.noRL = noRL;
            this.noVL = noVL;
            this.noPL = noPL;
            this.noTL = noTL;
            this.blurScore = blurScore;
            this.errorOnFit = errorOnFit;
            this.angle = angle;
            this.shiftThreshold = shiftThreshold;
            this.mitigation = mitigation;
            this.v = v ?? throw new ArgumentNullException(nameof(v));
            this.listEdge = listEdge ?? throw new ArgumentNullException(nameof(listEdge));
        }
    }
}
