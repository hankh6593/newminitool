﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class DeviceID
    {
        public int id { get; set; }
        public String deviceBrand { get; set; }
        public String deviceModel { get; set; }
        public String deviceID { get; set; }
        public String server { get; set; }

        public DeviceID()
        {
            this.id = -1;
            this.deviceBrand = "";
            this.deviceModel = "";
            this.deviceID = "";
        }
        public DeviceID(int id, string deviceBrand, string deviceID, string deviceModel, string server)
        {
            this.server = "";
            this.id = id;
            this.deviceBrand = deviceBrand;
            this.deviceModel = deviceModel;
            this.deviceID = deviceID;
        }


        public DeviceID(string deviceBrand, string deviceID, string deviceModel, string server)
        {
            this.deviceBrand = deviceBrand;
            this.deviceModel = deviceModel;
            this.deviceID = deviceID;
            this.server = server;
        }
    }
}
