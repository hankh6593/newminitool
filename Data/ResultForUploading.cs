﻿using MiniTool.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class ResultForUploading
    {
        public int id { get; set; }
        public String statisticID { get; set; }
        public int versionID { get; set; }
        public String version { get; set; }
        public String deviceModel { get; set; }
        public int productID { get; set; }
        public String productName { get; set; }
        public String createdDate { get; set; }
        public String resultText { get; set; }
        public int zone { get; set; }
        public String method { get; set; }
        public String value { get; set; }
        public String info { get; set; }

        public ResultForUploading()
        {
            this.statisticID = "";
            this.versionID = -1;
            this.productID = -1;
            this.resultText = "";
            this.zone = 0;
            this.method = "";
            this.value = "";
            this.info = "";
        }

        public ResultForUploading(int id, string statisticID, int versionID, string version, string deviceModel, int productID, string productName, string createdDate, string resultText, int zone, string method, string value, string info)
        {
            DatabaseConnection db = new DatabaseConnection();
            this.id = id;
            this.statisticID = statisticID ?? throw new ArgumentNullException(nameof(statisticID));
            this.versionID = versionID;
            this.version = version ?? throw new ArgumentNullException(nameof(version));
            this.deviceModel = deviceModel;
            this.productID = productID;
            this.productName = productName ?? throw new ArgumentNullException(nameof(productName));
            this.createdDate = createdDate;
            this.resultText = resultText ?? throw new ArgumentNullException(nameof(resultText));
            this.zone = zone;
            this.method = method ?? throw new ArgumentNullException(nameof(method));
            this.value = value ?? throw new ArgumentNullException(nameof(value));
            this.info = info ?? throw new ArgumentNullException(nameof(info));
        }

        public ResultForUploading(string statisticID, int versionID, string deviceModel, int productID, string resultText, int zone, string method, string value, string info)
        {
            DatabaseConnection db = new DatabaseConnection();
            this.statisticID = statisticID ?? throw new ArgumentNullException(nameof(statisticID));
            this.versionID = versionID;
            this.productID = productID;

            this.version = db.GetVersionByID(versionID).version;
            this.productName = db.GetProductByID(productID).productName;
            this.deviceModel = deviceModel;
            this.resultText = resultText ?? throw new ArgumentNullException(nameof(resultText));
            this.zone = zone;
            this.method = method ?? throw new ArgumentNullException(nameof(method));
            this.value = value ?? throw new ArgumentNullException(nameof(value));
            this.info = info ?? throw new ArgumentNullException(nameof(info));
        }

        public ResultForUploading(string statisticID, int versionID, string deviceModel, int productID, string resultText)
        {
            DatabaseConnection db = new DatabaseConnection();
            this.statisticID = statisticID ?? throw new ArgumentNullException(nameof(statisticID));
            this.versionID = versionID;
            this.productID = productID;

            this.version = db.GetVersionByID(versionID).version;

            this.deviceModel = deviceModel;

            this.productName = db.GetProductByID(productID).productName;

            this.resultText = resultText ?? throw new ArgumentNullException(nameof(resultText));
        }
    }
}
