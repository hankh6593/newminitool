﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class EdgeDetail
    {
        public string edgeName { get; set; }
        public int center { get; set; }
        public int e { get; set; }
        public int eTriggerNegative { get; set; }
        public int eTriggerPositive { get; set; }
        public String eMov { get; set; }
        public int shiftingNegative { get; set; }
        public int shiftingPositive { get; set; }


        public EdgeDetail(int e, int eTriggerNegative, int eTriggerPositive, string eMov, int shifting)
        {
            this.e = e;
            this.eTriggerNegative = eTriggerNegative;
            this.eTriggerPositive = eTriggerPositive;
            this.eMov = eMov ?? throw new ArgumentNullException(nameof(eMov));
            this.shiftingPositive = shifting;
            this.shiftingNegative = shifting * -1;
            this.center = 0;
        }

        public EdgeDetail()
        {
            this.e = 0;
            this.eTriggerNegative = 0;
            this.eTriggerPositive = 0;
            this.eMov = "0,00";
            this.shiftingPositive = 0;
            this.shiftingNegative = 0;
            this.center = 0;
        }
    }
}
