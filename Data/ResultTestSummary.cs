﻿using MiniTool.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class ResultTestSummary
    {
        public String testName { get; set; }

        public List<ResultTestInfo> listResult { get; set; }
        public int countValid { get; set; }
        public int countQualityReject { get; set; }
        public int countQualityRejectWrongDebug { get; set; }
        public int countNotRead { get; set; }
        public int countWrongCode { get; set; }
        public double MinResult { get; set; }
        public double MaxResult { get; set; }

        public String summary { get; set; }


        public ResultTestSummary()
        {
            this.testName = "";
            this.countValid = 0;
            this.countNotRead = 0;
            this.countQualityReject = 0;
            this.countQualityRejectWrongDebug = 0;
            this.countWrongCode = 0;

        }
        public void GetTestResult(Zone zone, int zoneNo)
        {
            this.countValid = 0;
            this.countNotRead = 0;
            this.countQualityReject = 0;
            this.countQualityRejectWrongDebug = 0;
            this.countWrongCode = 0;
            List<ResultTestInfo> listZoneResult = GetListResultTestInfoByZone(listResult, zone.zoneNo);
            if (!zone.method.Equals("STC") && !zone.method.Equals("Others"))
            {
                this.MinResult = Double.Parse(listZoneResult[0].resultZone);
                this.MaxResult = Double.Parse(listZoneResult[0].resultZone);
            }
            if(zone.method.Equals("CD1") || zone.method.Equals("CD2") || zone.method.Equals("CD3"))
            {
                this.MinResult = GetMinResult(listZoneResult);
                this.MaxResult = GetMinResult(listZoneResult);
            }
            for (int i = 0; i < listZoneResult.Count; i++)
            {
                ResultTestInfo result = listZoneResult[i];

                if (zone.method.Equals("STC"))
                {
                    String[] zoneCode = zone.code.Split(',');
                    String[] zoneMov = zone.mov.Split(',');
                    int countInvalid1 = 0;
                    int countInvalid2 = 0;
                    int countInvalid3 = 0;
                    if (Convert.ToInt64(result.resultZone) == -1 && Ultility.FindPosition(result.infoZone.Split(';').ToList(), "Mov") == -1)
                    {
                        countNotRead++;
                    }
                    else
                    {
                        for (int j = 0; j < zoneCode.Length; j++)
                        {
                            String pattern = "Mov=" + zoneMov[j];
                            if (result.infoZone != null)
                            {
                                List<String> listInfoZone = result.infoZone.Split(';').ToList();
                                if (Ultility.FindPosition(listInfoZone, "Mov") != -1)
                                {
                                    int position = Ultility.FindPosition(listInfoZone, "Mov");
                                    if (Convert.ToInt64(result.resultZone) == -1 && listInfoZone[position].Equals(pattern))
                                    {
                                        countInvalid1++;
                                    }
                                    else if (Convert.ToInt64(result.resultZone) == -1 && !listInfoZone[position].Equals(pattern))
                                    {
                                        countInvalid2++;
                                    }

                                    if (!result.resultZone.Equals(zoneCode[j]) && Convert.ToInt64(result.resultZone) != -1 && !listInfoZone[position].Equals(pattern))
                                    {
                                        countInvalid3++;
                                    }
                                }

                            }

                        }

                    }

                    if (countInvalid1 == zoneCode.Length)
                    {
                        countQualityReject++;
                    }
                    if (countInvalid2 == zoneCode.Length)
                    {
                        countQualityRejectWrongDebug++;
                    }
                    if (countInvalid3 == zoneCode.Length)
                    {
                        countWrongCode++;
                    }
                }
                else if (zone.method.Equals("Others"))
                {

                }
                else if(zone.method.Equals("CDS"))
                {

                    if (Double.Parse(result.resultZone) < MinResult && Ultility.FindPosition(result.infoZone.Split(';').ToList(), "FineSigma") != -1)
                    {
                        MinResult = Double.Parse(result.resultZone);
                    }

                    if (Double.Parse(result.resultZone) > MaxResult && Ultility.FindPosition(result.infoZone.Split(';').ToList(), "FineSigma") != -1)
                    {
                        MaxResult = Double.Parse(result.resultZone);
                    }
                    if (Convert.ToDouble(result.resultZone) == -1 && Ultility.FindPosition(result.infoZone.Split(';').ToList(), "FineSigma") == -1)
                    {
                        countNotRead++;
                    }
                }
                else if (zone.method.Equals("CD1") || zone.method.Equals("CD2") || zone.method.Equals("CD3"))
                {
                    

                    if (Double.Parse(result.resultZone) < MinResult && Double.Parse(result.resultZone)!=-1)
                    {
                        MinResult = Double.Parse(result.resultZone);
                    }

                    if (Double.Parse(result.resultZone) > MaxResult && Double.Parse(result.resultZone) != -1)
                    {
                        MaxResult = Double.Parse(result.resultZone);
                    }
                    if (Convert.ToDouble(result.resultZone) == -1)
                    {
                        countNotRead++;
                    }
                }

            }
            this.countValid = CountValidQty(listZoneResult);
        }

        public List<ResultTestInfo> GetListResultTestInfoByZone(List<ResultTestInfo> listTestInfo, int zone)
        {
            List<ResultTestInfo> listResult = new List<ResultTestInfo>();
            foreach (ResultTestInfo result in listTestInfo)
            {
                if (result.zone == zone)
                {
                    listResult.Add(result);
                }
            }
            return listResult;
        }
        public int CountValidQty(List<ResultTestInfo> listSummary)
        {
            return listSummary.Count - this.countQualityReject - this.countQualityRejectWrongDebug - this.countWrongCode - this.countNotRead;

        }

        public double GetMinResult(List<ResultTestInfo> listZoneResult)
        {
            double min = -1; 
            for(int i = 0; i< listZoneResult.Count; i++)
            {
                if (Double.Parse(listZoneResult[i].resultZone) > 0)
                {
                    min = Double.Parse(listZoneResult[i].resultZone);
                    return min;
                }
            }
            return min;
        }

        public int CountTotalValidQty(String status)
        {
            int count = 0;
            for (int i = 0; i < listResult.Count; i++)
            {
                if (listResult[i].resultText.Equals(status))
                {
                    count++;
                }
            }
            return count;
        }

        public String GetSummary(Product product)
        {
            summary = "";
            string[] statuses = product.result.Split(',');
            List<int> listCount = new List<int>();
            List<ResultTestInfo> listSummary = GetListResultTestInfoByZone(listResult, listResult[0].zone);
            for (int i = 0; i < statuses.Length; i++)
            {
                listCount.Add(0);
                for (int k = 0; k < listSummary.Count(); k++)
                {
                    if (listSummary[k].resultText.ToUpper().Equals(statuses[i].ToUpper()))
                    {
                        listCount[i]++;
                    }
                }
                if (listCount[i] > 0)
                {
                    summary += listCount[i] + " " + statuses[i] + "\r\n";
                }

            }
            return summary;
        }

    }
}
