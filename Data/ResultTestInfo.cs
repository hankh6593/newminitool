﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class ResultTestInfo
    {
        public String statisticID { get; set; }
        public String resultText { get; set; }
        public int zone { get; set; }
        public String resultZone { get; set; }
        public String infoZone { get; set; }
        public ResultTestInfo()
        {
            this.statisticID = "";
            this.resultText = "";
            this.zone = 0;
            this.resultZone = "";
            this.infoZone = "";
        }

        public ResultTestInfo(string statisticID, string resultText, int zone, string resultZone, string infoZone)
        {
            this.statisticID = statisticID;
            this.resultText = resultText;
            this.zone = zone;
            this.resultZone = resultZone;
            this.infoZone = infoZone;
        }
    }


}
