﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class AutomationTestResult
    {
        public String testDateTime { get; set; }
        public List<String> listResult { get; set; }

        public AutomationTestResult()
        {
            this.testDateTime = "";
            this.listResult = new List<String>();
        }

        public AutomationTestResult(string testDateTime, List<string> listResult)
        {
            this.testDateTime = testDateTime;
            this.listResult = listResult;
        }
    }
}
