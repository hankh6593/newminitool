﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class DataDrawChart
    {
        public String statisticID { get; set; }
        public int center { get; set; }
        public int eTriggerNegative { get; set; }
        public int eTriggerPositive { get; set; }
        public int shiftingNegative { get; set; }
        public int shiftingPositive { get; set; }
        public int cal { get; set; }
        public int e { get; set; }
        public double pointValue { get; set; }

        public String resultText { get; set; }

    }
}
