﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class JSONParameter
    {
        public string deviceID { get; set; }
        public string comment { get; set; }
        public string CDS_Separation_Shift { get; set; }
        public string QUALITY_Blur_Threshold { get; set; }
        public string version { get; set; }

        public JSONParameter()
        {
            this.deviceID = "";
            this.comment = "";
            this.CDS_Separation_Shift = "";
            this.QUALITY_Blur_Threshold = "";
            this.version = version;
        }

        public JSONParameter(string deviceID, string comment, string cDS_Separation_Shift, string qUALITY_Blur_Threshold, string version)
        {
            this.deviceID = deviceID;
            this.comment = comment;
            this.CDS_Separation_Shift = cDS_Separation_Shift;
            this.QUALITY_Blur_Threshold = qUALITY_Blur_Threshold;
            this.version = version;
        }
    }
}
