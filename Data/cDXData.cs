﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniTool.Data
{
    public class cDXData
    {
        public String deviceID { get; set; }
        public String productName { get; set; }
        public String version { get; set; }
        public List<ResultTestInfo> listResultOrig { get; set; }
        public List<ResultTestInfo> listResultCopy { get; set; }
    }
}
