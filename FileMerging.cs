﻿using MiniTool.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class FileMerging : MasterForm
    {
        public FileMerging() : base()
        {
            InitializeComponent();
        }

        private void btnMergeData_Click(object sender, EventArgs e)
        {
            DirectoryInfo d = new DirectoryInfo(txtRawResult.Text);
            FileInfo[] files = d.GetFiles("*.csv");
            List<String> listFile = new List<String>();
            for (int i = 0; i < files.Length; i++)
            {
                String[] splittedfileName = files[i].Name.Split('_');
                String fileName = "";
                for (int k = 0; k < splittedfileName.Length - 1; k++)
                {
                    if (k == splittedfileName.Length - 2)
                    {
                        fileName += splittedfileName[k];
                    }
                    else
                    {
                        fileName += splittedfileName[k] + "_";
                    }
                }
                if (!listFile.Contains(fileName))
                {
                    listFile.Add(fileName);
                }
            }
            for(int i=0; i< listFile.Count; i++)
            {
                int count = 0;
                List<String> listDataRaw = new List<String>();
                for (int k = 0; k< files.Length; k++)
                {
                    if (files[k].Name.Contains(listFile[i]))
                    {
                        if(count ==0)
                        {
                            listDataRaw.AddRange(Ultility.GetRawDataExport(files[k].FullName, true));
                        }
                        else
                        {
                            listDataRaw.AddRange(Ultility.GetRawDataExport(files[k].FullName, false));
                        }
                        count++;
                    }
                }
                var csv = new StringBuilder();
                for (int k = 0; k < listDataRaw.Count; k++)
                {
                    csv.AppendLine(listDataRaw[k]);
                }
                System.IO.File.AppendAllText(txtResultFolder.Text + @"\" + listFile[i] + ".csv", csv.ToString());
                

            }
            MessageBox.Show("All files have been merged successfully. Please help to check.");
        }

        private void btnBrowseRaw_Click(object sender, EventArgs e)
        {
            FolderDialog.ShowDialog();
            txtRawResult.Text = FolderDialog.SelectedPath;
        }

        private void btnResultBrowse_Click(object sender, EventArgs e)
        {
            FolderDialog.ShowDialog();
            txtResultFolder.Text = FolderDialog.SelectedPath;
        }
    }
}
