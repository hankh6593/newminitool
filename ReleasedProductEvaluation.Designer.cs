﻿namespace MiniTool
{
    partial class ReleasedProductEvaluation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnBrowseLocation = new System.Windows.Forms.Button();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSaveAs = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbEdge = new System.Windows.Forms.ComboBox();
            this.cbbZone = new System.Windows.Forms.ComboBox();
            this.ResultOpenDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnBrowseResult = new System.Windows.Forms.Button();
            this.btnGetResult = new System.Windows.Forms.Button();
            this.txtResultPath = new System.Windows.Forms.TextBox();
            this.lbPath = new System.Windows.Forms.Label();
            this.cbRegressionTest = new System.Windows.Forms.CheckBox();
            this.txtSeperatedQty = new System.Windows.Forms.TextBox();
            this.lblSeperatedQty = new System.Windows.Forms.Label();
            this.txtTestQty = new System.Windows.Forms.TextBox();
            this.lblTestQty = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chartEvaluation = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgvResult = new System.Windows.Forms.DataGridView();
            this.btnUploadData = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartEvaluation)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(1213, 226);
            this.progressBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(528, 37);
            this.progressBar.TabIndex = 68;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.ForeColor = System.Drawing.Color.White;
            this.btnExport.Location = new System.Drawing.Point(1528, 282);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(217, 49);
            this.btnExport.TabIndex = 67;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnBrowseLocation
            // 
            this.btnBrowseLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseLocation.FlatAppearance.BorderSize = 0;
            this.btnBrowseLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseLocation.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseLocation.Location = new System.Drawing.Point(1431, 282);
            this.btnBrowseLocation.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrowseLocation.Name = "btnBrowseLocation";
            this.btnBrowseLocation.Size = new System.Drawing.Size(61, 49);
            this.btnBrowseLocation.TabIndex = 60;
            this.btnBrowseLocation.Text = "...";
            this.btnBrowseLocation.UseVisualStyleBackColor = false;
            this.btnBrowseLocation.Click += new System.EventHandler(this.btnBrowseLocation_Click);
            // 
            // txtFileName
            // 
            this.txtFileName.BackColor = System.Drawing.Color.White;
            this.txtFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileName.Location = new System.Drawing.Point(175, 289);
            this.txtFileName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(592, 34);
            this.txtFileName.TabIndex = 56;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(21, 289);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 29);
            this.label2.TabIndex = 53;
            this.label2.Text = "File Name:";
            // 
            // txtSaveAs
            // 
            this.txtSaveAs.BackColor = System.Drawing.Color.White;
            this.txtSaveAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaveAs.Location = new System.Drawing.Point(944, 289);
            this.txtSaveAs.Margin = new System.Windows.Forms.Padding(4);
            this.txtSaveAs.Name = "txtSaveAs";
            this.txtSaveAs.Size = new System.Drawing.Size(460, 34);
            this.txtSaveAs.TabIndex = 57;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(803, 294);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 29);
            this.label1.TabIndex = 54;
            this.label1.Text = "Save As:";
            // 
            // cbbEdge
            // 
            this.cbbEdge.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbEdge.FormattingEnabled = true;
            this.cbbEdge.Location = new System.Drawing.Point(649, 30);
            this.cbbEdge.Margin = new System.Windows.Forms.Padding(4);
            this.cbbEdge.Name = "cbbEdge";
            this.cbbEdge.Size = new System.Drawing.Size(137, 37);
            this.cbbEdge.TabIndex = 64;
            this.cbbEdge.SelectedIndexChanged += new System.EventHandler(this.cbbEdge_SelectedIndexChanged);
            // 
            // cbbZone
            // 
            this.cbbZone.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbZone.FormattingEnabled = true;
            this.cbbZone.Location = new System.Drawing.Point(177, 30);
            this.cbbZone.Margin = new System.Windows.Forms.Padding(4);
            this.cbbZone.Name = "cbbZone";
            this.cbbZone.Size = new System.Drawing.Size(300, 37);
            this.cbbZone.TabIndex = 63;
            this.cbbZone.SelectedIndexChanged += new System.EventHandler(this.cbbZone_SelectedIndexChanged);
            // 
            // btnBrowseResult
            // 
            this.btnBrowseResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseResult.FlatAppearance.BorderSize = 0;
            this.btnBrowseResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseResult.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseResult.Location = new System.Drawing.Point(1217, 149);
            this.btnBrowseResult.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrowseResult.Name = "btnBrowseResult";
            this.btnBrowseResult.Size = new System.Drawing.Size(61, 49);
            this.btnBrowseResult.TabIndex = 61;
            this.btnBrowseResult.Text = "...";
            this.btnBrowseResult.UseVisualStyleBackColor = false;
            this.btnBrowseResult.Click += new System.EventHandler(this.btnBrowseResult_Click);
            // 
            // btnGetResult
            // 
            this.btnGetResult.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnGetResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetResult.ForeColor = System.Drawing.Color.White;
            this.btnGetResult.Location = new System.Drawing.Point(1303, 149);
            this.btnGetResult.Margin = new System.Windows.Forms.Padding(4);
            this.btnGetResult.Name = "btnGetResult";
            this.btnGetResult.Size = new System.Drawing.Size(189, 49);
            this.btnGetResult.TabIndex = 59;
            this.btnGetResult.Text = "Evaluate";
            this.btnGetResult.UseVisualStyleBackColor = false;
            this.btnGetResult.Click += new System.EventHandler(this.btnGetResult_Click);
            // 
            // txtResultPath
            // 
            this.txtResultPath.BackColor = System.Drawing.Color.White;
            this.txtResultPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultPath.Location = new System.Drawing.Point(175, 158);
            this.txtResultPath.Margin = new System.Windows.Forms.Padding(4);
            this.txtResultPath.Name = "txtResultPath";
            this.txtResultPath.Size = new System.Drawing.Size(1003, 34);
            this.txtResultPath.TabIndex = 58;
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPath.Location = new System.Drawing.Point(21, 162);
            this.lbPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(118, 29);
            this.lbPath.TabIndex = 55;
            this.lbPath.Text = "Input File:";
            // 
            // cbRegressionTest
            // 
            this.cbRegressionTest.AutoSize = true;
            this.cbRegressionTest.Font = new System.Drawing.Font("Arial", 14F);
            this.cbRegressionTest.Location = new System.Drawing.Point(175, 226);
            this.cbRegressionTest.Margin = new System.Windows.Forms.Padding(4);
            this.cbRegressionTest.Name = "cbRegressionTest";
            this.cbRegressionTest.Size = new System.Drawing.Size(283, 31);
            this.cbRegressionTest.TabIndex = 69;
            this.cbRegressionTest.Text = "Has Separated Result?";
            this.cbRegressionTest.UseVisualStyleBackColor = true;
            this.cbRegressionTest.CheckedChanged += new System.EventHandler(this.cbRegressionTest_CheckedChanged);
            // 
            // txtSeperatedQty
            // 
            this.txtSeperatedQty.BackColor = System.Drawing.Color.White;
            this.txtSeperatedQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSeperatedQty.Location = new System.Drawing.Point(1075, 228);
            this.txtSeperatedQty.Margin = new System.Windows.Forms.Padding(4);
            this.txtSeperatedQty.Name = "txtSeperatedQty";
            this.txtSeperatedQty.Size = new System.Drawing.Size(103, 34);
            this.txtSeperatedQty.TabIndex = 73;
            this.txtSeperatedQty.Text = "3";
            // 
            // lblSeperatedQty
            // 
            this.lblSeperatedQty.AutoSize = true;
            this.lblSeperatedQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeperatedQty.Location = new System.Drawing.Point(808, 228);
            this.lblSeperatedQty.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSeperatedQty.Name = "lblSeperatedQty";
            this.lblSeperatedQty.Size = new System.Drawing.Size(234, 29);
            this.lblSeperatedQty.TabIndex = 72;
            this.lblSeperatedQty.Text = "Seperate Result Qty:";
            // 
            // txtTestQty
            // 
            this.txtTestQty.BackColor = System.Drawing.Color.White;
            this.txtTestQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTestQty.Location = new System.Drawing.Point(631, 223);
            this.txtTestQty.Margin = new System.Windows.Forms.Padding(4);
            this.txtTestQty.Name = "txtTestQty";
            this.txtTestQty.Size = new System.Drawing.Size(137, 34);
            this.txtTestQty.TabIndex = 71;
            this.txtTestQty.Text = "10;5;10";
            // 
            // lblTestQty
            // 
            this.lblTestQty.AutoSize = true;
            this.lblTestQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestQty.Location = new System.Drawing.Point(505, 228);
            this.lblTestQty.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTestQty.Name = "lblTestQty";
            this.lblTestQty.Size = new System.Drawing.Size(109, 29);
            this.lblTestQty.TabIndex = 70;
            this.lblTestQty.Text = "Test Qty:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(524, 34);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 29);
            this.label3.TabIndex = 70;
            this.label3.Text = "Edge:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(31, 34);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 29);
            this.label5.TabIndex = 70;
            this.label5.Text = "Zone:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Arial", 14F);
            this.tabControl1.Location = new System.Drawing.Point(20, 343);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1729, 784);
            this.tabControl1.TabIndex = 74;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgvData);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.cbbEdge);
            this.tabPage1.Controls.Add(this.cbbZone);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Location = new System.Drawing.Point(4, 35);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1721, 745);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Chart";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgvData
            // 
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(1237, 86);
            this.dgvData.Margin = new System.Windows.Forms.Padding(4);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersWidth = 51;
            this.dgvData.Size = new System.Drawing.Size(460, 640);
            this.dgvData.TabIndex = 72;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.chartEvaluation);
            this.panel1.Location = new System.Drawing.Point(35, 86);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1173, 640);
            this.panel1.TabIndex = 71;
            // 
            // chartEvaluation
            // 
            chartArea1.Name = "ChartArea1";
            this.chartEvaluation.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartEvaluation.Legends.Add(legend1);
            this.chartEvaluation.Location = new System.Drawing.Point(27, 18);
            this.chartEvaluation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chartEvaluation.Name = "chartEvaluation";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartEvaluation.Series.Add(series1);
            this.chartEvaluation.Size = new System.Drawing.Size(1144, 609);
            this.chartEvaluation.TabIndex = 0;
            this.chartEvaluation.Text = "chart1";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgvResult);
            this.tabPage2.Location = new System.Drawing.Point(4, 35);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1721, 745);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Test Result";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgvResult
            // 
            this.dgvResult.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 14F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvResult.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvResult.GridColor = System.Drawing.Color.Gray;
            this.dgvResult.Location = new System.Drawing.Point(24, 31);
            this.dgvResult.Margin = new System.Windows.Forms.Padding(4);
            this.dgvResult.Name = "dgvResult";
            this.dgvResult.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(189)))), ((int)(((byte)(196)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 14F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvResult.RowHeadersWidth = 51;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            this.dgvResult.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvResult.Size = new System.Drawing.Size(1665, 688);
            this.dgvResult.TabIndex = 37;
            // 
            // btnUploadData
            // 
            this.btnUploadData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUploadData.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadData.ForeColor = System.Drawing.Color.White;
            this.btnUploadData.Location = new System.Drawing.Point(1528, 150);
            this.btnUploadData.Margin = new System.Windows.Forms.Padding(4);
            this.btnUploadData.Name = "btnUploadData";
            this.btnUploadData.Size = new System.Drawing.Size(213, 49);
            this.btnUploadData.TabIndex = 75;
            this.btnUploadData.Text = "Upload Data";
            this.btnUploadData.UseVisualStyleBackColor = false;
            this.btnUploadData.Click += new System.EventHandler(this.btnUploadData_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(19, 79);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(354, 39);
            this.label4.TabIndex = 55;
            this.label4.Text = "sTC + CDx Evaluation";
            // 
            // ReleasedProductEvaluation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1789, 1147);
            this.Controls.Add(this.btnUploadData);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtSeperatedQty);
            this.Controls.Add(this.lblSeperatedQty);
            this.Controls.Add(this.txtTestQty);
            this.Controls.Add(this.lblTestQty);
            this.Controls.Add(this.cbRegressionTest);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnBrowseLocation);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSaveAs);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBrowseResult);
            this.Controls.Add(this.btnGetResult);
            this.Controls.Add(this.txtResultPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbPath);
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "ReleasedProductEvaluation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini tool v3.11";
            this.Controls.SetChildIndex(this.lbPath, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.txtResultPath, 0);
            this.Controls.SetChildIndex(this.btnGetResult, 0);
            this.Controls.SetChildIndex(this.btnBrowseResult, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtSaveAs, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtFileName, 0);
            this.Controls.SetChildIndex(this.btnBrowseLocation, 0);
            this.Controls.SetChildIndex(this.btnExport, 0);
            this.Controls.SetChildIndex(this.progressBar, 0);
            this.Controls.SetChildIndex(this.cbRegressionTest, 0);
            this.Controls.SetChildIndex(this.lblTestQty, 0);
            this.Controls.SetChildIndex(this.txtTestQty, 0);
            this.Controls.SetChildIndex(this.lblSeperatedQty, 0);
            this.Controls.SetChildIndex(this.txtSeperatedQty, 0);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.Controls.SetChildIndex(this.btnUploadData, 0);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartEvaluation)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnBrowseLocation;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSaveAs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbbEdge;
        private System.Windows.Forms.ComboBox cbbZone;
        private System.Windows.Forms.OpenFileDialog ResultOpenDialog;
        private System.Windows.Forms.Button btnBrowseResult;
        private System.Windows.Forms.Button btnGetResult;
        private System.Windows.Forms.TextBox txtResultPath;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.CheckBox cbRegressionTest;
        private System.Windows.Forms.TextBox txtSeperatedQty;
        private System.Windows.Forms.Label lblSeperatedQty;
        private System.Windows.Forms.TextBox txtTestQty;
        private System.Windows.Forms.Label lblTestQty;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartEvaluation;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvResult;
        private System.Windows.Forms.Button btnUploadData;
        private System.Windows.Forms.Label label4;
    }
}