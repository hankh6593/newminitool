﻿using MathNet.Numerics.Statistics;
using MiniTool.Controller;
using MiniTool.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class JSONGenerator : MasterForm
    {
        DatabaseConnection db;
        List<DeviceThreshold> listDeviceModel;
        Product product;
        public JSONGenerator() : base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            product = new Product();
        }

        private void btnBrowseResult_Click(object sender, EventArgs e)
        {
            FolderDialog.ShowDialog();
            txtFolderPath.Text = FolderDialog.SelectedPath;
        }


        private void btnClear_Click(object sender, EventArgs e)
        {
            txtFolderPath.Text = "";
            dgvDevice.DataSource = null;
            txtMeanOrig.Text = "";
            txtStdDev.Text = "";
            txtPercentage.Text = "";
            txtPStdDev.Text = "";
            txtDeviceName.Text = "";
            txtThreshold.Text = "";
            txtBlur.Text = "";
            txtVersion.Text = "";
            txtComment.Text = "";

        }

        private void btnGetResult_Click(object sender, EventArgs e)
        {
            listDeviceModel = ListDeviceModel(db.GetAllDeviceModelByServer(cbbMasterServer.Text.Split('-')[0].Replace(" ","")));

            UpdateDeviceTable(sender, e);
        }

        private List<DeviceThreshold> ListDeviceModel(List<DeviceID> listDeviceID)
        {
            listDeviceModel = new List<DeviceThreshold>();
            List<String> listModel = new List<String>();
            foreach (DeviceID deviceID in listDeviceID)
            {
                if (!listModel.Contains(deviceID.deviceModel))
                {
                    listModel.Add(deviceID.deviceModel);
                }
            }
            for (int i = 0; i < listModel.Count; i++)
            {
                listDeviceModel.Add(new DeviceThreshold(listModel[i]));
            }
            return listDeviceModel.ToList();

        }

        private void UpdateDeviceTable(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Device Name");
            dt.Columns.Add("Threshold");

            for (int i = 0; i < listDeviceModel.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[i][0] = listDeviceModel[i].deviceModel;
                dt.Rows[i][1] = listDeviceModel[i].threshold;
            }
            dgvDevice.DataSource = dt;
            dgvDevice.Columns[0].Width = 200;
            dgvDevice.Columns[1].Width = 50;

            dgvDevice_CellClick(sender, null);

        }

        private double CalculateMean(List<DataDrawChart> listData)
        {
            double sum = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                sum += listData[i].pointValue;
            }
            return Math.Round(sum / listData.Count, 2);
        }

        private double CalculateStdDev(List<DataDrawChart> listOrig)
        {
            List<double> listData = new List<double>();

            for (int i = 0; i < listOrig.Count; i++)
            {
                listData.Add(listOrig[i].pointValue);
            }

            return listData.StandardDeviation();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = dgvDevice.SelectedRows[0].Index;
            if (selectedRowCount >= 0)
            {
                listDeviceModel[selectedRowCount + 1].threshold = 0.0;
                listDeviceModel[selectedRowCount + 1].quality_Blur_Threshold = "0";
                listDeviceModel[selectedRowCount + 1].version = "1.0";
                listDeviceModel[selectedRowCount + 1].comment = "";
                CalculateMeanDefault();
            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            int deviceSelectedRowCount = dgvDevice.SelectedRows[0].Index;
            listDeviceModel[deviceSelectedRowCount].threshold = Double.Parse(txtThreshold.Text);
            listDeviceModel[deviceSelectedRowCount].quality_Blur_Threshold = txtBlur.Text;
            listDeviceModel[deviceSelectedRowCount].version = txtVersion.Text;
            listDeviceModel[deviceSelectedRowCount].comment = txtComment.Text;
            UpdateDeviceTable(sender, e);
            dgvDevice_CellClick(sender, null);
            CalculateMeanDefault();
        }

        private void CalculateMeanDefault()
        {
            double sum = 0;
            int count = 0;
            for (int i = 1; i < listDeviceModel.Count; i++)
            {
                if (listDeviceModel[i].threshold != 0)
                {
                    sum += listDeviceModel[i].threshold;
                    count++;
                }
            }
            listDeviceModel[0].threshold = Math.Round(sum / count, 1);
            UpdateDeviceTable(null, null);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            List<JSONParameter> listExport = ParseJSONData(db.GetAllDeviceModel());

            JObject dataJSON = new JObject();
            foreach (JSONParameter p in listExport)
            {
                JProperty rss =
         new JProperty(
             new JProperty(p.deviceID,
                 new JObject(
                     new JProperty("parameters",
                         new JArray(
                             new JObject(
                                 new JProperty("comment", p.comment),
                                 new JProperty("version", p.version),
                                 new JProperty("CDS_Separation_Shift", Double.Parse(p.CDS_Separation_Shift)),
                                 new JProperty("QUALITY_Blur_Threshold", Math.Round(Double.Parse(p.QUALITY_Blur_Threshold), 1))))))));
                dataJSON.Add(rss);
            }





            String json = JsonConvert.SerializeObject(dataJSON);
            String file = txtFolderPath.Text + @"\" + "zone_parameter.json";
            //write string to file
            System.IO.File.WriteAllText(file, json);
            MessageBox.Show(file + " has been exported successfully.");
        }

        private List<JSONParameter> ParseJSONData(List<DeviceID> listDeviceID)
        {
            List<JSONParameter> listJSONData = new List<JSONParameter>();
            for (int i = 0; i < listDeviceID.Count; i++)
            {
                JSONParameter parameter = new JSONParameter();
                for (int k = 0; k < listDeviceModel.Count; k++)
                {
                    if (listDeviceID[i].deviceModel.Equals(listDeviceModel[k].deviceModel))
                    {
                        parameter.deviceID = listDeviceID[i].deviceID;
                        parameter.CDS_Separation_Shift = listDeviceModel[k].threshold.ToString();
                        parameter.QUALITY_Blur_Threshold = listDeviceModel[k].quality_Blur_Threshold;
                        parameter.version = listDeviceModel[k].version;
                        parameter.comment = listDeviceModel[k].comment;
                        k = listDeviceModel.Count;
                    }
                }
                listJSONData.Add(parameter);
            }
            return listJSONData;
        }

        private void btnViewChart_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = dgvDevice.SelectedRows[0].Index;
            if (selectedRowCount > 0)
            {
                String deviceID = dgvDevice.Rows[selectedRowCount].Cells[0].Value.ToString();
                String fileOrig = txtFolderPath.Text + @"\" + deviceID + "_Orig.csv";
                String fileCopy = txtFolderPath.Text + @"\" + deviceID + "_Copy.csv";
                double threshold = Double.Parse(txtThreshold.Text);
                DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
                FileInfo[] files = d.GetFiles("*.csv");
                bool isExisted = false;
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].FullName.Equals(fileOrig))
                    {
                        isExisted = true;
                        i = files.Length;
                    }
                }
                if (isExisted)
                {
                    DrawCDSChart form = new DrawCDSChart(fileOrig, fileCopy, threshold);
                    form.Show();
                }
                else
                {
                    MessageBox.Show("This device don't have csv files.");
                }
            }

        }

        private void dgvDevice_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Int32 selectedRowCount = dgvDevice.SelectedRows[0].Index;
            if (selectedRowCount > 0)
            {
                JSONZoneData data = new JSONZoneData();
                data.deviceID = dgvDevice.Rows[selectedRowCount].Cells[0].Value.ToString();
                String fileOrig = txtFolderPath.Text + @"\" + data.deviceID + "_Orig.csv";
                String fileCopy = txtFolderPath.Text + @"\" + data.deviceID + "_Copy.csv";
                DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
                FileInfo[] files = d.GetFiles("*.csv");
                if(files.Length > 0)
                {
                    bool isExisted = false;
                    for (int i = 0; i < files.Length; i++)
                    {
                        if (files[i].FullName.Equals(fileOrig))
                        {
                            isExisted = true;
                            i = files.Length;
                        }
                    }
                    if (isExisted)
                    {
                        List<String> rawData = Ultility.GetRawData(fileOrig);
                        product = Ultility.GetProductFromCSV(rawData);

                        if (product != null && product.id != -1)
                        {
                            data.productName = product.productName;


                            List<List<String>> listDataOrig = Ultility.GetCleanData(fileOrig, product);
                            data.listResultOrig = Ultility.GetResultListFromDataRaw(listDataOrig, product.listZone, 1, listDataOrig.Count, false);

                            List<List<String>> listDataCopy = Ultility.GetCleanData(fileCopy, product);
                            data.listResultCopy = Ultility.GetResultListFromDataRaw(listDataCopy, product.listZone, 1, listDataCopy.Count, false);

                            List<DataDrawChart> listOrg = new List<DataDrawChart>();
                            List<DataDrawChart> listCopy = new List<DataDrawChart>();
                            for (int i = 0; i < data.listResultOrig.Count; i++)
                            {
                                DataDrawChart dataCDS = new DataDrawChart();
                                for (int k = 0; k < product.listZone.Count; k++)
                                {
                                    if (product.listZone[k].method.Equals("CDS"))
                                    {
                                        if (product.listZone[k].zoneNo == data.listResultOrig[i].zone)
                                        {
                                            if (Double.Parse(data.listResultOrig[i].resultZone) != -1 || (Double.Parse(data.listResultOrig[i].resultZone) == -1 && data.listResultOrig[i].infoZone.Contains("FineSigma")))
                                            {
                                                dataCDS.pointValue = Double.Parse(data.listResultOrig[i].resultZone);
                                                listOrg.Add(dataCDS);
                                            }
                                        }

                                    }
                                }

                            }
                            for (int i = 0; i < data.listResultCopy.Count; i++)
                            {
                                DataDrawChart dataCDS = new DataDrawChart();
                                for (int k = 0; k < product.listZone.Count; k++)
                                {
                                    if (product.listZone[k].method.Equals("CDS"))
                                    {
                                        if (product.listZone[k].zoneNo == data.listResultCopy[i].zone)
                                        {
                                            if (Double.Parse(data.listResultCopy[i].resultZone) != -1 || (Double.Parse(data.listResultCopy[i].resultZone) == -1 && data.listResultCopy[i].infoZone.Contains("FineSigma")))
                                            {
                                                dataCDS.pointValue = Double.Parse(data.listResultCopy[i].resultZone);
                                                listCopy.Add(dataCDS);
                                            }
                                        }
                                    }
                                }

                            }
                            txtMeanOrig.Text = CalculateMean(listOrg).ToString();
                            txtStdDev.Text = CalculateStdDev(listOrg).ToString();
                            txtPStdDev.Text = (Double.Parse(txtStdDev.Text) * Double.Parse(txtPercentage.Text) / 100).ToString();

                            txtThreshold.Text = Math.Round((CalculateMean(listOrg) + Double.Parse(txtPStdDev.Text)), 1).ToString();
                        }
                        if (product == null || (product != null && product.id == -1))
                        {
                            MessageBox.Show("The product has not been setup. Please setup the product");
                        }
                    }
                    else
                    {
                        MessageBox.Show("This device don't have csv files.");
                    }
                }
                else
                {
                    txtThreshold.Text = "0";
                }
            }
            if (selectedRowCount == 0)
            {
                txtThreshold.Text = listDeviceModel[selectedRowCount].threshold.ToString();
            }

            txtDeviceName.Text = listDeviceModel[selectedRowCount].deviceModel;
            txtBlur.Text = listDeviceModel[selectedRowCount].quality_Blur_Threshold;
            txtVersion.Text = listDeviceModel[selectedRowCount].version;
            txtComment.Text = listDeviceModel[selectedRowCount].comment;
        }

        private void txtPercentage_TextChanged(object sender, EventArgs e)
        {
            double mean = Double.Parse(txtMeanOrig.Text);
            double stdDev = Double.Parse(txtStdDev.Text);
            double PStdDev = stdDev * Double.Parse(txtPercentage.Text) / 100;
            txtPStdDev.Text = PStdDev.ToString();
            txtThreshold.Text = Math.Round(mean + PStdDev, 1).ToString();
        }
    }
}
