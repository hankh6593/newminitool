﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Version = MiniTool.Data.Version;

namespace MiniTool
{
    public partial class TrialProductEvaluation : MasterForm
    {
        static DatabaseConnection db;
        List<String> listData;
        Product product;
        List<List<String>> listCleanedData;
        List<ResultForEvaluation> listDataForEvaluation;
        public TrialProductEvaluation() : base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            progressBar.Visible = false;

        }

        private void btnBrowseResult_Click(object sender, EventArgs e)
        {
            ResultOpenDialog.ShowDialog();
            ResultOpenDialog.DefaultExt = "csv";
            ResultOpenDialog.Filter = "Comma Delimited|*.csv";
            txtResultPath.Text = ResultOpenDialog.FileName;
            if (ResultOpenDialog.FileName.Contains(".csv"))
            {
                txtResultPath.Text = ResultOpenDialog.FileName;
            }
            else
            {
                MessageBox.Show("Please input CSV file.");
            }
        }

        private void btnGetResult_Click(object sender, EventArgs e)
        {
            if (txtResultPath.Text.Length == 0)
            {
                MessageBox.Show("Please select the file");
            }
            else
            {

                listData = Ultility.RemoveDoubleQuoteFromRawData(Ultility.GetRawData(txtResultPath.Text));
                if (listData.Count != 0)
                {
                    product = Ultility.GetProductFromCSV(listData);
                    listCleanedData = Ultility.GetCleanData(txtResultPath.Text, product);
                    if (listCleanedData.Count != 0)
                    {
                        if (Ultility.isOtherZone(listCleanedData))
                        {
                            MessageBox.Show("This feature only support the product that only has STC zone.");
                        }
                        else
                        {
                            Version version = Ultility.GetVersionFromCSV(listCleanedData);
                            Device device = Ultility.GetDeviceFromCSV(listCleanedData, 1);
                            String fileName = "EVA_" + version.os + "_" + version.version + "_" + device.name.Replace(" ", "-") + "_" + product.productName.Replace(" ", "-");
                            txtFileName.Text = fileName;
                            listDataForEvaluation = GetListDataForEvaluation(listCleanedData, product);
                            LoadZoneData();
                        }


                    }

                }
            }
        }

        private List<ResultForEvaluation> GetListDataForEvaluation(List<List<String>> listCleanedData, Product product)
        {

            List<ResultForEvaluation> listDataForEvaluation = new List<ResultForEvaluation>();
            progressBar.Visible = true;
            progressBar.Value = 0;
            progressBar.Maximum = listCleanedData.Count * 10;
            for (int row = 1; row < listCleanedData.Count; row++)
            {
                progressBar.Value = row * 10;
                string resultText = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Result Text")] + "(" + listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Result Code")] + ")"; ;
                string statisticID = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Statistic ID")];
                Version version = Ultility.GetVersionFromCSV(listCleanedData);
                Device device = Ultility.GetDeviceFromCSV(listCleanedData, row);
                for (int k = 0; k < product.listZone.Count; k++)
                {

                    ResultForEvaluation resultEva = new ResultForEvaluation(statisticID.ToString(), version.id, device, product.id, resultText);
                    resultEva.createdDate = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Date and Time")];
                    resultEva.value = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Zone Result Zone " + product.listZone[k].zoneNo)];
                    resultEva.info = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Debug Info Zone " + product.listZone[k].zoneNo)];
                    resultEva.zone = product.listZone[k].zoneNo;
                    resultEva.method = product.listZone[k].method;
                    listDataForEvaluation.Add(resultEva);
                }
            }
            progressBar.Visible = false;
            return listDataForEvaluation;

        }

        private void LoadZoneData()
        {

            List<String> listZone = new List<String>();
            for (int i = 0; i < product.listZone.Count; i++)
            {
                if (product.listZone[i].method.Equals("STC"))
                {
                    listZone.Add((product.listZone[i].zoneNo) + " - " + product.listZone[i].method);
                }
               
            }
            cbbZone.DataSource = listZone;
        }



        private void cbbZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            int zone = Int32.Parse(cbbZone.Text.Split('-')[0].Trim());
            String method = cbbZone.Text.Split('-')[1].Trim();
            if (method.Equals("STC"))
            {
                List<String> listEdge = new List<String>();
                int countEdge = Ultility.GetEdgeQtyFromCSV(listCleanedData, zone);
                for (int i = 0; i < countEdge; i++)
                {
                    listEdge.Add("Edge " + (i + 1));
                }
                var bindingEdge = new BindingSource();
                bindingEdge.DataSource = listEdge;

                cbbEdge.DataSource = bindingEdge.DataSource;
            }
        }

        private void cbbEdge_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbEdge.SelectedIndex != -1)
            {
                int zone = Int32.Parse(cbbZone.Text.Split('-')[0].Trim());
                int edgeQty = Ultility.GetEdgeQtyFromCSV(listCleanedData, zone);
                Ultility.DrawChartSTC(chartEvaluation, dgvData, listDataForEvaluation, zone, edgeQty, cbbEdge.SelectedIndex);
            }

        }

        private void btnBrowseLocation_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.ShowDialog();
            txtSaveAs.Text = folderBrowserDialog.SelectedPath;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (listData !=null)
            {
                progressBar.Value = 0;
                progressBar.Visible = true;
                for(int i= 0; i< product.listZone.Count; i++)
                {
                 
                    if (product.listZone[i].method.Equals("STC"))
                    {
                        List<ResultForEvaluation> listSave = Ultility.GetResultForEvaluationByZone(listDataForEvaluation, product.listZone[i].zoneNo);
                        int edgeQty = Ultility.GetEdgeQtyFromCSV(listCleanedData, product.listZone[i].zoneNo);
                        String fileName = @"" + txtSaveAs.Text + @"\" + txtFileName.Text + "_Z"+product.listZone[i].zoneNo;
                        Ultility.ExportSTCZoneEvaluation(progressBar, listSave, product.listZone[i].zoneNo, edgeQty, fileName);
                    }else if (product.listZone[i].method.Equals("CDS"))
                    {
                        List<ResultForEvaluation> listSave = Ultility.GetResultForEvaluationByZone(listDataForEvaluation, product.listZone[i].zoneNo);
                        String fileName = @"" + txtSaveAs.Text + @"\" + txtFileName.Text + "_Z" + product.listZone[i].zoneNo;
                        Ultility.ExportCDSZoneEvaluation(progressBar, listSave, product.listZone[i].zoneNo, fileName);
                    }
                }
            }
            else
            {
                MessageBox.Show("Please evaluate the file before export.");
            }
        }

       

     
    }
}
