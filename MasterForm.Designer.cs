﻿namespace MiniTool
{
    partial class MasterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.regressionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scryptoTRACEEvaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regresionTestEvaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markingEvaluationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.honeywellRegressionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cDSToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.jSONGeneratorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationHistoryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.comparisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filteredComparisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionComparisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherZonesResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renamecsvFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mergecsvFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automationTestToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.replaceTextMessageHoneywellCompanyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(123)))), ((int)(((byte)(177)))));
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regressionTestToolStripMenuItem,
            this.evaluationHistoryToolStripMenuItem,
            this.otherZonesResultToolStripMenuItem,
            this.automationTestToolStripMenuItem1,
            this.configurationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 6, 0, 6);
            this.menuStrip1.Size = new System.Drawing.Size(1595, 44);
            this.menuStrip1.TabIndex = 26;
            this.menuStrip1.Text = "JSON Generator";
            // 
            // regressionTestToolStripMenuItem
            // 
            this.regressionTestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scryptoTRACEEvaluationToolStripMenuItem,
            this.regresionTestEvaluationToolStripMenuItem,
            this.markingEvaluationToolStripMenuItem,
            this.jSONGeneratorToolStripMenuItem1});
            this.regressionTestToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.regressionTestToolStripMenuItem.Name = "regressionTestToolStripMenuItem";
            this.regressionTestToolStripMenuItem.Size = new System.Drawing.Size(138, 32);
            this.regressionTestToolStripMenuItem.Text = "Evaluation";
            // 
            // scryptoTRACEEvaluationToolStripMenuItem
            // 
            this.scryptoTRACEEvaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scryptoTRACEEvaluationToolStripMenuItem.Name = "scryptoTRACEEvaluationToolStripMenuItem";
            this.scryptoTRACEEvaluationToolStripMenuItem.Size = new System.Drawing.Size(328, 28);
            this.scryptoTRACEEvaluationToolStripMenuItem.Text = "sTC Evaluation (No Setup)";
            this.scryptoTRACEEvaluationToolStripMenuItem.Click += new System.EventHandler(this.scryptoTRACEEvaluationToolStripMenuItem_Click);
            // 
            // regresionTestEvaluationToolStripMenuItem
            // 
            this.regresionTestEvaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F);
            this.regresionTestEvaluationToolStripMenuItem.Name = "regresionTestEvaluationToolStripMenuItem";
            this.regresionTestEvaluationToolStripMenuItem.Size = new System.Drawing.Size(328, 28);
            this.regresionTestEvaluationToolStripMenuItem.Text = "sTC + CDx Evaluation";
            this.regresionTestEvaluationToolStripMenuItem.Click += new System.EventHandler(this.regresionTestEvaluationToolStripMenuItem_Click);
            // 
            // markingEvaluationToolStripMenuItem
            // 
            this.markingEvaluationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.honeywellRegressionTestToolStripMenuItem,
            this.cDSToolStripMenuItem1});
            this.markingEvaluationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F);
            this.markingEvaluationToolStripMenuItem.Name = "markingEvaluationToolStripMenuItem";
            this.markingEvaluationToolStripMenuItem.Size = new System.Drawing.Size(328, 28);
            this.markingEvaluationToolStripMenuItem.Text = "Bulk Export";
            // 
            // honeywellRegressionTestToolStripMenuItem
            // 
            this.honeywellRegressionTestToolStripMenuItem.Name = "honeywellRegressionTestToolStripMenuItem";
            this.honeywellRegressionTestToolStripMenuItem.Size = new System.Drawing.Size(331, 28);
            this.honeywellRegressionTestToolStripMenuItem.Text = "Honeywell Regression Test";
            this.honeywellRegressionTestToolStripMenuItem.Click += new System.EventHandler(this.honeywellRegressionTestToolStripMenuItem_Click);
            // 
            // cDSToolStripMenuItem1
            // 
            this.cDSToolStripMenuItem1.Name = "cDSToolStripMenuItem1";
            this.cDSToolStripMenuItem1.Size = new System.Drawing.Size(331, 28);
            this.cDSToolStripMenuItem1.Text = "CDx Evaluation";
            this.cDSToolStripMenuItem1.Click += new System.EventHandler(this.cDSToolStripMenuItem1_Click);
            // 
            // jSONGeneratorToolStripMenuItem1
            // 
            this.jSONGeneratorToolStripMenuItem1.Font = new System.Drawing.Font("Arial", 12F);
            this.jSONGeneratorToolStripMenuItem1.Name = "jSONGeneratorToolStripMenuItem1";
            this.jSONGeneratorToolStripMenuItem1.Size = new System.Drawing.Size(328, 28);
            this.jSONGeneratorToolStripMenuItem1.Text = "JSON Generator";
            this.jSONGeneratorToolStripMenuItem1.Click += new System.EventHandler(this.jSONGeneratorToolStripMenuItem1_Click);
            // 
            // evaluationHistoryToolStripMenuItem
            // 
            this.evaluationHistoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evaluationHistoryToolStripMenuItem1,
            this.comparisonToolStripMenuItem});
            this.evaluationHistoryToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.evaluationHistoryToolStripMenuItem.Name = "evaluationHistoryToolStripMenuItem";
            this.evaluationHistoryToolStripMenuItem.Size = new System.Drawing.Size(122, 32);
            this.evaluationHistoryToolStripMenuItem.Text = "Statistics";
            // 
            // evaluationHistoryToolStripMenuItem1
            // 
            this.evaluationHistoryToolStripMenuItem1.Font = new System.Drawing.Font("Arial", 12F);
            this.evaluationHistoryToolStripMenuItem1.Name = "evaluationHistoryToolStripMenuItem1";
            this.evaluationHistoryToolStripMenuItem1.Size = new System.Drawing.Size(252, 28);
            this.evaluationHistoryToolStripMenuItem1.Text = "Evaluation History";
            this.evaluationHistoryToolStripMenuItem1.Click += new System.EventHandler(this.evaluationHistoryToolStripMenuItem1_Click);
            // 
            // comparisonToolStripMenuItem
            // 
            this.comparisonToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filteredComparisonToolStripMenuItem,
            this.versionComparisonToolStripMenuItem});
            this.comparisonToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F);
            this.comparisonToolStripMenuItem.Name = "comparisonToolStripMenuItem";
            this.comparisonToolStripMenuItem.Size = new System.Drawing.Size(252, 28);
            this.comparisonToolStripMenuItem.Text = "Comparison";
            // 
            // filteredComparisonToolStripMenuItem
            // 
            this.filteredComparisonToolStripMenuItem.Name = "filteredComparisonToolStripMenuItem";
            this.filteredComparisonToolStripMenuItem.Size = new System.Drawing.Size(271, 28);
            this.filteredComparisonToolStripMenuItem.Text = "Filtered Comparison";
            this.filteredComparisonToolStripMenuItem.Click += new System.EventHandler(this.filteredComparisonToolStripMenuItem_Click);
            // 
            // versionComparisonToolStripMenuItem
            // 
            this.versionComparisonToolStripMenuItem.Name = "versionComparisonToolStripMenuItem";
            this.versionComparisonToolStripMenuItem.Size = new System.Drawing.Size(271, 28);
            this.versionComparisonToolStripMenuItem.Text = "Version Comparison";
            this.versionComparisonToolStripMenuItem.Click += new System.EventHandler(this.versionComparisonToolStripMenuItem_Click);
            // 
            // otherZonesResultToolStripMenuItem
            // 
            this.otherZonesResultToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.renamecsvFileToolStripMenuItem,
            this.mergecsvFileToolStripMenuItem,
            this.replaceTextMessageHoneywellCompanyToolStripMenuItem});
            this.otherZonesResultToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.75F);
            this.otherZonesResultToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.otherZonesResultToolStripMenuItem.Name = "otherZonesResultToolStripMenuItem";
            this.otherZonesResultToolStripMenuItem.Size = new System.Drawing.Size(94, 32);
            this.otherZonesResultToolStripMenuItem.Text = "Ultility";
            // 
            // renamecsvFileToolStripMenuItem
            // 
            this.renamecsvFileToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F);
            this.renamecsvFileToolStripMenuItem.Name = "renamecsvFileToolStripMenuItem";
            this.renamecsvFileToolStripMenuItem.Size = new System.Drawing.Size(479, 28);
            this.renamecsvFileToolStripMenuItem.Text = "Rename .csv file";
            this.renamecsvFileToolStripMenuItem.Click += new System.EventHandler(this.renamecsvFileToolStripMenuItem_Click);
            // 
            // mergecsvFileToolStripMenuItem
            // 
            this.mergecsvFileToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F);
            this.mergecsvFileToolStripMenuItem.Name = "mergecsvFileToolStripMenuItem";
            this.mergecsvFileToolStripMenuItem.Size = new System.Drawing.Size(479, 28);
            this.mergecsvFileToolStripMenuItem.Text = "Merge .csv file";
            this.mergecsvFileToolStripMenuItem.Click += new System.EventHandler(this.mergecsvFileToolStripMenuItem_Click);
            // 
            // automationTestToolStripMenuItem1
            // 
            this.automationTestToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.automationTestToolStripMenuItem1.Name = "automationTestToolStripMenuItem1";
            this.automationTestToolStripMenuItem1.Size = new System.Drawing.Size(197, 32);
            this.automationTestToolStripMenuItem1.Text = "Automation Test";
            this.automationTestToolStripMenuItem1.Click += new System.EventHandler(this.automationTestToolStripMenuItem1_Click);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Font = new System.Drawing.Font("Arial", 14.75F);
            this.configurationToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(176, 32);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // replaceTextMessageHoneywellCompanyToolStripMenuItem
            // 
            this.replaceTextMessageHoneywellCompanyToolStripMenuItem.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.replaceTextMessageHoneywellCompanyToolStripMenuItem.Name = "replaceTextMessageHoneywellCompanyToolStripMenuItem";
            this.replaceTextMessageHoneywellCompanyToolStripMenuItem.Size = new System.Drawing.Size(495, 28);
            this.replaceTextMessageHoneywellCompanyToolStripMenuItem.Text = "Replace Text Message (Honeywell Company)";
            this.replaceTextMessageHoneywellCompanyToolStripMenuItem.Click += new System.EventHandler(this.replaceTextMessageHoneywellCompanyToolStripMenuItem_Click);
            // 
            // MasterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1595, 833);
            this.Controls.Add(this.menuStrip1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MasterForm";
            this.Text = "U-nica Mini tool v3.11";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MasterForm_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem regressionTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scryptoTRACEEvaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regresionTestEvaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markingEvaluationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationHistoryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem comparisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filteredComparisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionComparisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherZonesResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renamecsvFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mergecsvFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automationTestToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem jSONGeneratorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem honeywellRegressionTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cDSToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem replaceTextMessageHoneywellCompanyToolStripMenuItem;
    }
}