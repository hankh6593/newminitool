﻿namespace MiniTool
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabConfiguration = new System.Windows.Forms.TabControl();
            this.tabDevice = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDeviceID = new System.Windows.Forms.TextBox();
            this.btnRemoveDevice = new System.Windows.Forms.Button();
            this.btnUpdateDevice = new System.Windows.Forms.Button();
            this.btnResetDevice = new System.Windows.Forms.Button();
            this.btnAddDevice = new System.Windows.Forms.Button();
            this.txtUDID = new System.Windows.Forms.TextBox();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDeviceEmail = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDeviceName = new System.Windows.Forms.TextBox();
            this.txtDeviceCode = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDeviceModel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvDevices = new System.Windows.Forms.DataGridView();
            this.tabProduct = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbbType = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtProductNo = new System.Windows.Forms.TextBox();
            this.btnSetupZone = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProject = new System.Windows.Forms.TextBox();
            this.btnRemoveProduct = new System.Windows.Forms.Button();
            this.btnUpdateProduct = new System.Windows.Forms.Button();
            this.btnResetProduct = new System.Windows.Forms.Button();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.txtCompanyID = new System.Windows.Forms.TextBox();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvProduct = new System.Windows.Forms.DataGridView();
            this.tabVersion = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtOS = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtVersionID = new System.Windows.Forms.TextBox();
            this.btnRemoveVersion = new System.Windows.Forms.Button();
            this.btnUpdateVersion = new System.Windows.Forms.Button();
            this.btnResetVersion = new System.Windows.Forms.Button();
            this.btnAddVersion = new System.Windows.Forms.Button();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dgvVersion = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cbbThresholdServer = new System.Windows.Forms.ComboBox();
            this.cbbMasterServer = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnBrowseSaveAs = new System.Windows.Forms.Button();
            this.btnBrowseThreshold = new System.Windows.Forms.Button();
            this.progressBarMaster = new System.Windows.Forms.ProgressBar();
            this.txtSaveAsThreshold = new System.Windows.Forms.TextBox();
            this.txtThresholdJSONPath = new System.Windows.Forms.TextBox();
            this.btnBrowseMasterJSON = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtMasterJSONPath = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.btnExportJSON = new System.Windows.Forms.Button();
            this.btnImportThreshold = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.btnImportMasterJSON = new System.Windows.Forms.Button();
            this.dgvMasterList = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.tabConfiguration.SuspendLayout();
            this.tabDevice.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevices)).BeginInit();
            this.tabProduct.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
            this.tabVersion.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVersion)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMasterList)).BeginInit();
            this.SuspendLayout();
            // 
            // tabConfiguration
            // 
            this.tabConfiguration.Controls.Add(this.tabDevice);
            this.tabConfiguration.Controls.Add(this.tabProduct);
            this.tabConfiguration.Controls.Add(this.tabVersion);
            this.tabConfiguration.Controls.Add(this.tabPage1);
            this.tabConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabConfiguration.Location = new System.Drawing.Point(18, 107);
            this.tabConfiguration.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabConfiguration.Name = "tabConfiguration";
            this.tabConfiguration.SelectedIndex = 0;
            this.tabConfiguration.Size = new System.Drawing.Size(1795, 819);
            this.tabConfiguration.TabIndex = 27;
            // 
            // tabDevice
            // 
            this.tabDevice.Controls.Add(this.groupBox1);
            this.tabDevice.Controls.Add(this.dgvDevices);
            this.tabDevice.Location = new System.Drawing.Point(4, 31);
            this.tabDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabDevice.Name = "tabDevice";
            this.tabDevice.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabDevice.Size = new System.Drawing.Size(1787, 784);
            this.tabDevice.TabIndex = 0;
            this.tabDevice.Text = "Devices";
            this.tabDevice.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDeviceID);
            this.groupBox1.Controls.Add(this.btnRemoveDevice);
            this.groupBox1.Controls.Add(this.btnUpdateDevice);
            this.groupBox1.Controls.Add(this.btnResetDevice);
            this.groupBox1.Controls.Add(this.btnAddDevice);
            this.groupBox1.Controls.Add(this.txtUDID);
            this.groupBox1.Controls.Add(this.txtLocation);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtType);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtDeviceEmail);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtDeviceName);
            this.groupBox1.Controls.Add(this.txtDeviceCode);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtDeviceModel);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1099, 19);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(668, 725);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Device Info";
            // 
            // txtDeviceID
            // 
            this.txtDeviceID.Location = new System.Drawing.Point(211, 60);
            this.txtDeviceID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceID.Name = "txtDeviceID";
            this.txtDeviceID.ReadOnly = true;
            this.txtDeviceID.Size = new System.Drawing.Size(433, 28);
            this.txtDeviceID.TabIndex = 8;
            // 
            // btnRemoveDevice
            // 
            this.btnRemoveDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRemoveDevice.ForeColor = System.Drawing.Color.White;
            this.btnRemoveDevice.Location = new System.Drawing.Point(281, 657);
            this.btnRemoveDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoveDevice.Name = "btnRemoveDevice";
            this.btnRemoveDevice.Size = new System.Drawing.Size(173, 46);
            this.btnRemoveDevice.TabIndex = 7;
            this.btnRemoveDevice.Text = "Remove";
            this.btnRemoveDevice.UseVisualStyleBackColor = false;
            this.btnRemoveDevice.Click += new System.EventHandler(this.btnRemoveDevice_Click);
            // 
            // btnUpdateDevice
            // 
            this.btnUpdateDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdateDevice.ForeColor = System.Drawing.Color.White;
            this.btnUpdateDevice.Location = new System.Drawing.Point(471, 657);
            this.btnUpdateDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateDevice.Name = "btnUpdateDevice";
            this.btnUpdateDevice.Size = new System.Drawing.Size(173, 46);
            this.btnUpdateDevice.TabIndex = 6;
            this.btnUpdateDevice.Text = "Update";
            this.btnUpdateDevice.UseVisualStyleBackColor = false;
            this.btnUpdateDevice.Click += new System.EventHandler(this.btnUpdateDevice_Click);
            // 
            // btnResetDevice
            // 
            this.btnResetDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetDevice.ForeColor = System.Drawing.Color.White;
            this.btnResetDevice.Location = new System.Drawing.Point(281, 589);
            this.btnResetDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetDevice.Name = "btnResetDevice";
            this.btnResetDevice.Size = new System.Drawing.Size(173, 46);
            this.btnResetDevice.TabIndex = 5;
            this.btnResetDevice.Text = "Reset";
            this.btnResetDevice.UseVisualStyleBackColor = false;
            this.btnResetDevice.Click += new System.EventHandler(this.btnResetDevice_Click);
            // 
            // btnAddDevice
            // 
            this.btnAddDevice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnAddDevice.ForeColor = System.Drawing.Color.White;
            this.btnAddDevice.Location = new System.Drawing.Point(471, 589);
            this.btnAddDevice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddDevice.Name = "btnAddDevice";
            this.btnAddDevice.Size = new System.Drawing.Size(173, 46);
            this.btnAddDevice.TabIndex = 4;
            this.btnAddDevice.Text = "Add";
            this.btnAddDevice.UseVisualStyleBackColor = false;
            this.btnAddDevice.Click += new System.EventHandler(this.btnAddDevice_Click);
            // 
            // txtUDID
            // 
            this.txtUDID.BackColor = System.Drawing.Color.White;
            this.txtUDID.Location = new System.Drawing.Point(211, 536);
            this.txtUDID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUDID.Name = "txtUDID";
            this.txtUDID.Size = new System.Drawing.Size(433, 28);
            this.txtUDID.TabIndex = 3;
            // 
            // txtLocation
            // 
            this.txtLocation.BackColor = System.Drawing.Color.White;
            this.txtLocation.Location = new System.Drawing.Point(211, 471);
            this.txtLocation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(433, 28);
            this.txtLocation.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(19, 538);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 24);
            this.label13.TabIndex = 1;
            this.label13.Text = "UDID:";
            // 
            // txtType
            // 
            this.txtType.BackColor = System.Drawing.Color.White;
            this.txtType.Location = new System.Drawing.Point(211, 404);
            this.txtType.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(433, 28);
            this.txtType.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 473);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 24);
            this.label12.TabIndex = 1;
            this.label12.Text = "Location:";
            // 
            // txtDeviceEmail
            // 
            this.txtDeviceEmail.BackColor = System.Drawing.Color.White;
            this.txtDeviceEmail.Location = new System.Drawing.Point(211, 336);
            this.txtDeviceEmail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceEmail.Name = "txtDeviceEmail";
            this.txtDeviceEmail.Size = new System.Drawing.Size(433, 28);
            this.txtDeviceEmail.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 406);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 24);
            this.label8.TabIndex = 1;
            this.label8.Text = "Platform:";
            // 
            // txtDeviceName
            // 
            this.txtDeviceName.BackColor = System.Drawing.Color.White;
            this.txtDeviceName.Location = new System.Drawing.Point(211, 265);
            this.txtDeviceName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceName.Name = "txtDeviceName";
            this.txtDeviceName.Size = new System.Drawing.Size(433, 28);
            this.txtDeviceName.TabIndex = 3;
            // 
            // txtDeviceCode
            // 
            this.txtDeviceCode.BackColor = System.Drawing.Color.White;
            this.txtDeviceCode.Location = new System.Drawing.Point(211, 195);
            this.txtDeviceCode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceCode.Name = "txtDeviceCode";
            this.txtDeviceCode.Size = new System.Drawing.Size(433, 28);
            this.txtDeviceCode.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 338);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(117, 24);
            this.label11.TabIndex = 1;
            this.label11.Text = "Device User:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 268);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(129, 24);
            this.label14.TabIndex = 1;
            this.label14.Text = "Device Name:";
            // 
            // txtDeviceModel
            // 
            this.txtDeviceModel.BackColor = System.Drawing.Color.White;
            this.txtDeviceModel.Location = new System.Drawing.Point(211, 126);
            this.txtDeviceModel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDeviceModel.Name = "txtDeviceModel";
            this.txtDeviceModel.Size = new System.Drawing.Size(433, 28);
            this.txtDeviceModel.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Device Code:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(19, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 24);
            this.label16.TabIndex = 0;
            this.label16.Text = "No.:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Device Model:";
            // 
            // dgvDevices
            // 
            this.dgvDevices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDevices.Location = new System.Drawing.Point(21, 19);
            this.dgvDevices.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvDevices.MultiSelect = false;
            this.dgvDevices.Name = "dgvDevices";
            this.dgvDevices.ReadOnly = true;
            this.dgvDevices.RowHeadersWidth = 51;
            this.dgvDevices.RowTemplate.Height = 24;
            this.dgvDevices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDevices.Size = new System.Drawing.Size(1051, 725);
            this.dgvDevices.TabIndex = 0;
            this.dgvDevices.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDevices_CellClick);
            // 
            // tabProduct
            // 
            this.tabProduct.Controls.Add(this.groupBox3);
            this.tabProduct.Controls.Add(this.dgvProduct);
            this.tabProduct.Location = new System.Drawing.Point(4, 31);
            this.tabProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabProduct.Name = "tabProduct";
            this.tabProduct.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabProduct.Size = new System.Drawing.Size(1787, 784);
            this.tabProduct.TabIndex = 1;
            this.tabProduct.Text = "Products";
            this.tabProduct.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbbType);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtProductNo);
            this.groupBox3.Controls.Add(this.btnSetupZone);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtProject);
            this.groupBox3.Controls.Add(this.btnRemoveProduct);
            this.groupBox3.Controls.Add(this.btnUpdateProduct);
            this.groupBox3.Controls.Add(this.btnResetProduct);
            this.groupBox3.Controls.Add(this.btnAddProduct);
            this.groupBox3.Controls.Add(this.txtResult);
            this.groupBox3.Controls.Add(this.txtProductID);
            this.groupBox3.Controls.Add(this.txtCompanyID);
            this.groupBox3.Controls.Add(this.txtProductName);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.txtCompanyName);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(1095, 12);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(661, 747);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Product Info";
            // 
            // cbbType
            // 
            this.cbbType.FormattingEnabled = true;
            this.cbbType.Items.AddRange(new object[] {
            "Manual",
            "Video Guidance"});
            this.cbbType.Location = new System.Drawing.Point(198, 482);
            this.cbbType.Name = "cbbType";
            this.cbbType.Size = new System.Drawing.Size(432, 30);
            this.cbbType.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 490);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(58, 24);
            this.label15.TabIndex = 12;
            this.label15.Text = "Type:";
            // 
            // txtProductNo
            // 
            this.txtProductNo.Location = new System.Drawing.Point(199, 58);
            this.txtProductNo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProductNo.Name = "txtProductNo";
            this.txtProductNo.ReadOnly = true;
            this.txtProductNo.Size = new System.Drawing.Size(433, 28);
            this.txtProductNo.TabIndex = 11;
            // 
            // btnSetupZone
            // 
            this.btnSetupZone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnSetupZone.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSetupZone.Location = new System.Drawing.Point(198, 673);
            this.btnSetupZone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSetupZone.Name = "btnSetupZone";
            this.btnSetupZone.Size = new System.Drawing.Size(432, 46);
            this.btnSetupZone.TabIndex = 10;
            this.btnSetupZone.Text = "Setup Zone";
            this.btnSetupZone.UseVisualStyleBackColor = false;
            this.btnSetupZone.Click += new System.EventHandler(this.btnSetupZone_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 63);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 24);
            this.label17.TabIndex = 9;
            this.label17.Text = "No.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "Project:";
            // 
            // txtProject
            // 
            this.txtProject.BackColor = System.Drawing.Color.White;
            this.txtProject.Location = new System.Drawing.Point(199, 114);
            this.txtProject.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProject.Name = "txtProject";
            this.txtProject.Size = new System.Drawing.Size(433, 28);
            this.txtProject.TabIndex = 8;
            // 
            // btnRemoveProduct
            // 
            this.btnRemoveProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRemoveProduct.ForeColor = System.Drawing.Color.White;
            this.btnRemoveProduct.Location = new System.Drawing.Point(198, 604);
            this.btnRemoveProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoveProduct.Name = "btnRemoveProduct";
            this.btnRemoveProduct.Size = new System.Drawing.Size(196, 46);
            this.btnRemoveProduct.TabIndex = 7;
            this.btnRemoveProduct.Text = "Remove";
            this.btnRemoveProduct.UseVisualStyleBackColor = false;
            this.btnRemoveProduct.Click += new System.EventHandler(this.btnRemoveProduct_Click);
            // 
            // btnUpdateProduct
            // 
            this.btnUpdateProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdateProduct.ForeColor = System.Drawing.Color.White;
            this.btnUpdateProduct.Location = new System.Drawing.Point(439, 604);
            this.btnUpdateProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateProduct.Name = "btnUpdateProduct";
            this.btnUpdateProduct.Size = new System.Drawing.Size(193, 46);
            this.btnUpdateProduct.TabIndex = 6;
            this.btnUpdateProduct.Text = "Update";
            this.btnUpdateProduct.UseVisualStyleBackColor = false;
            this.btnUpdateProduct.Click += new System.EventHandler(this.btnUpdateProduct_Click);
            // 
            // btnResetProduct
            // 
            this.btnResetProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetProduct.ForeColor = System.Drawing.Color.White;
            this.btnResetProduct.Location = new System.Drawing.Point(198, 538);
            this.btnResetProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetProduct.Name = "btnResetProduct";
            this.btnResetProduct.Size = new System.Drawing.Size(196, 46);
            this.btnResetProduct.TabIndex = 5;
            this.btnResetProduct.Text = "Reset";
            this.btnResetProduct.UseVisualStyleBackColor = false;
            this.btnResetProduct.Click += new System.EventHandler(this.btnResetProduct_Click);
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnAddProduct.ForeColor = System.Drawing.Color.White;
            this.btnAddProduct.Location = new System.Drawing.Point(439, 538);
            this.btnAddProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(193, 46);
            this.btnAddProduct.TabIndex = 4;
            this.btnAddProduct.Text = "Add";
            this.btnAddProduct.UseVisualStyleBackColor = false;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // txtResult
            // 
            this.txtResult.BackColor = System.Drawing.Color.White;
            this.txtResult.Location = new System.Drawing.Point(199, 382);
            this.txtResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(433, 70);
            this.txtResult.TabIndex = 2;
            // 
            // txtProductID
            // 
            this.txtProductID.BackColor = System.Drawing.Color.White;
            this.txtProductID.Location = new System.Drawing.Point(199, 272);
            this.txtProductID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.Size = new System.Drawing.Size(433, 28);
            this.txtProductID.TabIndex = 2;
            // 
            // txtCompanyID
            // 
            this.txtCompanyID.BackColor = System.Drawing.Color.White;
            this.txtCompanyID.Location = new System.Drawing.Point(198, 165);
            this.txtCompanyID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCompanyID.Name = "txtCompanyID";
            this.txtCompanyID.Size = new System.Drawing.Size(433, 28);
            this.txtCompanyID.TabIndex = 2;
            // 
            // txtProductName
            // 
            this.txtProductName.BackColor = System.Drawing.Color.White;
            this.txtProductName.Location = new System.Drawing.Point(198, 324);
            this.txtProductName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(433, 28);
            this.txtProductName.TabIndex = 2;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 275);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(102, 24);
            this.label19.TabIndex = 0;
            this.label19.Text = "Product ID:";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.BackColor = System.Drawing.Color.White;
            this.txtCompanyName.Location = new System.Drawing.Point(198, 218);
            this.txtCompanyName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(433, 28);
            this.txtCompanyName.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 168);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(118, 24);
            this.label18.TabIndex = 0;
            this.label18.Text = "Company ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 327);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Product Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 382);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 24);
            this.label7.TabIndex = 0;
            this.label7.Text = "Result:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 221);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = "Company Name:";
            // 
            // dgvProduct
            // 
            this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduct.Location = new System.Drawing.Point(22, 25);
            this.dgvProduct.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvProduct.MultiSelect = false;
            this.dgvProduct.Name = "dgvProduct";
            this.dgvProduct.ReadOnly = true;
            this.dgvProduct.RowHeadersWidth = 51;
            this.dgvProduct.RowTemplate.Height = 24;
            this.dgvProduct.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduct.Size = new System.Drawing.Size(1051, 734);
            this.dgvProduct.TabIndex = 2;
            this.dgvProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduct_CellClick);
            // 
            // tabVersion
            // 
            this.tabVersion.Controls.Add(this.groupBox2);
            this.tabVersion.Controls.Add(this.dgvVersion);
            this.tabVersion.Location = new System.Drawing.Point(4, 31);
            this.tabVersion.Margin = new System.Windows.Forms.Padding(4);
            this.tabVersion.Name = "tabVersion";
            this.tabVersion.Padding = new System.Windows.Forms.Padding(4);
            this.tabVersion.Size = new System.Drawing.Size(1787, 784);
            this.tabVersion.TabIndex = 2;
            this.tabVersion.Text = "Versions";
            this.tabVersion.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtOS);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtVersionID);
            this.groupBox2.Controls.Add(this.btnRemoveVersion);
            this.groupBox2.Controls.Add(this.btnUpdateVersion);
            this.groupBox2.Controls.Add(this.btnResetVersion);
            this.groupBox2.Controls.Add(this.btnAddVersion);
            this.groupBox2.Controls.Add(this.txtVersion);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(1105, 21);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(675, 733);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Version Info";
            // 
            // txtOS
            // 
            this.txtOS.BackColor = System.Drawing.Color.White;
            this.txtOS.Location = new System.Drawing.Point(203, 193);
            this.txtOS.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtOS.Name = "txtOS";
            this.txtOS.Size = new System.Drawing.Size(437, 28);
            this.txtOS.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 24);
            this.label10.TabIndex = 9;
            this.label10.Text = "OS:";
            // 
            // txtVersionID
            // 
            this.txtVersionID.Location = new System.Drawing.Point(203, 58);
            this.txtVersionID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVersionID.Name = "txtVersionID";
            this.txtVersionID.ReadOnly = true;
            this.txtVersionID.Size = new System.Drawing.Size(437, 28);
            this.txtVersionID.TabIndex = 8;
            // 
            // btnRemoveVersion
            // 
            this.btnRemoveVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRemoveVersion.ForeColor = System.Drawing.Color.White;
            this.btnRemoveVersion.Location = new System.Drawing.Point(277, 332);
            this.btnRemoveVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoveVersion.Name = "btnRemoveVersion";
            this.btnRemoveVersion.Size = new System.Drawing.Size(173, 46);
            this.btnRemoveVersion.TabIndex = 7;
            this.btnRemoveVersion.Text = "Remove";
            this.btnRemoveVersion.UseVisualStyleBackColor = false;
            this.btnRemoveVersion.Click += new System.EventHandler(this.btnRemoveVersion_Click);
            // 
            // btnUpdateVersion
            // 
            this.btnUpdateVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdateVersion.ForeColor = System.Drawing.Color.White;
            this.btnUpdateVersion.Location = new System.Drawing.Point(467, 332);
            this.btnUpdateVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnUpdateVersion.Name = "btnUpdateVersion";
            this.btnUpdateVersion.Size = new System.Drawing.Size(173, 46);
            this.btnUpdateVersion.TabIndex = 6;
            this.btnUpdateVersion.Text = "Update";
            this.btnUpdateVersion.UseVisualStyleBackColor = false;
            this.btnUpdateVersion.Click += new System.EventHandler(this.btnUpdateVersion_Click);
            // 
            // btnResetVersion
            // 
            this.btnResetVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResetVersion.ForeColor = System.Drawing.Color.White;
            this.btnResetVersion.Location = new System.Drawing.Point(277, 264);
            this.btnResetVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnResetVersion.Name = "btnResetVersion";
            this.btnResetVersion.Size = new System.Drawing.Size(173, 46);
            this.btnResetVersion.TabIndex = 5;
            this.btnResetVersion.Text = "Reset";
            this.btnResetVersion.UseVisualStyleBackColor = false;
            this.btnResetVersion.Click += new System.EventHandler(this.btnResetVersion_Click);
            // 
            // btnAddVersion
            // 
            this.btnAddVersion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnAddVersion.ForeColor = System.Drawing.Color.White;
            this.btnAddVersion.Location = new System.Drawing.Point(467, 264);
            this.btnAddVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddVersion.Name = "btnAddVersion";
            this.btnAddVersion.Size = new System.Drawing.Size(173, 46);
            this.btnAddVersion.TabIndex = 4;
            this.btnAddVersion.Text = "Add";
            this.btnAddVersion.UseVisualStyleBackColor = false;
            this.btnAddVersion.Click += new System.EventHandler(this.btnAddVersion_Click);
            // 
            // txtVersion
            // 
            this.txtVersion.BackColor = System.Drawing.Color.White;
            this.txtVersion.Location = new System.Drawing.Point(203, 129);
            this.txtVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(437, 28);
            this.txtVersion.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 63);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 24);
            this.label20.TabIndex = 0;
            this.label20.Text = "No.:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 24);
            this.label9.TabIndex = 0;
            this.label9.Text = "App Version:";
            // 
            // dgvVersion
            // 
            this.dgvVersion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVersion.Location = new System.Drawing.Point(30, 35);
            this.dgvVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvVersion.MultiSelect = false;
            this.dgvVersion.Name = "dgvVersion";
            this.dgvVersion.ReadOnly = true;
            this.dgvVersion.RowHeadersWidth = 51;
            this.dgvVersion.RowTemplate.Height = 24;
            this.dgvVersion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVersion.Size = new System.Drawing.Size(1044, 719);
            this.dgvVersion.TabIndex = 2;
            this.dgvVersion.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVersion_CellClick);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbbThresholdServer);
            this.tabPage1.Controls.Add(this.cbbMasterServer);
            this.tabPage1.Controls.Add(this.progressBar1);
            this.tabPage1.Controls.Add(this.btnBrowseSaveAs);
            this.tabPage1.Controls.Add(this.btnBrowseThreshold);
            this.tabPage1.Controls.Add(this.progressBarMaster);
            this.tabPage1.Controls.Add(this.txtSaveAsThreshold);
            this.tabPage1.Controls.Add(this.txtThresholdJSONPath);
            this.tabPage1.Controls.Add(this.btnBrowseMasterJSON);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.txtMasterJSONPath);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.btnExportJSON);
            this.tabPage1.Controls.Add(this.btnImportThreshold);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.btnImportMasterJSON);
            this.tabPage1.Controls.Add(this.dgvMasterList);
            this.tabPage1.Location = new System.Drawing.Point(4, 31);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1787, 784);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "JSON";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cbbThresholdServer
            // 
            this.cbbThresholdServer.FormattingEnabled = true;
            this.cbbThresholdServer.Items.AddRange(new object[] {
            "EU - Europe",
            "AP - Asia & Pacific",
            "US - United State",
            "SBC - SBC Test Server",
            "999999 - Test 999999",
            "J&J - J&J Visioncare"});
            this.cbbThresholdServer.Location = new System.Drawing.Point(1080, 81);
            this.cbbThresholdServer.Name = "cbbThresholdServer";
            this.cbbThresholdServer.Size = new System.Drawing.Size(221, 30);
            this.cbbThresholdServer.TabIndex = 105;
            // 
            // cbbMasterServer
            // 
            this.cbbMasterServer.FormattingEnabled = true;
            this.cbbMasterServer.Items.AddRange(new object[] {
            "EU - Europe",
            "AP - Asia & Pacific",
            "US - United State",
            "SBC - SBC Test Server",
            "999999 - Test 999999",
            "J&J - J&J Visioncare"});
            this.cbbMasterServer.Location = new System.Drawing.Point(245, 87);
            this.cbbMasterServer.Name = "cbbMasterServer";
            this.cbbMasterServer.Size = new System.Drawing.Size(368, 30);
            this.cbbMasterServer.TabIndex = 105;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(1171, 187);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(571, 35);
            this.progressBar1.TabIndex = 98;
            // 
            // btnBrowseSaveAs
            // 
            this.btnBrowseSaveAs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseSaveAs.FlatAppearance.BorderSize = 0;
            this.btnBrowseSaveAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseSaveAs.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseSaveAs.Location = new System.Drawing.Point(1542, 135);
            this.btnBrowseSaveAs.Name = "btnBrowseSaveAs";
            this.btnBrowseSaveAs.Size = new System.Drawing.Size(45, 37);
            this.btnBrowseSaveAs.TabIndex = 97;
            this.btnBrowseSaveAs.Text = "...";
            this.btnBrowseSaveAs.UseVisualStyleBackColor = false;
            this.btnBrowseSaveAs.Click += new System.EventHandler(this.btnBrowseSaveAs_Click);
            // 
            // btnBrowseThreshold
            // 
            this.btnBrowseThreshold.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseThreshold.FlatAppearance.BorderSize = 0;
            this.btnBrowseThreshold.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseThreshold.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseThreshold.Location = new System.Drawing.Point(1643, 29);
            this.btnBrowseThreshold.Name = "btnBrowseThreshold";
            this.btnBrowseThreshold.Size = new System.Drawing.Size(45, 37);
            this.btnBrowseThreshold.TabIndex = 97;
            this.btnBrowseThreshold.Text = "...";
            this.btnBrowseThreshold.UseVisualStyleBackColor = false;
            this.btnBrowseThreshold.Click += new System.EventHandler(this.btnBrowseThreshold_Click);
            // 
            // progressBarMaster
            // 
            this.progressBarMaster.Location = new System.Drawing.Point(245, 136);
            this.progressBarMaster.Name = "progressBarMaster";
            this.progressBarMaster.Size = new System.Drawing.Size(565, 35);
            this.progressBarMaster.TabIndex = 98;
            // 
            // txtSaveAsThreshold
            // 
            this.txtSaveAsThreshold.BackColor = System.Drawing.Color.White;
            this.txtSaveAsThreshold.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaveAsThreshold.Location = new System.Drawing.Point(1171, 136);
            this.txtSaveAsThreshold.Multiline = true;
            this.txtSaveAsThreshold.Name = "txtSaveAsThreshold";
            this.txtSaveAsThreshold.Size = new System.Drawing.Size(363, 33);
            this.txtSaveAsThreshold.TabIndex = 96;
            // 
            // txtThresholdJSONPath
            // 
            this.txtThresholdJSONPath.BackColor = System.Drawing.Color.White;
            this.txtThresholdJSONPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThresholdJSONPath.Location = new System.Drawing.Point(1080, 31);
            this.txtThresholdJSONPath.Multiline = true;
            this.txtThresholdJSONPath.Name = "txtThresholdJSONPath";
            this.txtThresholdJSONPath.Size = new System.Drawing.Size(544, 33);
            this.txtThresholdJSONPath.TabIndex = 96;
            // 
            // btnBrowseMasterJSON
            // 
            this.btnBrowseMasterJSON.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseMasterJSON.FlatAppearance.BorderSize = 0;
            this.btnBrowseMasterJSON.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseMasterJSON.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseMasterJSON.Location = new System.Drawing.Point(765, 29);
            this.btnBrowseMasterJSON.Name = "btnBrowseMasterJSON";
            this.btnBrowseMasterJSON.Size = new System.Drawing.Size(45, 37);
            this.btnBrowseMasterJSON.TabIndex = 97;
            this.btnBrowseMasterJSON.Text = "...";
            this.btnBrowseMasterJSON.UseVisualStyleBackColor = false;
            this.btnBrowseMasterJSON.Click += new System.EventHandler(this.btnBrowseJSON_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(905, 90);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 24);
            this.label24.TabIndex = 95;
            this.label24.Text = "Server:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(975, 144);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(84, 24);
            this.label25.TabIndex = 95;
            this.label25.Text = "Save As:";
            // 
            // txtMasterJSONPath
            // 
            this.txtMasterJSONPath.BackColor = System.Drawing.Color.White;
            this.txtMasterJSONPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMasterJSONPath.Location = new System.Drawing.Point(245, 31);
            this.txtMasterJSONPath.Multiline = true;
            this.txtMasterJSONPath.Name = "txtMasterJSONPath";
            this.txtMasterJSONPath.Size = new System.Drawing.Size(507, 35);
            this.txtMasterJSONPath.TabIndex = 96;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(905, 34);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(156, 24);
            this.label22.TabIndex = 95;
            this.label22.Text = "Threshold JSON:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(43, 93);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(70, 24);
            this.label23.TabIndex = 95;
            this.label23.Text = "Server:";
            // 
            // btnExportJSON
            // 
            this.btnExportJSON.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnExportJSON.ForeColor = System.Drawing.Color.White;
            this.btnExportJSON.Location = new System.Drawing.Point(1607, 136);
            this.btnExportJSON.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportJSON.Name = "btnExportJSON";
            this.btnExportJSON.Size = new System.Drawing.Size(135, 35);
            this.btnExportJSON.TabIndex = 7;
            this.btnExportJSON.Text = "Export JSON";
            this.btnExportJSON.UseVisualStyleBackColor = false;
            // 
            // btnImportThreshold
            // 
            this.btnImportThreshold.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnImportThreshold.ForeColor = System.Drawing.Color.White;
            this.btnImportThreshold.Location = new System.Drawing.Point(1643, 82);
            this.btnImportThreshold.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnImportThreshold.Name = "btnImportThreshold";
            this.btnImportThreshold.Size = new System.Drawing.Size(135, 35);
            this.btnImportThreshold.TabIndex = 7;
            this.btnImportThreshold.Text = "Import JSON";
            this.btnImportThreshold.UseVisualStyleBackColor = false;
            this.btnImportThreshold.Click += new System.EventHandler(this.btnImportThreshold_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(43, 39);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(189, 24);
            this.label21.TabIndex = 95;
            this.label21.Text = "Master Device JSON:";
            // 
            // btnImportMasterJSON
            // 
            this.btnImportMasterJSON.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnImportMasterJSON.ForeColor = System.Drawing.Color.White;
            this.btnImportMasterJSON.Location = new System.Drawing.Point(642, 84);
            this.btnImportMasterJSON.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnImportMasterJSON.Name = "btnImportMasterJSON";
            this.btnImportMasterJSON.Size = new System.Drawing.Size(168, 35);
            this.btnImportMasterJSON.TabIndex = 7;
            this.btnImportMasterJSON.Text = "Import JSON";
            this.btnImportMasterJSON.UseVisualStyleBackColor = false;
            this.btnImportMasterJSON.Click += new System.EventHandler(this.btnImportJSON_Click);
            // 
            // dgvMasterList
            // 
            this.dgvMasterList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMasterList.Location = new System.Drawing.Point(45, 197);
            this.dgvMasterList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvMasterList.MultiSelect = false;
            this.dgvMasterList.Name = "dgvMasterList";
            this.dgvMasterList.ReadOnly = true;
            this.dgvMasterList.RowHeadersWidth = 51;
            this.dgvMasterList.RowTemplate.Height = 24;
            this.dgvMasterList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMasterList.Size = new System.Drawing.Size(765, 551);
            this.dgvMasterList.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(12, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 31);
            this.label4.TabIndex = 56;
            this.label4.Text = "Configuration";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // Configuration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1830, 950);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tabConfiguration);
            this.Font = new System.Drawing.Font("Arial", 14.75F);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Configuration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini tool v3.11";
            this.Controls.SetChildIndex(this.tabConfiguration, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.tabConfiguration.ResumeLayout(false);
            this.tabDevice.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDevices)).EndInit();
            this.tabProduct.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
            this.tabVersion.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVersion)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMasterList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabControl tabConfiguration;
        private System.Windows.Forms.TabPage tabDevice;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDeviceID;
        private System.Windows.Forms.Button btnRemoveDevice;
        private System.Windows.Forms.Button btnUpdateDevice;
        private System.Windows.Forms.Button btnResetDevice;
        private System.Windows.Forms.Button btnAddDevice;
        private System.Windows.Forms.TextBox txtUDID;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDeviceEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDeviceCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDeviceModel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvDevices;
        private System.Windows.Forms.TabPage tabProduct;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cbbType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtProductNo;
        private System.Windows.Forms.Button btnSetupZone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtProject;
        private System.Windows.Forms.Button btnRemoveProduct;
        private System.Windows.Forms.Button btnUpdateProduct;
        private System.Windows.Forms.Button btnResetProduct;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvProduct;
        private System.Windows.Forms.TabPage tabVersion;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtOS;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtVersionID;
        private System.Windows.Forms.Button btnRemoveVersion;
        private System.Windows.Forms.Button btnUpdateVersion;
        private System.Windows.Forms.Button btnResetVersion;
        private System.Windows.Forms.Button btnAddVersion;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvVersion;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDeviceName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.TextBox txtCompanyID;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvMasterList;
        private System.Windows.Forms.Button btnImportMasterJSON;
        private System.Windows.Forms.Button btnBrowseMasterJSON;
        private System.Windows.Forms.TextBox txtMasterJSONPath;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ProgressBar progressBarMaster;
        private System.Windows.Forms.ComboBox cbbMasterServer;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cbbThresholdServer;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnBrowseThreshold;
        private System.Windows.Forms.TextBox txtThresholdJSONPath;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnExportJSON;
        private System.Windows.Forms.Button btnImportThreshold;
        private System.Windows.Forms.Button btnBrowseSaveAs;
        private System.Windows.Forms.TextBox txtSaveAsThreshold;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}