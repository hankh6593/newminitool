﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class ReplaceTextMessage : MasterForm
    {
        public ReplaceTextMessage() : base()
        {
            InitializeComponent();
            progressBar.Visible = false;
            progressBar.Value = 0;
        }

        private void btnBrowseRaw_Click(object sender, EventArgs e)
        {
            folderBrowseDialog.ShowDialog();
            txtDestinationFolder.Text = folderBrowseDialog.SelectedPath;
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            DirectoryInfo d = new DirectoryInfo(txtDestinationFolder.Text);
            FileInfo[] files = d.GetFiles("*.csv");
            progressBar.Visible = true;
            progressBar.Value = 0;
            progressBar.Maximum = files.Length * 10;
            for (int i=0; i< files.Length; i++)
            {
                progressBar.Value = i*10;
                try
                {
                    // StreamReader sr = new StreamReader(path, Encoding.GetEncoding(1250));
                    StreamReader sr = new StreamReader(files[i].FullName, System.Text.Encoding.UTF8);
                    string strline="";
                    while (!sr.EndOfStream)
                    {
                        strline = sr.ReadToEnd();
                        strline = strline.Replace("有效 / Valid\nTouch screen to continue", "有效 / Valid");
                        strline = strline.Replace("过期 / Expired\nTouch screen to continue", "过期 / Expired");
                        strline = strline.Replace("无效 / Not valid\nTouch screen to continue", "无效 / Not valid");
                        strline = strline.Replace("再试一次 / Please try again\n Touch screen to continue", "再试一次 / Please try again");
                    }
                    sr.Close();
                    File.WriteAllText(files[i].FullName, strline, System.Text.Encoding.UTF8);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            progressBar.Visible = false;
            MessageBox.Show("All messages have been replaced. Please help to check again.");
        }
    }
}
