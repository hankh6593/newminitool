﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class DeviceDialog : Form
    {
        List<Device> listDevice = new List<Device>();
        public DeviceDialog()
        {
            InitializeComponent();
        }
        public DeviceDialog(List<Device> listDevice)
        {
            InitializeComponent();
            dgvDevice.DataSource = listDevice;
            dgvDevice.Columns[0].Visible = false;
            //dgvDevice.Columns[1].Visible = false;
            //dgvDevice.Columns[6].Visible = false;
            //dgvDevice.Columns[7].Visible = false;
            dgvDevice.Columns[2].Width = 200;
            this.listDevice = listDevice;
            if (listDevice.Count == 1)
            {
                lblMessage.Text = "There is a unknown device. To perform upload data, the device has been configured into database.";
            }
            else if (listDevice.Count > 1)
            {
                lblMessage.Text = "There are " + listDevice.Count + " unknown devices. To perform upload data, the device has been configured into database.";
            }
            else
            {
                lblMessage.Text = "All devices have been configured into database. Please perform upload data again.";
            }

        }

        private void btnAddDevice_Click(object sender, EventArgs e)
        {
            if (txtDeviceName.Text.Length == 0)
            {
                MessageBox.Show("Please fill in device name.");
            }
            else
            {
                DatabaseConnection db = new DatabaseConnection();
                Int32 selectedRowCount = dgvDevice.SelectedRows[0].Index;
                Device device = listDevice[selectedRowCount];
                db.AddDevice(device.model, device.code, device.name, device.email, device.location, device.type,device.UDID,false);
                MessageBox.Show("Device has been added to database.");
                txtDeviceCode.Text = "";
                txtDeviceEmail.Text = "";
                txtDeviceName.Text = "";
                txtDeviceModel.Text = "";
                txtLocation.Text = "";
                txtType.Text = "";
                listDevice.RemoveAt(selectedRowCount);
                dgvDevice.DataSource = null;
                dgvDevice.DataSource = listDevice;
                //dgvDevice.Columns[0].Visible = false;
                //dgvDevice.Columns[1].Visible = false;
                //dgvDevice.Columns[6].Visible = false;
                //dgvDevice.Columns[7].Visible = false;
                dgvDevice.Columns[2].Width = 200;
                txtDeviceName.Text = "";
                if (listDevice.Count == 1)
                {
                    lblMessage.Text = "There is a unknown device. To perform upload data, the device has been configured into database.";
                }
                else if (listDevice.Count > 1)
                {
                    lblMessage.Text = "There are " + listDevice.Count + " unknown devices. To perform upload data, the device has been configured into database.";
                }
                else
                {
                    lblMessage.Text = "All devices have been configured into database. Please perform upload data again.";
                }

            }
        }

        private void dgvDevice_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Int32 selectedRowCount = dgvDevice.SelectedRows[0].Index;
            if (selectedRowCount >= 0)
            {
                txtDeviceCode.Text = (dgvDevice.Rows[selectedRowCount].Cells[2].Value.ToString());
                txtDeviceModel.Text = (dgvDevice.Rows[selectedRowCount].Cells[1].Value.ToString());
                txtDeviceEmail.Text = (dgvDevice.Rows[selectedRowCount].Cells[4].Value.ToString());
                txtLocation.Text = (dgvDevice.Rows[selectedRowCount].Cells[5].Value.ToString());
                txtType.Text = (dgvDevice.Rows[selectedRowCount].Cells[6].Value.ToString());
            }
        }
    }
}
