﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Version = MiniTool.Data.Version;

namespace MiniTool
{
    public partial class ReleasedProductEvaluation : MasterForm
    {
        List<String> listData;
        Product product;
        List<List<String>> listCleanedData;
        List<ResultForEvaluation> listDataForEvaluation;
        List<ResultTestSummary> listTestInfo;
        DatabaseConnection db;
        public ReleasedProductEvaluation() : base()
        {
            InitializeComponent();
            progressBar.Visible = false;
            ShowSeparateLine(false);
            db = new DatabaseConnection();

        }

        private void ShowSeparateLine(bool isShowed)
        {
            txtTestQty.Visible = isShowed;
            lblTestQty.Visible = isShowed;
            txtSeperatedQty.Visible = isShowed;
            lblSeperatedQty.Visible = isShowed;
        }

        private void cbRegressionTest_CheckedChanged(object sender, EventArgs e)
        {
            ShowSeparateLine(cbRegressionTest.Checked ? true : false);
        }

        private void btnBrowseResult_Click(object sender, EventArgs e)
        {
            ResultOpenDialog.ShowDialog();
            ResultOpenDialog.DefaultExt = "csv";
            ResultOpenDialog.Filter = "Comma Delimited|*.csv";
            if (ResultOpenDialog.FileName.Contains(".csv"))
            {
                txtResultPath.Text = ResultOpenDialog.FileName;
            }
            else {
                MessageBox.Show("Please input CSV file.");
            }
         
        }

        private void btnBrowseLocation_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.ShowDialog();
            txtSaveAs.Text = folderBrowserDialog.SelectedPath;
        }

        private void btnGetResult_Click(object sender, EventArgs e)
        {
            if (txtResultPath.Text.Length == 0)
            {
                MessageBox.Show("Please select the file");
            }
            else
            {

                listData = Ultility.RemoveDoubleQuoteFromRawData(Ultility.GetRawData(txtResultPath.Text));
                if (listData.Count != 0)
                {
                    product = Ultility.GetProductFromCSV(listData);
                    if (product != null && product.id != -1)
                    {
                        listCleanedData = Ultility.GetCleanData(txtResultPath.Text, product);
                        if (listCleanedData.Count != 0)
                        {
                            if (!cbRegressionTest.Checked)
                            {

                                listDataForEvaluation = GetListDataForEvaluation(listCleanedData, product);
                                listTestInfo = new List<ResultTestSummary>();
                                ResultTestSummary testInfo = new ResultTestSummary();
                                testInfo.testName = "Test 1";
                                testInfo.listResult = Ultility.GetResultListFromDataRaw(listCleanedData, product.listZone, 1, listCleanedData.Count, true);
                                listTestInfo.Add(testInfo);
                                progressBar.Visible = false;
                                ShowTestResult(listDataForEvaluation.Count);
                                LoadZoneData();
                            }
                            else
                            {
                                String[] testQty = txtTestQty.Text.Split(';');
                                int totalRow = 0;
                                for (int i = 0; i < testQty.Length; i++)
                                {
                                    totalRow += Int32.Parse(testQty[i]);
                                }
                                totalRow += Int32.Parse(txtSeperatedQty.Text) * (testQty.Length - 1);
                                if (listCleanedData.Count <= totalRow)
                                {
                                    MessageBox.Show("The result records (" + (listCleanedData.Count - 1) + ") is not equal as your input (" + totalRow + "). Please check again.");
                                }
                                else
                                {
                                    int deliminatorQty = Int32.Parse(txtSeperatedQty.Text);
                                    int startNumber = 1;
                                    listTestInfo = new List<ResultTestSummary>();
                                    for (int i = 0; i < testQty.Length; i++)
                                    {
                                        ResultTestSummary testInfo = new ResultTestSummary();
                                        testInfo.testName = "Test " + (i + 1);
                                        if (startNumber == 1)
                                        {
                                            testInfo.listResult = Ultility.GetResultListFromDataRaw(listCleanedData, product.listZone, startNumber, Int32.Parse(testQty[i]) + 1, true);
                                            startNumber += Int32.Parse(testQty[i]) + deliminatorQty;
                                        }
                                        else
                                        {
                                            testInfo.listResult = Ultility.GetResultListFromDataRaw(listCleanedData, product.listZone, startNumber, startNumber + Int32.Parse(testQty[i]), true);
                                            startNumber += Int32.Parse(testQty[i]) + deliminatorQty;
                                        }
                                        listTestInfo.Add(testInfo);
                                    }
                                    listDataForEvaluation = GetListDataForEvaluation(listCleanedData, product);
                                    ShowTestResult(-1);
                                    LoadZoneData();
                                }
                            }
                            Version version = Ultility.GetVersionFromCSV(listCleanedData);
                            Device device = Ultility.GetDeviceFromCSV(listCleanedData, 1);
                            String fileName = product.project + "_EVA_" + version.os + "_" + version.version + "_" + device.model.Replace(" ", "-") + "_" + product.productName.Replace(" ", "-");
                            txtFileName.Text = fileName;
                        }
                        else
                        {

                        }
                    }
                    if (product == null || (product != null && product.id == -1))
                    {
                        String[] infos = Ultility.GetProductInfoFromCSV(listData).Split('-');
                        MessageBox.Show("The product " + infos[1] + "(" + infos[0] + ") in company " + infos[3] + "(" + infos[2] + ") has not been setup. Please setup the product");
                    }

                }
            }
        }

        private List<ResultForEvaluation> GetListDataForEvaluation(List<List<String>> listCleanedData, Product product)
        {

            List<ResultForEvaluation> listDataForEvaluation = new List<ResultForEvaluation>();
            progressBar.Visible = true;
            progressBar.Value = 0;
            progressBar.Maximum = listCleanedData.Count * 10;
            for (int row = 1; row < listCleanedData.Count; row++)
            {
                progressBar.Value = row * 10;
                string resultText = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Result Text")].Replace("\"", "") + "(" + listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Result Code")].Replace("\"", "") + ")"; ;
                string statisticID = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Statistic ID")].Replace("\"", "");
                Version version = Ultility.GetVersionFromCSV(listCleanedData);
                Device device = Ultility.GetDeviceFromCSV(listCleanedData, row);
                int zoneCDS = 0;
                for (int k = 0; k < product.listZone.Count; k++)
                {
                    if (product.listZone[k].method.Equals("CDS"))
                    {
                        zoneCDS = product.listZone[k].zoneNo;
                        k = product.listZone.Count;
                    }
                }
                for (int k = 0; k < product.listZone.Count; k++)
                {

                    ResultForEvaluation resultEva = new ResultForEvaluation(statisticID.ToString(), version.id, device, product.id, resultText);
                    resultEva.createdDate = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Date and Time")].Replace("\"", "");
                    resultEva.deviceID = device.id;
                    resultEva.deviceCode = device.code;
                    resultEva.deviceEmail = device.email;
                    resultEva.deviceModel = device.model;

                    if (product.listZone[k].zoneNo == zoneCDS)
                    {
                        resultEva.value = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Zone Result Zone " + (product.listZone[k].zoneNo))];
                        resultEva.info = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Debug Info Zone " + (product.listZone[k].zoneNo))];
                        if (Double.Parse(resultEva.value) == -1 && !resultEva.info.Contains("FineSigma"))
                        {
                            resultEva.value = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Zone Result Zone " + (zoneCDS + 1))];
                            resultEva.info = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Debug Info Zone " + (zoneCDS + 1))].Replace("Zone=" + (zoneCDS + 1), "Zone=" + zoneCDS);
                        }

                    }
                    else
                    {
                        resultEva.value = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Zone Result Zone " + (product.listZone[k].zoneNo))];
                        resultEva.info = listCleanedData[row][Ultility.FindColumn(listCleanedData[0], "Debug Info Zone " + (product.listZone[k].zoneNo))];
                    }

                    resultEva.zone = product.listZone[k].zoneNo;
                    resultEva.method = product.listZone[k].method;
                    listDataForEvaluation.Add(resultEva);
                }
            }
            progressBar.Visible = false;
            return listDataForEvaluation;

        }

        private void LoadZoneData()
        {

            List<String> listZone = new List<String>();
            int countCDS = 0;
            for (int i = 0; i < product.listZone.Count; i++)
            {
                if (product.listZone[i].method.Equals("CDS"))
                {
                    if (countCDS != 1)
                    {
                        listZone.Add((product.listZone[i].zoneNo) + " - " + product.listZone[i].method);
                    }

                    countCDS++;
                }
                else
                {
                    listZone.Add((product.listZone[i].zoneNo) + " - " + product.listZone[i].method);
                }

            }
            cbbZone.DataSource = listZone;
        }

        private void ShowTestResult(int total)
        {
            dgvResult.Columns.Clear();
            dgvResult.DataSource = null;
            String result = "";

            dgvResult.Columns.Add("DeviceModel", "Device Model");
            dgvResult.Columns.Add("DeviceID", "Device ID");
            dgvResult.Columns.Add("Procedure", "Procedure");
            dgvResult.Columns.Add("Summary", "Summary");

            int jump = 4;
            for (int i = 0; i < product.listZone.Count; i++)
            {
                if (product.productName.Length == 0)
                {
                    dgvResult.Columns.Add("product" + i, "Trial Product");
                    dgvResult.Columns.Add("product" + i, "Trial Product");
                }
                else
                {
                    dgvResult.Columns.Add("product" + i, product.productName);
                    dgvResult.Columns.Add("product" + i, product.productName);
                }

                dgvResult.Rows.Add();
                if (product.listZone[i].method.Equals("STC"))
                {
                    dgvResult.Rows[0].Cells[jump].Value = "Zone " + (i + 1) + " (STC)";
                }
                else if (product.listZone[i].method.Equals("CDS"))
                {
                    dgvResult.Rows[0].Cells[jump].Value = "Zone " + (i + 1) + " (CDS)";
                }
                else if (product.listZone[i].method.Equals("CD1"))
                {
                    dgvResult.Rows[0].Cells[jump].Value = "Zone " + (i + 1) + " (CD1)";
                }
                else if (product.listZone[i].method.Equals("CD3"))
                {
                    dgvResult.Rows[0].Cells[jump].Value = "Zone " + (i + 1) + " (CD3)";
                }
                else if (product.listZone[i].method.Equals("CD2"))
                {
                    dgvResult.Rows[0].Cells[jump].Value = "Zone " + (i + 1) + " (CD2)";
                }
                else if (product.listZone[i].method.Equals("Others"))
                {
                    dgvResult.Rows[0].Cells[jump].Value = "Others";
                }
                dgvResult.Rows.Add();
                dgvResult.Rows[1].Cells[jump].Value = "Result";
                dgvResult.Rows[1].Cells[jump + 1].Value = "Remarks";
                jump += 2;
            }

            int rowPosition = 2;
            for (int i = 0; i < listTestInfo.Count; i++)
            {

                int columnPosition = 4;
                dgvResult.Rows.Add();
                dgvResult.Rows[rowPosition].Cells[0].Value = Ultility.GetDeviceFromCSV(listCleanedData, 2).code;
                dgvResult.Rows[rowPosition].Cells[1].Value = Ultility.GetDeviceFromCSV(listCleanedData, 2).code;
                dgvResult.Rows[rowPosition].Cells[2].Value = listTestInfo[i].testName;
                dgvResult.Rows[rowPosition].Cells[3].Value = listTestInfo[i].GetSummary(product);
                for (int j = 0; j < product.listZone.Count; j++)
                {
                    result = "";
                    ResultTestSummary testInfo = listTestInfo[i];

                    testInfo.GetTestResult(product.listZone[j], product.listZone[j].zoneNo);
                    if (product.listZone[j].method.Equals("STC"))
                    {
                        string status = product.result.Split(',')[0];
                        if (total == -1)
                        {
                            dgvResult.Rows[rowPosition].Cells[columnPosition].Value = testInfo.countValid + "/" + txtTestQty.Text.Split(';')[i];
                        }
                        else
                        {
                            dgvResult.Rows[rowPosition].Cells[columnPosition].Value = testInfo.countValid + "/" + (total/product.listZone.Count);
                        }

                        if (testInfo.countQualityReject != 0)
                        {
                            result += testInfo.countQualityReject + " Quality Reject\r\n";
                        }
                        if (testInfo.countQualityRejectWrongDebug != 0)
                        {
                            result += testInfo.countQualityRejectWrongDebug + " Quality Reject With Wrong Debug\r\n";
                        }
                        if (testInfo.countWrongCode != 0)
                        {
                            result += testInfo.countWrongCode + " Wrong Code\r\n";
                        }
                        if (testInfo.countNotRead != 0)
                        {
                            result += testInfo.countNotRead + " Not Read\r\n";
                        }
                        dgvResult.Rows[rowPosition].Cells[columnPosition + 1].Value = result;
                        columnPosition += 2;
                    }
                    else if (product.listZone[j].method.Equals("CDS"))
                    {
                        dgvResult.Rows[rowPosition].Cells[columnPosition].Value = "(" + testInfo.MinResult + ") - (" + testInfo.MaxResult + ")";
                        if (testInfo.countNotRead != 0)
                        {
                            result += testInfo.countNotRead + " Not Read\r\n";
                        }
                        dgvResult.Rows[rowPosition].Cells[columnPosition + 1].Value = result;
                        columnPosition += 2;

                    }
                    else if (product.listZone[j].method.Equals("Others"))
                    {
                        dgvResult.Rows[rowPosition].Cells[columnPosition].Value = "N/A";
                        if (testInfo.countNotRead != 0)
                        {
                            result += testInfo.countNotRead + " Not Read\r\n";
                        }
                        dgvResult.Rows[rowPosition].Cells[columnPosition + 1].Value = "N/A";
                        columnPosition += 2;

                    }
                    else if (product.listZone[j].method.Equals("CD1") || product.listZone[j].method.Equals("CD3") || product.listZone[j].method.Equals("CD2"))
                    {
                        if (Math.Round(testInfo.MinResult, 2) == Math.Round(testInfo.MaxResult, 2))
                        {
                            dgvResult.Rows[rowPosition].Cells[columnPosition].Value = Math.Round(testInfo.MinResult, 2);
                        }
                        else
                        {
                            dgvResult.Rows[rowPosition].Cells[columnPosition].Value = "(" + Math.Round(testInfo.MinResult, 2) + ") - (" + Math.Round(testInfo.MaxResult, 2) + ")";
                        }

                        dgvResult.Rows[rowPosition].Cells[columnPosition + 1].Value = testInfo.countNotRead + " Not Read\r\n";
                        columnPosition += 2;
                    }

                }
                rowPosition++;
            }

            for (int i = 0; i < dgvResult.Columns.Count; i++)
            {
                dgvResult.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void cbbZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            int zone = Int32.Parse(cbbZone.Text.Split('-')[0].Trim());
            String method = cbbZone.Text.Split('-')[1].Trim();
            if (method.Equals("STC"))
            {
                List<String> listEdge = new List<String>();
                int countEdge = Ultility.GetEdgeQtyFromCSV(listCleanedData, zone);
                for (int i = 0; i < countEdge; i++)
                {
                    listEdge.Add("Edge " + (i + 1));
                }
                var bindingEdge = new BindingSource();
                bindingEdge.DataSource = listEdge;
                cbbEdge.DataSource = bindingEdge.DataSource;
            }
            else if (method.Equals("Others"))
            {
                cbbEdge.DataSource = null;
            }
            else
            {
                cbbEdge.DataSource = null;
                Ultility.DrawChartCDS(chartEvaluation, dgvData, listDataForEvaluation, zone);
            }
        }

        private void cbbEdge_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbEdge.SelectedIndex != -1)
            {
                int zone = Int32.Parse(cbbZone.Text.Split('-')[0].Trim());
                int edgeQty = Ultility.GetEdgeQtyFromCSV(listCleanedData, zone);
                Ultility.DrawChartSTC(chartEvaluation, dgvData, listDataForEvaluation, zone, edgeQty, cbbEdge.SelectedIndex);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (listCleanedData != null)
            {
                int countCDS = 0;
                int countOthers = 0;
                for (int i = 0; i < product.listZone.Count; i++)
                {
                    if (product.listZone[i].method.Equals("STC"))
                    {
                        progressBar.Value = 0;
                        progressBar.Visible = true;
                        List<ResultForEvaluation> listSave = Ultility.GetResultForEvaluationByZone(listDataForEvaluation, product.listZone[i].zoneNo);
                        int edgeQty = Ultility.GetEdgeQtyFromCSV(listCleanedData, product.listZone[i].zoneNo);
                        String fileName = @"" + txtSaveAs.Text + @"\" + txtFileName.Text + "_Z" + product.listZone[i].zoneNo;
                        Ultility.ExportSTCZoneEvaluation(progressBar, listSave, product.listZone[i].zoneNo, edgeQty, fileName);
                    }
                    else if (product.listZone[i].method.Equals("CDS"))
                    {
                        if (countCDS == 0)
                        {
                            progressBar.Value = 0;
                            progressBar.Visible = true;
                            List<ResultForEvaluation> listSave = Ultility.GetResultForEvaluationByZone(listDataForEvaluation, product.listZone[i].zoneNo);
                            String fileName = @"" + txtSaveAs.Text + @"\" + txtFileName.Text + "_Z" + product.listZone[i].zoneNo;
                            Ultility.ExportCDSZoneEvaluation(progressBar, listSave, product.listZone[i].zoneNo, fileName);
                            countCDS++;
                        }
                    }
                    else if (product.listZone[i].method.Equals("Others"))
                    {
                        countOthers++;
                    }
                }
                MessageBox.Show("Evaluation file has been exported for " + (product.listZone.Count - countCDS - countOthers) + " zone(s).");
            }
            else
            {
                MessageBox.Show("Please evaluate the file before export.");
            }
        }

        private void btnUploadData_Click(object sender, EventArgs e)
        {
            listData = Ultility.RemoveDoubleQuoteFromRawData(Ultility.GetRawData(txtResultPath.Text));
            product = Ultility.GetProductFromCSV(listData);
            listCleanedData = Ultility.GetCleanData(txtResultPath.Text, product);
            if (listCleanedData.Count != 0)
            {
                List<Device> listDeviceUnknown = Ultility.GetUnknownListDevice(listCleanedData);
                if (listDeviceUnknown.Count > 0)
                {
                    DeviceDialog dialog = new DeviceDialog(listDeviceUnknown);
                    dialog.Show();

                }
                else
                {

                    db.DeleteResultByFullInfo(Ultility.GetKnownListDevice(listCleanedData), product, Ultility.GetVersionFromCSV(listCleanedData));
                    List<ResultForEvaluation> listResultEvaluation = GetListDataForEvaluation(listCleanedData, product);
                    List<ResultForUploading> listUploading = new List<ResultForUploading>();
                    progressBar.Value = 0;
                    progressBar.Visible = true;
                    progressBar.Maximum = listResultEvaluation.Count * 10;
                    int count = 0;
                    foreach (ResultForEvaluation eva in listResultEvaluation)
                    {

                        ResultForUploading result = new ResultForUploading();
                        result.createdDate = eva.createdDate;
                        result.statisticID = eva.statisticID;
                        result.deviceModel = eva.deviceModel;
                        result.info = eva.info;
                        result.method = eva.method;
                        result.productID = eva.productID;
                        result.productName = eva.productName;
                        result.resultText = eva.resultText;
                        result.value = eva.value;
                        result.version = eva.version;
                        result.versionID = eva.versionID;
                        result.zone = eva.zone;
                        count++;
                        progressBar.Value = count * 10;
                        db.SaveEvaluationResult(result);
                    }
                    progressBar.Visible = false;
                    MessageBox.Show("Data has been uploaded to database.");
                }

            }

        }
    }
}
