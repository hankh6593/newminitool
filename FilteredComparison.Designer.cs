﻿namespace MiniTool
{
    partial class FilteredComparison
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.chartStatistic = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cbbData = new System.Windows.Forms.ComboBox();
            this.txtEdge = new System.Windows.Forms.TextBox();
            this.txtZone = new System.Windows.Forms.TextBox();
            this.txtDevice = new System.Windows.Forms.TextBox();
            this.txtProduct = new System.Windows.Forms.TextBox();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.dgvDataStatistic = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.chartStatistic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataStatistic)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(701, 110);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 24);
            this.label14.TabIndex = 38;
            this.label14.Text = "Line:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(561, 110);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 24);
            this.label13.TabIndex = 39;
            this.label13.Text = "Edge:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(425, 110);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 24);
            this.label12.TabIndex = 40;
            this.label12.Text = "Zone:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(285, 110);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 24);
            this.label10.TabIndex = 41;
            this.label10.Text = "Device:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(151, 110);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 24);
            this.label9.TabIndex = 42;
            this.label9.Text = "Product:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 110);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 24);
            this.label8.TabIndex = 43;
            this.label8.Text = "Version:";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnClear.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(958, 141);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(134, 46);
            this.btnClear.TabIndex = 37;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chartStatistic
            // 
            chartArea1.Name = "ChartArea1";
            this.chartStatistic.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartStatistic.Legends.Add(legend1);
            this.chartStatistic.Location = new System.Drawing.Point(3, 26);
            this.chartStatistic.Name = "chartStatistic";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartStatistic.Series.Add(series1);
            this.chartStatistic.Size = new System.Drawing.Size(787, 544);
            this.chartStatistic.TabIndex = 36;
            this.chartStatistic.Text = "chart1";
            // 
            // cbbData
            // 
            this.cbbData.BackColor = System.Drawing.Color.White;
            this.cbbData.Font = new System.Drawing.Font("Arial", 14F);
            this.cbbData.FormattingEnabled = true;
            this.cbbData.Items.AddRange(new object[] {
            "Trigger 1",
            "Trigger 2",
            "Shifting 1",
            "Shifting 2",
            "Cal",
            "E"});
            this.cbbData.Location = new System.Drawing.Point(705, 149);
            this.cbbData.Name = "cbbData";
            this.cbbData.Size = new System.Drawing.Size(205, 30);
            this.cbbData.TabIndex = 35;
            // 
            // txtEdge
            // 
            this.txtEdge.BackColor = System.Drawing.Color.White;
            this.txtEdge.Font = new System.Drawing.Font("Arial", 14F);
            this.txtEdge.Location = new System.Drawing.Point(565, 149);
            this.txtEdge.Name = "txtEdge";
            this.txtEdge.Size = new System.Drawing.Size(100, 29);
            this.txtEdge.TabIndex = 31;
            this.txtEdge.TextChanged += new System.EventHandler(this.txtEdge_TextChanged);
            // 
            // txtZone
            // 
            this.txtZone.BackColor = System.Drawing.Color.White;
            this.txtZone.Font = new System.Drawing.Font("Arial", 14F);
            this.txtZone.Location = new System.Drawing.Point(429, 149);
            this.txtZone.Name = "txtZone";
            this.txtZone.Size = new System.Drawing.Size(100, 29);
            this.txtZone.TabIndex = 32;
            this.txtZone.TextChanged += new System.EventHandler(this.txtZone_TextChanged);
            // 
            // txtDevice
            // 
            this.txtDevice.BackColor = System.Drawing.Color.White;
            this.txtDevice.Font = new System.Drawing.Font("Arial", 14F);
            this.txtDevice.Location = new System.Drawing.Point(289, 149);
            this.txtDevice.Name = "txtDevice";
            this.txtDevice.Size = new System.Drawing.Size(100, 29);
            this.txtDevice.TabIndex = 33;
            this.txtDevice.TextChanged += new System.EventHandler(this.txtDevice_TextChanged);
            // 
            // txtProduct
            // 
            this.txtProduct.BackColor = System.Drawing.Color.White;
            this.txtProduct.Font = new System.Drawing.Font("Arial", 14F);
            this.txtProduct.Location = new System.Drawing.Point(155, 149);
            this.txtProduct.Name = "txtProduct";
            this.txtProduct.Size = new System.Drawing.Size(100, 29);
            this.txtProduct.TabIndex = 34;
            this.txtProduct.TextChanged += new System.EventHandler(this.txtProduct_TextChanged);
            // 
            // txtVersion
            // 
            this.txtVersion.BackColor = System.Drawing.Color.White;
            this.txtVersion.Font = new System.Drawing.Font("Arial", 14F);
            this.txtVersion.Location = new System.Drawing.Point(22, 149);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(100, 29);
            this.txtVersion.TabIndex = 30;
            this.txtVersion.TextChanged += new System.EventHandler(this.txtVersion_TextChanged);
            // 
            // dgvDataStatistic
            // 
            this.dgvDataStatistic.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 14F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDataStatistic.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDataStatistic.Location = new System.Drawing.Point(22, 204);
            this.dgvDataStatistic.MultiSelect = false;
            this.dgvDataStatistic.Name = "dgvDataStatistic";
            this.dgvDataStatistic.ReadOnly = true;
            this.dgvDataStatistic.RowHeadersWidth = 51;
            this.dgvDataStatistic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDataStatistic.Size = new System.Drawing.Size(675, 587);
            this.dgvDataStatistic.TabIndex = 29;
            this.dgvDataStatistic.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDataStatistic_CellClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(12, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(258, 31);
            this.label4.TabIndex = 87;
            this.label4.Text = "Filtered Comparison";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.chartStatistic);
            this.panel1.Location = new System.Drawing.Point(722, 204);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(808, 587);
            this.panel1.TabIndex = 88;
            // 
            // FilteredComparison
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1542, 816);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.cbbData);
            this.Controls.Add(this.txtEdge);
            this.Controls.Add(this.txtZone);
            this.Controls.Add(this.txtDevice);
            this.Controls.Add(this.txtProduct);
            this.Controls.Add(this.txtVersion);
            this.Controls.Add(this.dgvDataStatistic);
            this.Name = "FilteredComparison";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini tool v3.11";
            this.Controls.SetChildIndex(this.dgvDataStatistic, 0);
            this.Controls.SetChildIndex(this.txtVersion, 0);
            this.Controls.SetChildIndex(this.txtProduct, 0);
            this.Controls.SetChildIndex(this.txtDevice, 0);
            this.Controls.SetChildIndex(this.txtZone, 0);
            this.Controls.SetChildIndex(this.txtEdge, 0);
            this.Controls.SetChildIndex(this.cbbData, 0);
            this.Controls.SetChildIndex(this.btnClear, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.label13, 0);
            this.Controls.SetChildIndex(this.label14, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.chartStatistic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataStatistic)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartStatistic;
        private System.Windows.Forms.ComboBox cbbData;
        private System.Windows.Forms.TextBox txtEdge;
        private System.Windows.Forms.TextBox txtZone;
        private System.Windows.Forms.TextBox txtDevice;
        private System.Windows.Forms.TextBox txtProduct;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.DataGridView dgvDataStatistic;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
    }
}