﻿namespace MiniTool
{
    partial class VersionComparison
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea8 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea11 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend11 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea12 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend12 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend13 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend14 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea15 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend15 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea16 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend16 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea17 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend17 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea18 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend18 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea19 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend19 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea20 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend20 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series20 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea21 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend21 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series21 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea22 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend22 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series22 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Chart11B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart10B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart9B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart8B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart7B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart6B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart5B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart4B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart3B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart2B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart1B = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cbbZoneCompare = new System.Windows.Forms.ComboBox();
            this.cbbEdgeCompare = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Chart11A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart10A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart9A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart8A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart7A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart6A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart5A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart4A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart3A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart2A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Chart1A = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cbbDeviceCompare = new System.Windows.Forms.ComboBox();
            this.cbbProductCompare = new System.Windows.Forms.ComboBox();
            this.cbbVersionB = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbbVersionA = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Chart11B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart10B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart9B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart8B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart7B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart6B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart5B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart4B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart3B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart2B)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1B)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Chart11A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart10A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart9A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart8A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart7A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart6A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart5A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart4A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart3A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart2A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1A)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnBack.Font = new System.Drawing.Font("Arial", 14F);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(682, 286);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(88, 33);
            this.btnBack.TabIndex = 40;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnNext.Font = new System.Drawing.Font("Arial", 14F);
            this.btnNext.ForeColor = System.Drawing.Color.White;
            this.btnNext.Location = new System.Drawing.Point(778, 286);
            this.btnNext.Margin = new System.Windows.Forms.Padding(2);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(85, 33);
            this.btnNext.TabIndex = 39;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.AutoScrollMinSize = new System.Drawing.Size(0, 484);
            this.panel2.Controls.Add(this.Chart11B);
            this.panel2.Controls.Add(this.Chart10B);
            this.panel2.Controls.Add(this.Chart9B);
            this.panel2.Controls.Add(this.Chart8B);
            this.panel2.Controls.Add(this.Chart7B);
            this.panel2.Controls.Add(this.Chart6B);
            this.panel2.Controls.Add(this.Chart5B);
            this.panel2.Controls.Add(this.Chart4B);
            this.panel2.Controls.Add(this.Chart3B);
            this.panel2.Controls.Add(this.Chart2B);
            this.panel2.Controls.Add(this.Chart1B);
            this.panel2.Location = new System.Drawing.Point(880, 182);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(652, 498);
            this.panel2.TabIndex = 38;
            // 
            // Chart11B
            // 
            chartArea1.Name = "ChartArea1";
            this.Chart11B.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.Chart11B.Legends.Add(legend1);
            this.Chart11B.Location = new System.Drawing.Point(11, 4005);
            this.Chart11B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart11B.Name = "Chart11B";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.Chart11B.Series.Add(series1);
            this.Chart11B.Size = new System.Drawing.Size(623, 393);
            this.Chart11B.TabIndex = 10;
            this.Chart11B.Text = "chart3";
            // 
            // Chart10B
            // 
            chartArea2.Name = "ChartArea1";
            this.Chart10B.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.Chart10B.Legends.Add(legend2);
            this.Chart10B.Location = new System.Drawing.Point(11, 3607);
            this.Chart10B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart10B.Name = "Chart10B";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.Chart10B.Series.Add(series2);
            this.Chart10B.Size = new System.Drawing.Size(623, 393);
            this.Chart10B.TabIndex = 9;
            this.Chart10B.Text = "chart3";
            // 
            // Chart9B
            // 
            chartArea3.Name = "ChartArea1";
            this.Chart9B.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.Chart9B.Legends.Add(legend3);
            this.Chart9B.Location = new System.Drawing.Point(11, 3209);
            this.Chart9B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart9B.Name = "Chart9B";
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.Chart9B.Series.Add(series3);
            this.Chart9B.Size = new System.Drawing.Size(623, 393);
            this.Chart9B.TabIndex = 8;
            this.Chart9B.Text = "chart3";
            // 
            // Chart8B
            // 
            chartArea4.Name = "ChartArea1";
            this.Chart8B.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.Chart8B.Legends.Add(legend4);
            this.Chart8B.Location = new System.Drawing.Point(11, 2810);
            this.Chart8B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart8B.Name = "Chart8B";
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.Chart8B.Series.Add(series4);
            this.Chart8B.Size = new System.Drawing.Size(623, 393);
            this.Chart8B.TabIndex = 7;
            this.Chart8B.Text = "chart1";
            // 
            // Chart7B
            // 
            chartArea5.Name = "ChartArea1";
            this.Chart7B.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.Chart7B.Legends.Add(legend5);
            this.Chart7B.Location = new System.Drawing.Point(11, 2412);
            this.Chart7B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart7B.Name = "Chart7B";
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.Chart7B.Series.Add(series5);
            this.Chart7B.Size = new System.Drawing.Size(623, 393);
            this.Chart7B.TabIndex = 6;
            this.Chart7B.Text = "chart1";
            // 
            // Chart6B
            // 
            chartArea6.Name = "ChartArea1";
            this.Chart6B.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.Chart6B.Legends.Add(legend6);
            this.Chart6B.Location = new System.Drawing.Point(11, 2014);
            this.Chart6B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart6B.Name = "Chart6B";
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.Chart6B.Series.Add(series6);
            this.Chart6B.Size = new System.Drawing.Size(623, 393);
            this.Chart6B.TabIndex = 5;
            this.Chart6B.Text = "chart1";
            // 
            // Chart5B
            // 
            chartArea7.Name = "ChartArea1";
            this.Chart5B.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.Chart5B.Legends.Add(legend7);
            this.Chart5B.Location = new System.Drawing.Point(14, 1616);
            this.Chart5B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart5B.Name = "Chart5B";
            series7.ChartArea = "ChartArea1";
            series7.Legend = "Legend1";
            series7.Name = "Series1";
            this.Chart5B.Series.Add(series7);
            this.Chart5B.Size = new System.Drawing.Size(623, 393);
            this.Chart5B.TabIndex = 4;
            this.Chart5B.Text = "chart1";
            // 
            // Chart4B
            // 
            chartArea8.Name = "ChartArea1";
            this.Chart4B.ChartAreas.Add(chartArea8);
            legend8.Name = "Legend1";
            this.Chart4B.Legends.Add(legend8);
            this.Chart4B.Location = new System.Drawing.Point(14, 1218);
            this.Chart4B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart4B.Name = "Chart4B";
            series8.ChartArea = "ChartArea1";
            series8.Legend = "Legend1";
            series8.Name = "Series1";
            this.Chart4B.Series.Add(series8);
            this.Chart4B.Size = new System.Drawing.Size(623, 393);
            this.Chart4B.TabIndex = 3;
            this.Chart4B.Text = "chart1";
            // 
            // Chart3B
            // 
            chartArea9.Name = "ChartArea1";
            this.Chart3B.ChartAreas.Add(chartArea9);
            legend9.Name = "Legend1";
            this.Chart3B.Legends.Add(legend9);
            this.Chart3B.Location = new System.Drawing.Point(14, 820);
            this.Chart3B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart3B.Name = "Chart3B";
            series9.ChartArea = "ChartArea1";
            series9.Legend = "Legend1";
            series9.Name = "Series1";
            this.Chart3B.Series.Add(series9);
            this.Chart3B.Size = new System.Drawing.Size(623, 393);
            this.Chart3B.TabIndex = 2;
            this.Chart3B.Text = "chart1";
            // 
            // Chart2B
            // 
            chartArea10.Name = "ChartArea1";
            this.Chart2B.ChartAreas.Add(chartArea10);
            legend10.Name = "Legend1";
            this.Chart2B.Legends.Add(legend10);
            this.Chart2B.Location = new System.Drawing.Point(14, 414);
            this.Chart2B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart2B.Name = "Chart2B";
            series10.ChartArea = "ChartArea1";
            series10.Legend = "Legend1";
            series10.Name = "Series1";
            this.Chart2B.Series.Add(series10);
            this.Chart2B.Size = new System.Drawing.Size(623, 393);
            this.Chart2B.TabIndex = 1;
            this.Chart2B.Text = "chart1";
            // 
            // Chart1B
            // 
            chartArea11.Name = "ChartArea1";
            this.Chart1B.ChartAreas.Add(chartArea11);
            legend11.Name = "Legend1";
            this.Chart1B.Legends.Add(legend11);
            this.Chart1B.Location = new System.Drawing.Point(14, 15);
            this.Chart1B.Margin = new System.Windows.Forms.Padding(2);
            this.Chart1B.Name = "Chart1B";
            series11.ChartArea = "ChartArea1";
            series11.Legend = "Legend1";
            series11.Name = "Series1";
            this.Chart1B.Series.Add(series11);
            this.Chart1B.Size = new System.Drawing.Size(623, 393);
            this.Chart1B.TabIndex = 0;
            this.Chart1B.Text = "chart1";
            // 
            // cbbZoneCompare
            // 
            this.cbbZoneCompare.Font = new System.Drawing.Font("Arial", 14F);
            this.cbbZoneCompare.FormattingEnabled = true;
            this.cbbZoneCompare.Location = new System.Drawing.Point(682, 182);
            this.cbbZoneCompare.Margin = new System.Windows.Forms.Padding(2);
            this.cbbZoneCompare.Name = "cbbZoneCompare";
            this.cbbZoneCompare.Size = new System.Drawing.Size(177, 30);
            this.cbbZoneCompare.TabIndex = 36;
            this.cbbZoneCompare.SelectedIndexChanged += new System.EventHandler(this.cbbZoneCompare_SelectedIndexChanged);
            // 
            // cbbEdgeCompare
            // 
            this.cbbEdgeCompare.Font = new System.Drawing.Font("Arial", 14F);
            this.cbbEdgeCompare.FormattingEnabled = true;
            this.cbbEdgeCompare.Location = new System.Drawing.Point(682, 233);
            this.cbbEdgeCompare.Margin = new System.Windows.Forms.Padding(2);
            this.cbbEdgeCompare.Name = "cbbEdgeCompare";
            this.cbbEdgeCompare.Size = new System.Drawing.Size(177, 30);
            this.cbbEdgeCompare.TabIndex = 37;
            this.cbbEdgeCompare.SelectedIndexChanged += new System.EventHandler(this.cbbEdgeCompare_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoScrollMinSize = new System.Drawing.Size(0, 484);
            this.panel1.Controls.Add(this.Chart11A);
            this.panel1.Controls.Add(this.Chart10A);
            this.panel1.Controls.Add(this.Chart9A);
            this.panel1.Controls.Add(this.Chart8A);
            this.panel1.Controls.Add(this.Chart7A);
            this.panel1.Controls.Add(this.Chart6A);
            this.panel1.Controls.Add(this.Chart5A);
            this.panel1.Controls.Add(this.Chart4A);
            this.panel1.Controls.Add(this.Chart3A);
            this.panel1.Controls.Add(this.Chart2A);
            this.panel1.Controls.Add(this.Chart1A);
            this.panel1.Location = new System.Drawing.Point(15, 182);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(652, 498);
            this.panel1.TabIndex = 35;
            // 
            // Chart11A
            // 
            chartArea12.Name = "ChartArea1";
            this.Chart11A.ChartAreas.Add(chartArea12);
            legend12.Name = "Legend1";
            this.Chart11A.Legends.Add(legend12);
            this.Chart11A.Location = new System.Drawing.Point(2, 3997);
            this.Chart11A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart11A.Name = "Chart11A";
            series12.ChartArea = "ChartArea1";
            series12.Legend = "Legend1";
            series12.Name = "Series1";
            this.Chart11A.Series.Add(series12);
            this.Chart11A.Size = new System.Drawing.Size(623, 393);
            this.Chart11A.TabIndex = 10;
            this.Chart11A.Text = "chart3";
            // 
            // Chart10A
            // 
            chartArea13.Name = "ChartArea1";
            this.Chart10A.ChartAreas.Add(chartArea13);
            legend13.Name = "Legend1";
            this.Chart10A.Legends.Add(legend13);
            this.Chart10A.Location = new System.Drawing.Point(11, 3599);
            this.Chart10A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart10A.Name = "Chart10A";
            series13.ChartArea = "ChartArea1";
            series13.Legend = "Legend1";
            series13.Name = "Series1";
            this.Chart10A.Series.Add(series13);
            this.Chart10A.Size = new System.Drawing.Size(623, 393);
            this.Chart10A.TabIndex = 9;
            this.Chart10A.Text = "chart3";
            // 
            // Chart9A
            // 
            chartArea14.Name = "ChartArea1";
            this.Chart9A.ChartAreas.Add(chartArea14);
            legend14.Name = "Legend1";
            this.Chart9A.Legends.Add(legend14);
            this.Chart9A.Location = new System.Drawing.Point(11, 3200);
            this.Chart9A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart9A.Name = "Chart9A";
            series14.ChartArea = "ChartArea1";
            series14.Legend = "Legend1";
            series14.Name = "Series1";
            this.Chart9A.Series.Add(series14);
            this.Chart9A.Size = new System.Drawing.Size(623, 393);
            this.Chart9A.TabIndex = 8;
            this.Chart9A.Text = "chart3";
            // 
            // Chart8A
            // 
            chartArea15.Name = "ChartArea1";
            this.Chart8A.ChartAreas.Add(chartArea15);
            legend15.Name = "Legend1";
            this.Chart8A.Legends.Add(legend15);
            this.Chart8A.Location = new System.Drawing.Point(11, 2802);
            this.Chart8A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart8A.Name = "Chart8A";
            series15.ChartArea = "ChartArea1";
            series15.Legend = "Legend1";
            series15.Name = "Series1";
            this.Chart8A.Series.Add(series15);
            this.Chart8A.Size = new System.Drawing.Size(623, 393);
            this.Chart8A.TabIndex = 7;
            this.Chart8A.Text = "chart1";
            // 
            // Chart7A
            // 
            chartArea16.Name = "ChartArea1";
            this.Chart7A.ChartAreas.Add(chartArea16);
            legend16.Name = "Legend1";
            this.Chart7A.Legends.Add(legend16);
            this.Chart7A.Location = new System.Drawing.Point(11, 2404);
            this.Chart7A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart7A.Name = "Chart7A";
            series16.ChartArea = "ChartArea1";
            series16.Legend = "Legend1";
            series16.Name = "Series1";
            this.Chart7A.Series.Add(series16);
            this.Chart7A.Size = new System.Drawing.Size(623, 393);
            this.Chart7A.TabIndex = 6;
            this.Chart7A.Text = "chart1";
            // 
            // Chart6A
            // 
            chartArea17.Name = "ChartArea1";
            this.Chart6A.ChartAreas.Add(chartArea17);
            legend17.Name = "Legend1";
            this.Chart6A.Legends.Add(legend17);
            this.Chart6A.Location = new System.Drawing.Point(11, 2006);
            this.Chart6A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart6A.Name = "Chart6A";
            series17.ChartArea = "ChartArea1";
            series17.Legend = "Legend1";
            series17.Name = "Series1";
            this.Chart6A.Series.Add(series17);
            this.Chart6A.Size = new System.Drawing.Size(623, 393);
            this.Chart6A.TabIndex = 5;
            this.Chart6A.Text = "chart1";
            // 
            // Chart5A
            // 
            chartArea18.Name = "ChartArea1";
            this.Chart5A.ChartAreas.Add(chartArea18);
            legend18.Name = "Legend1";
            this.Chart5A.Legends.Add(legend18);
            this.Chart5A.Location = new System.Drawing.Point(11, 1608);
            this.Chart5A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart5A.Name = "Chart5A";
            series18.ChartArea = "ChartArea1";
            series18.Legend = "Legend1";
            series18.Name = "Series1";
            this.Chart5A.Series.Add(series18);
            this.Chart5A.Size = new System.Drawing.Size(623, 393);
            this.Chart5A.TabIndex = 4;
            this.Chart5A.Text = "chart1";
            // 
            // Chart4A
            // 
            chartArea19.Name = "ChartArea1";
            this.Chart4A.ChartAreas.Add(chartArea19);
            legend19.Name = "Legend1";
            this.Chart4A.Legends.Add(legend19);
            this.Chart4A.Location = new System.Drawing.Point(14, 1210);
            this.Chart4A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart4A.Name = "Chart4A";
            series19.ChartArea = "ChartArea1";
            series19.Legend = "Legend1";
            series19.Name = "Series1";
            this.Chart4A.Series.Add(series19);
            this.Chart4A.Size = new System.Drawing.Size(623, 393);
            this.Chart4A.TabIndex = 3;
            this.Chart4A.Text = "chart1";
            // 
            // Chart3A
            // 
            chartArea20.Name = "ChartArea1";
            this.Chart3A.ChartAreas.Add(chartArea20);
            legend20.Name = "Legend1";
            this.Chart3A.Legends.Add(legend20);
            this.Chart3A.Location = new System.Drawing.Point(14, 812);
            this.Chart3A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart3A.Name = "Chart3A";
            series20.ChartArea = "ChartArea1";
            series20.Legend = "Legend1";
            series20.Name = "Series1";
            this.Chart3A.Series.Add(series20);
            this.Chart3A.Size = new System.Drawing.Size(623, 393);
            this.Chart3A.TabIndex = 2;
            this.Chart3A.Text = "chart1";
            // 
            // Chart2A
            // 
            chartArea21.Name = "ChartArea1";
            this.Chart2A.ChartAreas.Add(chartArea21);
            legend21.Name = "Legend1";
            this.Chart2A.Legends.Add(legend21);
            this.Chart2A.Location = new System.Drawing.Point(14, 414);
            this.Chart2A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart2A.Name = "Chart2A";
            series21.ChartArea = "ChartArea1";
            series21.Legend = "Legend1";
            series21.Name = "Series1";
            this.Chart2A.Series.Add(series21);
            this.Chart2A.Size = new System.Drawing.Size(623, 393);
            this.Chart2A.TabIndex = 1;
            this.Chart2A.Text = "chart1";
            // 
            // Chart1A
            // 
            chartArea22.Name = "ChartArea1";
            this.Chart1A.ChartAreas.Add(chartArea22);
            legend22.Name = "Legend1";
            this.Chart1A.Legends.Add(legend22);
            this.Chart1A.Location = new System.Drawing.Point(14, 15);
            this.Chart1A.Margin = new System.Windows.Forms.Padding(2);
            this.Chart1A.Name = "Chart1A";
            series22.ChartArea = "ChartArea1";
            series22.Legend = "Legend1";
            series22.Name = "Series1";
            this.Chart1A.Series.Add(series22);
            this.Chart1A.Size = new System.Drawing.Size(623, 393);
            this.Chart1A.TabIndex = 0;
            this.Chart1A.Text = "chart1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 14F);
            this.label17.Location = new System.Drawing.Point(1145, 122);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 22);
            this.label17.TabIndex = 32;
            this.label17.Text = "Device:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 14F);
            this.label16.Location = new System.Drawing.Point(678, 120);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 22);
            this.label16.TabIndex = 33;
            this.label16.Text = "Product:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 14F);
            this.label15.Location = new System.Drawing.Point(343, 120);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 22);
            this.label15.TabIndex = 34;
            this.label15.Text = "Version:";
            // 
            // cbbDeviceCompare
            // 
            this.cbbDeviceCompare.Font = new System.Drawing.Font("Arial", 14F);
            this.cbbDeviceCompare.FormattingEnabled = true;
            this.cbbDeviceCompare.Location = new System.Drawing.Point(1244, 120);
            this.cbbDeviceCompare.Margin = new System.Windows.Forms.Padding(2);
            this.cbbDeviceCompare.Name = "cbbDeviceCompare";
            this.cbbDeviceCompare.Size = new System.Drawing.Size(290, 30);
            this.cbbDeviceCompare.TabIndex = 29;
            this.cbbDeviceCompare.SelectedIndexChanged += new System.EventHandler(this.cbbDeviceCompare_SelectedIndexChanged);
            // 
            // cbbProductCompare
            // 
            this.cbbProductCompare.Font = new System.Drawing.Font("Arial", 14F);
            this.cbbProductCompare.FormattingEnabled = true;
            this.cbbProductCompare.Location = new System.Drawing.Point(778, 117);
            this.cbbProductCompare.Margin = new System.Windows.Forms.Padding(2);
            this.cbbProductCompare.Name = "cbbProductCompare";
            this.cbbProductCompare.Size = new System.Drawing.Size(306, 30);
            this.cbbProductCompare.TabIndex = 30;
            this.cbbProductCompare.SelectedIndexChanged += new System.EventHandler(this.cbbProductCompare_SelectedIndexChanged);
            // 
            // cbbVersionB
            // 
            this.cbbVersionB.Font = new System.Drawing.Font("Arial", 14F);
            this.cbbVersionB.FormattingEnabled = true;
            this.cbbVersionB.Location = new System.Drawing.Point(442, 117);
            this.cbbVersionB.Margin = new System.Windows.Forms.Padding(2);
            this.cbbVersionB.Name = "cbbVersionB";
            this.cbbVersionB.Size = new System.Drawing.Size(187, 30);
            this.cbbVersionB.TabIndex = 31;
            this.cbbVersionB.SelectedIndexChanged += new System.EventHandler(this.cbbVersionB_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 14F);
            this.label11.Location = new System.Drawing.Point(11, 120);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(79, 22);
            this.label11.TabIndex = 28;
            this.label11.Text = "Version:";
            // 
            // cbbVersionA
            // 
            this.cbbVersionA.Font = new System.Drawing.Font("Arial", 14F);
            this.cbbVersionA.FormattingEnabled = true;
            this.cbbVersionA.Location = new System.Drawing.Point(112, 117);
            this.cbbVersionA.Margin = new System.Windows.Forms.Padding(2);
            this.cbbVersionA.Name = "cbbVersionA";
            this.cbbVersionA.Size = new System.Drawing.Size(187, 30);
            this.cbbVersionA.TabIndex = 27;
            this.cbbVersionA.SelectedIndexChanged += new System.EventHandler(this.cbbVersionA_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(12, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(259, 31);
            this.label4.TabIndex = 57;
            this.label4.Text = "Version Comparison";
            // 
            // VersionComparison
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1562, 751);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.cbbZoneCompare);
            this.Controls.Add(this.cbbEdgeCompare);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cbbDeviceCompare);
            this.Controls.Add(this.cbbProductCompare);
            this.Controls.Add(this.cbbVersionB);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbbVersionA);
            this.Name = "VersionComparison";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini tool v3.11";
            this.Controls.SetChildIndex(this.cbbVersionA, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.cbbVersionB, 0);
            this.Controls.SetChildIndex(this.cbbProductCompare, 0);
            this.Controls.SetChildIndex(this.cbbDeviceCompare, 0);
            this.Controls.SetChildIndex(this.label15, 0);
            this.Controls.SetChildIndex(this.label16, 0);
            this.Controls.SetChildIndex(this.label17, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.cbbEdgeCompare, 0);
            this.Controls.SetChildIndex(this.cbbZoneCompare, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.btnNext, 0);
            this.Controls.SetChildIndex(this.btnBack, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Chart11B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart10B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart9B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart8B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart7B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart6B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart5B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart4B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart3B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart2B)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1B)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Chart11A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart10A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart9A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart8A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart7A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart6A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart5A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart4A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart3A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart2A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Chart1A)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart11B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart10B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart9B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart8B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart7B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart6B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart5B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart4B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart3B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart2B;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart1B;
        private System.Windows.Forms.ComboBox cbbZoneCompare;
        private System.Windows.Forms.ComboBox cbbEdgeCompare;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart11A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart10A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart9A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart8A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart7A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart6A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart5A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart4A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart3A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart2A;
        private System.Windows.Forms.DataVisualization.Charting.Chart Chart1A;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbbDeviceCompare;
        private System.Windows.Forms.ComboBox cbbProductCompare;
        private System.Windows.Forms.ComboBox cbbVersionB;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbbVersionA;
        private System.Windows.Forms.Label label4;
    }
}