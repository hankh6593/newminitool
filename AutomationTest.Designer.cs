﻿namespace MiniTool
{
    partial class AutomationTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbFlash = new System.Windows.Forms.CheckBox();
            this.cbImageSet = new System.Windows.Forms.CheckBox();
            this.cbManual = new System.Windows.Forms.CheckBox();
            this.txtAppiumPort = new System.Windows.Forms.TextBox();
            this.txtPlatformVersion = new System.Windows.Forms.TextBox();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.txtAppiumHost = new System.Windows.Forms.TextBox();
            this.txtPresetName = new System.Windows.Forms.TextBox();
            this.txtAppActivity = new System.Windows.Forms.TextBox();
            this.txtAppPackage = new System.Windows.Forms.TextBox();
            this.txtPlatformName = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.lbPath = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbDevice = new System.Windows.Forms.ComboBox();
            this.cbbProduct = new System.Windows.Forms.ComboBox();
            this.cbbPreset = new System.Windows.Forms.ComboBox();
            this.btnAddPreset = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbFlash
            // 
            this.cbFlash.AutoSize = true;
            this.cbFlash.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFlash.Location = new System.Drawing.Point(1280, 489);
            this.cbFlash.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFlash.Name = "cbFlash";
            this.cbFlash.Size = new System.Drawing.Size(106, 33);
            this.cbFlash.TabIndex = 156;
            this.cbFlash.Text = "Flash?";
            this.cbFlash.UseVisualStyleBackColor = true;
            // 
            // cbImageSet
            // 
            this.cbImageSet.AutoSize = true;
            this.cbImageSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbImageSet.Location = new System.Drawing.Point(857, 490);
            this.cbImageSet.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbImageSet.Name = "cbImageSet";
            this.cbImageSet.Size = new System.Drawing.Size(156, 33);
            this.cbImageSet.TabIndex = 155;
            this.cbImageSet.Text = "Image Set?";
            this.cbImageSet.UseVisualStyleBackColor = true;
            // 
            // cbManual
            // 
            this.cbManual.AutoSize = true;
            this.cbManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbManual.Location = new System.Drawing.Point(1060, 489);
            this.cbManual.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbManual.Name = "cbManual";
            this.cbManual.Size = new System.Drawing.Size(125, 33);
            this.cbManual.TabIndex = 154;
            this.cbManual.Text = "Manual?";
            this.cbManual.UseVisualStyleBackColor = true;
            // 
            // txtAppiumPort
            // 
            this.txtAppiumPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAppiumPort.Location = new System.Drawing.Point(1077, 687);
            this.txtAppiumPort.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAppiumPort.Name = "txtAppiumPort";
            this.txtAppiumPort.Size = new System.Drawing.Size(367, 34);
            this.txtAppiumPort.TabIndex = 153;
            this.txtAppiumPort.Text = "4723";
            // 
            // txtPlatformVersion
            // 
            this.txtPlatformVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlatformVersion.Location = new System.Drawing.Point(1077, 415);
            this.txtPlatformVersion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPlatformVersion.Name = "txtPlatformVersion";
            this.txtPlatformVersion.Size = new System.Drawing.Size(367, 34);
            this.txtPlatformVersion.TabIndex = 152;
            // 
            // txtQuantity
            // 
            this.txtQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantity.Location = new System.Drawing.Point(255, 486);
            this.txtQuantity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(556, 34);
            this.txtQuantity.TabIndex = 151;
            // 
            // txtAppiumHost
            // 
            this.txtAppiumHost.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAppiumHost.Location = new System.Drawing.Point(255, 690);
            this.txtAppiumHost.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAppiumHost.Name = "txtAppiumHost";
            this.txtAppiumHost.Size = new System.Drawing.Size(556, 34);
            this.txtAppiumHost.TabIndex = 150;
            this.txtAppiumHost.Text = "127.0.0.1";
            // 
            // txtPresetName
            // 
            this.txtPresetName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPresetName.Location = new System.Drawing.Point(255, 212);
            this.txtPresetName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPresetName.Name = "txtPresetName";
            this.txtPresetName.Size = new System.Drawing.Size(1191, 34);
            this.txtPresetName.TabIndex = 149;
            // 
            // txtAppActivity
            // 
            this.txtAppActivity.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAppActivity.Location = new System.Drawing.Point(255, 623);
            this.txtAppActivity.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAppActivity.Name = "txtAppActivity";
            this.txtAppActivity.Size = new System.Drawing.Size(1191, 34);
            this.txtAppActivity.TabIndex = 148;
            this.txtAppActivity.Text = "com.anteleon.scrypto.controller.StartActivity";
            // 
            // txtAppPackage
            // 
            this.txtAppPackage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAppPackage.Location = new System.Drawing.Point(255, 554);
            this.txtAppPackage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAppPackage.Name = "txtAppPackage";
            this.txtAppPackage.Size = new System.Drawing.Size(1191, 34);
            this.txtAppPackage.TabIndex = 147;
            this.txtAppPackage.Text = "scryptoTRACE.codepub";
            // 
            // txtPlatformName
            // 
            this.txtPlatformName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlatformName.Location = new System.Drawing.Point(255, 420);
            this.txtPlatformName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPlatformName.Name = "txtPlatformName";
            this.txtPlatformName.Size = new System.Drawing.Size(556, 34);
            this.txtPlatformName.TabIndex = 146;
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.White;
            this.btnStart.Location = new System.Drawing.Point(255, 764);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(204, 57);
            this.btnStart.TabIndex = 144;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(13, 218);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(160, 29);
            this.label12.TabIndex = 126;
            this.label12.Text = "Preset Name:";
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPath.Location = new System.Drawing.Point(12, 154);
            this.lbPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(89, 29);
            this.lbPath.TabIndex = 125;
            this.lbPath.Text = "Preset:";
            this.lbPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(852, 692);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(151, 29);
            this.label11.TabIndex = 142;
            this.label11.Text = "Appium Port:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(852, 420);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(196, 29);
            this.label5.TabIndex = 141;
            this.label5.Text = "Platform Version:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 690);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 29);
            this.label10.TabIndex = 134;
            this.label10.Text = "Appium Host:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 486);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 29);
            this.label7.TabIndex = 135;
            this.label7.Text = "Quantity:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 623);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 29);
            this.label9.TabIndex = 140;
            this.label9.Text = "App Activity:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 554);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(162, 29);
            this.label8.TabIndex = 137;
            this.label8.Text = "App Package:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 420);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 29);
            this.label2.TabIndex = 136;
            this.label2.Text = "Platform Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 354);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 29);
            this.label1.TabIndex = 138;
            this.label1.Text = "Device:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 282);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 29);
            this.label6.TabIndex = 139;
            this.label6.Text = "Product:";
            // 
            // cbbDevice
            // 
            this.cbbDevice.BackColor = System.Drawing.Color.White;
            this.cbbDevice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbDevice.FormattingEnabled = true;
            this.cbbDevice.Location = new System.Drawing.Point(255, 346);
            this.cbbDevice.Margin = new System.Windows.Forms.Padding(4);
            this.cbbDevice.Name = "cbbDevice";
            this.cbbDevice.Size = new System.Drawing.Size(1191, 37);
            this.cbbDevice.TabIndex = 133;
            this.cbbDevice.SelectedIndexChanged += new System.EventHandler(this.cbbDevice_SelectedIndexChanged);
            // 
            // cbbProduct
            // 
            this.cbbProduct.BackColor = System.Drawing.Color.White;
            this.cbbProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbProduct.FormattingEnabled = true;
            this.cbbProduct.Location = new System.Drawing.Point(255, 274);
            this.cbbProduct.Margin = new System.Windows.Forms.Padding(4);
            this.cbbProduct.Name = "cbbProduct";
            this.cbbProduct.Size = new System.Drawing.Size(1191, 37);
            this.cbbProduct.TabIndex = 132;
            // 
            // cbbPreset
            // 
            this.cbbPreset.BackColor = System.Drawing.Color.White;
            this.cbbPreset.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbPreset.FormattingEnabled = true;
            this.cbbPreset.Location = new System.Drawing.Point(253, 144);
            this.cbbPreset.Margin = new System.Windows.Forms.Padding(4);
            this.cbbPreset.Name = "cbbPreset";
            this.cbbPreset.Size = new System.Drawing.Size(1191, 37);
            this.cbbPreset.TabIndex = 131;
            this.cbbPreset.SelectedIndexChanged += new System.EventHandler(this.cbbPreset_SelectedIndexChanged);
            this.cbbPreset.TextChanged += new System.EventHandler(this.cbbPreset_TextChanged);
            // 
            // btnAddPreset
            // 
            this.btnAddPreset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnAddPreset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPreset.ForeColor = System.Drawing.Color.White;
            this.btnAddPreset.Location = new System.Drawing.Point(479, 764);
            this.btnAddPreset.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddPreset.Name = "btnAddPreset";
            this.btnAddPreset.Size = new System.Drawing.Size(195, 57);
            this.btnAddPreset.TabIndex = 129;
            this.btnAddPreset.Text = "Add Preset";
            this.btnAddPreset.UseVisualStyleBackColor = false;
            this.btnAddPreset.Click += new System.EventHandler(this.btnAddPreset_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(692, 764);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(195, 57);
            this.btnUpdate.TabIndex = 128;
            this.btnUpdate.Text = "Update Preset";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.ForeColor = System.Drawing.Color.White;
            this.btnRemove.Location = new System.Drawing.Point(903, 764);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(4);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(240, 57);
            this.btnRemove.TabIndex = 130;
            this.btnRemove.Text = "Remove Preset";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(1160, 764);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(171, 57);
            this.btnClear.TabIndex = 127;
            this.btnClear.Text = "Clear All";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(16, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(264, 39);
            this.label4.TabIndex = 157;
            this.label4.Text = "Automation Test";
            // 
            // AutomationTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1505, 846);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbFlash);
            this.Controls.Add(this.cbImageSet);
            this.Controls.Add(this.cbManual);
            this.Controls.Add(this.txtAppiumPort);
            this.Controls.Add(this.txtPlatformVersion);
            this.Controls.Add(this.txtQuantity);
            this.Controls.Add(this.txtAppiumHost);
            this.Controls.Add(this.txtPresetName);
            this.Controls.Add(this.txtAppActivity);
            this.Controls.Add(this.txtAppPackage);
            this.Controls.Add(this.txtPlatformName);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lbPath);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbbDevice);
            this.Controls.Add(this.cbbProduct);
            this.Controls.Add(this.cbbPreset);
            this.Controls.Add(this.btnAddPreset);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnClear);
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "AutomationTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini tool v3.11";
            this.Controls.SetChildIndex(this.btnClear, 0);
            this.Controls.SetChildIndex(this.btnRemove, 0);
            this.Controls.SetChildIndex(this.btnUpdate, 0);
            this.Controls.SetChildIndex(this.btnAddPreset, 0);
            this.Controls.SetChildIndex(this.cbbPreset, 0);
            this.Controls.SetChildIndex(this.cbbProduct, 0);
            this.Controls.SetChildIndex(this.cbbDevice, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.lbPath, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.btnStart, 0);
            this.Controls.SetChildIndex(this.txtPlatformName, 0);
            this.Controls.SetChildIndex(this.txtAppPackage, 0);
            this.Controls.SetChildIndex(this.txtAppActivity, 0);
            this.Controls.SetChildIndex(this.txtPresetName, 0);
            this.Controls.SetChildIndex(this.txtAppiumHost, 0);
            this.Controls.SetChildIndex(this.txtQuantity, 0);
            this.Controls.SetChildIndex(this.txtPlatformVersion, 0);
            this.Controls.SetChildIndex(this.txtAppiumPort, 0);
            this.Controls.SetChildIndex(this.cbManual, 0);
            this.Controls.SetChildIndex(this.cbImageSet, 0);
            this.Controls.SetChildIndex(this.cbFlash, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbFlash;
        private System.Windows.Forms.CheckBox cbImageSet;
        private System.Windows.Forms.CheckBox cbManual;
        private System.Windows.Forms.TextBox txtAppiumPort;
        private System.Windows.Forms.TextBox txtPlatformVersion;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.TextBox txtAppiumHost;
        private System.Windows.Forms.TextBox txtPresetName;
        private System.Windows.Forms.TextBox txtAppActivity;
        private System.Windows.Forms.TextBox txtAppPackage;
        private System.Windows.Forms.TextBox txtPlatformName;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbbDevice;
        private System.Windows.Forms.ComboBox cbbProduct;
        private System.Windows.Forms.ComboBox cbbPreset;
        private System.Windows.Forms.Button btnAddPreset;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label4;
    }
}