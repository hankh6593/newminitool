﻿namespace MiniTool
{
    partial class FileMerging
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBrowseRaw = new System.Windows.Forms.Button();
            this.txtRawResult = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtResultFolder = new System.Windows.Forms.TextBox();
            this.lbPath = new System.Windows.Forms.Label();
            this.btnResultBrowse = new System.Windows.Forms.Button();
            this.btnMergeData = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.FolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // btnBrowseRaw
            // 
            this.btnBrowseRaw.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnBrowseRaw.FlatAppearance.BorderSize = 0;
            this.btnBrowseRaw.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowseRaw.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnBrowseRaw.Location = new System.Drawing.Point(1025, 143);
            this.btnBrowseRaw.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrowseRaw.Name = "btnBrowseRaw";
            this.btnBrowseRaw.Size = new System.Drawing.Size(56, 46);
            this.btnBrowseRaw.TabIndex = 84;
            this.btnBrowseRaw.Text = "...";
            this.btnBrowseRaw.UseVisualStyleBackColor = false;
            this.btnBrowseRaw.Click += new System.EventHandler(this.btnBrowseRaw_Click);
            // 
            // txtRawResult
            // 
            this.txtRawResult.BackColor = System.Drawing.Color.White;
            this.txtRawResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRawResult.Location = new System.Drawing.Point(273, 146);
            this.txtRawResult.Margin = new System.Windows.Forms.Padding(4);
            this.txtRawResult.Multiline = true;
            this.txtRawResult.Name = "txtRawResult";
            this.txtRawResult.Size = new System.Drawing.Size(728, 38);
            this.txtRawResult.TabIndex = 83;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 151);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(228, 29);
            this.label10.TabIndex = 85;
            this.label10.Text = "*Raw Result Folder:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(13, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(205, 39);
            this.label4.TabIndex = 86;
            this.label4.Text = "File Merging";
            // 
            // txtResultFolder
            // 
            this.txtResultFolder.BackColor = System.Drawing.Color.White;
            this.txtResultFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultFolder.Location = new System.Drawing.Point(273, 219);
            this.txtResultFolder.Margin = new System.Windows.Forms.Padding(4);
            this.txtResultFolder.Multiline = true;
            this.txtResultFolder.Name = "txtResultFolder";
            this.txtResultFolder.Size = new System.Drawing.Size(728, 38);
            this.txtResultFolder.TabIndex = 88;
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPath.Location = new System.Drawing.Point(16, 229);
            this.lbPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(174, 29);
            this.lbPath.TabIndex = 87;
            this.lbPath.Text = "*Result Folder:";
            // 
            // btnResultBrowse
            // 
            this.btnResultBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnResultBrowse.FlatAppearance.BorderSize = 0;
            this.btnResultBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResultBrowse.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnResultBrowse.Location = new System.Drawing.Point(1025, 214);
            this.btnResultBrowse.Margin = new System.Windows.Forms.Padding(4);
            this.btnResultBrowse.Name = "btnResultBrowse";
            this.btnResultBrowse.Size = new System.Drawing.Size(56, 46);
            this.btnResultBrowse.TabIndex = 84;
            this.btnResultBrowse.Text = "...";
            this.btnResultBrowse.UseVisualStyleBackColor = false;
            this.btnResultBrowse.Click += new System.EventHandler(this.btnResultBrowse_Click);
            // 
            // btnMergeData
            // 
            this.btnMergeData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnMergeData.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMergeData.ForeColor = System.Drawing.Color.White;
            this.btnMergeData.Location = new System.Drawing.Point(273, 293);
            this.btnMergeData.Margin = new System.Windows.Forms.Padding(4);
            this.btnMergeData.Name = "btnMergeData";
            this.btnMergeData.Size = new System.Drawing.Size(205, 44);
            this.btnMergeData.TabIndex = 89;
            this.btnMergeData.Text = "Merge Data";
            this.btnMergeData.UseVisualStyleBackColor = false;
            this.btnMergeData.Click += new System.EventHandler(this.btnMergeData_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(517, 293);
            this.progressBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(564, 44);
            this.progressBar.TabIndex = 90;
            // 
            // FileMerging
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1111, 369);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnMergeData);
            this.Controls.Add(this.txtResultFolder);
            this.Controls.Add(this.lbPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnResultBrowse);
            this.Controls.Add(this.btnBrowseRaw);
            this.Controls.Add(this.txtRawResult);
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "FileMerging";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini tool v3.11";
            this.Controls.SetChildIndex(this.txtRawResult, 0);
            this.Controls.SetChildIndex(this.btnBrowseRaw, 0);
            this.Controls.SetChildIndex(this.btnResultBrowse, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.lbPath, 0);
            this.Controls.SetChildIndex(this.txtResultFolder, 0);
            this.Controls.SetChildIndex(this.btnMergeData, 0);
            this.Controls.SetChildIndex(this.progressBar, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBrowseRaw;
        private System.Windows.Forms.TextBox txtRawResult;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtResultFolder;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.Button btnResultBrowse;
        private System.Windows.Forms.Button btnMergeData;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.FolderBrowserDialog FolderDialog;
    }
}