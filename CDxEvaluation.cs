﻿using MathNet.Numerics.Statistics;
using Microsoft.Office.Interop.Excel;
using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using DataTable = System.Data.DataTable;
using Excel = Microsoft.Office.Interop.Excel;
using Version = MiniTool.Data.Version;

namespace MiniTool
{
    public partial class CDxEvaluation : MasterForm
    {
        List<cDXData> listResult = null;
        List<Device> listDevice = null;
        Product product = null;
        List<Excel.Range> listRange = null;
        DatabaseConnection db;
        public CDxEvaluation() : base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            progressExport.Visible = false;
            btnReset.Enabled = false;
        }

        private void btnBrowseResult_Click(object sender, EventArgs e)
        {
            FolderDialog.ShowDialog();
            txtFolderPath.Text = FolderDialog.SelectedPath;
            btnGetResult.Enabled = true;
        }

        private void btnGetResult_Click(object sender, EventArgs e)
        {
            listResult = new List<cDXData>();
            listDevice = new List<Device>();
            product = new Product();
            listRange = new List<Excel.Range>();
            cbbDeviceName.DataSource = null;
            dgvResult.DataSource = null;
            txtThreshold.Text = "";
            txtPercentage.Text = "300";
            txtMean.Text = "";
            txtStdDev.Text = "";
            txtPStdDev.Text = "";
            txtFalseNegative.Text = "";
            txtFalsePositive.Text = "";
            txtPTA.Text = "";
            ChartCDS.ResetAutoValues();
            ChartCDS.Series.Clear();
            ChartCDS.Legends[0].Docking = Docking.Bottom;
            ChartCDS.MinimumSize = new Size(1300, 500);
            if (txtFolderPath.Text.Length == 0)
            {
                MessageBox.Show("Please browse the result folder.");
            }
            else
            {
                List<String> listCode = new List<String>();
                DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
                FileInfo[] files = d.GetFiles("*.csv");
                for (int i = 0; i < files.Count(); i++)
                {
                    if (files[i].Name.Contains("Orig"))
                    {
                        listCode.Add(files[i].Name.Replace("Orig.csv", ""));
                    }
                }
                
                product = Ultility.GetTheProductFromBulk(files);
                if (product != null && product.id != -1)
                {
                    LoadDevice(listCode);
                    progressExport.Value = 0;
                    progressExport.Minimum = 0;
                    progressExport.Visible = true;
                    progressExport.Maximum = listCode.Count() * 2;
                    int index = 1;
                    listRange = new List<Excel.Range>();
                    for (int i = 0; i < listCode.Count(); i++)
                    {

                        cDXData data = new cDXData();
                        data.deviceID = listCode[i].Split('_')[0];
                        data.productName = product.productName;
                        String fileOrig = txtFolderPath.Text + @"\" + listCode[i] + "Orig.csv";
                        String fileCopy = txtFolderPath.Text + @"\" + listCode[i] + "Copy.csv";

                        List<List<String>> listDataOrig = Ultility.GetCleanData(fileOrig, product);
                        data.listResultOrig = Ultility.GetResultListFromDataRaw(listDataOrig, product.listZone, 1, listDataOrig.Count, false);
                        progressExport.Value = index;

                        List<List<String>> listDataCopy = Ultility.GetCleanData(fileCopy, product);
                        data.listResultCopy = Ultility.GetResultListFromDataRaw(listDataCopy, product.listZone, 1, listDataCopy.Count, false);
                        progressExport.Value = index + 1;

                        index += 2;
                        listResult.Add(data);
                    }
                    if (listCode.Count > 0)
                    {
                        progressExport.Value = listCode.Count() * 2 - 1;
                    }
                }
                if (product == null || (product != null && product.id == -1))
                {
                    //  MessageBox.Show("The product has not been setup. Please setup the product");
                }


            }

            progressExport.Visible = false;
        }

        private void LoadDevice(List<String> listCode)
        {
            listDevice = new List<Device>();
            for (int i = 0; i < listCode.Count; i++)
            {
                listDevice.Add(db.GetDeviceByCodeAndLocation(listCode[i].Split('_')[0], listCode[i].Split('_')[1]));
            }

            if (listDevice.Count > 0)
            {
                Dictionary<int, String> listDislplay = new Dictionary<int, string>();
                for (int i = 0; i < listDevice.Count; i++)
                {
                    listDislplay.Add(listDevice[i].id, listDevice[i].model + " (" + listDevice[i].location + ")");
                }

                var bindingDevice = new BindingSource();
                bindingDevice.DataSource = listDislplay;

                cbbDeviceName.DataSource = new BindingSource(listDislplay, null);
                cbbDeviceName.ValueMember = "Key";
                cbbDeviceName.DisplayMember = "Value";
                cbbDeviceName.SelectedIndex = -1;
            }

        }

        private void cbbDeviceName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listResult.Count > 0 && cbbDeviceName.SelectedIndex != -1)
            {
                btnReset.Enabled = true;
                ChartCDS.ResetAutoValues();
                ChartCDS.Series.Clear();
                ChartCDS.Legends[0].Docking = Docking.Bottom;
                ChartCDS.MinimumSize = new Size(1300, 500);

                cDXData result = new cDXData();
                for (int i = 0; i < listResult.Count; i++)
                {
                    int id;
                    if (Int32.TryParse(cbbDeviceName.SelectedValue.ToString(), out id))
                    {
                        if (listResult[i].deviceID.Equals(db.GetDeviceByID(Int32.Parse(cbbDeviceName.SelectedValue.ToString())).code))
                        {
                            result = listResult[i];
                            i = listResult.Count;
                        }
                    }
                    else
                    {
                        if (listResult[i].deviceID.Equals(db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.SelectedValue).Key.ToString())).code))
                        {
                            result = listResult[i];
                            i = listResult.Count;
                        }
                    }
                }

                int totalData = 0;
                for (int i = 0; i < product.listZone.Count; i++)
                {
                    if (!product.listZone[i].method.Equals("STC"))
                    {
                        totalData += result.listResultOrig.Count * 2;
                    }
                }

                List<DataDrawChart> listChart = new List<DataDrawChart>();
                List<DataDrawChart> listOrg = new List<DataDrawChart>();
                List<DataDrawChart> listCopy = new List<DataDrawChart>();

                List<String> type = new List<string>();
                for (int i = 0; i < product.listZone.Count; i++)
                {
                    type.Add(product.listZone[i].method);
                }
                int CDSZone = Ultility.FindZoneNo(product, "CDS");
                int CD1Zone = Ultility.FindZoneNo(product, "CD1");
                int CD2Zone = Ultility.FindZoneNo(product, "CD2");
                int CD3Zone = Ultility.FindZoneNo(product, "CD3");
                for (int i = 0; i < result.listResultOrig.Count; i++)
                {
                    if (type.Contains("CDS"))
                    {
                        if (result.listResultOrig[i].zone == CDSZone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.statisticID = result.listResultOrig[i].statisticID;
                            data.resultText = result.listResultOrig[i].resultText.ToUpper();
                            if (result.listResultOrig[i].resultZone.Equals("-1") && result.listResultOrig[i].infoZone.Contains("FineSigma="))
                            {
                                data.pointValue = Double.Parse(result.listResultOrig[i].resultZone);
                            }else if (!result.listResultOrig[i].resultZone.Equals("-1"))
                            {
                                data.pointValue = Double.Parse(result.listResultOrig[i].resultZone);
                            }
                            else
                            {
                                data.pointValue = Double.Parse(result.listResultOrig[i + 1].resultZone);
                            }
                            listChart.Add(data);
                            listOrg.Add(data);
                        }
                    }
                    if (type.Contains("CD1") || type.Contains("CD2") || type.Contains("CD3"))
                    {
                        int CDxZone = -1;
                        if (type.Contains("CD1"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD1"))].zoneNo;
                        }
                        else if (type.Contains("CD2"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD2"))].zoneNo;
                        }
                        else if (type.Contains("CD3"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD3"))].zoneNo;
                        }

                        if (result.listResultOrig[i].zone == CDxZone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.statisticID = result.listResultOrig[i].statisticID;
                            data.pointValue = Double.Parse(result.listResultOrig[i].resultZone);
                            listChart.Add(data);
                            listOrg.Add(data);
                        }
                    }

                }
                for (int i = 0; i < result.listResultCopy.Count; i++)
                {
                    if (type.Contains("CDS"))
                    {
                        if (result.listResultCopy[i].zone == CDSZone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.statisticID = result.listResultCopy[i].statisticID;
                            data.resultText = result.listResultCopy[i].resultText.ToUpper();
                            if (result.listResultCopy[i].resultZone.Equals("-1") && result.listResultCopy[i].infoZone.Contains("FineSigma="))
                            {
                                data.pointValue = Double.Parse(result.listResultCopy[i].resultZone);
                            }
                            else if (!result.listResultCopy[i].resultZone.Equals("-1"))
                            {
                                data.pointValue = Double.Parse(result.listResultCopy[i].resultZone);
                            }
                            else
                            {
                                data.pointValue = Double.Parse(result.listResultCopy[i + 1].resultZone);
                            }
                            listChart.Add(data);
                            listCopy.Add(data);
                        }
                    }
                    if (type.Contains("CD1") || type.Contains("CD2") || type.Contains("CD3"))
                    {
                        int CDxZone = -1;
                        if (type.Contains("CD1"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD1"))].zoneNo;
                        }
                        else if (type.Contains("CD2"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD2"))].zoneNo;
                        }
                        else if (type.Contains("CD3"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD3"))].zoneNo;
                        }

                        if (result.listResultCopy[i].zone == CDxZone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.statisticID = result.listResultCopy[i].statisticID;
                            data.pointValue = Double.Parse(result.listResultCopy[i].resultZone);
                            listChart.Add(data);
                            listCopy.Add(data);
                        }
                    }


                }
                ShowResult(listOrg, listCopy);
                ChartCDS.ChartAreas[0].Position.X = 0;
                ChartCDS.ChartAreas[0].Position.Y = 0;
                ChartCDS.ChartAreas[0].Position.Width = 100;
                ChartCDS.Width = (listOrg.Count + listCopy.Count) * 10;

                Random random = new Random();

                var seriesOrig = new System.Windows.Forms.DataVisualization.Charting.Series();
                seriesOrig.Name = "Orig";
                seriesOrig.ChartType = SeriesChartType.Point;
                seriesOrig.IsValueShownAsLabel = true;
                seriesOrig.MarkerSize = 8;
                seriesOrig.MarkerStyle = MarkerStyle.Circle;
                seriesOrig.Color = Color.GreenYellow;
                ChartCDS.Series.Add(seriesOrig);
                for (int i = 0; i < listOrg.Count; i++)
                {
                    int index = i + 1;
                    ChartCDS.Series[0].Points.AddXY(index, listOrg[i].pointValue);
                }

                var seriesCopy = new System.Windows.Forms.DataVisualization.Charting.Series();
                seriesCopy.Name = "Copy";
                seriesCopy.ChartType = SeriesChartType.Point;
                seriesCopy.IsValueShownAsLabel = true;
                seriesCopy.MarkerSize = 8;
                seriesCopy.MarkerStyle = MarkerStyle.Circle;
                seriesCopy.Color = Color.Red;
                ChartCDS.Series.Add(seriesCopy);
                for (int i = 0; i < listCopy.Count; i++)
                {

                    int index = i + 1;
                    ChartCDS.Series[1].Points.AddXY(index, listCopy[i].pointValue);

                }

                var SeriesThreshold = new System.Windows.Forms.DataVisualization.Charting.Series();
                SeriesThreshold.Name = "Threshold";
                SeriesThreshold.ChartType = SeriesChartType.Point;
                SeriesThreshold.MarkerSize = 8;
                SeriesThreshold.MarkerStyle = MarkerStyle.Circle;
                SeriesThreshold.Color = Color.LightGray;
                ChartCDS.Series.Add(SeriesThreshold);

                txtMean.Text = CalculateMean(listOrg).ToString();
                txtStdDev.Text = CalculateStdDev(listOrg).ToString();
                txtPStdDev.Text = (Double.Parse(txtStdDev.Text) * Double.Parse(txtPercentage.Text) / 100).ToString();
                txtThreshold.Text = Math.Round((CalculateMean(listOrg) + Double.Parse(txtPStdDev.Text)), 1).ToString();
                double threshold = Double.Parse(txtThreshold.Text);
                int count = listOrg.Count > listCopy.Count ? listOrg.Count : listCopy.Count;
                for (int i = 0; i < count; i++)
                {
                    int index = i + 1;
                    ChartCDS.Series[2].Points.AddXY(index, threshold);
                }

                if (Double.TryParse(txtThreshold.Text, out threshold))
                {
                    txtFalseNegative.Text = Ultility.CalculateFalseNegative(listOrg, threshold).ToString();
                    txtFalsePositive.Text = Ultility.CalculateFalsePositive(listCopy, threshold).ToString();
                    txtPTA.Text = Ultility.CalculatePTA(listOrg, listCopy).ToString();
                }

            }

        }

        private double CalculateMean(List<DataDrawChart> listData)
        {
            double sum = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                sum += listData[i].pointValue;
            }
            return Math.Round(sum / listData.Count, 2);
        }

        private double CalculateStdDev(List<DataDrawChart> listOrig)
        {
            List<double> listData = new List<double>();

            for (int i = 0; i < listOrig.Count; i++)
            {
                listData.Add(listOrig[i].pointValue);
            }

            return listData.StandardDeviation();
        }

        private void ShowResult(List<DataDrawChart> listOrig, List<DataDrawChart> listCopy)
        {

            dgvResult.Columns.Clear();
            dgvResult.DataSource = null;

            DataTable dt = new DataTable();
            dt.Columns.Add("Index ");
            for (int i = 0; i < product.listZone.Count; i++)
            {
                if (!product.listZone[i].method.Equals("STC"))
                {
                    dt.Columns.Add("Zone Result " + (i + 1) + " Original");
                    dt.Columns.Add("Zone Result " + (i + 1) + " Copy");
                }
            }
            dt.Columns.Add("Device ID");

            int count = listOrig.Count > listCopy.Count ? listOrig.Count : listCopy.Count;
            int distance = Math.Abs(listCopy.Count - listOrig.Count);
            bool isCopyMore = listCopy.Count > listOrig.Count ? true : false;
            for (int i = 0; i < count; i++)
            {
                int column = 1;
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                for (int k = 0; k < product.listZone.Count; k++)
                {
                    if (!product.listZone[k].method.Equals("STC"))
                    {
                        if (i >= count - distance)
                        {
                            if (isCopyMore)
                            {
                                dt.Rows[dt.Rows.Count - 1][column] = "";
                                dt.Rows[dt.Rows.Count - 1][column + 1] = listCopy[i].pointValue;
                            }
                            else
                            {
                                dt.Rows[dt.Rows.Count - 1][column] = listOrig[i].pointValue;
                                dt.Rows[dt.Rows.Count - 1][column + 1] = "";
                            }
                        }
                        else
                        {
                            dt.Rows[dt.Rows.Count - 1][column] = listOrig[i].pointValue;
                            dt.Rows[dt.Rows.Count - 1][column + 1] = listCopy[i].pointValue;

                        }
                        column += 2;
                    }
                }
                int id = 0;
                if (Int32.TryParse(cbbDeviceName.SelectedValue.ToString(), out id))
                {
                    dt.Rows[dt.Rows.Count - 1][column] = db.GetDeviceByID(Int32.Parse(cbbDeviceName.SelectedValue.ToString())).code;
                }
                else
                {
                    dt.Rows[dt.Rows.Count - 1][column] = db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.SelectedValue).Key.ToString())).code;
                }

            }

            dgvResult.DataSource = dt;
        }

        private void txtThreshold_TextChanged(object sender, EventArgs e)
        {
            if (listResult.Count > 0 && cbbDeviceName.SelectedIndex != -1)
            {
                btnReset.Enabled = true;
                ChartCDS.ResetAutoValues();
                ChartCDS.Series.Clear();
                ChartCDS.Legends[0].Docking = Docking.Bottom;
                ChartCDS.MinimumSize = new Size(1300, 500);

                cDXData result = new cDXData();
                for (int i = 0; i < listResult.Count; i++)
                {
                    int id;
                    if (Int32.TryParse(cbbDeviceName.SelectedValue.ToString(), out id))
                    {
                        if (listResult[i].deviceID.Equals(db.GetDeviceByID(Int32.Parse(cbbDeviceName.SelectedValue.ToString())).code))
                        {
                            result = listResult[i];
                            i = listResult.Count;
                        }
                    }
                    else
                    {
                        if (listResult[i].deviceID.Equals(db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.SelectedValue).Key.ToString())).code))
                        {
                            result = listResult[i];
                            i = listResult.Count;
                        }
                    }
                }

                int totalData = 0;
                for (int i = 0; i < product.listZone.Count; i++)
                {
                    if (!product.listZone[i].method.Equals("STC"))
                    {
                        totalData += result.listResultOrig.Count * 2;
                    }
                }

                List<DataDrawChart> listChart = new List<DataDrawChart>();
                List<DataDrawChart> listOrg = new List<DataDrawChart>();
                List<DataDrawChart> listCopy = new List<DataDrawChart>();

                List<String> type = new List<string>();
                for (int i = 0; i < product.listZone.Count; i++)
                {
                    type.Add(product.listZone[i].method);
                }
                for (int i = 0; i < result.listResultOrig.Count; i++)
                {
                    if (type.Contains("CDS"))
                    {
                        int CDSZone = product.listZone[type.FindIndex(x => x.StartsWith("CDS"))].zoneNo;
                        if (result.listResultOrig[i].zone == CDSZone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.statisticID = result.listResultOrig[i].statisticID;
                            data.resultText = result.listResultOrig[i].resultText.ToUpper();
                            if (result.listResultOrig[i].resultZone.Equals("-1") && result.listResultOrig[i].infoZone.Contains("FineSigma="))
                            {
                                data.pointValue = Double.Parse(result.listResultOrig[i].resultZone);
                            }
                            else if (!result.listResultOrig[i].resultZone.Equals("-1"))
                            {
                                data.pointValue = Double.Parse(result.listResultOrig[i].resultZone);
                            }
                            else
                            {
                                data.pointValue = Double.Parse(result.listResultOrig[i + 1].resultZone);
                            }
                            listChart.Add(data);
                            listOrg.Add(data);
                        }
                    }
                    if (type.Contains("CD1") || type.Contains("CD2") || type.Contains("CD3"))
                    {
                        int CDxZone = -1;
                        if (type.Contains("CD1"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD1"))].zoneNo;
                        }
                        else if (type.Contains("CD2"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD2"))].zoneNo;
                        }
                        else if (type.Contains("CD3"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD3"))].zoneNo;
                        }

                        if (result.listResultOrig[i].zone == CDxZone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.statisticID = result.listResultOrig[i].statisticID;
                            data.pointValue = Double.Parse(result.listResultOrig[i].resultZone);
                            listChart.Add(data);
                            listOrg.Add(data);
                        }
                    }

                }
                for (int i = 0; i < result.listResultCopy.Count; i++)
                {
                    if (type.Contains("CDS"))
                    {
                        int CDSZone = product.listZone[type.FindIndex(x => x.StartsWith("CDS"))].zoneNo;
                        if (result.listResultCopy[i].zone == CDSZone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.statisticID = result.listResultCopy[i].statisticID;
                            data.resultText = result.listResultCopy[i].resultText.ToUpper();
                            if (result.listResultCopy[i].resultZone.Equals("-1") && result.listResultCopy[i].infoZone.Contains("FineSigma="))
                            {
                                data.pointValue = Double.Parse(result.listResultCopy[i].resultZone);
                            }
                            else if (!result.listResultCopy[i].resultZone.Equals("-1"))
                            {
                                data.pointValue = Double.Parse(result.listResultCopy[i].resultZone);
                            }
                            else
                            {
                                data.pointValue = Double.Parse(result.listResultCopy[i + 1].resultZone);
                            }
                            listChart.Add(data);
                            listCopy.Add(data);
                        }
                    }
                    if (type.Contains("CD1") || type.Contains("CD2") || type.Contains("CD3"))
                    {
                        int CDxZone = -1;
                        if (type.Contains("CD1"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD1"))].zoneNo;
                        }
                        else if (type.Contains("CD2"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD2"))].zoneNo;
                        }
                        else if (type.Contains("CD3"))
                        {
                            CDxZone = product.listZone[type.FindIndex(x => x.StartsWith("CD3"))].zoneNo;
                        }

                        if (result.listResultCopy[i].zone == CDxZone)
                        {
                            DataDrawChart data = new DataDrawChart();
                            data.statisticID = result.listResultCopy[i].statisticID;
                            data.pointValue = Double.Parse(result.listResultCopy[i].resultZone);
                            listChart.Add(data);
                            listCopy.Add(data);
                        }
                    }


                }
                ShowResult(listOrg, listCopy);
                ChartCDS.ChartAreas[0].Position.X = 0;
                ChartCDS.ChartAreas[0].Position.Y = 0;
                ChartCDS.ChartAreas[0].Position.Width = 100;
                ChartCDS.Width = (listOrg.Count + listCopy.Count) * 10;

                Random random = new Random();

                var seriesOrig = new System.Windows.Forms.DataVisualization.Charting.Series();
                seriesOrig.Name = "Orig";
                seriesOrig.ChartType = SeriesChartType.Point;
                seriesOrig.IsValueShownAsLabel = true;
                seriesOrig.MarkerSize = 8;
                seriesOrig.MarkerStyle = MarkerStyle.Circle;
                seriesOrig.Color = Color.GreenYellow;
                ChartCDS.Series.Add(seriesOrig);
                for (int i = 0; i < listOrg.Count; i++)
                {
                    int index = i + 1;
                    ChartCDS.Series[0].Points.AddXY(index, listOrg[i].pointValue);
                }

                var seriesCopy = new System.Windows.Forms.DataVisualization.Charting.Series();
                seriesCopy.Name = "Copy";
                seriesCopy.ChartType = SeriesChartType.Point;
                seriesCopy.IsValueShownAsLabel = true;
                seriesCopy.MarkerSize = 8;
                seriesCopy.MarkerStyle = MarkerStyle.Circle;
                seriesCopy.Color = Color.Red;
                ChartCDS.Series.Add(seriesCopy);
                for (int i = 0; i < listCopy.Count; i++)
                {

                    int index = i + 1;
                    ChartCDS.Series[1].Points.AddXY(index, listCopy[i].pointValue);

                }

                var SeriesThreshold = new System.Windows.Forms.DataVisualization.Charting.Series();
                SeriesThreshold.Name = "Threshold";
                SeriesThreshold.ChartType = SeriesChartType.Point;
                SeriesThreshold.MarkerSize = 8;
                SeriesThreshold.MarkerStyle = MarkerStyle.Circle;
                SeriesThreshold.Color = Color.LightGray;
                ChartCDS.Series.Add(SeriesThreshold);

                double threshold = Double.Parse(txtThreshold.Text);
                int count = listOrg.Count > listCopy.Count ? listOrg.Count : listCopy.Count;
                for (int i = 0; i < count; i++)
                {
                    int index = i + 1;
                    ChartCDS.Series[2].Points.AddXY(index, threshold);
                }

                if (Double.TryParse(txtThreshold.Text, out threshold))
                {
                    txtFalseNegative.Text = Ultility.CalculateFalseNegative(listOrg, threshold).ToString();
                    txtFalsePositive.Text = Ultility.CalculateFalsePositive(listCopy, threshold).ToString();
                    txtPTA.Text = Ultility.CalculatePTA(listOrg, listCopy).ToString();
                }

            }
        }

        private void txtPercentage_TextChanged(object sender, EventArgs e)
        {
            if (txtPercentage.Text.Length == 0)
            {
                txtPercentage.Text = "0";
            }
            else
            {
                double mean = Double.Parse(txtMean.Text);
                double StdDev = Double.Parse(txtStdDev.Text);
                double PStdDev = StdDev * Double.Parse(txtPercentage.Text) / 100;
                txtPStdDev.Text = PStdDev.ToString();
                txtThreshold.Text = Math.Round(mean + PStdDev, 1).ToString();
            }


        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            cbbDeviceName_SelectedIndexChanged(sender, e);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {

            listResult = new List<cDXData>();
            listDevice = new List<Device>();
            product = new Product();
            listRange = new List<Excel.Range>();
            txtFolderPath.Text = "";
            cbbDeviceName.DataSource = null;
            dgvResult.DataSource = null;
            txtThreshold.Text = "";
            txtPercentage.Text = "300";
            txtMean.Text = "";
            txtStdDev.Text = "";
            txtPStdDev.Text = "";
            txtFalseNegative.Text = "";
            txtFalsePositive.Text = "";
            txtPTA.Text = "";
            ChartCDS.ResetAutoValues();
            ChartCDS.Series.Clear();
            ChartCDS.Legends[0].Docking = Docking.Bottom;
            ChartCDS.MinimumSize = new Size(1300, 500);


        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet = new Excel.Worksheet();
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();

            progressExport.Minimum = 0;
            progressExport.Value = 0;
            progressExport.Visible = true;



            xlWorkBook = xlApp.Workbooks.Add(misValue);

            int index = 0;
            DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
            FileInfo[] files = d.GetFiles("*.csv");
            progressExport.Maximum = (files.Length + (files.Length / 2)) * 10;
            for (int i = 0; i < cbbDeviceName.Items.Count; i++)
            {
                progressExport.Value = index * 10;
                Device device = db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.Items[i]).Key.ToString()));
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                int qtyCopy = 0;
                int qtyOrignal = 0;
                List<List<String>> listCleanedData = Ultility.GetCleanData(files[index].FullName, product);
                DataTable dtRaw = Ultility.ParseDataRawToTable(listCleanedData);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtRaw.Rows.Count, dtRaw.Columns.Count]].Value = Ultility.ParseDtToArray(dtRaw);
                xlWorkSheet.Name = "D_C_" + device.code + "(" + device.location + ")";
                Excel.Range rangeRaw = xlWorkSheet.Rows[1];
                rangeRaw.Select();
                rangeRaw.Font.Bold = true;
                rangeRaw.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                index++;
                progressExport.Value = index * 10;
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                listCleanedData = Ultility.GetCleanData(files[index].FullName, product);
                dtRaw = Ultility.ParseDataRawToTable(listCleanedData);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtRaw.Rows.Count, dtRaw.Columns.Count]].Value = Ultility.ParseDtToArray(dtRaw);
                xlWorkSheet.Name = "D_O_" + device.code + "(" + device.location + ")";
                rangeRaw = xlWorkSheet.Rows[1];
                rangeRaw.Select();
                rangeRaw.Font.Bold = true;
                rangeRaw.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                index++;
                progressExport.Value = index * 10;
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                xlWorkSheet.Name = "Graph_" + device.code + "(" + device.location + ")";
                List<ResultTestInfo> listSave = new List<ResultTestInfo>();
                List<String> type = new List<string>();
                for (int k = 0; k < product.listZone.Count; k++)
                {
                    type.Add(product.listZone[k].method);
                }
                if (product.listZone[type.FindIndex(x => x.StartsWith("CDS"))] != null)
                {

                }
                int CDSZone = Ultility.FindZoneNo(product, "CDS");
                int CD1Zone = Ultility.FindZoneNo(product, "CD1");
                int CD2Zone = Ultility.FindZoneNo(product, "CD2");
                int CD3Zone = Ultility.FindZoneNo(product, "CD3");
                for (int k = 0; k < listResult.Count; k++)
                {
                    if (device.code.Equals(listResult[k].deviceID))
                    {
                        for (int m = 0; m < listResult[k].listResultOrig.Count; m++)
                        {
                            if (listResult[k].listResultOrig[m].zone == CDSZone)
                            {
                                if (listResult[k].listResultOrig[m].resultZone.Equals("-1") && listResult[k].listResultOrig[m].infoZone.Contains("FineSigma="))
                                {
                                    listSave.Add(listResult[k].listResultOrig[m]);
                                }
                                else if (!listResult[k].listResultOrig[m].resultZone.Equals("-1"))
                                {
                                    listSave.Add(listResult[k].listResultOrig[m]);
                                }
                                else
                                {
                                    listSave.Add(listResult[k].listResultOrig[m + 1]);
                                }

                                qtyOrignal++;
                            }
                            else if (listResult[k].listResultOrig[m].zone == CD1Zone || listResult[k].listResultOrig[m].zone == CD2Zone || listResult[k].listResultOrig[m].zone == CD3Zone)
                            {
                                listSave.Add(listResult[k].listResultOrig[m]);
                            }

                        }

                        for (int m = 0; m < listResult[k].listResultCopy.Count; m++)
                        {

                            if (listResult[k].listResultCopy[m].zone == CDSZone)
                            {

                                if (listResult[k].listResultCopy[m].Equals("-1") && listResult[k].listResultCopy[m].infoZone.Contains("FineSigma="))
                                {
                                    listSave.Add(listResult[k].listResultCopy[m]);
                                }
                                else if (!listResult[k].listResultCopy[m].resultZone.Equals("-1"))
                                {
                                    listSave.Add(listResult[k].listResultCopy[m]);
                                }
                                else
                                {
                                    listSave.Add(listResult[k].listResultCopy[m + 1]);
                                }
                                qtyCopy++;
                            }


                            else if (listResult[k].listResultCopy[m].zone == CD1Zone || listResult[k].listResultCopy[m].zone == CD2Zone || listResult[k].listResultCopy[m].zone == CD3Zone)
                            {
                                listSave.Add(listResult[k].listResultCopy[m]);
                            }

                        }
                        k = listResult.Count;



                    }
                }
                List<DataDrawChart> listChart = new List<DataDrawChart>();
                for (int k = 0; k < listSave.Count; k++)
                {
                    DataDrawChart data = new DataDrawChart();
                    data.statisticID = listSave[k].statisticID;
                    data.resultText = listSave[k].resultText.ToUpper();
                    data.pointValue = Double.Parse(listSave[k].resultZone);
                    listChart.Add(data);

                }
                DataTable dt = GetDataTable(listChart, device, qtyOrignal, qtyCopy);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = Ultility.ParseDtToArray(dt);
                Excel.Range range = xlWorkSheet.Rows[1];
                range.Select();
                range.Font.Bold = true;
                range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                int valid = 0;
                for (int m = 0; m < product.listZone.Count; m++)
                {
                    if (product.listZone[m].method.Equals("STC"))
                    {
                        valid++;
                    }
                }

                Excel.ChartObjects xlChart = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
                Excel.ChartObject myChart = (Excel.ChartObject)xlChart.Add(180 * valid, 130, 700, 425);
                Excel.Chart chartPage = myChart.Chart;

                chartPage.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;
                myChart.Select();

                chartPage.ChartType = Excel.XlChartType.xlXYScatter;
                Excel.SeriesCollection seriesCollection = chartPage.SeriesCollection();
                int row = dt.Rows.Count;
                String[] rangeColumn = new String[] { "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P" };
                int position = 0;
                xlWorkSheet.Cells[2, 8] = "False Negative";
                xlWorkSheet.Cells[3, 8] = "False Positive";
                xlWorkSheet.Cells[4, 8] = "PTA";
                xlWorkSheet.Cells[5, 8] = "Mean Orig";
                xlWorkSheet.Cells[6, 8] = "Std Dev Sample";
                xlWorkSheet.Cells[7, 8] = "300%";
                xlWorkSheet.Cells[8, 8] = "Threshold (Mean+ % of Std Dev)";

                xlWorkSheet.Cells[2, 9].Formula = "=COUNTIF(B2:B" + (qtyOrignal + 1) + ",\">\"&D2)-COUNTIF(B2:B" + (qtyOrignal + 1) + ",-1)";
                xlWorkSheet.Cells[3, 9].Formula = "=COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",\"<\"&D2)-COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",-1)-COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",0)";
                xlWorkSheet.Cells[4, 9].Formula = "=COUNTIF(B2:B" + row + ",-1)";
                xlWorkSheet.Cells[5, 9].Formula = "=AVERAGEIF(B2:B" + (qtyOrignal + 1) + ",\"<>-1\")";
                xlWorkSheet.Cells[6, 9].Formula = "=STDEV.S(B2:B" + (qtyOrignal + 1) + ")";
                xlWorkSheet.Cells[7, 9].Formula = "=I6*SUBSTITUTE(H7,\"%\",)";
                xlWorkSheet.Cells[8, 9].Formula = "=I5+I7";

                for (int k = 2; k <= row; k++)
                {
                    xlWorkSheet.Cells[k, 4].Formula = "=ROUND($I$8,1)";
                }

                FormatCondition formatOrig = (FormatCondition)(xlWorkSheet.get_Range("B2:B" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlGreater, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatOrig.Interior.Color = 0x000000FF;

                FormatCondition formatCopy = (FormatCondition)(xlWorkSheet.get_Range("C2:C" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlLess, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatCopy.Interior.Color = 0x000000FF;
                xlWorkSheet.Columns[8].ColumnWidth = 20;
                xlWorkSheet.Columns[9].ColumnWidth = 20;

                Excel.Series seriesOrig = seriesCollection.NewSeries();
                seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesOrig.Name = "Original";
                seriesOrig.MarkerSize = 5;
                seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                Excel.Series seriesCopy = seriesCollection.NewSeries();
                seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesCopy.Name = "Copy";
                seriesCopy.MarkerSize = 5;
                seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (row+1));
                seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);

                Excel.Series seriesThreshold = seriesCollection.NewSeries();
                seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesThreshold.Name = "Threshold";
                seriesThreshold.MarkerSize = 5;
                seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                position++;


                if (i != cbbDeviceName.Items.Count - 1)
                {
                    xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                }


            }

            String fileName = txtFolderPath.Text + @"\" + "Data_Set";

            try
            {
                xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookDefault, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Marshal.ReleaseComObject(xlWorkSheet);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            progressExport.Value = progressExport.Maximum;
            MessageBox.Show("Excel file " + fileName + " has been created.");
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            progressExport.Visible = false;
        }

        private DataTable GetDataTable(List<DataDrawChart> listSave, Device device, int qtyOrig, int qtyCopy)
        {
            DataTable dt = new DataTable();

            for (int i = 0; i < product.listZone.Count; i++)
            {
                if (!product.listZone[i].Equals("STC"))
                {
                    dt.Columns.Add();
                    dt.Columns.Add();
                    dt.Columns.Add();
                }

            }
            dt.Columns.Add();
            dt.Columns.Add();
            int column = 1;
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "Statistic ID";
            for (int i = 0; i < product.listZone.Count; i++)
            {
                if (!product.listZone[i].method.Equals("STC"))
                {
                    dt.Rows[0][column] = "Zone Result Zone " + (i + 1);
                    dt.Rows[0][column + 1] = "Type";
                    dt.Rows[0][column + 2] = "Threshold Zone " + (i + 1);
                    column += 3;
                }

            }

            dt.Rows[0][column] = "Device Model";



            int index = 1;
            List<String> type = new List<String>();
            for (int k = 0; k < product.listZone.Count; k++)
            {
                type.Add(product.listZone[k].method);
            }
            int CDSZone = product.listZone[type.FindIndex(x => x.StartsWith("CDS"))].zoneNo;

            for (int i = 0; i < qtyOrig; i++)
            {
                column = 0;
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][column] = index;
                index++;
                column++;
                dt.Rows[dt.Rows.Count - 1][column] = listSave[i].pointValue;
                dt.Rows[dt.Rows.Count - 1][column + 1] = "Original";
                dt.Rows[dt.Rows.Count - 1][column + 2] = "";
                column += 3;
                dt.Rows[dt.Rows.Count - 1][column] = device.code + " - " + device.model;

            }

            for (int i = qtyOrig; i < qtyOrig + qtyCopy; i++)
            {
                column = 0;
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][column] = index;
                index++;
                column++;
                dt.Rows[dt.Rows.Count - 1][column] = listSave[i].pointValue;
                dt.Rows[dt.Rows.Count - 1][column + 1] = "Copy";
                dt.Rows[dt.Rows.Count - 1][column + 2] = "";
                column += 3;
                dt.Rows[dt.Rows.Count - 1][column] = device.code + " - " + device.model;
            }
            return dt;

        }

        private void btnExportGraph_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet = new Excel.Worksheet();
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();

            progressExport.Minimum = 0;
            progressExport.Value = 0;
            progressExport.Visible = true;
            xlWorkBook = xlApp.Workbooks.Add(misValue);

            DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
            FileInfo[] files = d.GetFiles("*.csv");
            progressExport.Maximum = files.Length / 2 * 10;
            int index = 0;
            for (int i = 0; i < cbbDeviceName.Items.Count; i++)
            {
                progressExport.Value = index * 10;
                Device device = db.GetDeviceByID(Int32.Parse(((KeyValuePair<int, string>)cbbDeviceName.Items[i]).Key.ToString()));
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                int qtyCopy = 0;
                int qtyOrignal = 0;
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                xlWorkSheet.Name = "Graph_" + device.code + "(" + device.location + ")";
                List<ResultTestInfo> listSave = new List<ResultTestInfo>();
                List<String> type = new List<string>();
                for (int k = 0; k < product.listZone.Count; k++)
                {
                    type.Add(product.listZone[k].method);
                }
                int CDSZone = Ultility.FindZoneNo(product, "CDS");
                int CD1Zone = Ultility.FindZoneNo(product, "CD1");
                int CD2Zone = Ultility.FindZoneNo(product, "CD2");
                int CD3Zone = Ultility.FindZoneNo(product, "CD3");
                for (int k = 0; k < listResult.Count; k++)
                {
                    if (device.code.Equals(listResult[k].deviceID))
                    {
                        for (int m = 0; m < listResult[k].listResultOrig.Count; m++)
                        {
                            if (listResult[k].listResultOrig[m].zone == CDSZone)
                            {
                                if (listResult[k].listResultOrig[m].resultZone.Equals("-1") && listResult[k].listResultOrig[m].infoZone.Contains("FineSigma="))
                                {
                                    listSave.Add(listResult[k].listResultOrig[m]);
                                }
                                else if (!listResult[k].listResultOrig[m].resultZone.Equals("-1"))
                                {
                                    listSave.Add(listResult[k].listResultOrig[m]);
                                }
                                else
                                {
                                    listSave.Add(listResult[k].listResultOrig[m + 1]);
                                }

                                qtyOrignal++;
                            }
                            else if (listResult[k].listResultOrig[m].zone == CD1Zone || listResult[k].listResultOrig[m].zone == CD2Zone || listResult[k].listResultOrig[m].zone == CD3Zone)
                            {
                                listSave.Add(listResult[k].listResultOrig[m]);
                            }

                        }

                        for (int m = 0; m < listResult[k].listResultCopy.Count; m++)
                        {

                            if (listResult[k].listResultCopy[m].zone == CDSZone)
                            {

                                if (listResult[k].listResultCopy[m].Equals("-1") && listResult[k].listResultCopy[m].infoZone.Contains("FineSigma="))
                                {
                                    listSave.Add(listResult[k].listResultCopy[m]);
                                }
                                else if (!listResult[k].listResultCopy[m].resultZone.Equals("-1"))
                                {
                                    listSave.Add(listResult[k].listResultCopy[m]);
                                }
                                else
                                {
                                    listSave.Add(listResult[k].listResultCopy[m + 1]);
                                }
                                qtyCopy++;
                            }


                            else if (listResult[k].listResultCopy[m].zone == CD1Zone || listResult[k].listResultCopy[m].zone == CD2Zone || listResult[k].listResultCopy[m].zone == CD3Zone)
                            {
                                listSave.Add(listResult[k].listResultCopy[m]);
                            }

                        }
                        k = listResult.Count;



                    }
                }
                List<DataDrawChart> listChart = new List<DataDrawChart>();
                for (int k = 0; k < listSave.Count; k++)
                {
                    DataDrawChart data = new DataDrawChart();
                    data.statisticID = listSave[k].statisticID;
                    data.resultText = listSave[k].resultText.ToUpper();
                    data.pointValue = Double.Parse(listSave[k].resultZone);
                    listChart.Add(data);

                }
                DataTable dt = GetDataTable(listChart, device, qtyOrignal, qtyCopy);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = Ultility.ParseDtToArray(dt);
                Excel.Range range = xlWorkSheet.Rows[1];
                range.Select();
                range.Font.Bold = true;
                range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                int valid = 0;
                for (int m = 0; m < product.listZone.Count; m++)
                {
                    if (product.listZone[m].method.Equals("STC"))
                    {
                        valid++;
                    }
                }

                Excel.ChartObjects xlChart = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
                Excel.ChartObject myChart = (Excel.ChartObject)xlChart.Add(180 * valid, 130, 700, 425);
                Excel.Chart chartPage = myChart.Chart;

                chartPage.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;
                myChart.Select();

                chartPage.ChartType = Excel.XlChartType.xlXYScatter;
                Excel.SeriesCollection seriesCollection = chartPage.SeriesCollection();
                int row = dt.Rows.Count;
                String[] rangeColumn = new String[] { "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P" };
                int position = 0;
                xlWorkSheet.Cells[2, 8] = "False Negative";
                xlWorkSheet.Cells[3, 8] = "False Positive";
                xlWorkSheet.Cells[4, 8] = "PTA";
                xlWorkSheet.Cells[5, 8] = "Mean Orig";
                xlWorkSheet.Cells[6, 8] = "Std Dev Sample";
                xlWorkSheet.Cells[7, 8] = "300%";
                xlWorkSheet.Cells[8, 8] = "Threshold (Mean+ % of Std Dev)";

                xlWorkSheet.Cells[2, 9].Formula = "=COUNTIF(B2:B" + (qtyOrignal + 1) + ",\">\"&D2)-COUNTIF(B2:B" + (qtyOrignal + 1) + ",-1)";
                xlWorkSheet.Cells[3, 9].Formula = "=COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",\"<\"&D2)-COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",-1)-COUNTIF(B" + (qtyOrignal + 2) + ":B" + row + ",0)";
                xlWorkSheet.Cells[4, 9].Formula = "=COUNTIF(B2:B" + row + ",-1)";
                xlWorkSheet.Cells[5, 9].Formula = "=AVERAGEIF(B2:B" + (qtyOrignal + 1) + ",\"<>-1\")";
                xlWorkSheet.Cells[6, 9].Formula = "=STDEV.S(B2:B" + (qtyOrignal + 1) + ")";
                xlWorkSheet.Cells[7, 9].Formula = "=I6*SUBSTITUTE(H7,\"%\",)";
                xlWorkSheet.Cells[8, 9].Formula = "=I5+I7";

                for (int k = 2; k <= row; k++)
                {
                    xlWorkSheet.Cells[k, 4].Formula = "=ROUND($I$8,1)";
                }

                FormatCondition formatOrig = (FormatCondition)(xlWorkSheet.get_Range("B2:B" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlGreater, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatOrig.Interior.Color = 0x000000FF;

                FormatCondition formatCopy = (FormatCondition)(xlWorkSheet.get_Range("C2:C" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlLess, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatCopy.Interior.Color = 0x000000FF;
                xlWorkSheet.Columns[8].ColumnWidth = 20;
                xlWorkSheet.Columns[9].ColumnWidth = 20;

                Excel.Series seriesOrig = seriesCollection.NewSeries();
                seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesOrig.Name = "Original";
                seriesOrig.MarkerSize = 5;
                seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbGreenYellow;
                seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + (qtyOrignal + 1));
                seriesOrig.Values = xlWorkSheet.get_Range(rangeColumn[position] + "2", rangeColumn[position] + (qtyOrignal + 1));


                Excel.Series seriesCopy = seriesCollection.NewSeries();
                seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesCopy.Name = "Copy";
                seriesCopy.MarkerSize = 5;
                seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + (row + 1));
                seriesCopy.Values = xlWorkSheet.get_Range(rangeColumn[position] + (qtyOrignal + 2), rangeColumn[position] + row);

                Excel.Series seriesThreshold = seriesCollection.NewSeries();
                seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesThreshold.Name = "Threshold";
                seriesThreshold.MarkerSize = 5;
                seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                position++;


                if (i != cbbDeviceName.Items.Count - 1)
                {
                    xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                }


            }

            String fileName = txtFolderPath.Text + @"\" + "Graph";

            try
            {
                xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookDefault, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Marshal.ReleaseComObject(xlWorkSheet);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            progressExport.Value = progressExport.Maximum;
            MessageBox.Show("Excel file " + fileName + " has been created.");



            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            progressExport.Visible = false;

        }
    }
}
