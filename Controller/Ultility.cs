﻿using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Version = MiniTool.Data.Version;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MiniTool.Controller
{
    public static class Ultility
    {
        static DatabaseConnection db = new DatabaseConnection();

        public static int FindPosition(List<String> listData, string keyword)
        {
            int position = -1;
            for (int i = 0; i < listData.Count; i++)
            {
                if (listData[i].Contains(keyword))
                {
                    position = i;
                    i = listData.Count;
                }
            }
            return position;
        }
        public static int FindColumn(List<String> headers, string keyword)
        {
            int position = -1;
            for (int i = 0; i < headers.Count; i++)
            {
                if (headers[i].Equals(keyword))
                {
                    position = i;
                    i = headers.Count;
                }
            }
            return position;
        }
        public static List<List<String>> GetCleanData(String path, Product product)
        {
            List<String> listRawData = Ultility.RemoveDoubleQuoteFromRawData(Ultility.GetRawData(path));
            List<List<String>> listData = new List<List<String>>();
            if (listRawData.Count > 0)
            {

                String error = VerifyData(listRawData, path);
                if (error.Length > 0)
                {
                    MessageBox.Show(error);
                }
                else
                {
                    error = VerifyTheSameProduct(listRawData, path);
                    if (error.Length > 0)
                    {
                        MessageBox.Show(error);
                    }
                    else
                    {
                        if (listRawData.Count > 0)
                        {

                            listData.Add(Ultility.ConvertDataStringToList(listRawData[0]));
                            List<int> listZoneNo = GetListZoneNumber(listRawData);
                            for (int i = 1; i < listRawData.Count; i++)
                            {
                                listData.Add(Ultility.ReadDataInLine(listRawData[i], product.listZone, listData[0], listZoneNo));
                            }

                        }
                    }
                }
                return listData;
            }
            return listData;
        }
        public static List<String> GetRawData(String path)
        {
            List<String> listData = new List<String>();
            try
            {
                // StreamReader sr = new StreamReader(path, Encoding.GetEncoding(1250));
                StreamReader sr = new StreamReader(path, System.Text.Encoding.UTF8);

                while (!sr.EndOfStream)
                {
                    string strline = sr.ReadLine();
                    listData.Add(strline);
                }
                sr.Close();
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            return listData;

        }
        public static List<String> RemoveDoubleQuoteFromRawData(List<String> listData)
        {
            List<String> listNewData = new List<String>();
            try
            {
                for (int i = 0; i < listData.Count; i++)
                {
                    string lineWithoutDouble = "";
                    if (listData[i].Split(';')[0].Length != 0)
                    {
                        string[] arrayStrline = listData[i].Split(';');
                        for (int k = 0; k < arrayStrline.Length; k++)
                        {
                            String removedString = arrayStrline[k].Replace("\"", "");
                            if (removedString.Length == 0)
                            {
                                removedString = "\"\"";
                            }
                            if (k == arrayStrline.Length - 1)
                            {
                                lineWithoutDouble += removedString;
                            }
                            else
                            {
                                lineWithoutDouble += removedString + ";";
                            }

                        }
                        listNewData.Add(lineWithoutDouble);

                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            return listNewData;

        }
        public static List<String> ConvertDataStringToList(String data)
        {
            String[] array = data.Split(';');
            List<String> listString = new List<String>();
            for (int i = 0; i < array.Length; i++)
            {
                listString.Add(array[i]);
            }
            return listString;
        }

        public static String VerifyData(List<String> listData, String path)
        {
            String error = "";

            List<String> title = Ultility.ConvertDataStringToList(listData[0]);
            List<int> listZone = new List<int>();
            int zoneNo = Ultility.CountTheZone(listData);
            for (int i = 0; i < listData.Count; i++)
            {
                string[] zones = listData[i].Split(';')[FindColumn(title, "Zone Number")].Split('|');
                for (int k = 0; k < zones.Length; k++)
                {
                    int zone;
                    if (Int32.TryParse(zones[k], out zone))
                    {
                        if (!listZone.Contains(zone))
                        {
                            listZone.Add(zone);
                        }
                    }

                }
            }
            for (int i = 1; i < listData.Count; i++)
            {
                int countValid = 0;
                for (int k = 0; k < zoneNo; k++)
                {
                    if (listData[i].Contains("Zone=" + listZone[k]))
                    {
                        countValid++;
                    }
                }
                if (countValid != zoneNo)
                {
                    error += "The file " + path + " at the row " + (i + 1) + " got issue. Please check the row data.\n";
                }
            }
            return error;
        }

        public static String VerifyTheSameProduct(List<String> listData, String path)
        {
            String error = "";

            if (!isProductGroup(listData))
            {
                List<String> title = Ultility.RemoveDoubleQuoteFromRawData(listData[0].Split(';').ToList());
                int companyID = Int32.Parse(listData[1].Split(';').ToList()[FindColumn(title, "User Company ID")]);
                String companyName = listData[1].Split(';').ToList()[FindColumn(title, "User Company Name")];
                int productID = Int32.Parse(listData[1].Split(';').ToList()[FindColumn(title, "Product ID")]);
                String productName = listData[1].Split(';').ToList()[FindColumn(title, "Product Name")];
                for (int i = 1; i < listData.Count; i++)
                {
                    List<String> data = Ultility.RemoveDoubleQuoteFromRawData(listData[i].Split(';').ToList());
                    if (Int32.Parse(data[FindColumn(title, "User Company ID")]) != companyID || !data[FindColumn(title, "User Company Name")].Equals(companyName) || Int32.Parse(data[FindColumn(title, "Product ID")]) != productID || !data[FindColumn(title, "Product Name")].Equals(productName))
                    {
                        error += "The file " + path + " has more than one product. Please check row " + i;
                        i = listData.Count;
                    }
                }
            }

            return error;
        }

        public static bool isProductGroup(List<String> listData)
        {
            List<String> title = Ultility.RemoveDoubleQuoteFromRawData(listData[0].Split(';').ToList());
            for (int i = 1; i < listData.Count; i++)
            {
                String isGroup = listData[i].Split(';').ToList()[FindColumn(title, "Is Product Group")];
                if (isGroup.ToUpper().Equals("TRUE"))
                {
                    return true;
                }
            }

            return false;
        }


        public static int CountTheZone(List<String> listData)
        {
            int zone = 0;
            for (int i = 1; i < listData.Count; i++)
            {
                String[] spliitedZone = listData[i].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "Zone Number")].Split('|');
                if (spliitedZone.Length - 1 >= zone)
                {
                    zone = spliitedZone.Length - 1;
                }
            }
            return zone;
        }
        public static List<String> ReadDataInLine(String data, List<Zone> listZone, List<String> title, List<int> listZoneNo)
        {
            List<String> hashData = new List<String>();
            List<String> splitData = Ultility.ConvertDataStringToList(data);
            int countSpecial = 0;
            String[] conditions = splitData[Ultility.FindColumn(title, "Is ST Zone")].Split('|');
            //int countZone = conditions.Length - 1;
            int countZone = listZone.Count();
           // int countZone = 1;
            for (int i = 0; i < conditions.Length; i++)
            {

                if (conditions[i].Contains("false"))
                {
                    countSpecial++;
                }
            }
            if (countSpecial == 0)
            {
                int start = -1;
                int end = -1;
                for (int i = 0; i < Ultility.FindPosition(splitData, "Zone=" + listZoneNo[0]) - 1; i++)
                {
                    hashData.Add(splitData[i]);
                }
                for (int i = 0; i < countZone; i++)
                {
                    String debugInfo = "";
                    if (countZone - i != 1)
                    {
                        start = Ultility.FindPosition(splitData, "Zone=" + (listZoneNo[i]));
                        end = Ultility.FindPosition(splitData, "Zone=" + listZoneNo[i + 1]) - 1;
                    }
                    else
                    {
                        start = Ultility.FindPosition(splitData, "Zone=" + (listZoneNo[i]));
                        end = splitData.Count;
                    }
                    for (int k = start; k < end - 1; k++)
                    {
                        if (k == (end - 1))
                        {
                            debugInfo += splitData[k];
                        }
                        else
                        {
                            debugInfo += splitData[k] + ";";
                        }

                    }
                    if (debugInfo.Split(';')[1].Contains("="))
                    {
                        hashData.Add(debugInfo.Split(';')[1].Split('=')[1]);
                        hashData.Add(debugInfo.Replace("\"", "").Trim());
                    }
                    else
                    {
                        hashData.Add(debugInfo.Split(';')[1]);
                        hashData.Add(debugInfo.Replace("\"", "").Trim());
                    }

                }

            }
            else
            {
                int start;
                int end;
                for (int i = 0; i < 46; i++)
                {
                    hashData.Add(splitData[i]);
                }
                String sigma = "";
                for (int i = 46; i < 51; i++)
                {
                    if (i == 50)
                    {
                        sigma += splitData[i];
                    }
                    else
                    {
                        sigma += splitData[i] + ";";
                    }

                }
                hashData.Add(sigma.Replace("\"", "").Trim());
                for (int i = 51; i < 54; i++)
                {
                    hashData.Add(splitData[i]);
                }
                for (int i = 0; i < countZone; i++)
                {
                    String debugInfo = "";
                    if (countZone - i != 1)
                    {
                        start = Ultility.FindPosition(splitData, "Zone=" + listZoneNo[i]);
                        end = Ultility.FindPosition(splitData, "Zone=" + listZoneNo[i + 1]) - 1;
                    }
                    else
                    {
                        start = Ultility.FindPosition(splitData, "Zone=" + listZoneNo[i]);
                        end = splitData.Count;
                    }
                    for (int k = start; k < end - 1; k++)
                    {
                        if (k == (end - 1))
                        {
                            debugInfo += splitData[k];
                        }
                        else
                        {
                            debugInfo += splitData[k] + ";";
                        }
                    }
                    if (listZone[i].method.Equals("STC"))
                    {
                        if (debugInfo.Split(';')[1].Contains("="))
                        {
                            hashData.Add(debugInfo.Split(';')[1].Split('=')[1]);
                        }
                        else
                        {
                            hashData.Add(debugInfo.Split(';')[1]);
                        }

                    }
                    else if (listZone[i].method.Equals("CD1") || listZone[i].method.Equals("CD2") || listZone[i].method.Equals("CD3"))
                    {
                        if (debugInfo.Split(';')[1].Contains("Code="))
                        {

                            hashData.Add(debugInfo.Split(';')[1].Split('=')[1]);
                        }
                        else
                        {
                            hashData.Add(debugInfo.Split(';')[1]);
                        }
                    }
                    else if (listZone[i].method.Equals("CDS"))
                    {
                        List<String> fineSigmaValue = Ultility.ConvertDataStringToList(debugInfo);
                        int fineSigmaPosition = Ultility.FindPosition(fineSigmaValue, "FineSigma");
                        if (fineSigmaPosition != -1)
                        {
                            hashData.Add(fineSigmaValue[fineSigmaPosition].Replace("FineSigma=", ""));
                        }
                        else
                        {
                            hashData.Add("-1");
                        }
                    }
                    else
                    {
                        hashData.Add(debugInfo.Split(';')[1]);
                    }

                    hashData.Add(debugInfo.Replace("\"", "").Trim());
                }
            }


            return hashData;
        }

        public static Product GetProductFromCSV(List<String> listData)
        {
            if (listData.Count > 0)
            {
                listData = Ultility.RemoveDoubleQuoteFromRawData(listData);
                Product product = new Product();
                product.productID = Int32.Parse(listData[1].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "Product ID")]);
                product.productName = listData[1].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "Product Name")];
                product.companyID = Int32.Parse(listData[1].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "User Company ID")]);
                product.companyName = listData[1].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "User Company Name")];
                product = db.GetUniqueProduct(product.productID, product.productName, product.companyID, product.companyName);
                if (product.id == -1)
                {
                    int zoneNo = Ultility.CountTheZone(listData);
                    for (int i = 0; i < zoneNo; i++)
                    {
                        Zone zone = new Zone(i, product.productID, (i + 1), "STC", "0", "C", 0);
                        product.listZone.Add(zone);
                    }
                }
                return product;
            }
            return null;

        }

        public static List<Device> GetUnknownListDevice(List<List<String>> listData)
        {
            List<Device> listUnknownDevice = new List<Device>();
            for (int i = 1; i < listData.Count; i++)
            {
                String code = listData[i][Ultility.FindColumn(listData[0], "Device Model")];
                String email = listData[i][Ultility.FindColumn(listData[0], "User Email")];
                Device device = db.GetDeviceByEmailAndCode(email, code);
                if (device.id == -1)
                {
                    device.email = email;
                    device.code = code;
                    device.location = listData[i][Ultility.FindColumn(listData[0], "Country")];
                    device.type = listData[i][Ultility.FindColumn(listData[0], "Software Name")].Split('-')[0].TrimEnd();
                    device.UDID = "";
                    device.model = db.GetDeviceModelByDeviceID(code);
                    device.isAnonymous = false;
                    if (listUnknownDevice.Where(l => l.email.Equals(device.email) && l.code.Equals(device.code)).ToList().Count == 0)
                    {
                        listUnknownDevice.Add(device);
                    }

                }
            }
            return listUnknownDevice;
        }

        public static List<Device> GetKnownListDevice(List<List<String>> listData)
        {
            List<Device> listKnownDevice = new List<Device>();
            for (int i = 1; i < listData.Count; i++)
            {
                String code = listData[i][Ultility.FindColumn(listData[0], "Device Model")];
                String email = listData[i][Ultility.FindColumn(listData[0], "User Email")];
                Device device = db.GetDeviceByEmailAndCode(email, code);
                if (device.id != -1)

                    if (listKnownDevice.Where(l => l.email.Equals(device.email) && l.code.Equals(device.code)).ToList().Count == 0)
                    {
                        listKnownDevice.Add(device);
                    }

            }

            return listKnownDevice;
        }

        public static List<Zone> GetListZoneFromCSV(List<String> listData)
        {
            Product product = Ultility.GetProductFromCSV(listData);
            if (product.id == -1)
            {
                int zoneNo = Ultility.CountTheZone(listData);
                for (int i = 0; i < zoneNo; i++)
                {
                    Zone zone = new Zone(i, product.productID, (i + 1), "STC", "0", "C", 0);
                    product.listZone.Add(zone);
                }
            }
            return product.listZone;
        }

        public static Version GetVersionFromCSV(List<List<String>> listData)
        {
            Version version = new Version();
            version.os = listData[1][Ultility.FindColumn(listData[0], "Software Name")].Split('-')[0].TrimEnd();
            version.version = listData[1][Ultility.FindColumn(listData[0], "Software Version")]; ;
            version = db.GetVersionByOS_Name(version.os, version.version);
            if (version.id == -1)
            {
                version.os = listData[1][Ultility.FindColumn(listData[0], "Software Name")].Split('-')[0].TrimEnd();
                version.version = listData[1][Ultility.FindColumn(listData[0], "Software Version")]; ;
                db.AddVersion(version.version, version.os);
                version = db.GetVersionByOS_Name(version.os, version.version);
            }
            return version;
        }

        public static Device GetDeviceFromCSV(List<List<String>> listData, int row)
        {
            String code = listData[row][Ultility.FindColumn(listData[0], "Device Model")];
            String email = listData[row][Ultility.FindColumn(listData[0], "User Email")];
            Device device = db.GetDeviceByEmailAndCode(email, code);
            if (device.id == -1)
            {
                device.code = listData[row][Ultility.FindColumn(listData[0], "Device Model")];
                device.email = listData[row][Ultility.FindColumn(listData[0], "User Email")];
                device.location = listData[row][Ultility.FindColumn(listData[0], "Country")];
                device.type = listData[row][Ultility.FindColumn(listData[0], "Software Name")].Split('-')[0].TrimEnd();
                device.UDID = "";
                device.model = db.GetDeviceModelByDeviceID(code);
                device.isAnonymous = false;
            }
            return device;
        }

        public static String GetDeviceIDFromCSV(String path)
        {
            List<String> listRawData = Ultility.RemoveDoubleQuoteFromRawData(Ultility.GetRawData(path));
            if (listRawData.Count > 0)
            {
                String deviceID = "";
                String error = VerifyData(listRawData, path);
                if (error.Length > 0)
                {
                    MessageBox.Show(error);
                }
                else
                {
                    error = VerifyTheSameProduct(listRawData, path);
                    if (error.Length > 0)
                    {
                        MessageBox.Show(error);
                    }
                    else
                    {
                        deviceID = listRawData[1].Split(';')[Ultility.FindColumn(listRawData[0].Split(';').ToList(), "Device Model")].Replace("\"", "");
                    }

                }
                return deviceID;
            }
            else
            {
                return "";
            }

        }

        public static int GetEdgeQtyFromCSV(List<List<String>> listData, int zone)
        {
            int edge = 0;
            for (int i = 1; i < listData.Count; i++)
            {
                String listInfo = listData[i][Ultility.FindColumn(listData[0], "Debug Info Zone " + zone)];
                if (listInfo.Contains("Mov"))
                {
                    List<String> splittedInfo = listInfo.Split(';').ToList();
                    int countChar = splittedInfo[Ultility.FindPosition(splittedInfo, "Mov")].Split('=')[1].Length;
                    if (countChar >= edge)
                    {
                        edge = countChar;
                    }
                }
            }
            return edge;
        }

        public static int GetEdgeQtyFromUploadedData(List<ResultForEvaluation> listData)
        {
            int edge = 0;
            for (int i = 1; i < listData.Count; i++)
            {
                if (listData[i].info.Contains("Mov"))
                {
                    List<String> splittedInfo = listData[i].info.Split(';').ToList();
                    int countChar = splittedInfo[Ultility.FindPosition(splittedInfo, "Mov")].Split('=')[1].Length;
                    if (countChar >= edge)
                    {
                        edge = countChar;
                    }
                }
            }
            return edge;
        }

        public static sTCEvaluationDetail GetsTCEvaluationDetail(String statisticID, int row, String info, int edgeNo)
        {
            sTCEvaluationDetail stcEvaluationDetail = new sTCEvaluationDetail();
            List<String> splittedInfo = info.Split(';').ToList();
            stcEvaluationDetail.statisticID = statisticID;

            if (Ultility.FindPosition(splittedInfo, "Zone") != -1)
            {
                int zone;
                if (Int32.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "Zone")].Split('=')[1], out zone))
                {
                    stcEvaluationDetail.zone = zone;
                    if (Ultility.FindPosition(splittedInfo, "Cal") != -1)
                    {
                        double cal;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "Cal")].Split('=')[1], out cal))
                        {
                            stcEvaluationDetail.cal = Int32.Parse(Math.Round(cal, 0).ToString());
                        }
                        else
                        {
                            stcEvaluationDetail.cal = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "StdDev") != -1)
                    {
                        double stdDev;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "StdDev")].Split('=')[1], out stdDev))
                        {
                            stcEvaluationDetail.stdDev = Int32.Parse(Math.Round(stdDev, 0).ToString());
                        }
                        else
                        {
                            stcEvaluationDetail.stdDev = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "NoRL") != -1)
                    {
                        double NoRL;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "NoRL")].Split('=')[1], out NoRL))
                        {
                            stcEvaluationDetail.noRL = Int32.Parse(Math.Round(NoRL, 0).ToString());
                        }
                        else
                        {
                            stcEvaluationDetail.noRL = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "NoPL") != -1)
                    {
                        double NoPL;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "NoPL")].Split('=')[1], out NoPL))
                        {
                            stcEvaluationDetail.noPL = Int32.Parse(Math.Round(NoPL, 0).ToString());
                        }
                        else
                        {
                            stcEvaluationDetail.noPL = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "NoVL") != -1)
                    {
                        double NoVL;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "NoVL")].Split('=')[1], out NoVL))
                        {
                            stcEvaluationDetail.noVL = Int32.Parse(Math.Round(NoVL, 0).ToString());
                        }
                        else
                        {
                            stcEvaluationDetail.noVL = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "NoTL") != -1)
                    {
                        double NoTL;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "NoTL")].Split('=')[1], out NoTL))
                        {
                            stcEvaluationDetail.noTL = Int32.Parse(Math.Round(NoTL, 0).ToString());
                        }
                        else
                        {
                            stcEvaluationDetail.noTL = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "BlurScore") != -1)
                    {
                        double BlurScore;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "BlurScore")].Split('=')[1], out BlurScore))
                        {
                            stcEvaluationDetail.blurScore = BlurScore;
                        }
                        else
                        {
                            stcEvaluationDetail.blurScore = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "ErrorOnFit") != -1)
                    {
                        double ErrorOnFit;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "ErrorOnFit")].Split('=')[1], out ErrorOnFit))
                        {
                            stcEvaluationDetail.errorOnFit = ErrorOnFit;
                        }
                        else
                        {
                            stcEvaluationDetail.errorOnFit = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "Angle") != -1)
                    {
                        double Angle;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "Angle")].Split('=')[1], out Angle))
                        {
                            stcEvaluationDetail.angle = Angle;
                        }
                        else
                        {
                            stcEvaluationDetail.angle = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "ShiftThreshold") != -1)
                    {
                        double ShiftThreshold;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "ShiftThreshold")].Split('=')[1], out ShiftThreshold))
                        {
                            stcEvaluationDetail.shiftThreshold = ShiftThreshold;
                        }
                        else
                        {
                            stcEvaluationDetail.shiftThreshold = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "QualityCheckMitigation") != -1)
                    {
                        double QualityCheckMitigation;
                        if (Double.TryParse(splittedInfo[Ultility.FindPosition(splittedInfo, "QualityCheckMitigation")].Split('=')[1], out QualityCheckMitigation))
                        {
                            stcEvaluationDetail.mitigation = QualityCheckMitigation;
                        }
                        else
                        {
                            stcEvaluationDetail.mitigation = 0;
                        }
                    }
                    if (Ultility.FindPosition(splittedInfo, "V=") != -1)
                    {

                        stcEvaluationDetail.v = splittedInfo[Ultility.FindPosition(splittedInfo, "V=")].Split('=')[1];
                    }


                    stcEvaluationDetail.listEdge = Ultility.GetListEdge(edgeNo, info);
                }
                else
                {
                    MessageBox.Show("The row " + row + "got data issue");
                    stcEvaluationDetail.isValid = false;
                }
            }
            else
            {
                MessageBox.Show("The row " + row + "got data issue");
                stcEvaluationDetail.isValid = false;

            }

            return stcEvaluationDetail;
        }

        public static List<EdgeDetail> GetListEdge(int edgeNo, String info)
        {
            List<EdgeDetail> listEdge = new List<EdgeDetail>();
            List<String> edgeDetail = info.Split(';').ToList();
            for (int i = 1; i <= edgeNo; i++)
            {

                if (info.Contains("E" + i))
                {
                    int position = Ultility.FindPosition(edgeDetail, "E" + i);
                    EdgeDetail edge = new EdgeDetail(Int32.Parse(edgeDetail[position].Split('=')[1]), Int32.Parse(edgeDetail[position + 1].Split('=')[1]) * (-1), Int32.Parse(edgeDetail[position + 1].Split('=')[1]), edgeDetail[position + 3].Split('=')[1], Int32.Parse(edgeDetail[position + 2].Split('=')[1]));
                    edge.edgeName = "E" + i;
                    listEdge.Add(edge);
                }
                else
                {
                    EdgeDetail edge = new EdgeDetail(0, 0, 0, "0,00", 0);
                    edge.edgeName = "E" + i;
                    listEdge.Add(edge);
                }
            }

            return listEdge;
        }

        public static List<DataDrawChart> GetListDataDrawChartSTC(List<ResultForEvaluation> listDrawChart, int edgeQty, int edgeNo)
        {
            List<DataDrawChart> listChart = new List<DataDrawChart>();
            List<sTCEvaluationDetail> listEvaluationDetail = new List<sTCEvaluationDetail>();
            for (int i = 0; i < listDrawChart.Count; i++)
            {
                listEvaluationDetail.Add(Ultility.GetsTCEvaluationDetail(listDrawChart[i].statisticID, i + 1, listDrawChart[i].info, edgeQty));
            }
            for (int i = 0; i < listEvaluationDetail.Count; i++)
            {
                DataDrawChart data = new DataDrawChart();
                data.statisticID = listEvaluationDetail[i].statisticID;
                data.cal = listEvaluationDetail[i].cal;
                data.center = 0;
                data.shiftingNegative = listEvaluationDetail[i].listEdge[edgeNo].shiftingNegative;
                data.shiftingPositive = listEvaluationDetail[i].listEdge[edgeNo].shiftingPositive;
                data.eTriggerNegative = listEvaluationDetail[i].listEdge[edgeNo].eTriggerNegative;
                data.eTriggerPositive = listEvaluationDetail[i].listEdge[edgeNo].eTriggerPositive;
                data.e = listEvaluationDetail[i].listEdge[edgeNo].e;
                listChart.Add(data);
            }
            return listChart;

        }

        public static List<DataDrawChart> GetListDataDrawChartCDS(List<ResultForEvaluation> listDrawChart, int zone)
        {
            List<DataDrawChart> listChart = new List<DataDrawChart>();

            for (int i = 0; i < listDrawChart.Count; i++)
            {
                if (listDrawChart[i].zone == zone)
                {
                    DataDrawChart data = new DataDrawChart();
                    data.statisticID = listDrawChart[i].statisticID;
                    data.pointValue = Double.Parse(listDrawChart[i].value);
                    listChart.Add(data);
                }

            }
            return listChart;

        }

        public static void DrawChartSTC(Chart chartEvaluation, DataGridView dgvData, List<ResultForEvaluation> listDrawChart, int zone, int edgeQty, int edgeNo)
        {
            List<DataDrawChart> listChart = Ultility.GetListDataDrawChartSTC(Ultility.GetResultForEvaluationByZone(listDrawChart, zone), edgeQty, edgeNo);

            chartEvaluation.ResetAutoValues();
            chartEvaluation.Series.Clear();
            chartEvaluation.Legends[0].Docking = Docking.Bottom;



            chartEvaluation.MinimumSize = new Size(1200, 500);
            chartEvaluation.Width = listChart.Count() * 10;
            dgvData.DataSource = listChart;
            dgvData.Columns[0].Width = 50;
            dgvData.Columns[0].HeaderText = "Statistic ID";
            dgvData.Columns[1].Width = 50;
            dgvData.Columns[1].HeaderText = "Center";
            dgvData.Columns[2].Width = 50;
            dgvData.Columns[2].HeaderText = "Trigger 1";
            dgvData.Columns[3].Width = 50;
            dgvData.Columns[3].HeaderText = "Trigger 2";
            dgvData.Columns[4].Width = 50;
            dgvData.Columns[4].HeaderText = "Shifting 1";
            dgvData.Columns[5].Width = 50;
            dgvData.Columns[5].HeaderText = "Shifting 2";
            dgvData.Columns[6].Width = 50;
            dgvData.Columns[6].HeaderText = "Cal";
            dgvData.Columns[7].Width = 50;
            dgvData.Columns[7].HeaderText = "E" + (edgeNo + 1);
            var seriesCenter = new Series();
            seriesCenter.ChartType = SeriesChartType.Line;
            seriesCenter.Name = "Center";
            seriesCenter.BorderWidth = 3;
            seriesCenter.Color = Color.Gray;

            chartEvaluation.Series.Add(seriesCenter);

            for (int i = 0; i < listChart.Count(); i++)
            {
                chartEvaluation.Series[0].Points.AddXY(listChart[i].statisticID, listChart[i].center);
            }


            var seriesTrigger1 = new Series();
            seriesTrigger1.ChartType = SeriesChartType.Line;
            seriesTrigger1.Name = "Trigger1";
            seriesTrigger1.BorderWidth = 3;
            seriesTrigger1.Color = Color.Green;
            chartEvaluation.Series.Add(seriesTrigger1);
            for (int i = 0; i < listChart.Count(); i++)
            {
                chartEvaluation.Series[1].Points.AddXY(listChart[i].statisticID, listChart[i].eTriggerPositive);
            }



            var seriesTrigger2 = new Series();
            seriesTrigger2.ChartType = SeriesChartType.Line;
            seriesTrigger2.Name = "Trigger2";
            seriesTrigger2.BorderWidth = 3;
            seriesTrigger2.Color = Color.Green;
            chartEvaluation.Series.Add(seriesTrigger2);
            for (int i = 0; i < listChart.Count(); i++)
            {
                chartEvaluation.Series[2].Points.AddXY(listChart[i].statisticID, listChart[i].eTriggerNegative);
            }


            var seriesShifting1 = new Series();
            seriesShifting1.ChartType = SeriesChartType.Line;
            seriesShifting1.Name = "Shifting1";
            seriesShifting1.BorderWidth = 3;
            seriesShifting1.Color = Color.Yellow;
            chartEvaluation.Series.Add(seriesShifting1);
            for (int i = 0; i < listChart.Count(); i++)
            {
                chartEvaluation.Series[3].Points.AddXY(listChart[i].statisticID, listChart[i].shiftingPositive);
            }


            var seriesShifting2 = new Series();
            seriesShifting2.ChartType = SeriesChartType.Line;
            seriesShifting2.Name = "Shifting2";
            seriesShifting2.BorderWidth = 3;
            seriesShifting2.Color = Color.Yellow;
            chartEvaluation.Series.Add(seriesShifting2);
            for (int i = 0; i < listChart.Count(); i++)
            {
                chartEvaluation.Series[4].Points.AddXY(listChart[i].statisticID, listChart[i].shiftingNegative);
            }


            var seriesCal = new Series();
            seriesCal.ChartType = SeriesChartType.Line;
            seriesCal.Name = "Cal";
            seriesCal.BorderWidth = 3;
            seriesCal.Color = Color.Red;
            chartEvaluation.Series.Add(seriesCal);
            for (int i = 0; i < listChart.Count(); i++)
            {
                chartEvaluation.Series[5].Points.AddXY(listChart[i].statisticID, listChart[i].cal);
            }


            var seriesE = new Series();
            seriesE.ChartType = SeriesChartType.Line;
            seriesE.Name = "E";
            seriesE.BorderWidth = 3;
            seriesE.Color = Color.DarkBlue;
            seriesE.IsValueShownAsLabel = true;
            seriesE.SmartLabelStyle.Enabled = true;
            chartEvaluation.Series.Add(seriesE);
            for (int i = 0; i < listChart.Count(); i++)
            {
                chartEvaluation.Series[6].Points.AddXY(listChart[i].statisticID, listChart[i].e);
            }
        }

        public static void DrawChartCDS(Chart chartEvaluation, DataGridView dgvData, List<ResultForEvaluation> listDrawChart, int zone)
        {
            List<DataDrawChart> listChart = Ultility.GetListDataDrawChartCDS(Ultility.GetResultForEvaluationByZone(listDrawChart, zone), zone);

            chartEvaluation.ResetAutoValues();
            chartEvaluation.Series.Clear();
            chartEvaluation.Legends[0].Docking = Docking.Bottom;
            dgvData.DataSource = null;

            chartEvaluation.MinimumSize = new Size(1200, 500);
            chartEvaluation.Width = listChart.Count * 10;

            DataTable dt = new DataTable();
            dt.Columns.Add("Statistic ID");
            dt.Columns.Add("CDS Value");
            for (int i = 0; i < listChart.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = listChart[i].statisticID;
                dt.Rows[dt.Rows.Count - 1][1] = listChart[i].pointValue;
            }
            dgvData.DataSource = dt;
            Random random = new Random();
            var series = new Series();
            series.Name = "Zone " + zone;
            series.ChartType = SeriesChartType.Point;
            series.IsValueShownAsLabel = true;
            series.MarkerSize = 8;
            series.MarkerStyle = MarkerStyle.Circle;
            series.Color = Color.FromArgb(random.Next(200, 255), random.Next(0, 255), random.Next(0, 255));
            chartEvaluation.Series.Add(series);

            for (int j = 0; j < listChart.Count; j++)
            {
                chartEvaluation.Series[0].Points.AddXY(listChart[j].statisticID, listChart[j].pointValue);
            }
        }
        public static List<ResultForEvaluation> GetResultForEvaluationByZone(List<ResultForEvaluation> listResultForEvaluation, int zone)
        {
            List<ResultForEvaluation> newListResult = new List<ResultForEvaluation>();
            foreach (ResultForEvaluation result in listResultForEvaluation)
            {
                if (result.zone == zone)
                {
                    newListResult.Add(result);
                }
            }
            return newListResult;
        }

        public static DataTable GetDtCSVSheetSTCZone(List<ResultForEvaluation> listSave, int zone)
        {
            DataTable dt = new DataTable();
            ResultForEvaluation evaluation = new ResultForEvaluation();
            for (int i = 0; i < listSave.Count; i++)
            {
                if (Int32.Parse(listSave[i].value) != -1)
                {
                    evaluation = listSave[i];
                    i = listSave.Count;
                }
            }
            for (int i = 0; i < 300; i++)
            {
                dt.Columns.Add();
            }
            int column;

            dt.Rows.Add();
            dt.Rows[0][0] = "Statistic ID";
            dt.Rows[0][1] = "Date and Time";
            dt.Rows[0][2] = "User Email";
            dt.Rows[0][3] = "User Firstname";
            dt.Rows[0][4] = "Product Name";
            dt.Rows[0][5] = "Result Text";
            dt.Rows[0][6] = "Zone Result " + zone;
            dt.Rows[0][7] = "Debug Info " + zone;

            for (int i = 0; i < listSave.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = listSave[i].statisticID;
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].createdDate;
                dt.Rows[dt.Rows.Count - 1][2] = listSave[i].deviceEmail;
                dt.Rows[dt.Rows.Count - 1][3] = listSave[i].deviceModel;
                dt.Rows[dt.Rows.Count - 1][4] = listSave[i].version;
                dt.Rows[dt.Rows.Count - 1][5] = listSave[i].productName;
                dt.Rows[dt.Rows.Count - 1][6] = listSave[i].resultText;
                dt.Rows[dt.Rows.Count - 1][7] = listSave[i].value;

                String[] infos = listSave[i].info.Split(';');
                column = 8;
                for (int j = 0; j < infos.Length - 1; j++)
                {
                    //dt.Columns.Add();
                    //dt.Columns.Add();
                    dt.Rows[dt.Rows.Count - 1][column] = infos[j].Split('=')[0];
                    dt.Rows[dt.Rows.Count - 1][column + 1] = infos[j].Split('=')[1];
                    column += 2;
                }
            }

            return dt;

        }

        public static DataTable GetDtEvaluationSheetSTCZone(List<ResultForEvaluation> listSave, int edgeQty)
        {
            DataTable dt = new DataTable();
            List<sTCEvaluationDetail> listEvaluationDetail = new List<sTCEvaluationDetail>();
            int neededColumn = edgeQty * 5 + 11;
            for (int i = 0; i < neededColumn; i++)
            {
                dt.Columns.Add();
            }
            for (int i = 0; i < listSave.Count; i++)
            {
                listEvaluationDetail.Add(Ultility.GetsTCEvaluationDetail(listSave[i].statisticID, i + 1, listSave[i].info, edgeQty));
            }
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "No.";
            dt.Rows[0][1] = "Index";
            dt.Rows[0][2] = "Time";
            dt.Rows[0][3] = "Code";
            dt.Rows[0][4] = "Mov";
            dt.Rows[0][5] = "Zone Value";
            dt.Rows[0][6] = "Cal";
            dt.Rows[0][7] = "StdDev";
            dt.Rows[0][8] = "NoRL";
            dt.Rows[0][9] = "NoVL";
            dt.Rows[0][10] = "NoTL";

            int column = 11;
            for (int i = 0; i < edgeQty; i++)
            {
                dt.Rows[0][column] = "E" + (i + 1);
                dt.Rows[0][column + 1] = "E" + (i + 1) + " Trigger";
                dt.Rows[0][column + 2] = "E" + (i + 1) + " Trigger";
                dt.Rows[0][column + 3] = "E" + (i + 1) + " Mov";
                dt.Rows[0][column + 4] = " Shifting";
                column += 5;
            }

            for (int i = 0; i < listSave.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].statisticID;
                dt.Rows[dt.Rows.Count - 1][2] = listSave[i].createdDate;
                dt.Rows[dt.Rows.Count - 1][3] = listSave[i].value;
                if (listSave[i].info.Split(';').Length > 3)
                {
                    dt.Rows[dt.Rows.Count - 1][4] = listSave[i].info.Split(';')[2].Split('=')[1];
                }
                else
                {
                    dt.Rows[dt.Rows.Count - 1][4] = -1;
                }
                dt.Rows[dt.Rows.Count - 1][5] = listSave[i].resultText;
                dt.Rows[dt.Rows.Count - 1][6] = listEvaluationDetail[i].cal;
                dt.Rows[dt.Rows.Count - 1][7] = listEvaluationDetail[i].stdDev;
                dt.Rows[dt.Rows.Count - 1][8] = listEvaluationDetail[i].noRL;
                dt.Rows[dt.Rows.Count - 1][9] = listEvaluationDetail[i].noVL; ;
                dt.Rows[dt.Rows.Count - 1][10] = listEvaluationDetail[i].noTL;
                column = 11;
                for (int k = 0; k < listEvaluationDetail[i].listEdge.Count; k++)
                {
                    dt.Rows[dt.Rows.Count - 1][column] = listEvaluationDetail[i].listEdge[k].e;
                    dt.Rows[dt.Rows.Count - 1][column + 1] = listEvaluationDetail[i].listEdge[k].eTriggerNegative;
                    dt.Rows[dt.Rows.Count - 1][column + 2] = listEvaluationDetail[i].listEdge[k].eTriggerPositive;
                    dt.Rows[dt.Rows.Count - 1][column + 3] = listEvaluationDetail[i].listEdge[k].eMov;
                    dt.Rows[dt.Rows.Count - 1][column + 4] = listEvaluationDetail[i].listEdge[k].shiftingPositive;
                    column += 5;
                }

            }

            return dt;
        }

        public static DataTable GetDtCalibrationSheetSTCZone(List<ResultForEvaluation> listSave, int edgeQty)
        {
            DataTable dt = new DataTable();
            List<sTCEvaluationDetail> listEvaluationDetail = new List<sTCEvaluationDetail>();
            for (int i = 0; i < listSave.Count; i++)
            {
                listEvaluationDetail.Add(Ultility.GetsTCEvaluationDetail(listSave[i].statisticID, i + 1, listSave[i].info, edgeQty));
            }
            for (int i = 0; i < 11; i++)
            {
                dt.Columns.Add();
            }
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "No.";
            dt.Rows[0][1] = "Index";
            dt.Rows[0][2] = "Date and Time";
            dt.Rows[0][3] = "Result";
            dt.Rows[0][4] = "Zone Result";
            dt.Rows[0][5] = "Center";
            dt.Rows[0][6] = "StdDev";
            dt.Rows[0][7] = "NoRL";
            dt.Rows[0][8] = "NoVL";
            dt.Rows[0][9] = "NoTL";
            dt.Rows[0][10] = "Cal";

            for (int i = 0; i < listSave.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].statisticID;
                dt.Rows[dt.Rows.Count - 1][2] = listSave[i].createdDate;
                dt.Rows[dt.Rows.Count - 1][3] = listSave[i].resultText;
                dt.Rows[dt.Rows.Count - 1][4] = listSave[i].value;
                dt.Rows[dt.Rows.Count - 1][5] = 0;
                dt.Rows[dt.Rows.Count - 1][6] = listEvaluationDetail[i].stdDev;
                dt.Rows[dt.Rows.Count - 1][7] = listEvaluationDetail[i].noRL;
                dt.Rows[dt.Rows.Count - 1][8] = listEvaluationDetail[i].noVL;
                dt.Rows[dt.Rows.Count - 1][9] = listEvaluationDetail[i].noTL;
                dt.Rows[dt.Rows.Count - 1][10] = listEvaluationDetail[i].cal;
            }

            return dt;

        }

        public static DataTable GetDtEdge(List<ResultForEvaluation> listSave, int edgeQty, int edge)
        {
            DataTable dt = new DataTable();
            List<sTCEvaluationDetail> listEvaluationDetail = new List<sTCEvaluationDetail>();
            for (int i = 0; i < listSave.Count; i++)
            {
                listEvaluationDetail.Add(Ultility.GetsTCEvaluationDetail(listSave[i].statisticID, i + 1, listSave[i].info, edgeQty));
            }
            for (int i = 0; i < 11; i++)
            {
                dt.Columns.Add();
            }
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "No.";
            dt.Rows[0][1] = "Index";
            dt.Rows[0][2] = "Result";
            dt.Rows[0][3] = "Zone Result";
            dt.Rows[0][4] = "Center";
            dt.Rows[0][5] = "Trigger 1";
            dt.Rows[0][6] = "Trigger 2";
            dt.Rows[0][7] = "Shifting 1";
            dt.Rows[0][8] = "Shifting 2";
            dt.Rows[0][9] = "Cal";
            dt.Rows[0][10] = "E" + (edge + 1);

            for (int i = 0; i < listSave.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].statisticID;
                dt.Rows[dt.Rows.Count - 1][2] = listSave[i].resultText;
                dt.Rows[dt.Rows.Count - 1][3] = listSave[i].value;
                dt.Rows[dt.Rows.Count - 1][4] = 0;
                if (listEvaluationDetail[i].listEdge.Count > 0)
                {
                    dt.Rows[dt.Rows.Count - 1][5] = listEvaluationDetail[i].listEdge[edge].eTriggerPositive;
                    dt.Rows[dt.Rows.Count - 1][6] = listEvaluationDetail[i].listEdge[edge].eTriggerNegative;
                    dt.Rows[dt.Rows.Count - 1][7] = listEvaluationDetail[i].listEdge[edge].shiftingPositive;
                    dt.Rows[dt.Rows.Count - 1][8] = listEvaluationDetail[i].listEdge[edge].shiftingNegative;
                    dt.Rows[dt.Rows.Count - 1][9] = listEvaluationDetail[i].cal;
                    dt.Rows[dt.Rows.Count - 1][10] = listEvaluationDetail[i].listEdge[edge].e;
                }

            }

            return dt;
        }

        public static object[,] ParseDtToArray(DataTable dt)
        {
            object[,] array = new object[dt.Rows.Count, dt.Columns.Count];
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    array[k, j] = dt.Rows[k][j];
                }

            }
            return array;
        }

        public static void ExportSTCZoneEvaluation(ProgressBar progressBar, List<ResultForEvaluation> listSave, int zone, int edgeQty, string fileName)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);

            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            xlWorkSheet.Name = "CSV";


            DataTable dt = Ultility.GetDtCSVSheetSTCZone(listSave, zone);

            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = Ultility.ParseDtToArray(dt);
            Excel.Range range = xlWorkSheet.Rows[1];
            range.Select();
            range.Font.Bold = true;
            range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
            xlWorkSheet.Name = "Evaluation";
            dt = Ultility.GetDtEvaluationSheetSTCZone(listSave, edgeQty);
            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = Ultility.ParseDtToArray(dt);
            range = xlWorkSheet.Rows[1];
            range.Select();
            range.Font.Bold = true;
            range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;


            xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(3);
            xlWorkSheet.Name = "Calibration";



            dt = Ultility.GetDtCalibrationSheetSTCZone(listSave, edgeQty);
            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = Ultility.ParseDtToArray(dt);
            range = xlWorkSheet.Rows[1];
            range.Select();
            range.Font.Bold = true;
            range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            Excel.ChartObjects xlChart = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
            Excel.ChartObject myChart = (Excel.ChartObject)xlChart.Add(600, 15, 700, 425);
            Excel.Chart chartPage = myChart.Chart;

            chartPage.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;

            myChart.Select();

            chartPage.ChartType = Excel.XlChartType.xlLineMarkers;
            Excel.SeriesCollection seriesCollection = chartPage.SeriesCollection();
            int row = listSave.Count() + 1;
            Excel.Range XVal_Range = xlWorkSheet.get_Range("B2", "B" + row);
            Excel.Axis xAxis = (Excel.Axis)chartPage.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.CategoryNames = XVal_Range;
            Excel.Series series1 = seriesCollection.NewSeries();

            series1.Name = "Center";
            series1.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
            series1.XValues = xlWorkSheet.get_Range("B2", "B" + row);
            series1.Values = xlWorkSheet.get_Range("F2", "F" + row);

            Excel.Series series2 = seriesCollection.NewSeries();

            series2.Name = "StdDev";
            series2.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
            series2.XValues = xlWorkSheet.get_Range("B2", "B" + row);
            series2.Values = xlWorkSheet.get_Range("G2", "G" + row);

            Excel.Series series3 = seriesCollection.NewSeries();

            series3.Name = "NoRL";
            series3.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
            series3.XValues = xlWorkSheet.get_Range("B2", "B" + row);
            series3.Values = xlWorkSheet.get_Range("H2", "H" + row);

            Excel.Series series4 = seriesCollection.NewSeries();

            series4.Name = "NoVL";
            series4.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
            series4.XValues = xlWorkSheet.get_Range("B2", "B" + row);
            series4.Values = xlWorkSheet.get_Range("I2", "I" + row);

            Excel.Series series5 = seriesCollection.NewSeries();

            series5.Name = "NoTL";
            series5.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
            series5.XValues = xlWorkSheet.get_Range("B2", "B" + row);
            series5.Values = xlWorkSheet.get_Range("J2", "J" + row);

            Excel.Series series6 = seriesCollection.NewSeries();
            series6.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
            series6.Name = "Cal";
            series6.Border.Color = Color.Red;
            series6.MarkerSize = 5;
            series6.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
            series6.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
            series6.XValues = xlWorkSheet.get_Range("B2", "B" + row);
            series6.Values = xlWorkSheet.get_Range("K2", "K" + row);


            progressBar.Maximum = edgeQty;
            for (int i = 0; i < edgeQty; i++)
            {
                progressBar.Value = i + 1;
                xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                xlWorkSheet.Name = "E" + (i + 1);
                DataTable dtEdge = Ultility.GetDtEdge(listSave, edgeQty, i);
                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtEdge.Rows.Count, dtEdge.Columns.Count]].Value = Ultility.ParseDtToArray(dtEdge);
                range = xlWorkSheet.Rows[1];
                range.Select();
                range.Font.Bold = true;
                range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                Excel.ChartObjects xlChartEdge = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
                Excel.ChartObject myChartEdge = (Excel.ChartObject)xlChartEdge.Add(630, 15, 700, 425);
                Excel.Chart chartPageEdge = myChartEdge.Chart;

                chartPageEdge.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;
                myChartEdge.Select();
                chartPageEdge.HasTitle = true;
                chartPageEdge.ChartTitle.Text = "E" + (i + 1);

                chartPageEdge.ChartType = Excel.XlChartType.xlLine;
                Excel.SeriesCollection seriesCollectionEdge = chartPageEdge.SeriesCollection();
                int rowEdge = listSave.Count() + 1;

                Excel.Series seriesCenter = seriesCollectionEdge.NewSeries();
                seriesCenter.Name = "Center";
                seriesCenter.XValues = xlWorkSheet.get_Range("B2", "B" + row);
                seriesCenter.Values = xlWorkSheet.get_Range("E2", "E" + row);
                seriesCenter.Border.Color = Color.LightGray;

                Excel.Series seriesTrigger1 = seriesCollectionEdge.NewSeries();
                seriesTrigger1.Name = "Trigger 1";
                seriesTrigger1.XValues = xlWorkSheet.get_Range("B2", "B" + row);
                seriesTrigger1.Values = xlWorkSheet.get_Range("F2", "F" + row);
                seriesTrigger1.Border.Color = Color.FromArgb(33, 191, 115);

                Excel.Series seriesTrigger2 = seriesCollectionEdge.NewSeries();
                seriesTrigger2.Name = "Trigger 2";
                seriesTrigger2.XValues = xlWorkSheet.get_Range("B2", "B" + row);
                seriesTrigger2.Values = xlWorkSheet.get_Range("G2", "G" + row);
                seriesTrigger2.Border.Color = Color.FromArgb(33, 191, 115);

                Excel.Series seriesShifting1 = seriesCollectionEdge.NewSeries();
                seriesShifting1.Name = "Shifting 1";
                seriesShifting1.XValues = xlWorkSheet.get_Range("B2", "B" + row);
                seriesShifting1.Values = xlWorkSheet.get_Range("H2", "H" + row);
                seriesShifting1.Border.Color = Color.FromArgb(253, 219, 52);

                Excel.Series seriesShifting2 = seriesCollectionEdge.NewSeries();
                seriesShifting2.Name = "Shifting 2";
                seriesShifting2.XValues = xlWorkSheet.get_Range("B2", "B" + row);
                seriesShifting2.Values = xlWorkSheet.get_Range("I2", "I" + row);
                seriesShifting2.Border.Color = Color.FromArgb(253, 219, 52);

                Excel.Series seriesCal = seriesCollectionEdge.NewSeries();
                seriesCal.Name = "Cal";
                seriesCal.XValues = xlWorkSheet.get_Range("B2", "B" + row);
                seriesCal.Values = xlWorkSheet.get_Range("J2", "J" + row);
                seriesCal.Border.Color = Color.FromArgb(214, 52, 71);

                Excel.Series seriesEdge = seriesCollectionEdge.NewSeries();
                seriesEdge.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesEdge.Name = "E" + (i + 1);
                seriesEdge.Border.Color = Color.FromArgb(73, 76, 162);
                seriesEdge.MarkerSize = 5;
                seriesEdge.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbBlueViolet;
                seriesEdge.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbBlueViolet;
                seriesEdge.XValues = xlWorkSheet.get_Range("B2", "B" + row);
                seriesEdge.Values = xlWorkSheet.get_Range("K2", "K" + row);

            }


            try
            {
                xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookDefault, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            if (System.IO.File.Exists(fileName))
            {
                try
                {
                    System.IO.File.Copy(fileName, fileName + ".xlsx", true);
                    System.IO.File.Delete(fileName);
                    MessageBox.Show("Excel file " + fileName + " has been created.");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            progressBar.Visible = false;
            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
        }

        public static void ExportCDSZoneEvaluation(ProgressBar progressBar, List<ResultForEvaluation> listSave, int zone, string fileName)
        {

            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(Missing.Value);

            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            List<DataDrawChart> listChart = Ultility.GetListExportCDS(listSave, zone);

            DataTable dt = new DataTable();
            dt.Columns.Add("No.");
            dt.Columns.Add("Index");
            dt.Columns.Add("CDS Value");

            dt.Rows.Add();
            dt.Rows[dt.Rows.Count - 1][0] = "No.";
            dt.Rows[dt.Rows.Count - 1][1] = "Index";
            dt.Rows[dt.Rows.Count - 1][2] = "CDS Value";

            for (int i = 0; i < listChart.Count; i++)
            {
                dt.Rows.Add();
                dt.Rows[dt.Rows.Count - 1][0] = (i + 1);
                dt.Rows[dt.Rows.Count - 1][1] = listSave[i].statisticID;
                dt.Rows[dt.Rows.Count - 1][2] = listChart[i].pointValue;

            }

            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = Ultility.ParseDtToArray(dt);
            Excel.Range range = xlWorkSheet.Rows[1];
            range.Select();
            range.Font.Bold = true;
            range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;


            Excel.ChartObjects xlChartEdge = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
            Excel.ChartObject myChartEdge = (Excel.ChartObject)xlChartEdge.Add(250, 15, 700, 425);
            Excel.Chart chartPageEdge = myChartEdge.Chart;

            chartPageEdge.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;
            myChartEdge.Select();

            chartPageEdge.ChartType = Excel.XlChartType.xlXYScatter;
            Excel.SeriesCollection seriesCollectionEdge = chartPageEdge.SeriesCollection();

            Excel.Series seriesCenter = seriesCollectionEdge.NewSeries();
            seriesCenter.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
            seriesCenter.MarkerSize = 5;
            seriesCenter.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbDarkOrange;
            seriesCenter.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbDarkOrange;
            seriesCenter.Name = "CDS";
            seriesCenter.XValues = xlWorkSheet.get_Range("A2", "A" + listChart.Count);
            seriesCenter.Values = xlWorkSheet.get_Range("C2", "C" + listChart.Count);

            try
            {
                xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookDefault, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            xlWorkBook.Close(true, Missing.Value, Missing.Value);
            xlApp.Quit();

            if (System.IO.File.Exists(fileName))
            {
                try
                {
                    System.IO.File.Copy(fileName, fileName + ".xlsx", true);
                    System.IO.File.Delete(fileName);
                    MessageBox.Show("Excel file " + fileName + " has been created.");


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            progressBar.Visible = false;
            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

        }

        public static List<DataDrawChart> GetListExportCDS(List<ResultForEvaluation> listSave, int zone)
        {
            List<DataDrawChart> listDrawChart = new List<DataDrawChart>();
            foreach (ResultForEvaluation eva in listSave)
            {
                if (eva.zone == zone)
                {
                    if (Double.Parse(eva.value) == -1 && !eva.info.Contains("FineSigma"))
                    {
                        DataDrawChart data = new DataDrawChart();
                        data.statisticID = eva.statisticID;
                        if (Ultility.GetResultForEvaluation(listSave, eva.statisticID, (zone + 1)) != null)
                        {
                            data.pointValue = Double.Parse(Ultility.GetResultForEvaluation(listSave, eva.statisticID, (zone + 1)).value);
                        }
                        else
                        {
                            data.pointValue = -1;
                        }

                        listDrawChart.Add(data);
                    }
                    else
                    {
                        DataDrawChart data = new DataDrawChart();
                        data.statisticID = eva.statisticID;
                        data.pointValue = Double.Parse(eva.value);
                        listDrawChart.Add(data);
                    }

                }
            }
            return listDrawChart;
        }

        public static ResultForEvaluation GetResultForEvaluation(List<ResultForEvaluation> listSave, String statisticID, int zone)
        {
            foreach (ResultForEvaluation eva in listSave)
            {
                if (eva.statisticID.Equals(statisticID) && eva.zone == zone)
                {
                    return eva;
                }
            }
            return null;
        }
        public static List<ResultTestInfo> GetResultListFromDataRaw(List<List<String>> listDataCleaned, List<Zone> listZone, int startIndex, int endIndex, bool isReversed)
        {
            List<List<String>> listData;
            if (!isReversed)
            {
                listData = listDataCleaned;
            }
            else
            {
                listData = new List<List<String>>();
                listData.Add(listDataCleaned[0]);
                for (int i = listDataCleaned.Count - 1; i > 0; i--)
                {
                    listData.Add(listDataCleaned[i]);
                }
            }

            List<ResultTestInfo> listResult = new List<ResultTestInfo>();

            for (int correctR = startIndex; correctR < endIndex; correctR++)
            {

                for (int i = 0; i < listZone.Count; i++)
                {
                    ResultTestInfo result = new ResultTestInfo();
                    result.resultText = listData[correctR][Ultility.FindColumn(listData[0], "Result Text")].Replace("\"", "") + "(" + listData[correctR][Ultility.FindColumn(listData[0], "Result Code")].Replace("\"", "") + ")";

                    int position = Ultility.FindColumn(listData[0], "Zone Result Zone " + listZone[i].zoneNo);
                    if (listZone[i].method.Equals("CDS"))
                    {
                        result.zone = listZone[i].zoneNo;
                        result.statisticID = listData[correctR][Ultility.FindColumn(listData[0], "Statistic ID")];
                        if (Double.Parse(listData[correctR][position]) != -1 || (i == listZone.Count - 1))
                        {
                            result.zone = listZone[i].zoneNo;
                            result.resultZone = listData[correctR][position];
                            result.infoZone = listData[correctR][position + 1];
                        }
                        else
                        {
                            result.zone = listZone[i].zoneNo;
                            result.resultZone = listData[correctR][position];
                            result.infoZone = listData[correctR][position + 1].Replace("Zone " + listZone[i + 1].zoneNo, "Zone " + listZone[i].zoneNo);
                        }
                    }
                    else if (listZone[i].method.Equals("CD1") || listZone[i].method.Equals("CD3"))
                    {
                        result.zone = listZone[i].zoneNo;
                        result.resultZone = listData[correctR][position];
                        result.infoZone = listData[correctR][position + 1];
                    }
                    else
                    {
                        result.zone = listZone[i].zoneNo;
                        result.resultZone = listData[correctR][position].Replace("\\", "").Replace("\"", "");
                        result.infoZone = listData[correctR][position + 1];
                    }
                    listResult.Add(result);

                }

            }

            return listResult;
        }

        public static List<DeviceID> GetListDeviceIDFromJSON(String path, String server)
        {
            List<DeviceID> listDeviceID = db.GetAllDeviceModel();
            string json;
            using (StreamReader r = new StreamReader(path))
            {
                json = r.ReadToEnd();
            }
            var brandList = JsonConvert.DeserializeObject(json);
            List<DeviceID> listDevice = new List<DeviceID>();
            listDevice.Add(new DeviceID("", "DEFAULT", "DEFAULT", server));
            JObject jObject = ((JObject)brandList);
            foreach (var brand in jObject)
            {
                DeviceID deviceID = new DeviceID();
                deviceID.deviceBrand = brand.Key;
                jObject = ((JObject)brand.Value);
                foreach (var model in jObject)
                {
                    deviceID.deviceModel = model.Key;
                    jObject = ((JObject)model.Value);
                    foreach (var device in jObject)
                    {
                        DeviceID addedDevice = new DeviceID(deviceID.deviceBrand, device.Key, deviceID.deviceModel, server);
                        listDevice.Add(addedDevice);
                    }
                }

            }
            return listDevice;
        }

        public static List<ThresholdImported> GetListThresholdFromJSON(String path, int productID, String server)
        {
            List<ThresholdImported> listThreshold = new List<ThresholdImported>();
            string json;
            using (StreamReader r = new StreamReader(path))
            {
                json = r.ReadToEnd();
            }
            var brandList = JsonConvert.DeserializeObject(json);
            JObject jObject = ((JObject)brandList);
            foreach (var brand in jObject)
            {
                ThresholdImported thresholdImported = new ThresholdImported();
                thresholdImported.deviceID = brand.Key;
                thresholdImported.productID = productID;
                foreach (var model in ((JObject)brand.Value))
                {
                    if (!brand.Key.Equals("DEFAULT"))
                    {
                        JObject jArray = (JObject)model.Value.First();
                        foreach (var element in jArray)
                        {
                            if (element.Key.Equals("CDS_Separation_Shift"))
                            {
                                thresholdImported.threshold = Double.Parse(element.Value.ToString());
                                thresholdImported.productID = productID;
                                thresholdImported.server = server;
                            }
                        }
                    }
                    else if(brand.Key.Equals("DEFAULT"))
                    {
                       // JObject jObjectA = ((JObject)model.Value);
                        //JProperty jProperty = (JProperty)(jObjectA.First.First.ElementAt(1));
                        //thresholdImported.threshold = Double.Parse(jProperty.Value.ToString());
                    }

                }
                listThreshold.Add(thresholdImported);


            }
            return listThreshold;
        }

        public static List<String> GetRawDataExport(String path, bool isGetTitle)
        {
            StreamReader sr = new StreamReader(path, System.Text.Encoding.UTF8);
            List<String> listData = new List<String>();
            try
            {

                while (!sr.EndOfStream)
                {
                    string strline = sr.ReadLine();
                    if (strline.Split(';')[0].Length != 0)
                    {
                        listData.Add(strline);

                    }
                }
                sr.Close();
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (isGetTitle)
            {
                return listData;
            }
            else
            {
                listData.RemoveAt(0);
                return listData;
            }
        }

        public static DataTable ParseDataRawToTable(List<List<String>> listCleanData)
        {
            DataTable dt = new DataTable();

            for (int i = 0; i < listCleanData[0].Count; i++)
            {
                dt.Columns.Add();
            }
            dt.Rows.Add();
            for (int k = 0; k < listCleanData[0].Count; k++)
            {
                dt.Rows[dt.Rows.Count - 1][k] = listCleanData[0][k];
            }
            for (int j = 1; j < listCleanData.Count; j++)
            {
                dt.Rows.Add();
                for (int k = 0; k < listCleanData[j].Count; k++)
                {
                    dt.Rows[dt.Rows.Count - 1][k] = listCleanData[j][k];
                }
            }
            return dt;
        }

        public static Product GetTheProductFromBulk(FileInfo[] files)
        {
            Product product;
            String error = "";
            for (int i = 0; i < files.Length; i++)
            {
                List<String> listRawData = RemoveDoubleQuoteFromRawData(GetRawData(files[i].FullName));
                if (listRawData.Count > 0)
                {
                    error = VerifyData(listRawData, files[i].FullName);
                    if (error.Length > 0)
                    {
                        MessageBox.Show(error);
                        return null;
                    }
                    else
                    {
                        error = VerifyTheSameProduct(listRawData, files[i].FullName);
                        if (error.Length > 0)
                        {
                            MessageBox.Show(error);
                            return null;
                        }
                    }
                }

            }
            if (error.Length == 0)
            {
                List<String> listRawData = RemoveDoubleQuoteFromRawData(GetRawData(files[0].FullName));
                if (listRawData.Count > 0)
                {
                    List<String> title = listRawData[0].Split(';').ToList();
                    List<String> firstRow = listRawData[1].Split(';').ToList();
                    String standardCompanyID = firstRow[Ultility.FindColumn(title, "User Company ID")];
                    String standardCompanyName = firstRow[Ultility.FindColumn(title, "User Company Name")];
                    String standardProductID = firstRow[Ultility.FindColumn(title, "User Company ID")];
                    String standardProductName = firstRow[Ultility.FindColumn(title, "User Company Name")];

                    for (int i = 0; i < files.Length; i++)
                    {
                        List<String> listRawDataFromFile = RemoveDoubleQuoteFromRawData(GetRawData(files[i].FullName));
                        if (listRawDataFromFile.Count > 0)
                        {
                            List<String> titleFromFile = listRawDataFromFile[0].Split(';').ToList();
                            List<String> firstRowEachFile = listRawDataFromFile[1].Split(';').ToList();
                            String companyID = firstRowEachFile[Ultility.FindColumn(titleFromFile, "User Company ID")];
                            String companyName = firstRowEachFile[Ultility.FindColumn(titleFromFile, "User Company Name")];
                            String productID = firstRowEachFile[Ultility.FindColumn(titleFromFile, "User Company ID")];
                            String productName = firstRowEachFile[Ultility.FindColumn(titleFromFile, "User Company Name")];

                            if (!companyID.Equals(standardCompanyID) || !companyName.Equals(standardCompanyName) || !productID.Equals(standardProductID) || !productName.Equals(standardProductName))
                            {
                                error += "The file " + files[i].Name + " has different product with the file " + files[0].Name + "\n";
                            }
                        }

                    }

                }
                else
                {
                    return null;
                }

            }
            if (error.Length > 0)
            {
                MessageBox.Show(error);
                return null;
            }
            else
            {
                List<String> listRawDataFromFile = RemoveDoubleQuoteFromRawData(GetRawData(files[0].FullName));
                if (listRawDataFromFile.Count > 0)
                {
                    product = GetProductFromCSV(listRawDataFromFile);
                    return product;
                }
                else
                {
                    return null;
                }

            }



        }

        public static int CalculateFalseNegative(List<DataDrawChart> listOrig, double threshold)
        {

            int count = 0;
            for (int i = 0; i < listOrig.Count; i++)
            {
                if (listOrig[i].pointValue > threshold)
                {
                    count++;
                }
                else if (listOrig[i].pointValue == -1 && listOrig[i].resultText.Contains("PLEASE TRY AGAIN"))
                {
                    count--;
                }
            }
            return count;
        }
        public static int CalculateFalsePositive(List<DataDrawChart> listCopy, double threshold)
        {
            int count = 0;
            for (int i = 0; i < listCopy.Count; i++)
            {
                if (listCopy[i].pointValue < threshold)
                {
                    count++;
                }
                else if (listCopy[i].pointValue == 0)
                {
                    count--;
                }
                else if (listCopy[i].pointValue == -1 && listCopy[i].resultText.Contains("PLEASE TRY AGAIN"))
                {
                    count--;
                }
            }
            return count;
        }
        public static int CalculatePTA(List<DataDrawChart> listOrig, List<DataDrawChart> listCopy)
        {
            int count = 0;
            for (int i = 0; i < listOrig.Count; i++)
            {
                if (listOrig[i].pointValue == -1 && listOrig[i].resultText.Contains("PLEASE TRY AGAIN"))
                {
                    count++;
                }
            }
            for (int i = 0; i < listCopy.Count; i++)
            {
                if (listCopy[i].pointValue == -1 && listCopy[i].resultText.Contains("PLEASE TRY AGAIN"))
                {
                    count++;
                }
            }
            return count;
        }

        public static int FindZoneNo(Product product, String typeZone)
        {
            int zone = -1;
            List<String> type = new List<String>();
            for (int k = 0; k < product.listZone.Count; k++)
            {
                type.Add(product.listZone[k].method);
            }
            if (type.FindIndex(x => x.StartsWith(typeZone)) != -1)
            {
                zone = product.listZone[type.FindIndex(x => x.StartsWith(typeZone))].zoneNo;
            }
            return zone;
        }

        public static String GetProductInfoFromCSV(List<String> listData)
        {
            if (listData.Count > 0)
            {
                listData = Ultility.RemoveDoubleQuoteFromRawData(listData);
                Product product = new Product();
                product.productID = Int32.Parse(listData[1].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "Product ID")]);
                product.productName = listData[1].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "Product Name")];
                product.companyID = Int32.Parse(listData[1].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "User Company ID")]);
                product.companyName = listData[1].Split(';')[Ultility.FindColumn(listData[0].Split(';').ToList(), "User Company Name")];
                String info = product.productID.ToString() + "-" + product.productName + "-" + product.companyID.ToString() + "-" + product.companyName;
                return info;
            }
            else
            {
                return "";
            }

        }

        public static bool isOtherZone(List<List<String>> listCleanedData)
        {
            int countSpecial = 0;
            String[] conditions = listCleanedData[1][Ultility.FindColumn(listCleanedData[0], "Is ST Zone")].Split('|');
            //int countZone = conditions.Length - 1;
            for (int i = 0; i < conditions.Length; i++)
            {

                if (conditions[i].Contains("false"))
                {
                    countSpecial++;
                }
            }
            return countSpecial > 0 ? true : false;

        }

        public static List<int> GetListZoneNumber(List<String> listData)
        {
            List<String> title = Ultility.ConvertDataStringToList(listData[0]);
            List<int> listZone = new List<int>();
            int zoneNo = Ultility.CountTheZone(listData);
            for (int i = 0; i < listData.Count; i++)
            {
                string[] zones = listData[i].Split(';')[FindColumn(title, "Zone Number")].Split('|');
                for (int k = 0; k < zones.Length; k++)
                {
                    int zone;
                    if (Int32.TryParse(zones[k], out zone))
                    {
                        if (!listZone.Contains(zone))
                        {
                            listZone.Add(zone);
                        }
                    }

                }
            }

            return listZone;
        }

    }
}
