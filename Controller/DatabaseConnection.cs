﻿using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Version = MiniTool.Data.Version;

namespace MiniTool.Controller
{
    public class DatabaseConnection
    {
        SQLiteConnection sqlite_conn;
        public SQLiteConnection GetConnection()
        {
            SQLiteConnection sqlite_con;
            sqlite_con = new SQLiteConnection("Data Source=MiniToolDB.db; Version = 3; New = True; Compress = True; ");
            // Open the connection:
            try
            {
                sqlite_con.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return sqlite_con;
        }


        public List<Device> GetAllDevices(int isAnonymous)
        {

            sqlite_conn = GetConnection();
            List<Device> listDevice = new List<Device>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Device";
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Device device;
            while (sqlite_datareader.Read())
            {
                device = new Device(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetBoolean(8));
                listDevice.Add(device);
            }
            sqlite_conn.Close();
            return listDevice;
        }

        public List<Device> GetKnownDevices()
        {

            sqlite_conn = GetConnection();
            List<Device> listDevice = new List<Device>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Device WHERE isAnonymous = 0";
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Device device;
            while (sqlite_datareader.Read())
            {
                device = new Device(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetBoolean(8));
                listDevice.Add(device);
            }
            sqlite_conn.Close();
            return listDevice;
        }

        public Device GetDeviceByID(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Device Where id =" + id;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Device device = new Device();
            while (sqlite_datareader.Read())
            {
                device = new Device(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetBoolean(8));
            }
            sqlite_conn.Close();
            return device;
        }

        public Device GetDeviceByCode(String code)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Device Where code ='" + code + "'";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Device device = new Device();
            while (sqlite_datareader.Read())
            {
                device = new Device(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetBoolean(8));
            }
            sqlite_conn.Close();
            return device;
        }

        public Device GetDeviceByEmailAndCode(String email, String code)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Device Where code =@code and email =@email";
            sqlite_cmd.Parameters.AddWithValue("@code", code);
            sqlite_cmd.Parameters.AddWithValue("@email", email);
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Device device = new Device();
            while (sqlite_datareader.Read())
            {
                device = new Device(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetBoolean(8));
            }
            sqlite_conn.Close();
            return device;
        }

        public Device GetDeviceByCodeAndLocation(String code, String location)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Device Where code =@code and location =@location";
            sqlite_cmd.Parameters.AddWithValue("@code", code);
            sqlite_cmd.Parameters.AddWithValue("@location", location);
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Device device = new Device();
            while (sqlite_datareader.Read())
            {
                device = new Device(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetBoolean(8));
            }
            sqlite_conn.Close();
            return device;
        }

        public Device GetDeviceByEmail(String email)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Device Where email = @email";
            sqlite_cmd.Parameters.AddWithValue("@email", email);
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Device device = new Device();
            while (sqlite_datareader.Read())
            {
                device = new Device(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetBoolean(8));
            }
            sqlite_conn.Close();
            return device;
        }


        public void AddDevice(String model, String code, String name, String email, String location, String type, String UDID, Boolean isAnonymous)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO Device(model, code, name, email, location, type, UDID, isAnonymous) VALUES(@model, @code, @name, @email, @location, @type, @UDID, @isAnonymous)";

            sqlite_cmd.Parameters.AddWithValue("@model", model);
            sqlite_cmd.Parameters.AddWithValue("@code", code);
            sqlite_cmd.Parameters.AddWithValue("@name", name);
            sqlite_cmd.Parameters.AddWithValue("@email", email);
            sqlite_cmd.Parameters.AddWithValue("@location", location);
            sqlite_cmd.Parameters.AddWithValue("@type", type);
            sqlite_cmd.Parameters.AddWithValue("@UDID", UDID);
            sqlite_cmd.Parameters.AddWithValue("@isAnonymous", isAnonymous);
            sqlite_cmd.Prepare();

            sqlite_cmd.ExecuteNonQuery();
        }
        public void UpdateDeviceByID(int id, String model, String code, String name, String email, String location, String type, String UDID, Boolean isAnonymous)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "Update Device set model = :model, code = :code, name = :name, email = :email, location = :location, type = :type, UDID = :UDID, isAnonymous = :isAnonymous where id=:id";
            sqlite_cmd.Parameters.Add("model", System.Data.DbType.String).Value = model;
            sqlite_cmd.Parameters.Add("code", System.Data.DbType.String).Value = code;
            sqlite_cmd.Parameters.Add("name", System.Data.DbType.String).Value = name;
            sqlite_cmd.Parameters.Add("email", System.Data.DbType.String).Value = email;
            sqlite_cmd.Parameters.Add("location", System.Data.DbType.String).Value = location;
            sqlite_cmd.Parameters.Add("type", System.Data.DbType.String).Value = type;
            sqlite_cmd.Parameters.Add("UDID", System.Data.DbType.String).Value = UDID;
            sqlite_cmd.Parameters.Add("isAnonymous", System.Data.DbType.Boolean).Value = isAnonymous;
            sqlite_cmd.Parameters.Add("id", System.Data.DbType.Int32).Value = id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }
        public void RemoveDeviceByID(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "DELETE FROM Device Where id =" + id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public List<String> GetDeviceThroughVersionAndProduct(String version, int product)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            List<String> listDeviceModel = new List<String>();
            sqlite_cmd = sqlite_conn.CreateCommand();
         //   List<ResultForUploading> listResult = new List<ResultForUploading>();
            sqlite_cmd.CommandText = "SELECT DISTINCT deviceModel FROM Result Where versionName = @version AND productID =" + product;
            sqlite_cmd.Parameters.AddWithValue("@version", version);
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                listDeviceModel.Add(sqlite_datareader.GetString(0));
            }
            sqlite_conn.Close();
            return listDeviceModel;
        }



        public List<Product> GetAllProducts()
        {
            sqlite_conn = GetConnection();
            List<Product> listProduct = new List<Product>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Product";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Product product;
            while (sqlite_datareader.Read())
            {

                product = new Product(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetInt32(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7));
                listProduct.Add(product);
            }
            sqlite_conn.Close();
            return listProduct;
        }

        public Product GetProductByID(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Product Where id =" + id;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Product product = new Product();
            while (sqlite_datareader.Read())
            {
                product = new Product(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetInt32(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7));
                product.listZone = new List<Zone>();
                SQLiteDataReader sqlite_datareader1;
                SQLiteCommand sqlite_cmd1;
                sqlite_cmd1 = sqlite_conn.CreateCommand();
                sqlite_cmd1.CommandText = "SELECT * FROM Zone Where productID =" + product.id;
                sqlite_datareader1 = sqlite_cmd1.ExecuteReader();
                while (sqlite_datareader1.Read())
                {
                    Zone zone = new Zone(sqlite_datareader1.GetInt32(0), product.id, sqlite_datareader1.GetInt32(2), sqlite_datareader1.GetString(3), sqlite_datareader1.GetString(4), sqlite_datareader1.GetString(5), sqlite_datareader1.GetDouble(6));
                    product.listZone.Add(zone);
                }

            }
            sqlite_conn.Close();
            return product;
        }

        public Product GetUniqueProduct(int productID, String productName, int companyID, String companyName)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Product Where productID =" + productID + " AND productName=@productName AND companyID =" + companyID + " AND companyName=@companyName";
             sqlite_cmd.Parameters.AddWithValue("@productName", productName);
             sqlite_cmd.Parameters.AddWithValue("@companyName", companyName);
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Product product = new Product();
            while (sqlite_datareader.Read())
            {
                product = new Product(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetInt32(4), sqlite_datareader.GetString(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7));
                product.listZone = new List<Zone>();
                SQLiteDataReader sqlite_datareader1;
                SQLiteCommand sqlite_cmd1;
                sqlite_cmd1 = sqlite_conn.CreateCommand();
                sqlite_cmd1.CommandText = "SELECT * FROM Zone Where productID =" + product.id;
                sqlite_datareader1 = sqlite_cmd1.ExecuteReader();
                while (sqlite_datareader1.Read())
                {

                    Zone zone = new Zone(sqlite_datareader1.GetInt32(0), product.id, sqlite_datareader1.GetInt32(2), sqlite_datareader1.GetString(3), sqlite_datareader1.GetString(4), sqlite_datareader1.GetString(5), sqlite_datareader1.GetDouble(6));
                    product.listZone.Add(zone);
                }

            }
            sqlite_conn.Close();
            return product;
        }

        public int AddProduct(String project, int companyID, String companyName, int productID, String productName, String result, String type)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO Product(project, companyID, companyName, productID, productName, result, type) VALUES(@project, @companyID, @companyName, @productID, @productName, @result, @type)";

            sqlite_cmd.Parameters.AddWithValue("@project", project);
            sqlite_cmd.Parameters.AddWithValue("@companyID", companyID);
            sqlite_cmd.Parameters.AddWithValue("@companyName", companyName);
            sqlite_cmd.Parameters.AddWithValue("@productID", productID);
            sqlite_cmd.Parameters.AddWithValue("@productName", productName);
            sqlite_cmd.Parameters.AddWithValue("@result", result);
            sqlite_cmd.Parameters.AddWithValue("@type", type);

            sqlite_cmd.Prepare();
            sqlite_cmd.ExecuteNonQuery();
            sqlite_cmd.CommandText = "select last_insert_rowid()";
            Int64 LastRowID64 = (Int64)sqlite_cmd.ExecuteScalar();

            return (int)LastRowID64;
        }
        public void RemoveProductByID(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "DELETE FROM Product Where id =" + id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
            RemoveZoneByProductID(id);
        }

        public void UpdateProductByID(int id, String project, int companyID, String companyName, int productID, String productName, String result, String type)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "Update Product set project = :project, companyID = :companyID, companyName = :companyName, productID = :productID, productName = :productName, result = :result, type = :type where id=:id";
            sqlite_cmd.Parameters.Add("project", System.Data.DbType.String).Value = project;
            sqlite_cmd.Parameters.Add("companyID", System.Data.DbType.Int32).Value = companyID;
            sqlite_cmd.Parameters.Add("companyName", System.Data.DbType.String).Value = companyName;
            sqlite_cmd.Parameters.Add("productID", System.Data.DbType.Int32).Value = productID;
            sqlite_cmd.Parameters.Add("productName", System.Data.DbType.String).Value = productName;
            sqlite_cmd.Parameters.Add("result", System.Data.DbType.String).Value = result;
            sqlite_cmd.Parameters.Add("type", System.Data.DbType.String).Value = type;
            sqlite_cmd.Parameters.Add("id", System.Data.DbType.Int32).Value = id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public List<Product> GetProductThroughVersion(String version)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            List<Product> listProduct = new List<Product>();
            sqlite_cmd = sqlite_conn.CreateCommand();
        //    List<ResultForUploading> listResult = new List<ResultForUploading>();
            sqlite_cmd.CommandText = "SELECT DISTINCT productID FROM Result Where versionName = @versionName";
            sqlite_cmd.Parameters.AddWithValue("@versionName", version);
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                listProduct.Add(GetProductByID(sqlite_datareader.GetInt32(0)));
            }
            sqlite_conn.Close();
            return listProduct;
        }

        public List<Version> GetAllVersions()
        {
            sqlite_conn = GetConnection();
            List<Version> listVersions = new List<Data.Version>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Version";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Data.Version version;
            while (sqlite_datareader.Read())
            {

                version = new Version(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2));
                listVersions.Add(version);
            }
            sqlite_conn.Close();
            return listVersions;
        }

        public Version GetVersionByID(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Version Where id =" + id;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Version version = new Version();
            while (sqlite_datareader.Read())
            {
                version = new Version(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2));
            }
            sqlite_conn.Close();
            return version;
        }

        public Version GetVersionByOS_Name(String os, String versionName)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Version Where os = @os AND version = @versionName";
            sqlite_cmd.Parameters.AddWithValue("@versionName", versionName);
            sqlite_cmd.Parameters.AddWithValue("@os", os);
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Version version = new Version();
            while (sqlite_datareader.Read())
            {
                version = new Version(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2));
            }
            sqlite_conn.Close();
            return version;
        }

        public void AddVersion(String version, String os)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO Version(version, os) VALUES(@version, @os)";

            sqlite_cmd.Parameters.AddWithValue("@version", version);
            sqlite_cmd.Parameters.AddWithValue("@os", os);
            sqlite_cmd.Prepare();

            sqlite_cmd.ExecuteNonQuery();
        }
        public void RemoveVersionByID(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "DELETE FROM Version Where id =" + id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public void UpdateVersionByID(int id, String version, String os)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "UPDATE Version SET version = :version, os =:os WHERE id=:id";
            sqlite_cmd.Parameters.Add("version", System.Data.DbType.String).Value = version;
            sqlite_cmd.Parameters.Add("os", System.Data.DbType.String).Value = os;
            sqlite_cmd.Parameters.Add("id", System.Data.DbType.Int32).Value = id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }
        public List<Version> GetVersionByOS(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            List<Version> listVersion = new List<Version>();
            if (id == 0)
            {
                sqlite_cmd.CommandText = "SELECT * FROM Version Where OS = 'iOS'";
            }
            else if (id == 1)
            {
                sqlite_cmd.CommandText = "SELECT * FROM Version Where OS = 'Android'";
            }

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                Version version = new Version(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2));
                listVersion.Add(version);
            }
            sqlite_conn.Close();
            return listVersion;
        }

        public void AddZoneByProductID(int productID, int zoneNo, String method, String code, String mov, Double threshold)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO Zone(productID, zoneNo, method, code, mov, threshold) VALUES(@productID, @zoneNo, @method, @code, @mov, @threshold)";

            sqlite_cmd.Parameters.AddWithValue("@productID", productID);
            sqlite_cmd.Parameters.AddWithValue("@zoneNo", zoneNo);
            sqlite_cmd.Parameters.AddWithValue("@method", method);
            sqlite_cmd.Parameters.AddWithValue("@code", code+ ",");
            sqlite_cmd.Parameters.AddWithValue("@threshold", threshold);
            sqlite_cmd.Parameters.AddWithValue("@mov", mov + ",");
            sqlite_cmd.Prepare();

            sqlite_cmd.ExecuteNonQuery();
        }

        public List<Zone> GetZonesByProductID(int productID)
        {
            sqlite_conn = GetConnection();
            List<Zone> listZone = new List<Zone>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Zone Where productID=" + productID;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {
                Zone zone = new Zone(sqlite_datareader.GetInt32(0), productID, sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetDouble(6));
                listZone.Add(zone);
            }
            sqlite_conn.Close();
            return listZone;
        }

        public Zone GetZoneByID(int productID, int zoneID)
        {
            sqlite_conn = GetConnection();
            List<Zone> listZone = new List<Zone>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Zone Where productID=" + productID;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {

                Zone zone = new Zone(sqlite_datareader.GetInt32(0), productID, sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetString(5), sqlite_datareader.GetDouble(6));
                listZone.Add(zone);
            }
            sqlite_conn.Close();
            return listZone[zoneID - 1];
        }

        public void RemoveZoneByProductID(int productID)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "DELETE FROM Zone Where productID =" + productID;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public void UpdateZoneByID(int id, String method, String code, String mov, Double threshold)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "UPDATE Zone SET method = :method, code =:code, threshold =:threshold, mov =:mov WHERE id=:id";
            sqlite_cmd.Parameters.Add("method", System.Data.DbType.String).Value = method;
            sqlite_cmd.Parameters.Add("code", System.Data.DbType.String).Value = code;
            sqlite_cmd.Parameters.Add("mov", System.Data.DbType.String).Value = mov;
            sqlite_cmd.Parameters.Add("threshold", System.Data.DbType.Double).Value = threshold;
            sqlite_cmd.Parameters.Add("id", System.Data.DbType.Int32).Value = id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public void RemoveZoneByID(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "DELETE FROM Zone Where id =" + id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public void SaveEvaluationResult(ResultForUploading data)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO Result(statisticID, versionID, versionName, deviceModel, productID, productName, createdDate, resultText, zone, method, value, info) VALUES(@statisticID, @versionID, @versionName, @deviceModel, @productID, @productName, @createdDate, @resultText, @zone, @method, @value, @info)";

            sqlite_cmd.Parameters.AddWithValue("@statisticID", data.statisticID);
            sqlite_cmd.Parameters.AddWithValue("@versionID", data.versionID);
            sqlite_cmd.Parameters.AddWithValue("@versionName", data.version);
            sqlite_cmd.Parameters.AddWithValue("@deviceModel", data.deviceModel);
            sqlite_cmd.Parameters.AddWithValue("@productID", data.productID);
            sqlite_cmd.Parameters.AddWithValue("@productName", data.productName);
            sqlite_cmd.Parameters.AddWithValue("@createdDate", data.createdDate);
            sqlite_cmd.Parameters.AddWithValue("@resultText", data.resultText);
            sqlite_cmd.Parameters.AddWithValue("@zone", data.zone);
            sqlite_cmd.Parameters.AddWithValue("@method", data.method);
            sqlite_cmd.Parameters.AddWithValue("@value", data.value);
            sqlite_cmd.Parameters.AddWithValue("@info", data.info);
            sqlite_cmd.Prepare();

            sqlite_cmd.ExecuteNonQuery();


        }
        public List<ResultForUploading> GetResultByFullInfo(int versionID, int productID, string deviceModel, int zoneNo)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            List<ResultForUploading> listResult = new List<ResultForUploading>();
            sqlite_cmd.CommandText = "SELECT * FROM Result Where versionID = " + versionID + " and productID = " + productID + " and deviceModel = @deviceModel and zone = " + zoneNo;
            sqlite_cmd.Parameters.AddWithValue("@deviceModel", deviceModel);
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                ResultForUploading result = new ResultForUploading(sqlite_datareader.GetInt32(0), sqlite_datareader.GetInt64(1).ToString(), sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetInt32(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetString(8), sqlite_datareader.GetInt32(9), sqlite_datareader.GetString(10), sqlite_datareader.GetString(11), sqlite_datareader.GetString(12));
                listResult.Add(result);
            }
            sqlite_conn.Close();
            return listResult;
        }
        public List<ResultForUploading> GetResultByNameInfo(String version, String productName, String deviceModel, int zoneNo)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            List<ResultForUploading> listResult = new List<ResultForUploading>();
            sqlite_cmd.CommandText = "SELECT * FROM Result Where versionName = @version and productName = @productName and deviceModel = @deviceModel and zone = " + zoneNo;
            sqlite_cmd.Parameters.AddWithValue("@version", version);
            sqlite_cmd.Parameters.AddWithValue("@productName", productName);
            sqlite_cmd.Parameters.AddWithValue("@deviceModel", deviceModel);
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                ResultForUploading result = new ResultForUploading(sqlite_datareader.GetInt32(0), sqlite_datareader.GetInt64(1).ToString(), sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetInt32(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetString(8), sqlite_datareader.GetInt32(9), sqlite_datareader.GetString(10), sqlite_datareader.GetString(11), sqlite_datareader.GetString(12));
                listResult.Add(result);
            }
            sqlite_conn.Close();
            return listResult;
        }

        public List<ResultForUploading> GetAllResult()
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            List<ResultForUploading> listResult = new List<ResultForUploading>();
            sqlite_cmd.CommandText = "SELECT * FROM Result WHERE method = 'STC'";

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                ResultForUploading result = new ResultForUploading(sqlite_datareader.GetInt32(0), sqlite_datareader.GetInt64(1).ToString(), sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4), sqlite_datareader.GetInt32(5), sqlite_datareader.GetString(6), sqlite_datareader.GetString(7), sqlite_datareader.GetString(8), sqlite_datareader.GetInt32(9), sqlite_datareader.GetString(10), sqlite_datareader.GetString(11), sqlite_datareader.GetString(12));
                listResult.Add(result);
            }
            sqlite_conn.Close();
            return listResult;
        }

        public void DeleteResultByFullInfo(List<Device> listDeviceModel, Product product, Version version)
        {
            if (listDeviceModel.Count > 0)
            {
                sqlite_conn = GetConnection();
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = sqlite_conn.CreateCommand();
                for (int i = 0; i < listDeviceModel.Count; i++)
                {
                    sqlite_cmd.CommandText = "DELETE FROM Result Where versionID = " + version.id + " and productID = " + product.id + " and deviceModel = @deviceModel";
                    sqlite_cmd.Parameters.AddWithValue("@deviceModel", listDeviceModel[i].model);
                    sqlite_cmd.ExecuteNonQuery();
                }

               
                sqlite_conn.Close();
            }

        }
        public List<Preset> GetAllPresets()
        {
            sqlite_conn = GetConnection();
            List<Preset> listPresets = new List<Preset>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Preset";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Preset preset;
            while (sqlite_datareader.Read())
            {

                preset = new Preset(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetDouble(4), sqlite_datareader.GetInt32(5), sqlite_datareader.GetBoolean(6), sqlite_datareader.GetBoolean(7), sqlite_datareader.GetString(8), sqlite_datareader.GetString(9), sqlite_datareader.GetString(10), sqlite_datareader.GetInt64(11), sqlite_datareader.GetInt32(12), sqlite_datareader.GetBoolean(13));
                listPresets.Add(preset);
            }
            sqlite_conn.Close();
            return listPresets;
        }
        public Preset GetPresetByID(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Preset Where id =" + id;

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            Preset preset = new Preset();
            while (sqlite_datareader.Read())
            {
                preset = new Preset(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetInt32(2), sqlite_datareader.GetString(3), sqlite_datareader.GetDouble(4), sqlite_datareader.GetInt32(5), sqlite_datareader.GetBoolean(6), sqlite_datareader.GetBoolean(7), sqlite_datareader.GetString(8), sqlite_datareader.GetString(9), sqlite_datareader.GetString(10), sqlite_datareader.GetInt64(11), sqlite_datareader.GetInt32(12), sqlite_datareader.GetBoolean(13));
            }
            sqlite_conn.Close();
            return preset;
        }
        public int AddPreset(Preset preset)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO Preset(name,deviceID,platformName,platformVersion,productID,isFlash,quantity,isManual,appPackage,appActivity,appiumHost,appiumPort,isImageSet) VALUES(@name,@deviceID,@platformName,@platformVersion,@productID,@isFlash,@quantity,@isManual,@appPackage,@appActivity,@appiumHost,@appiumPort,@isImageSet)";

            sqlite_cmd.Parameters.AddWithValue("@name", preset.name);
            sqlite_cmd.Parameters.AddWithValue("@deviceID", preset.deviceID);
            sqlite_cmd.Parameters.AddWithValue("@platformName", preset.platformName);
            sqlite_cmd.Parameters.AddWithValue("@platformVersion", preset.platformVersion);
            sqlite_cmd.Parameters.AddWithValue("@productID", preset.productID);
            sqlite_cmd.Parameters.AddWithValue("@isFlash", preset.isFlash);
            sqlite_cmd.Parameters.AddWithValue("@quantity", preset.quantity);
            sqlite_cmd.Parameters.AddWithValue("@isManual", preset.isManual);
            sqlite_cmd.Parameters.AddWithValue("@appPackage", preset.appPackage);
            sqlite_cmd.Parameters.AddWithValue("@appActivity", preset.appActivity);
            sqlite_cmd.Parameters.AddWithValue("@appiumHost", preset.appiumHost);
            sqlite_cmd.Parameters.AddWithValue("@appiumPort", preset.appiumPort);
            sqlite_cmd.Parameters.AddWithValue("@isImageSet", preset.isImageSet);
            sqlite_cmd.Prepare();

            sqlite_cmd.ExecuteNonQuery();

            sqlite_cmd.CommandText = "select last_insert_rowid()";
            Int64 LastRowID64 = (Int64)sqlite_cmd.ExecuteScalar();

            return (int)LastRowID64;
        }
        public void RemovePreset(int id)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "DELETE FROM Preset Where id =" + id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }
        public void UpdatePreset(Preset preset)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "UPDATE Preset SET name = :name, deviceID = :deviceID, platformName = :platformName, platformVersion = :platformVersion, productID = :productID, isFlash = :isFlash, quantity = :quantity, isManual =:isManual,appPackage =:appPackage,appActivity =:appActivity, appiumHost =:appiumHost, appiumPort =:appiumPort, isImageSet =:isImageSet WHERE id=:id";
            sqlite_cmd.Parameters.Add("name", System.Data.DbType.String).Value = preset.name;
            sqlite_cmd.Parameters.Add("deviceID", System.Data.DbType.Int32).Value = preset.deviceID;
            sqlite_cmd.Parameters.Add("platformName", System.Data.DbType.String).Value = preset.platformName;
            sqlite_cmd.Parameters.Add("platformVersion", System.Data.DbType.Double).Value = preset.platformVersion;
            sqlite_cmd.Parameters.Add("productID", System.Data.DbType.Int32).Value = preset.productID;
            sqlite_cmd.Parameters.Add("isFlash", System.Data.DbType.Boolean).Value = preset.isFlash;
            sqlite_cmd.Parameters.Add("quantity", System.Data.DbType.Int32).Value = preset.quantity;
            sqlite_cmd.Parameters.Add("isManual", System.Data.DbType.Boolean).Value = preset.isManual;
            sqlite_cmd.Parameters.Add("appPackage", System.Data.DbType.String).Value = preset.appPackage;
            sqlite_cmd.Parameters.Add("appActivity", System.Data.DbType.String).Value = preset.appActivity;
            sqlite_cmd.Parameters.Add("appiumHost", System.Data.DbType.String).Value = preset.appiumHost;
            sqlite_cmd.Parameters.Add("appiumPort", System.Data.DbType.Int64).Value = preset.appiumPort;
            sqlite_cmd.Parameters.Add("isImageSet", System.Data.DbType.Boolean).Value = preset.isImageSet;
            sqlite_cmd.Parameters.Add("id", System.Data.DbType.Int32).Value = preset.id;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public List<DeviceID> GetAllDeviceModel()
        {
            sqlite_conn = GetConnection();
            List<DeviceID> listDeviceIDs = new List<DeviceID>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM DeviceModel";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            DeviceID deviceID;
            while (sqlite_datareader.Read())
            {

                deviceID = new DeviceID(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4));
                listDeviceIDs.Add(deviceID);
            }
            sqlite_conn.Close();
            return listDeviceIDs;
        }

        public List<DeviceID> GetAllDeviceModelByServer(String server)
        {
            sqlite_conn = GetConnection();
            List<DeviceID> listDeviceIDs = new List<DeviceID>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM DeviceModel WHERE server='" + server + "'";

            sqlite_datareader = sqlite_cmd.ExecuteReader();
            DeviceID deviceID;
            while (sqlite_datareader.Read())
            {

                deviceID = new DeviceID(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4));
                listDeviceIDs.Add(deviceID);
            }
            sqlite_conn.Close();
            return listDeviceIDs;
        }

        public String GetDeviceModelByDeviceID(String deviceID)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM DeviceModel WHERE deviceID=@deviceID";
            sqlite_cmd.Parameters.AddWithValue("@deviceID", deviceID);
            DeviceID device = new DeviceID();
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {
                device = new DeviceID(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetString(3), sqlite_datareader.GetString(4));
            }
            sqlite_conn.Close();
            return device.deviceModel;
        }

        public int AddDeviceModel(DeviceID deviceID)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO DeviceModel(brand,deviceID,deviceModel,server) VALUES(@brand,@deviceID,@deviceModel,@server)";

            sqlite_cmd.Parameters.AddWithValue("@brand", deviceID.deviceBrand);
            sqlite_cmd.Parameters.AddWithValue("@deviceID", deviceID.deviceID);
            sqlite_cmd.Parameters.AddWithValue("@deviceModel", deviceID.deviceModel);
            sqlite_cmd.Parameters.AddWithValue("@server", deviceID.server);
            sqlite_cmd.Prepare();

            sqlite_cmd.ExecuteNonQuery();

            sqlite_cmd.CommandText = "select last_insert_rowid()";
            Int64 LastRowID64 = (Int64)sqlite_cmd.ExecuteScalar();

            return (int)LastRowID64;
        }

        public void RemoveAllDeviceModelByServer(String server)
        {
            sqlite_conn = GetConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "DELETE FROM DeviceModel WHERE server ='" + server + "'";
            sqlite_cmd.ExecuteNonQuery();
            //sqlite_cmd.CommandText = "DELETE FROM sqlite_sequence WHERE name='DeviceModel'";
            //sqlite_cmd.ExecuteNonQuery();
            sqlite_conn.Close();
        }

        public List<Version> GetUploadedVersion(String OS)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            List<Version> listVersion = new List<Version>();
            sqlite_cmd = sqlite_conn.CreateCommand();
            //   List<ResultForUploading> listResult = new List<ResultForUploading>();
            sqlite_cmd.CommandText = "SELECT DISTINCT versionID FROM Result";

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                Version version = GetVersionByID(sqlite_datareader.GetInt32(0));
                if (version.os.Equals(OS))
                {
                    listVersion.Add(version);
                }
               
            }
            sqlite_conn.Close();
            return listVersion;
        }

        public List<String> GetUploadedDevice(int versionID)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            List<String> listDevice = new List<String>();
            sqlite_cmd = sqlite_conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT DISTINCT deviceModel FROM Result Where versionID=" + versionID;

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                listDevice.Add(sqlite_datareader.GetString(0));

            }
            sqlite_conn.Close();
            return listDevice;
        }

        public List<Product> GetUploadedProduct(int versionID, String deviceModel)
        {
            sqlite_conn = GetConnection();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            List<Product> listProduct = new List<Product>();
            sqlite_cmd = sqlite_conn.CreateCommand();
            //   List<ResultForUploading> listResult = new List<ResultForUploading>();
            sqlite_cmd.CommandText = "SELECT DISTINCT productID FROM Result Where versionID=" + versionID + " And deviceModel = @deviceModel";
            sqlite_cmd.Parameters.AddWithValue("@deviceModel", deviceModel);
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                listProduct.Add(GetProductByID(sqlite_datareader.GetInt32(0)));

            }
            sqlite_conn.Close();
            return listProduct;
        }
    }
}
