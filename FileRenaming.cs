﻿using MiniTool.Controller;
using MiniTool.Data;
using Renci.SshNet.Messages;
using Shell32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class FileRenaming : MasterForm
    {
        public FileRenaming() : base()
        {
            InitializeComponent();
            ShowLocation(false);
            ShowSequenceNo(false);
            progressBar.Visible = false;
        }

        private void btnFolderBrowse_Click(object sender, EventArgs e)
        {
            folderBrowseDialog.ShowDialog();
            txtRenamingFolder.Text = folderBrowseDialog.SelectedPath;
        }

        private void cbbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbType.Text.Equals("Device ID"))
            {
                ShowLocation(true);
            }
            else if (cbbType.Text.Equals("Convert DeviceModel to DeviceID"))
            {
                ShowLocation(true);
            }
            else if (cbbType.Text.Equals("Merge File"))
            {
                ShowLocation(true);
            }
            else if (cbbType.Text.Equals("Device Model"))
            {
                ShowLocation(false);
            }
            else if (cbbType.Text.Equals("Convert DeviceID to DeviceModel"))
            {
                ShowLocation(false);
            }
        }

        private void ShowLocation(bool isShowed)
        {
            lblLocation.Visible = isShowed;
            txtLocation.Text = "";
            txtLocation.Visible = isShowed;
        }

        private void ShowSequenceNo(bool isShowed)
        {
            lblSequenceNo.Visible = isShowed;
            txtSequenceNo.Text = "";
            txtSequenceNo.Visible = isShowed;
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            if (txtRenamingFolder.Text.Length != 0)
            {
                progressBar.Visible = true;
                DirectoryInfo d = new DirectoryInfo(txtRenamingFolder.Text);
                FileInfo[] files = d.GetFiles("*.csv");
                progressBar.Maximum = files.Length * 10;
                if (cbbType.Text.Equals("Device ID"))
                {
                    int count = 1;
                    foreach (FileInfo file in files)
                    {
                        progressBar.Value = count * 10;
                        String deviceID = Ultility.GetDeviceIDFromCSV(file.FullName);
                        if(deviceID.Length != 0)
                        {
                            String fileName = @"" + txtRenamingFolder.Text + @"\" + deviceID + "_" + txtLocation.Text + (cbIsOriginal.Checked ? "Orig" : "Copy");

                            if (System.IO.File.Exists(fileName))
                            {
                                try
                                {
                                    System.IO.File.Copy(file.FullName, fileName + ".csv", true);
                                    System.IO.File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }

                            }
                            else
                            {
                                System.IO.File.Copy(file.FullName, fileName + ".csv", true);
                                System.IO.File.Delete(file.FullName);

                            }
                           
                        }
                        count++;

                    }
                    progressBar.Visible = false;
                    MessageBox.Show("All files have been renamed successfully.");

                }
                else if (cbbType.Text.Equals("Device Model"))
                {
                    int count = 1;
                    foreach (FileInfo file in files)
                    {
                        progressBar.Value = count * 10;
                        String deviceID = Ultility.GetDeviceIDFromCSV(file.FullName);
                        if(deviceID.Length != 0)
                        {
                            DatabaseConnection db = new DatabaseConnection();
                            List<DeviceID> listDeviceID = db.GetAllDeviceModel();
                            String deviceModel = "";
                            for (int i = 0; i < listDeviceID.Count; i++)
                            {
                                if (listDeviceID[i].deviceID.Equals(deviceID))
                                {
                                    deviceModel = listDeviceID[i].deviceModel;
                                    i = listDeviceID.Count;
                                }
                            }

                            String fileName = @"" + txtRenamingFolder.Text + @"\" + deviceModel + "_" + (cbIsOriginal.Checked ? "Orig" : "Copy");

                            if (System.IO.File.Exists(fileName))
                            {
                                try
                                {
                                    System.IO.File.Copy(file.FullName, fileName + ".csv", true);
                                    System.IO.File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }

                            }
                            else
                            {
                                System.IO.File.Copy(file.FullName, fileName + ".csv", true);
                                System.IO.File.Delete(file.FullName);

                            }
                        }
                       
                        count++;
                    }
                    progressBar.Visible = false;
                    MessageBox.Show("All files have been renamed successfully.");
                }
                else if (cbbType.Text.Equals("Merge File"))
                {
                    int count = 1;
                    foreach (FileInfo file in files)
                    {
                        progressBar.Value = count * 10;
                        String deviceID = Ultility.GetDeviceIDFromCSV(file.FullName);
                        if(deviceID.Length != 0)
                        {
                            String fileName = @"" + txtRenamingFolder.Text + @"\" + deviceID + "_" + txtLocation.Text + "" + (cbIsOriginal.Checked ? "Orig" : "Copy") + "_" + txtSequenceNo.Text;

                            if (System.IO.File.Exists(fileName))
                            {
                                try
                                {
                                    System.IO.File.Copy(file.FullName, fileName + ".csv", true);
                                    System.IO.File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }

                            }
                            else
                            {
                                System.IO.File.Copy(file.FullName, fileName + ".csv", true);
                                System.IO.File.Delete(file.FullName);

                            }
                        }
                       
                        count++;
                    }
                    progressBar.Visible = false;
                    MessageBox.Show("All files have been renamed successfully.");
                }
                else if (cbbType.Text.Equals("Convert DeviceID to DeviceModel"))
                {
                    int count = 1;
                    foreach (FileInfo file in files)
                    {
                        progressBar.Value = count * 10;
                        String deviceID = file.Name.Split('_')[0];
                        String location = file.Name.Split('_')[1].Replace("Orig", "").Replace("Copy", "").Replace(".csv", "");
                        DatabaseConnection db = new DatabaseConnection();
                        List<DeviceID> listDeviceID = db.GetAllDeviceModel();
                        String deviceModel = "";
                        for (int i = 0; i < listDeviceID.Count; i++)
                        {
                            if (listDeviceID[i].deviceID.Equals(deviceID))
                            {
                                deviceModel = listDeviceID[i].deviceModel;
                                i = listDeviceID.Count;
                            }
                        }

                        string fileName = file.FullName.Replace(deviceID, deviceModel).Replace(location, "").Replace(".csv", "");

                        if (System.IO.File.Exists(fileName))
                        {
                            try
                            {
                                System.IO.File.Copy(file.FullName, fileName + ".csv", true);
                                System.IO.File.Delete(file.FullName);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }

                        }
                        else
                        {
                            System.IO.File.Copy(file.FullName, fileName + ".csv", true);
                            System.IO.File.Delete(file.FullName);

                        }
                        count++;
                    }
                    progressBar.Visible = false;
                    MessageBox.Show("All files have been renamed successfully.");
                }
                else if (cbbType.Text.Equals("Convert DeviceModel to DeviceID"))
                {
                    int count = 1;
                    foreach (FileInfo file in files)
                    {
                        progressBar.Value = count * 10;
                        String deviceModel = file.Name.Split('_')[0];
                        String deviceID = Ultility.GetDeviceIDFromCSV(file.FullName);
                        if(deviceID.Length != 0)
                        {
                            String location = txtLocation.Text;
                            DatabaseConnection db = new DatabaseConnection();
                            List<DeviceID> listDeviceID = db.GetAllDeviceModel();

                            string fileName = file.FullName.Replace(deviceModel + "_", deviceID + "_" + location);

                            if (System.IO.File.Exists(fileName))
                            {
                                try
                                {
                                    System.IO.File.Copy(file.FullName, fileName, true);
                                    System.IO.File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }

                            }
                            else
                            {
                                System.IO.File.Copy(file.FullName, fileName, true);
                                System.IO.File.Delete(file.FullName);

                            }
                        }
                        count++;

                    }
                    progressBar.Visible = false;
                    MessageBox.Show("All files have been renamed successfully.");
                }
                else
                {
                    MessageBox.Show("Please select type to rename the file.");
                }
            }
            else
            {
                MessageBox.Show("Please browse to the renaming folder.");
            }

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ShowSequenceNo(false);
            ShowLocation(false);
            txtRenamingFolder.Text = "";
            txtSequenceNo.Text = "";
            cbbType.SelectedIndex = -1;
            cbIsOriginal.Checked = false;
        }


        private void cbIsMerged_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIsMerged.Checked)
            {
                ShowSequenceNo(true);
            }
            else
            {
                ShowSequenceNo(false);
            }
        }

    }
}
