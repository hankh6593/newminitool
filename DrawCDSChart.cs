﻿using MiniTool.Controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MiniTool.Data
{
    public partial class DrawCDSChart : Form
    {
        public DrawCDSChart()
        {
            InitializeComponent();
        }

        public DrawCDSChart(String fileOrig, String fileCopy, double threshold)
        {
            InitializeComponent();
            JSONZoneData data = new JSONZoneData();
            List<String> rawData = Ultility.GetRawData(fileOrig);
            Product product = Ultility.GetProductFromCSV(rawData);
            data.productName = product.productName;
            List<List<String>> listDataOrig = Ultility.GetCleanData(fileOrig, product);
            data.listResultOrig = Ultility.GetResultListFromDataRaw(listDataOrig, product.listZone, 1, listDataOrig.Count, false);

            List<List<String>> listDataCopy = Ultility.GetCleanData(fileCopy, product);
            data.listResultCopy = Ultility.GetResultListFromDataRaw(listDataCopy, product.listZone, 1, listDataCopy.Count, false);

            List<DataDrawChart> listOrg = new List<DataDrawChart>();
            List<DataDrawChart> listCopy = new List<DataDrawChart>();
            for (int i = 0; i < data.listResultOrig.Count; i++)
            {
                DataDrawChart dataCDS = new DataDrawChart();
                dataCDS.statisticID = data.listResultOrig[i].statisticID;
                for (int k = 0; k < product.listZone.Count; k++)
                {
                    if (product.listZone[k].method.Equals("CDS"))
                    {
                        if (product.listZone[k].zoneNo == data.listResultOrig[i].zone)
                        {
                            dataCDS.pointValue = Double.Parse(data.listResultOrig[i].resultZone);
                            listOrg.Add(dataCDS);
                        }
                    }
                }

            }
            for (int i = 0; i < data.listResultCopy.Count; i++)
            {
                DataDrawChart dataCDS = new DataDrawChart();
                dataCDS.statisticID = data.listResultCopy[i].statisticID;
                for (int k = 0; k < product.listZone.Count; k++)
                {
                    if (product.listZone[k].method.Equals("CDS"))
                    {
                        if (product.listZone[k].zoneNo == data.listResultCopy[i].zone)
                        {
                            dataCDS.pointValue = Double.Parse(data.listResultCopy[i].resultZone);
                            listCopy.Add(dataCDS);
                        }
                    }
                }

            }

            DrawChart(listOrg, listCopy, threshold);
        }

        public void DrawChart(List<DataDrawChart> listOrg, List<DataDrawChart> listCopy, double threshold)
        {
            chartEvaluation.ResetAutoValues();
            chartEvaluation.Series.Clear();
            chartEvaluation.Legends[0].Docking = Docking.Bottom;
            chartEvaluation.MinimumSize = new Size(1000, 300);

            //chartEvaluation.ChartAreas[0].Position.X = 0;
            //chartEvaluation.ChartAreas[0].Position.Y = 0;
            //chartEvaluation.ChartAreas[0].Position.Width = 100;
            //chartEvaluation.ChartAreas[0].Position
            chartEvaluation.Width = (listOrg.Count + listCopy.Count) * 10;


            var seriesOrig = new System.Windows.Forms.DataVisualization.Charting.Series();
            seriesOrig.Name = "Orig";
            seriesOrig.ChartType = SeriesChartType.Point;
            seriesOrig.IsValueShownAsLabel = true;
            seriesOrig.MarkerSize = 8;
            seriesOrig.MarkerStyle = MarkerStyle.Circle;
            seriesOrig.Color = Color.Green;
            chartEvaluation.Series.Add(seriesOrig);
            for (int i = 0; i < listOrg.Count; i++)
            {
                int index = i + 1;
                chartEvaluation.Series[0].Points.AddXY(index, listOrg[i].pointValue);
            }

            var seriesCopy = new System.Windows.Forms.DataVisualization.Charting.Series();
            seriesCopy.Name = "Copy";
            seriesCopy.ChartType = SeriesChartType.Point;
            seriesCopy.IsValueShownAsLabel = true;
            seriesCopy.MarkerSize = 8;
            seriesCopy.MarkerStyle = MarkerStyle.Circle;
            seriesCopy.Color = Color.Red;
            chartEvaluation.Series.Add(seriesCopy);
            for (int i = 0; i < listCopy.Count; i++)
            {

                int index = i + 1;
                chartEvaluation.Series[1].Points.AddXY(index, listCopy[i].pointValue);

            }

            var SeriesThreshold = new System.Windows.Forms.DataVisualization.Charting.Series();
            SeriesThreshold.Name = "Threshold";
            SeriesThreshold.ChartType = SeriesChartType.Point;
            SeriesThreshold.MarkerSize = 8;
            SeriesThreshold.MarkerStyle = MarkerStyle.Circle;
            SeriesThreshold.Color = Color.LightGray;
            chartEvaluation.Series.Add(SeriesThreshold);

            int count = listOrg.Count > listCopy.Count ? listOrg.Count : listCopy.Count;
            for (int i = 0; i < count; i++)
            {

                int index = i + 1;
                chartEvaluation.Series[2].Points.AddXY(index, threshold);
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
