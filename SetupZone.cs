﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class SetupZone : Form
    {
        static DatabaseConnection db;
        public SetupZone()
        {
            InitializeComponent();
        }

        public SetupZone(int productID)
        {
            InitializeComponent();
            db = new DatabaseConnection();
            LoadDataZone(productID);
        }

        public void LoadDataZone(int productID)
        {
            txtProductID.Text = productID.ToString();
            txtProductName.Text = db.GetProductByID(productID).productName;
            List<Zone> listZone = db.GetZonesByProductID(productID);
            dgvZone.DataSource = listZone;
            dgvZone.Columns[1].Visible = false;
            dgvZone.Columns[6].Visible = false;
        }

        private void btnResetZone_Click(object sender, EventArgs e)
        {
            txtZoneID.Text = "";
            txtZoneNo.Text = "";
            cbbMethod.SelectedIndex = -1;
            txtCode.Text = "";
            txtMov.Text = "";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtZoneID.Text.Length == 0)
            {
                if(txtZoneNo.Text.Length != 0 && cbbMethod.Text.Length != 0)
                {
                    if (cbbMethod.Text.Equals("STC"))
                    {
                        if (txtCode.Text.Length != 0 && txtMov.Text.Length != 0)
                        {
                            db.AddZoneByProductID(Int32.Parse(txtProductID.Text), Int32.Parse(txtZoneNo.Text), cbbMethod.Text, txtCode.Text, txtMov.Text, 0);
                            LoadDataZone(Int32.Parse(txtProductID.Text));
                            btnResetZone_Click(sender, e);
                            MessageBox.Show("A new zone has been added successfully.");
                        }
                        else
                        {
                            MessageBox.Show("Please enter code and move fields.");
                        }
                    }
                    else
                    {
                        db.AddZoneByProductID(Int32.Parse(txtProductID.Text), Int32.Parse(txtZoneNo.Text), cbbMethod.Text, txtCode.Text, txtMov.Text, 0);
                        LoadDataZone(Int32.Parse(txtProductID.Text));
                        btnResetZone_Click(sender, e);
                        MessageBox.Show("A new zone has been added successfully.");
                    }
                }
                else
                {
                    MessageBox.Show("Please enter zone number and select method.");
                }
               

            }
            else
            {
                MessageBox.Show("Please click 'Reset' button and fill in information again to add new zone.");
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtZoneID.Text.Length != 0)
            {
                db.UpdateZoneByID(Int32.Parse(txtZoneID.Text), cbbMethod.Text, txtCode.Text, txtMov.Text, 0);
                LoadDataZone(Int32.Parse(txtProductID.Text));
                MessageBox.Show("A zone has been updated successfully.");

            }
            else
            {
                MessageBox.Show("Please select Zone to update.");
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (txtZoneID.Text.Length != 0)
            {
                db.RemoveZoneByID(Int32.Parse(txtZoneID.Text));
                LoadDataZone(Int32.Parse(txtProductID.Text));
                MessageBox.Show("A zone has been removed successfully.");
                btnResetZone_Click(sender, e);

            }
            else
            {
                MessageBox.Show("Please select Zone to update.");
            }
        }

        private void dgvZone_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Int32 selectedRowCount = dgvZone.SelectedRows[0].Index;
            if (selectedRowCount >= 0)
            {
                txtZoneID.Text = (dgvZone.Rows[selectedRowCount].Cells[0].Value.ToString());
                txtZoneNo.Text = (dgvZone.Rows[selectedRowCount].Cells[2].Value.ToString());
                cbbMethod.Text = (dgvZone.Rows[selectedRowCount].Cells[3].Value.ToString());
                txtCode.Text = (dgvZone.Rows[selectedRowCount].Cells[4].Value.ToString());
                txtMov.Text = (dgvZone.Rows[selectedRowCount].Cells[5].Value.ToString());
            }
        }
    }
}
