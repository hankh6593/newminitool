﻿using Microsoft.Office.Interop.Excel;
using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataTable = System.Data.DataTable;
using Excel = Microsoft.Office.Interop.Excel;

namespace MiniTool
{
    public partial class HoneywellRegressionTest : MasterForm
    {

        DatabaseConnection db;
        List<Microsoft.Office.Interop.Excel.Range> listRange = null;
        List<List<ResultTestInfo>> listSaveOrig;
        List<List<ResultTestInfo>> listSaveCopy;
        List<String> listCode = null;
        public HoneywellRegressionTest() : base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            progressExport.Visible = false;
        }

        private void btnBrowseResult_Click(object sender, EventArgs e)
        {
            ResultFolderDialog.ShowDialog();
            txtFolderPath.Text = ResultFolderDialog.SelectedPath;
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            SaveAsDialog.ShowDialog();
            txtSaveAs.Text = SaveAsDialog.SelectedPath;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtCriteriaOrig.Text = "True Positive-Valid-Valid(1);False Negative-Not Valid-Not Valid(3);Please Try Again-Please Try Again-Please Try Again(4)";
            txtCriteriaCopy.Text = "False Positive-Valid-Valid(1);True Negative-Not Valid-Not Valid(3);Please Try Again-Please Try Again-Please Try Again(4)";
            txtFolderPath.Text = "";
            txtSaveAs.Text = "";
            txtFileName.Text = "";
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            if (txtFolderPath.Text.Length == 0)
            {
                MessageBox.Show("Please select result folder.");
            }
            else if (txtSaveAs.Text.Length == 0)
            {
                MessageBox.Show("Please select save as location.");
            }
            else if (txtCriteriaOrig.Text.Length == 0)
            {
                MessageBox.Show("Please fill in criteria for Original result.");
            }
            else if (txtCriteriaCopy.Text.Length == 0)
            {
                MessageBox.Show("Please fill in criteria for Copy result.");
            }
            else
            {
                ExportHoneywellRegression();
            }
        }



        private void ExportHoneywellRegression()
        {
            GetResult();
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet = new Excel.Worksheet();
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();
            DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
            FileInfo[] files = d.GetFiles("*.csv");
            progressExport.Minimum = 0;
            progressExport.Value = 0;
            progressExport.Visible = true;
            progressExport.Maximum = (files.Length + (files.Length / 2)) * 10;

            List<DataTable> listDataRawOrig = new List<DataTable>();
            List<DataTable> listDataRawCopy = new List<DataTable>();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            int progressValue = 0;
            int index = 0;

            for (int i = 0; i < listCode.Count; i++)
            {
                progressValue++;
                progressExport.Value = progressValue * 10;

                if (files[index].Name.Contains("Copy"))
                {
                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                    List<List<String>> listCleanData = Ultility.GetCleanData(files[index].FullName, Ultility.GetProductFromCSV(Ultility.GetRawData(files[index].FullName)));
                    System.Data.DataTable dtRaw = Ultility.ParseDataRawToTable(listCleanData);
                    listDataRawCopy.Add(dtRaw);
                    object[,] arrayRaw = new object[dtRaw.Rows.Count, dtRaw.Columns.Count];
                    for (int k = 0; k < dtRaw.Rows.Count; k++)
                    {
                        for (int j = 0; j < dtRaw.Columns.Count; j++)
                        {
                            arrayRaw[k, j] = dtRaw.Rows[k][j];

                        }
                        xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtRaw.Rows.Count, dtRaw.Columns.Count]].Value = arrayRaw;
                        xlWorkSheet.Name = "D_C_" + listCode[i].Split('_')[0] + "(" + listCode[i].Split('_')[1] + ")";
                        Excel.Range rangeRaw = xlWorkSheet.Rows[1];
                        rangeRaw.Select();
                        rangeRaw.Font.Bold = true;
                        rangeRaw.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    }

                }

                if (files[index + 1].Name.Contains("Orig"))
                {
                    xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                    progressValue++;
                    progressExport.Value = progressValue * 10;
                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                    List<List<String>> listCleanData = Ultility.GetCleanData(files[index].FullName, Ultility.GetProductFromCSV(Ultility.GetRawData(files[index].FullName)));
                    System.Data.DataTable dtRaw = Ultility.ParseDataRawToTable(listCleanData);
                    listDataRawOrig.Add(dtRaw);
                    object[,] arrayRaw = new object[dtRaw.Rows.Count, dtRaw.Columns.Count];
                    arrayRaw = new object[dtRaw.Rows.Count, dtRaw.Columns.Count];
                    for (int k = 0; k < dtRaw.Rows.Count; k++)
                    {
                        for (int j = 0; j < dtRaw.Columns.Count; j++)
                        {
                            arrayRaw[k, j] = dtRaw.Rows[k][j];


                        }
                        xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtRaw.Rows.Count, dtRaw.Columns.Count]].Value = arrayRaw;
                        xlWorkSheet.Name = "D_O_" + listCode[i].Split('_')[0] + "(" + listCode[i].Split('_')[1] + ")";
                        Excel.Range rangeRaw = xlWorkSheet.Rows[1];
                        rangeRaw.Select();
                        rangeRaw.Font.Bold = true;
                        rangeRaw.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    }
                }
                xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);

                progressValue++;
                progressExport.Value = progressValue * 10;
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
                xlWorkSheet.Name = "Graph_" + listCode[i].Split('_')[0] + "(" + listCode[i].Split('_')[1] + ")";
                System.Data.DataTable dt = GetDataTable(listSaveOrig[i], listSaveCopy[i], listCode[i].Split('_')[0], Ultility.GetProductFromCSV(Ultility.GetRawData(files[index].FullName)));
                object[,] array = new object[dt.Rows.Count, dt.Columns.Count];
                for (int k = 0; k < dt.Rows.Count; k++)
                {
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        array[k, j] = dt.Rows[k][j];
                    }
                }

                xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dt.Rows.Count, dt.Columns.Count]].Value = array;
                Excel.Range range = xlWorkSheet.Rows[1];
                range.Select();
                range.Font.Bold = true;
                range.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                int valid = 0;
                Product product = Ultility.GetProductFromCSV(Ultility.GetRawData(files[index].FullName));
                for (int m = 0; m < product.listZone.Count; m++)
                {
                    if (!product.listZone[m].method.Equals("STC"))
                    {
                        valid++;
                    }
                }
                Excel.ChartObjects xlChart = (Excel.ChartObjects)xlWorkSheet.ChartObjects(Type.Missing);
                Excel.ChartObject myChart = (Excel.ChartObject)xlChart.Add(400 * valid, 100, 700, 425);
                Excel.Chart chartPage = myChart.Chart;

                chartPage.Legend.Position = Excel.XlLegendPosition.xlLegendPositionBottom;
                myChart.Select();

                chartPage.ChartType = Excel.XlChartType.xlXYScatter;
                Excel.SeriesCollection seriesCollection = chartPage.SeriesCollection();
                int row = dt.Rows.Count;
                int position = 0;
                xlWorkSheet.Cells[2, 8] = "False Negative";
                xlWorkSheet.Cells[3, 8] = "False Positive";
                xlWorkSheet.Cells[4, 8] = "PTA";
                xlWorkSheet.Cells[5, 8] = "Mean Orig";
                xlWorkSheet.Cells[6, 8] = "Std Dev Sample";
                xlWorkSheet.Cells[7, 8] = "300%";
                xlWorkSheet.Cells[8, 8] = "Threshold (Mean+ 300% of Std Dev)";
                xlWorkSheet.Cells[2, 9].Formula = "=COUNTIF(B2:B" + row + ",\">\"&D2)-COUNTIF(B2:B" + row + ",-1)";
                xlWorkSheet.Cells[3, 9].Formula = "=COUNTIF(C2:C" + row + ",\"<\"&D2)-COUNTIF(C2:C" + row + ",-1)-COUNTIF(C2:C" + row + ",0)";
                xlWorkSheet.Cells[4, 9].Formula = "=COUNTIF(B2:B" + row + ",-1)+COUNTIF(C2:C" + row + ",-1)";
                xlWorkSheet.Cells[5, 9].Formula = "=AVERAGEIF(B2:B" + row + ",\"<>-1\")";
                xlWorkSheet.Cells[6, 9].Formula = "=STDEV.S(B2:B" + row + ")";
                xlWorkSheet.Cells[7, 9].Formula = "=I6*SUBSTITUTE(H7,\"%\",)";
                xlWorkSheet.Cells[8, 9].Formula = "=I5+I7";

                FormatCondition formatOrig = (FormatCondition)(xlWorkSheet.get_Range("B2:B" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlGreater, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatOrig.Interior.Color = 0x000000FF;

                FormatCondition formatCopy = (FormatCondition)(xlWorkSheet.get_Range("C2:C" + row,
                Type.Missing).FormatConditions.Add(XlFormatConditionType.xlCellValue, XlFormatConditionOperator.xlLess, "=$D$2", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));
                formatCopy.Interior.Color = 0x000000FF;

                xlWorkSheet.Columns[8].ColumnWidth = 20;
                xlWorkSheet.Columns[9].ColumnWidth = 20;


                Excel.Series seriesOrig = seriesCollection.NewSeries();
                seriesOrig.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesOrig.Name = "Original";
                seriesOrig.MarkerSize = 5;
                seriesOrig.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbRed;
                seriesOrig.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbRed;
                seriesOrig.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                seriesOrig.Values = xlWorkSheet.get_Range("B2", "B" + row);


                Excel.Series seriesCopy = seriesCollection.NewSeries();
                seriesCopy.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesCopy.Name = "Copy";
                seriesCopy.MarkerSize = 5;
                seriesCopy.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbYellowGreen;
                seriesCopy.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbYellowGreen;
                seriesCopy.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                seriesCopy.Values = xlWorkSheet.get_Range("C2", "C" + row);

                Excel.Series seriesThreshold = seriesCollection.NewSeries();
                seriesThreshold.MarkerStyle = Excel.XlMarkerStyle.xlMarkerStyleCircle;
                seriesThreshold.Name = "Threshold";
                seriesThreshold.MarkerSize = 5;
                seriesThreshold.MarkerBackgroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                seriesThreshold.MarkerForegroundColor = (int)Excel.XlRgbColor.rgbLightGray;
                seriesThreshold.XValues = xlWorkSheet.get_Range("A2", "A" + row);
                seriesThreshold.Values = xlWorkSheet.get_Range("D2", "D" + row);
                position++;
                if (i != listCode.Count - 1)
                {
                    xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
                }

                index += 2;
            }

            xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);

            xlWorkSheet.Name = "Summary COPY all data";
            DataTable dtCopyAll = GetDataAll(listDataRawCopy);
            object[,] arrayRawCopy = new object[dtCopyAll.Rows.Count, dtCopyAll.Columns.Count];


            for (int k = 0; k < dtCopyAll.Rows.Count; k++)
            {
                for (int j = 0; j < dtCopyAll.Columns.Count; j++)
                {
                    arrayRawCopy[k, j] = dtCopyAll.Rows[k][j];


                }
            }
            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtCopyAll.Rows.Count, dtCopyAll.Columns.Count]].Value = arrayRawCopy;
            int rowCopy = dtCopyAll.Rows.Count + 2;
            xlWorkSheet.Cells[rowCopy, 1] = "Remarks";
            xlWorkSheet.Cells[rowCopy, 1].Font.Bold = true;
            xlWorkSheet.Cells[rowCopy, 1].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowCopy, 2] = "Decision Tree Rating";
            xlWorkSheet.Cells[rowCopy, 2].Font.Bold = true;
            xlWorkSheet.Cells[rowCopy, 2].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowCopy, 3] = "Decision Tree U-NICA";
            xlWorkSheet.Cells[rowCopy, 3].Font.Bold = true;
            xlWorkSheet.Cells[rowCopy, 3].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowCopy, 4] = "Total";
            xlWorkSheet.Cells[rowCopy, 4].Font.Bold = true;
            xlWorkSheet.Cells[rowCopy, 4].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowCopy, 5] = dtCopyAll.Rows.Count - 1;
            xlWorkSheet.Cells[rowCopy, 5].Font.Bold = true;
            xlWorkSheet.Cells[rowCopy, 5].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowCopy, 6] = "1";
            xlWorkSheet.Cells[rowCopy, 6].Font.Bold = true;
            xlWorkSheet.Cells[rowCopy, 6].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

            String[] resultCopy = txtCriteriaCopy.Text.Split(';');
            for (int i = 0; i < resultCopy.Length; i++)
            {
                rowCopy = dtCopyAll.Rows.Count + i + 3;
                xlWorkSheet.Cells[rowCopy, 2] = resultCopy[i].Split('-')[0];
                xlWorkSheet.Cells[rowCopy, 2].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowCopy, 3] = resultCopy[i].Split('-')[1];
                xlWorkSheet.Cells[rowCopy, 3].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowCopy, 4] = resultCopy[i].Split('-')[2];
                xlWorkSheet.Cells[rowCopy, 4].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowCopy, 5].Formula = "=SUMIF(AC2:AC" + dtCopyAll.Rows.Count + "," + resultCopy[i].Split('-')[2].Split('(')[1].Split(')')[0] + ",AB2:AB" + dtCopyAll.Rows.Count + ")";
                xlWorkSheet.Cells[rowCopy, 5].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowCopy, 6].Formula = "=E" + rowCopy + "/$E$" + (dtCopyAll.Rows.Count + 2);
                xlWorkSheet.Cells[rowCopy, 6].Font.Bold = true;
                xlWorkSheet.Cells[rowCopy, 6].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                xlWorkSheet.Cells[rowCopy - 1, 8] = resultCopy[i].Split('-')[0] + " Total";
                xlWorkSheet.Cells[rowCopy - 1, 8].Font.Bold = true;
                xlWorkSheet.Cells[rowCopy - 1, 8].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowCopy - 1, 9].Formula = "=SUMIF(B" + (dtCopyAll.Rows.Count + 2) + ":B" + (dtCopyAll.Rows.Count + 2 + resultCopy.Length) + ",\"" + resultCopy[i].Split('-')[0] + "\", F" + (dtCopyAll.Rows.Count + 2) + ": F" + (dtCopyAll.Rows.Count + 2 + resultCopy.Length) + ")";
                xlWorkSheet.Cells[rowCopy - 1, 9].Formula = "=SUMIF(B" + (dtCopyAll.Rows.Count + 2) + ":B" + (dtCopyAll.Rows.Count + 2 + resultCopy.Length) + ",\"" + resultCopy[i].Split('-')[0] + "\", F" + (dtCopyAll.Rows.Count + 2) + ": F" + (dtCopyAll.Rows.Count + 2 + resultCopy.Length) + ")";
                xlWorkSheet.Cells[rowCopy - 1, 9].Font.Bold = true;
                xlWorkSheet.Cells[rowCopy - 1, 9].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

            }
            Excel.Range percentCopy = xlWorkSheet.get_Range("F" + (dtCopyAll.Rows.Count + 2), "F" + (dtCopyAll.Rows.Count + 2 + resultCopy.Length));
            percentCopy.NumberFormat = "###.##%";
            percentCopy = xlWorkSheet.get_Range("I" + (dtCopyAll.Rows.Count + 2), "I" + (dtCopyAll.Rows.Count + 2 + resultCopy.Length));
            percentCopy.NumberFormat = "###.##%";
            Excel.Range autofitRange = xlWorkSheet.get_Range("A" + (dtCopyAll.Rows.Count + 2), "I" + (dtCopyAll.Rows.Count + 1 + resultCopy.Length));
            autofitRange.EntireColumn.AutoFit();
            autofitRange.Select();

            Excel.Range borderRange = xlWorkSheet.get_Range("A" + (dtCopyAll.Rows.Count + 2), "F" + (dtCopyAll.Rows.Count + 2 + resultCopy.Length));
            borderRange.Cells.Borders.LineStyle = XlLineStyle.xlContinuous;
            borderRange = xlWorkSheet.get_Range("H" + (dtCopyAll.Rows.Count + 2), "I" + (dtCopyAll.Rows.Count + 1 + resultCopy.Length));
            borderRange.Cells.Borders.LineStyle = XlLineStyle.xlContinuous;

            Excel.Range rangeRawCopy = xlWorkSheet.Rows[1];
            rangeRawCopy.Select();
            rangeRawCopy.Font.Bold = true;
            rangeRawCopy.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            xlWorkBook.Worksheets.Add(After: xlWorkBook.Sheets[xlWorkBook.Sheets.Count]);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(xlWorkBook.Sheets.Count);
            xlWorkSheet.Name = "Summary ORIG all data";
            DataTable dtOrigAll = GetDataAll(listDataRawOrig);
            object[,] arrayRawOrig = new object[dtOrigAll.Rows.Count, dtOrigAll.Columns.Count];

            for (int k = 0; k < dtOrigAll.Rows.Count; k++)
            {
                for (int j = 0; j < dtOrigAll.Columns.Count; j++)
                {
                    arrayRawOrig[k, j] = dtOrigAll.Rows[k][j];


                }
            }
            xlWorkSheet.Range[xlWorkSheet.Cells[1, 1], xlWorkSheet.Cells[dtOrigAll.Rows.Count, dtOrigAll.Columns.Count]].Value = arrayRawOrig;

            int rowOrig = dtOrigAll.Rows.Count + 2;
            xlWorkSheet.Cells[rowOrig, 1] = "Remarks";
            xlWorkSheet.Cells[rowOrig, 1].Font.Bold = true;
            xlWorkSheet.Cells[rowOrig, 1].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowOrig, 2] = "Decision Tree Rating";
            xlWorkSheet.Cells[rowOrig, 2].Font.Bold = true;
            xlWorkSheet.Cells[rowOrig, 2].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowOrig, 3] = "Decision Tree U-NICA";
            xlWorkSheet.Cells[rowOrig, 3].Font.Bold = true;
            xlWorkSheet.Cells[rowOrig, 3].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowOrig, 4] = "Total";
            xlWorkSheet.Cells[rowOrig, 4].Font.Bold = true;
            xlWorkSheet.Cells[rowOrig, 4].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowOrig, 5] = dtOrigAll.Rows.Count - 1;
            xlWorkSheet.Cells[rowOrig, 5].Font.Bold = true;
            xlWorkSheet.Cells[rowOrig, 5].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            xlWorkSheet.Cells[rowOrig, 6] = "1";
            xlWorkSheet.Cells[rowOrig, 6].Font.Bold = true;
            xlWorkSheet.Cells[rowOrig, 6].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

            String[] resultOrig = txtCriteriaCopy.Text.Split(';');
            for (int i = 0; i < resultOrig.Length; i++)
            {
                rowOrig = dtOrigAll.Rows.Count + i + 3;
                xlWorkSheet.Cells[rowOrig, 2] = resultOrig[i].Split('-')[0];
                xlWorkSheet.Cells[rowOrig, 2].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowOrig, 3] = resultOrig[i].Split('-')[1];
                xlWorkSheet.Cells[rowOrig, 3].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowOrig, 4] = resultOrig[i].Split('-')[2];
                xlWorkSheet.Cells[rowOrig, 4].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowOrig, 5].Formula = "=SUMIF(AC2:AC" + dtOrigAll.Rows.Count + "," + resultOrig[i].Split('-')[2].Split('(')[1].Split(')')[0] + ",AB2:AB" + dtOrigAll.Rows.Count + ")";
                xlWorkSheet.Cells[rowOrig, 5].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowOrig, 6].Formula = "=E" + rowOrig + "/$E$" + (dtOrigAll.Rows.Count + 2);
                xlWorkSheet.Cells[rowOrig, 6].Font.Bold = true;
                xlWorkSheet.Cells[rowOrig, 6].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                xlWorkSheet.Cells[rowOrig - 1, 8] = resultOrig[i].Split('-')[0] + " Total";
                xlWorkSheet.Cells[rowOrig - 1, 8].Font.Bold = true;
                xlWorkSheet.Cells[rowOrig - 1, 8].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                xlWorkSheet.Cells[rowOrig - 1, 9].Formula = "=SUMIF(B" + (dtOrigAll.Rows.Count + 2) + ":B" + (dtOrigAll.Rows.Count + 2 + resultOrig.Length) + ",\"" + resultOrig[i].Split('-')[0] + "\", F" + (dtOrigAll.Rows.Count + 2) + ": F" + (dtOrigAll.Rows.Count + 2 + resultOrig.Length) + ")";
                xlWorkSheet.Cells[rowOrig - 1, 9].Formula = "=SUMIF(B" + (dtOrigAll.Rows.Count + 2) + ":B" + (dtOrigAll.Rows.Count + 2 + resultOrig.Length) + ",\"" + resultOrig[i].Split('-')[0] + "\", F" + (dtOrigAll.Rows.Count + 2) + ": F" + (dtOrigAll.Rows.Count + 2 + resultOrig.Length) + ")";
                xlWorkSheet.Cells[rowOrig - 1, 9].Font.Bold = true;
                xlWorkSheet.Cells[rowOrig - 1, 9].Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

            }
            Excel.Range percentOrig = xlWorkSheet.get_Range("F" + (dtOrigAll.Rows.Count + 2), "F" + (dtOrigAll.Rows.Count + 2 + resultOrig.Length));
            percentOrig.NumberFormat = "###.##%";
            percentOrig = xlWorkSheet.get_Range("I" + (dtOrigAll.Rows.Count + 2), "I" + (dtOrigAll.Rows.Count + 2 + resultOrig.Length));
            percentOrig.NumberFormat = "###.##%";
            Excel.Range autofitRangeOrig = xlWorkSheet.get_Range("A" + (dtOrigAll.Rows.Count + 2), "I" + (dtOrigAll.Rows.Count + 1 + resultOrig.Length));
            autofitRangeOrig.EntireColumn.AutoFit();
            autofitRangeOrig.Select();

            Excel.Range borderRangeOrig = xlWorkSheet.get_Range("A" + (dtOrigAll.Rows.Count + 2), "F" + (dtOrigAll.Rows.Count + 2 + resultOrig.Length));
            borderRangeOrig.Cells.Borders.LineStyle = XlLineStyle.xlContinuous;
            borderRangeOrig = xlWorkSheet.get_Range("H" + (dtOrigAll.Rows.Count + 2), "I" + (dtOrigAll.Rows.Count + 1 + resultOrig.Length));
            borderRangeOrig.Cells.Borders.LineStyle = XlLineStyle.xlContinuous;

            Excel.Range rangeRawOrig = xlWorkSheet.Rows[1];
            rangeRawOrig.Select();
            rangeRawOrig.Font.Bold = true;
            rangeRawOrig.Cells.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

            String fileName = txtSaveAs.Text + @"\" + txtFileName.Text;

            try
            {
                xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookDefault, Missing.Value, Missing.Value, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, Missing.Value, Missing.Value, Missing.Value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Marshal.ReleaseComObject(xlWorkSheet);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();
            MessageBox.Show("Excel file " + fileName + " has been created.");
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            progressExport.Visible = false;
        }

        private void GetResult()
        {

            listSaveCopy = new List<List<ResultTestInfo>>();
            listSaveOrig = new List<List<ResultTestInfo>>();
            listCode = new List<String>();
            DirectoryInfo d = new DirectoryInfo(txtFolderPath.Text);
            FileInfo[] files = d.GetFiles("*.csv");

            for (int i = 0; i < files.Count(); i++)
            {
                if (files[i].Name.Contains("Orig"))
                {
                    listCode.Add(files[i].Name.Replace("Orig.csv", ""));
                }
            }
            listRange = new List<Excel.Range>();
            int index = 1;
            progressExport.Value = 0;
            progressExport.Minimum = 0;
            progressExport.Visible = true;
            progressExport.Maximum = listCode.Count() * 2;
            for (int i = 0; i < listCode.Count(); i++)
            {
                String fileOrig = txtFolderPath.Text + @"\" + listCode[i] + "Orig.csv";
                List<ResultTestInfo> listResult = ReadCSV(fileOrig, index);
                listSaveOrig.Add(listResult);
                index++;
            }

            for (int i = 0; i < listCode.Count(); i++)
            {
                String fileCopy = txtFolderPath.Text + @"\" + listCode[i] + "Copy.csv";
                List<ResultTestInfo> listResult = ReadCSV(fileCopy, index);
                listSaveCopy.Add(listResult);
                index++;
            }
            progressExport.Visible = false;
        }

        private List<ResultTestInfo> ReadCSV(String file, int progress)
        {
            List<String> listRawData = Ultility.GetRawData(file);
            progressExport.Value = progress;
            Product product = Ultility.GetProductFromCSV(listRawData);
            List<List<String>> listCleanedData = Ultility.GetCleanData(file, product);
            List<ResultTestInfo> listResult = Ultility.GetResultListFromDataRaw(listCleanedData, product.listZone, 1, listCleanedData.Count, false);
            return listResult;
        }

        private DataTable GetDataAll(List<DataTable> listData)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < listData[0].Columns.Count; i++)
            {
                dt.Columns.Add();
            }
            int column = dt.Columns.Count;
            for (int i = 0; i < listData.Count; i++)
            {
                if (i == 0)
                {
                    for (int rw = 0; rw < listData[i].Rows.Count; rw++)
                    {
                        dt.Rows.Add();
                        for (int cl = 0; cl < column; cl++)
                        {
                            dt.Rows[dt.Rows.Count - 1][cl] = listData[i].Rows[rw][cl];
                        }
                    }
                }
                else
                {
                    for (int rw = 1; rw < listData[i].Rows.Count; rw++)
                    {
                        dt.Rows.Add();
                        for (int cl = 0; cl < column; cl++)
                        {
                            dt.Rows[dt.Rows.Count - 1][cl] = listData[i].Rows[rw][cl];
                        }

                    }
                }
            }
            return dt;
        }

        private DataTable GetDataTable(List<ResultTestInfo> listSaveOrig, List<ResultTestInfo> listSaveCopy, String deviceCode, Product product)
        {
            Device device = db.GetDeviceByCode(deviceCode);
            DataTable dt = new DataTable();

            for (int i = 0; i < product.listZone.Count; i++)
            {
                if (!product.listZone[i].method.Equals("STC"))
                {
                    dt.Columns.Add();
                    dt.Columns.Add();
                    dt.Columns.Add();
                }

            }
            dt.Columns.Add();
            dt.Columns.Add();
            dt.Columns.Add();
            int column = 1;
            //Adding the Rows.
            dt.Rows.Add();
            dt.Rows[0][0] = "Statistic ID";
            for (int i = 0; i < product.listZone.Count; i++)
            {
                if (!product.listZone[i].method.Equals("STC"))
                {
                    dt.Rows[0][column] = "CDS Orig";
                    dt.Rows[0][column + 1] = "CDS Copy";
                    dt.Rows[0][column + 2] = "Threshold";
                    column += 3;
                }

            }

            dt.Rows[0][column] = "Device Model";

            List<DataDrawChart> listDataDrawChartOrig = GetListDataDrawChart(listSaveOrig, product);
            List<DataDrawChart> listDataDrawChartCopy = GetListDataDrawChart(listSaveCopy, product);

            int quantityRow = (listDataDrawChartOrig.Count > listDataDrawChartCopy.Count ? listDataDrawChartOrig.Count : listDataDrawChartCopy.Count);
            for (int i = 0; i <= quantityRow; i++)
            {
                dt.Rows.Add();
            }
            int row = 1;
            for (int i = 0; i < listDataDrawChartOrig.Count; i++)
            {
                dt.Rows[row][0] = row;
                dt.Rows[row][1] = listDataDrawChartOrig[i].pointValue;
                row++;
            }
            row = 1;
            for (int i = 0; i < listDataDrawChartCopy.Count; i++)
            {
                int countCDS = 0;
                for (int k = 0; k < product.listZone.Count; k++)
                {
                    if (countCDS == 0)
                    {
                        if (product.listZone[k].method.Equals("CDS"))
                        {
                            dt.Rows[row][2] = listDataDrawChartCopy[i].pointValue;
                            dt.Rows[row][3] = product.listZone[k].threshold;
                            dt.Rows[row][4] = device.code + " - " + device.model;
                            countCDS++;
                            row++;
                        }
                    }
                }
            }
            return dt;

        }
    
        private List<DataDrawChart> GetListDataDrawChart(List<ResultTestInfo> listData, Product product)
        {
            List<DataDrawChart> listDataDrawChart = new List<DataDrawChart>();
            int CDSzone = -1;
            for(int i = 0; i< product.listZone.Count; i++)
            {
                if (product.listZone[i].method.Equals("CDS"))
                {
                    CDSzone = product.listZone[i].zoneNo;
                    i = product.listZone.Count;
                }
            }
            for(int i = 0; i< listData.Count; i++)
            {
                if (listData[i].zone == CDSzone)
                {
                    DataDrawChart data = new DataDrawChart();
                    data.statisticID = listData[i].statisticID;
                    if (Double.Parse(listData[i].resultZone) == -1 && !listData[i].infoZone.Contains("FineSigma"))
                    {
                        data.pointValue = Double.Parse(listData[i + 1].resultZone);
                    }
                    else
                    {
                        data.pointValue = Double.Parse(listData[i].resultZone);

                    }

                    listDataDrawChart.Add(data);
                }
            }
            return listDataDrawChart;
        }
    }
}
