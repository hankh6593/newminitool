﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class AutomationTest : MasterForm
    {
        DatabaseConnection db;
        List<Device> listDevice;
        List<Product> listProduct;
        List<Preset> listPreset;
        public AutomationTest() : base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            listDevice = new List<Device>();
            listProduct = new List<Product>();
            listPreset = new List<Preset>();
            LoadPreset();
            LoadDevice();
            LoadProduct();
        }

        [Obsolete]
        private void btnStart_Click(object sender, EventArgs e)
        {
            if (cbbPreset.SelectedIndex == -1)
            {
                Preset preset = new Preset();
                preset.name = txtPresetName.Text;
                int id;
                if (Int32.TryParse(cbbDevice.SelectedValue.ToString(), out id))
                {
                    preset.deviceID = id;
                }
                else
                {
                    preset.deviceID = Int32.Parse(((KeyValuePair<int, string>)cbbDevice.SelectedValue).Key.ToString());
                }
                preset.productID = Int32.Parse(cbbProduct.SelectedValue.ToString());
                preset.platformName = txtPlatformName.Text;
                preset.platformVersion = Double.Parse(txtPlatformVersion.Text);
                preset.isFlash = cbFlash.Checked;
                preset.isManual = cbManual.Checked;
                preset.quantity = Int32.Parse(txtQuantity.Text);
                preset.appPackage = txtAppPackage.Text;
                preset.appActivity = txtAppActivity.Text;
                preset.appiumHost = txtAppiumHost.Text;
                preset.appiumPort = Int64.Parse(txtAppiumPort.Text);
                preset.isImageSet = cbImageSet.Checked;
                if (cbImageSet.Checked)
                {
                    AutomationTestDetailImage form = new AutomationTestDetailImage(preset);
                    form.Show();
                }
                else
                {
                    AutomationTestDetail form = new AutomationTestDetail(preset);
                    form.Show();
                }


            }
            else
            {
                Preset preset = db.GetPresetByID(Int32.Parse(cbbPreset.SelectedValue.ToString()));
                if (cbImageSet.Checked)
                {
                    AutomationTestDetailImage form = new AutomationTestDetailImage(preset);
                    form.Show();
                }
                else
                {
                    AutomationTestDetail form = new AutomationTestDetail(preset);
                    form.Show();
                }

            }
        }

        private void LoadPreset()
        {
            listPreset = db.GetAllPresets();
            var bindingPreset = new BindingSource();
            bindingPreset.DataSource = listPreset;

            cbbPreset.DataSource = bindingPreset.DataSource;
            cbbPreset.DisplayMember = "name";
            cbbPreset.ValueMember = "id";
            cbbPreset.SelectedIndex = -1;

        }
        private void LoadDevice()
        {
            listDevice = db.GetAllDevices(0);

            Dictionary<int, String> listDislplay = new Dictionary<int, string>();
            for (int i = 0; i < listDevice.Count; i++)
            {
                listDislplay.Add(listDevice[i].id, listDevice[i].name + " (" + listDevice[i].location + ")");
            }

            var bindingDevice = new BindingSource();
            bindingDevice.DataSource = listDislplay;

            cbbDevice.DataSource = new BindingSource(listDislplay, null);
            cbbDevice.ValueMember = "Key";
            cbbDevice.DisplayMember = "Value";
            cbbDevice.SelectedIndex = -1;

        }
        private void LoadProduct()
        {

            listProduct = db.GetAllProducts();
            var bindingProduct = new BindingSource();
            bindingProduct.DataSource = listProduct;

            cbbProduct.DataSource = bindingProduct.DataSource;

            cbbProduct.DisplayMember = "productName";
            cbbProduct.ValueMember = "id";
            cbbProduct.SelectedIndex = -1;

        }

        private void cbbPreset_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbPreset.SelectedIndex == -1)
            {
                btnAddPreset.Enabled = true;
                cbbPreset.SelectedIndex = -1;
                cbbDevice.SelectedIndex = -1;
                cbbProduct.SelectedIndex = -1;
                txtPlatformName.Text = "";
                txtPlatformVersion.Text = "";
                txtQuantity.Text = "";
                cbManual.Checked = false;
                cbFlash.Checked = false;
                cbImageSet.Checked = false;
                txtPresetName.Text = "";
            }
            else
            {
                btnAddPreset.Enabled = false;
                Preset preset;
                int result;
                if (Int32.TryParse(cbbPreset.SelectedValue.ToString(), out result))
                {
                    preset = db.GetPresetByID((int)cbbPreset.SelectedValue);
                }
                else
                {
                    preset = (Preset)cbbPreset.SelectedValue;
                }

                cbbDevice.SelectedValue = preset.deviceID;
                cbbProduct.SelectedValue = preset.productID;
                txtPresetName.Text = preset.name;
                txtPlatformName.Text = preset.platformName;
                txtPlatformVersion.Text = preset.platformVersion.ToString();
                txtAppActivity.Text = preset.appActivity;
                txtAppPackage.Text = preset.appPackage;
                txtAppiumHost.Text = preset.appiumHost;
                txtAppiumPort.Text = preset.appiumPort.ToString(); ;
                txtQuantity.Text = preset.quantity.ToString();
                cbFlash.Checked = preset.isFlash;
                cbManual.Checked = preset.isManual;
                cbImageSet.Checked = preset.isImageSet;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cbbPreset.SelectedIndex = -1;
            cbbDevice.SelectedIndex = -1;
            cbbProduct.SelectedIndex = -1;
            txtPlatformName.Text = "";
            txtPlatformVersion.Text = "";
            txtQuantity.Text = "";
            cbManual.Checked = false;
            cbFlash.Checked = false;
            btnAddPreset.Enabled = true;
            cbImageSet.Checked = false;
            txtPresetName.Text = "";
            txtAppActivity.Text = "com.anteleon.scrypto.controller.SplashActivity";
            txtAppiumHost.Text = "127.0.0.1";
            txtAppiumPort.Text = "4723";
            txtAppPackage.Text = "scryptoTRACE.codepub";
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            db.RemovePreset(Int32.Parse(cbbPreset.SelectedValue.ToString()));
            MessageBox.Show("The Preset has been removed.");
            LoadPreset();
            btnClear_Click(sender, e);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Preset preset = db.GetPresetByID(Int32.Parse(cbbPreset.SelectedValue.ToString()));
            preset.name = txtPresetName.Text;
            int id;
            if (Int32.TryParse(cbbDevice.SelectedValue.ToString(), out id))
            {
                preset.deviceID = id;
            }
            else
            {
                preset.deviceID = Int32.Parse(((KeyValuePair<int, string>)cbbDevice.SelectedValue).Key.ToString());
            }
            preset.productID = Int32.Parse(cbbProduct.SelectedValue.ToString());
            preset.platformName = txtPlatformName.Text;
            preset.platformVersion = Double.Parse(txtPlatformVersion.Text);
            preset.isFlash = cbFlash.Checked;
            preset.isManual = cbManual.Checked;
            preset.quantity = Int32.Parse(txtQuantity.Text);
            preset.appPackage = txtAppPackage.Text;
            preset.appActivity = txtAppActivity.Text;
            preset.appiumHost = txtAppiumHost.Text;
            preset.appiumPort = Int64.Parse(txtAppiumPort.Text);
            preset.isImageSet = cbImageSet.Checked;
            db.UpdatePreset(preset);
            MessageBox.Show("The Preset Information has been updated.");
        }

        private void cbbPreset_TextChanged(object sender, EventArgs e)
        {
            if (cbbPreset.Text.Length == 0)
            {
                btnAddPreset.Enabled = true;
            }
        }

        private void cbbDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id;
            if (cbbDevice.SelectedIndex != -1)
            {
                if (Int32.TryParse(cbbDevice.SelectedItem.ToString(), out id))
                {
                    Device device = db.GetDeviceByID(id);
                    txtPlatformName.Text = device.type;
                }
                else
                {
                    Device device = db.GetDeviceByID(((KeyValuePair<int, string>)cbbDevice.SelectedItem).Key);
                    txtPlatformName.Text = device.type;
                }
            }
        }

        private void btnAddPreset_Click(object sender, EventArgs e)
        {
            Preset preset = new Preset();
            preset.name = txtPresetName.Text;
            int id;
            if (Int32.TryParse(cbbDevice.SelectedValue.ToString(), out id))
            {
                preset.deviceID = id;
            }
            else
            {
                preset.deviceID = Int32.Parse(((KeyValuePair<int, string>)cbbDevice.SelectedValue).Key.ToString());
            }
            preset.productID = Int32.Parse(cbbProduct.SelectedValue.ToString());
            preset.platformName = txtPlatformName.Text;
            preset.platformVersion = Double.Parse(txtPlatformVersion.Text);
            preset.isFlash = cbFlash.Checked;
            preset.isManual = cbManual.Checked;
            preset.quantity = Int32.Parse(txtQuantity.Text);
            preset.appPackage = txtAppPackage.Text;
            preset.appActivity = txtAppActivity.Text;
            preset.appiumHost = txtAppiumHost.Text;
            preset.appiumPort = Int64.Parse(txtAppiumPort.Text);
            preset.isImageSet = cbImageSet.Checked;
            preset.id = db.AddPreset(preset);
            MessageBox.Show("New Preset has been added successfully.");
            btnClear_Click(sender, e);
            LoadPreset();
        }
    }
}
