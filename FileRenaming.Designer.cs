﻿namespace MiniTool
{
    partial class FileRenaming
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRename = new System.Windows.Forms.Button();
            this.btnFolderBrowse = new System.Windows.Forms.Button();
            this.txtRenamingFolder = new System.Windows.Forms.TextBox();
            this.lbPath = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbType = new System.Windows.Forms.ComboBox();
            this.cbIsOriginal = new System.Windows.Forms.CheckBox();
            this.lblSequenceNo = new System.Windows.Forms.Label();
            this.txtSequenceNo = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.folderBrowseDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.fileBrowseDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblLocation = new System.Windows.Forms.Label();
            this.txtLocation = new System.Windows.Forms.TextBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.cbIsMerged = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRename
            // 
            this.btnRename.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnRename.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRename.ForeColor = System.Drawing.Color.White;
            this.btnRename.Location = new System.Drawing.Point(243, 366);
            this.btnRename.Margin = new System.Windows.Forms.Padding(4);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(201, 49);
            this.btnRename.TabIndex = 65;
            this.btnRename.Text = "Rename";
            this.btnRename.UseVisualStyleBackColor = false;
            this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
            // 
            // btnFolderBrowse
            // 
            this.btnFolderBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnFolderBrowse.FlatAppearance.BorderSize = 0;
            this.btnFolderBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFolderBrowse.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnFolderBrowse.Location = new System.Drawing.Point(1213, 143);
            this.btnFolderBrowse.Margin = new System.Windows.Forms.Padding(4);
            this.btnFolderBrowse.Name = "btnFolderBrowse";
            this.btnFolderBrowse.Size = new System.Drawing.Size(61, 49);
            this.btnFolderBrowse.TabIndex = 61;
            this.btnFolderBrowse.Text = "...";
            this.btnFolderBrowse.UseVisualStyleBackColor = false;
            this.btnFolderBrowse.Click += new System.EventHandler(this.btnFolderBrowse_Click);
            // 
            // txtRenamingFolder
            // 
            this.txtRenamingFolder.BackColor = System.Drawing.Color.White;
            this.txtRenamingFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRenamingFolder.Location = new System.Drawing.Point(245, 151);
            this.txtRenamingFolder.Margin = new System.Windows.Forms.Padding(4);
            this.txtRenamingFolder.Name = "txtRenamingFolder";
            this.txtRenamingFolder.Size = new System.Drawing.Size(933, 34);
            this.txtRenamingFolder.TabIndex = 58;
            // 
            // lbPath
            // 
            this.lbPath.AutoSize = true;
            this.lbPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPath.Location = new System.Drawing.Point(21, 154);
            this.lbPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPath.Name = "lbPath";
            this.lbPath.Size = new System.Drawing.Size(206, 29);
            this.lbPath.TabIndex = 55;
            this.lbPath.Text = "Renaming Folder:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 300);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 29);
            this.label2.TabIndex = 55;
            this.label2.Text = "Type:";
            // 
            // cbbType
            // 
            this.cbbType.Font = new System.Drawing.Font("Arial", 14F);
            this.cbbType.FormattingEnabled = true;
            this.cbbType.Items.AddRange(new object[] {
            "Merge File",
            "Device ID",
            "Device Model",
            "Convert DeviceID to DeviceModel",
            "Convert DeviceModel to DeviceID"});
            this.cbbType.Location = new System.Drawing.Point(243, 297);
            this.cbbType.Margin = new System.Windows.Forms.Padding(4);
            this.cbbType.Name = "cbbType";
            this.cbbType.Size = new System.Drawing.Size(448, 34);
            this.cbbType.TabIndex = 67;
            this.cbbType.SelectedIndexChanged += new System.EventHandler(this.cbbType_SelectedIndexChanged);
            // 
            // cbIsOriginal
            // 
            this.cbIsOriginal.AutoSize = true;
            this.cbIsOriginal.Font = new System.Drawing.Font("Arial", 14F);
            this.cbIsOriginal.Location = new System.Drawing.Point(243, 226);
            this.cbIsOriginal.Margin = new System.Windows.Forms.Padding(4);
            this.cbIsOriginal.Name = "cbIsOriginal";
            this.cbIsOriginal.Size = new System.Drawing.Size(155, 31);
            this.cbIsOriginal.TabIndex = 68;
            this.cbIsOriginal.Text = "is Original?";
            this.cbIsOriginal.UseVisualStyleBackColor = true;
            // 
            // lblSequenceNo
            // 
            this.lblSequenceNo.AutoSize = true;
            this.lblSequenceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSequenceNo.Location = new System.Drawing.Point(732, 226);
            this.lblSequenceNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSequenceNo.Name = "lblSequenceNo";
            this.lblSequenceNo.Size = new System.Drawing.Size(173, 29);
            this.lblSequenceNo.TabIndex = 55;
            this.lblSequenceNo.Text = "Sequence No.:";
            // 
            // txtSequenceNo
            // 
            this.txtSequenceNo.BackColor = System.Drawing.Color.White;
            this.txtSequenceNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSequenceNo.Location = new System.Drawing.Point(924, 223);
            this.txtSequenceNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtSequenceNo.Name = "txtSequenceNo";
            this.txtSequenceNo.Size = new System.Drawing.Size(255, 34);
            this.txtSequenceNo.TabIndex = 58;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(737, 366);
            this.progressBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(443, 49);
            this.progressBar.TabIndex = 69;
            // 
            // fileBrowseDialog
            // 
            this.fileBrowseDialog.FileName = "openFileDialog1";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocation.Location = new System.Drawing.Point(801, 297);
            this.lblLocation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(110, 29);
            this.lblLocation.TabIndex = 55;
            this.lblLocation.Text = "Location:";
            // 
            // txtLocation
            // 
            this.txtLocation.BackColor = System.Drawing.Color.White;
            this.txtLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocation.Location = new System.Drawing.Point(924, 297);
            this.txtLocation.Margin = new System.Windows.Forms.Padding(4);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(255, 34);
            this.txtLocation.TabIndex = 58;
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(129)))), ((int)(((byte)(213)))));
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.White;
            this.btnReset.Location = new System.Drawing.Point(491, 366);
            this.btnReset.Margin = new System.Windows.Forms.Padding(4);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(201, 49);
            this.btnReset.TabIndex = 65;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // cbIsMerged
            // 
            this.cbIsMerged.AutoSize = true;
            this.cbIsMerged.Font = new System.Drawing.Font("Arial", 14F);
            this.cbIsMerged.Location = new System.Drawing.Point(527, 224);
            this.cbIsMerged.Margin = new System.Windows.Forms.Padding(4);
            this.cbIsMerged.Name = "cbIsMerged";
            this.cbIsMerged.Size = new System.Drawing.Size(152, 31);
            this.cbIsMerged.TabIndex = 68;
            this.cbIsMerged.Text = "is Merged?";
            this.cbIsMerged.UseVisualStyleBackColor = true;
            this.cbIsMerged.CheckedChanged += new System.EventHandler(this.cbIsMerged_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(12, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(238, 39);
            this.label4.TabIndex = 70;
            this.label4.Text = "File Renaming";
            // 
            // FileRenaming
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1300, 454);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.cbIsMerged);
            this.Controls.Add(this.cbIsOriginal);
            this.Controls.Add(this.cbbType);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnRename);
            this.Controls.Add(this.btnFolderBrowse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLocation);
            this.Controls.Add(this.txtSequenceNo);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.lblSequenceNo);
            this.Controls.Add(this.txtRenamingFolder);
            this.Controls.Add(this.lbPath);
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "FileRenaming";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "U-nica Mini tool v3.11";
            this.Controls.SetChildIndex(this.lbPath, 0);
            this.Controls.SetChildIndex(this.txtRenamingFolder, 0);
            this.Controls.SetChildIndex(this.lblSequenceNo, 0);
            this.Controls.SetChildIndex(this.lblLocation, 0);
            this.Controls.SetChildIndex(this.txtSequenceNo, 0);
            this.Controls.SetChildIndex(this.txtLocation, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.btnFolderBrowse, 0);
            this.Controls.SetChildIndex(this.btnRename, 0);
            this.Controls.SetChildIndex(this.btnReset, 0);
            this.Controls.SetChildIndex(this.cbbType, 0);
            this.Controls.SetChildIndex(this.cbIsOriginal, 0);
            this.Controls.SetChildIndex(this.cbIsMerged, 0);
            this.Controls.SetChildIndex(this.progressBar, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.Button btnFolderBrowse;
        private System.Windows.Forms.TextBox txtRenamingFolder;
        private System.Windows.Forms.Label lbPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbbType;
        private System.Windows.Forms.CheckBox cbIsOriginal;
        private System.Windows.Forms.Label lblSequenceNo;
        private System.Windows.Forms.TextBox txtSequenceNo;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.FolderBrowserDialog folderBrowseDialog;
        private System.Windows.Forms.OpenFileDialog fileBrowseDialog;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.TextBox txtLocation;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.CheckBox cbIsMerged;
        private System.Windows.Forms.Label label4;
    }
}