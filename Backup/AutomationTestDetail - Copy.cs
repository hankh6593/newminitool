﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MiniTool.Controller;
using MiniTool.Data;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniTool
{
    public partial class AutomationTestDetail : Form
    {
        [TestClass]
        public class UnitTest
        {
            public AppiumDriver<AppiumWebElement> driver;
            DatabaseConnection db = new DatabaseConnection();

            [Obsolete]
            public AndroidDriver<AppiumWebElement> BeforeAllAndroidManual(Preset preset)
            {

                AppiumOptions capabilities = new AppiumOptions();

                capabilities.AddAdditionalCapability(MobileCapabilityType.DeviceName, db.GetDeviceByID(preset.deviceID).name);
                capabilities.AddAdditionalCapability(MobileCapabilityType.PlatformName, preset.platformName);
                capabilities.AddAdditionalCapability(MobileCapabilityType.PlatformVersion, preset.platformVersion.ToString());
                capabilities.AddAdditionalCapability("appPackage", preset.appPackage);
                capabilities.AddAdditionalCapability("appActivity", preset.appActivity);
                capabilities.AddAdditionalCapability(MobileCapabilityType.Udid, db.GetDeviceByID(preset.deviceID).UDID);
                capabilities.AddAdditionalCapability(MobileCapabilityType.FullReset, "false");
                capabilities.AddAdditionalCapability(MobileCapabilityType.NoReset, "true");


                AndroidDriver<AppiumWebElement> driver = new AndroidDriver<AppiumWebElement>(
                                           new Uri("http://" + preset.appiumHost + ":" + preset.appiumPort + "/wd/hub"),
                                               capabilities);
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(180);

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                wait.Until(ExpectedConditions.ElementExists(By.Id("scryptoTRACE.codepub:id/gridViewProductSelectionText")));

                IList<AppiumWebElement> els = driver.FindElementsByClassName("android.widget.TextView");
                bool isExisted = false;
                Thread.Sleep(1000);
                for (int i = 0; i < els.Count; i++)
                {
                    if (els[i].Text.Equals(db.GetProductByID(preset.productID).productName))
                    {
                        isExisted = true;
                        els[i].Click();
                        i = els.Count;
                    }
                }
                while (!isExisted)
                {
                    ITouchAction touchAction = new TouchAction(driver)
                    .Press(727, 1438)
                    .Wait(0)
                    .MoveTo(727, 1000)
                    .Release();

                    touchAction.Perform();

                    els = driver.FindElementsByClassName("android.widget.TextView");
                    for (int i = 0; i < els.Count; i++)
                    {
                        if (els[i].Text.Equals(db.GetProductByID(preset.productID).productName))
                        {
                            isExisted = true;
                            els[i].Click();
                            if (preset.isFlash)
                            {
                                var btn_flash = driver.FindElementById("scryptoTRACE.codepub:id/flash_button");
                                btn_flash.Click();
                            }
                            i = els.Count;

                        }
                    }

                }
                return driver;
            }


            [Obsolete]
            public List<AutomationTestResult> BeforeAllAndroidVG(Preset preset, DataGridView dgvResult, Label lblQuantity)
            {

                AppiumOptions capabilities = new AppiumOptions();

                capabilities.AddAdditionalCapability(MobileCapabilityType.DeviceName, db.GetDeviceByID(preset.deviceID).name);
                capabilities.AddAdditionalCapability(MobileCapabilityType.PlatformName, preset.platformName);
                capabilities.AddAdditionalCapability(MobileCapabilityType.PlatformVersion, preset.platformVersion.ToString());
                capabilities.AddAdditionalCapability("appPackage", preset.appPackage);
                capabilities.AddAdditionalCapability("appActivity", preset.appActivity);
                capabilities.AddAdditionalCapability(MobileCapabilityType.Udid, db.GetDeviceByID(preset.deviceID).UDID);
                capabilities.AddAdditionalCapability(MobileCapabilityType.FullReset, "false");
                capabilities.AddAdditionalCapability(MobileCapabilityType.NoReset, "true");


                AndroidDriver<AppiumWebElement> driver = new AndroidDriver<AppiumWebElement>(
                                           new Uri("http://" + preset.appiumHost + ":" + preset.appiumPort + "/wd/hub"),
                                               capabilities);
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(180);

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
                wait.Until(ExpectedConditions.ElementExists(By.Id("scryptoTRACE.codepub:id/gridViewProductSelectionText")));

                IList<AppiumWebElement> els = driver.FindElementsByClassName("android.widget.TextView");
                bool isExisted = false;
                Thread.Sleep(1000);
                for (int i = 0; i < els.Count; i++)
                {
                    if (els[i].Text.Equals(db.GetProductByID(preset.productID).productName))
                    {
                        isExisted = true;
                        els[i].Click();
                        i = els.Count;
                    }
                }
                while (!isExisted)
                {
                    ITouchAction touchAction = new TouchAction(driver)
                    .Press(727, 1438)
                    .Wait(0)
                    .MoveTo(727, 1000)
                    .Release();

                    touchAction.Perform();

                    els = driver.FindElementsByClassName("android.widget.TextView");
                    for (int i = 0; i < els.Count; i++)
                    {
                        if (els[i].Text.Equals(db.GetProductByID(preset.productID).productName))
                        {
                            isExisted = true;
                            els[i].Click();
                            if (preset.isFlash)
                            {
                                var btn_flash = driver.FindElementById("scryptoTRACE.codepub:id/flash_button");
                                btn_flash.Click();
                            }
                            i = els.Count;

                        }
                    }

                }
                List<AutomationTestResult> listResult = new List<AutomationTestResult>();
                for (int i = 0; i < preset.quantity; i++)
                {
            //        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                    AutomationTestResult automationResult = new AutomationTestResult();
                    automationResult.testDateTime = DateTime.Now.ToString();
                    wait = new WebDriverWait(driver, TimeSpan.FromSeconds(180));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id("scryptoTRACE.codepub:id/camera_preview_buttonCLoseZoneResultsView")));
                    var closeZoneResult_button = driver.FindElementById("scryptoTRACE.codepub:id/camera_preview_buttonCLoseZoneResultsView");
                    closeZoneResult_button.Click();
                    wait.Until(ExpectedConditions.ElementIsVisible(By.Id("scryptoTRACE.codepub:id/camera_preview_debug_scroll_view")));
                    IList<AppiumWebElement> elsZone = driver.FindElementsByClassName("android.widget.TextView");

                    foreach (AppiumWebElement el in elsZone)
                    {
                        if (el.Text.Contains("Zone"))
                        {
                            automationResult.listResult.Add(el.Text);
                        }
                    }

                    var closeDebugResult_button = driver.FindElementById("scryptoTRACE.codepub:id/camera_preview_debug_linear_layout");
                    closeDebugResult_button.Click();
                    listResult.Add(automationResult);
                    DataTable dt = new DataTable();
                    dt.Columns.Add("ID");
                    dt.Columns.Add("Captured Time");
                    for (int m = 0; m < listResult[0].listResult.Count; m++)
                    {
                        dt.Columns.Add("Zone " + (m + 1));
                    }
                    int column = dt.Columns.Count;
                    for (int m = 0; m < listResult.Count; m++)
                    {
                        dt.Rows.Add();
                        dt.Rows[m][0] = (m + 1);
                        dt.Rows[m][1] = listResult[m].testDateTime;
                        for (int k = 2; k < column; k++)
                        {
                            dt.Rows[m][k] = listResult[m].listResult[k - 2];
                        }
                    }
                    dgvResult.DataSource = dt;
                    for (int k = 2; k < dt.Columns.Count; k++)
                    {
                        dgvResult.Columns[k].Width = 400;
                    }
                    dgvResult.Refresh();

                    lblQuantity.Text = (Int32.Parse(lblQuantity.Text) - 1).ToString();
                    lblQuantity.Refresh();
                }
               

               
                return listResult;
            }

            [TestCleanup]
            public void AfterAll()
            {
                driver.Quit();
            }


            [TestMethod]
            [Obsolete]
            public AutomationTestResult Test(AndroidDriver<AppiumWebElement> driver)
            {
                AutomationTestResult automationResult = new AutomationTestResult();
                automationResult.testDateTime = DateTime.Now.ToString();
                Thread.Sleep(3000);
                var camera_button = driver.FindElementById("scryptoTRACE.codepub:id/camera_button");
                camera_button.Click();
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(180));
                wait.Until(ExpectedConditions.ElementIsVisible(By.Id("scryptoTRACE.codepub:id/camera_preview_buttonCLoseZoneResultsView")));
                var closeZoneResult_button = driver.FindElementById("scryptoTRACE.codepub:id/camera_preview_buttonCLoseZoneResultsView");
                closeZoneResult_button.Click();
                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(180));
                wait.Until(ExpectedConditions.ElementIsVisible(By.Id("scryptoTRACE.codepub:id/camera_preview_debug_scroll_view")));
                IList<AppiumWebElement> elsZone = driver.FindElementsByClassName("android.widget.TextView");

                foreach (AppiumWebElement el in elsZone)
                {
                    if (el.Text.Contains("Zone"))
                    {
                        automationResult.listResult.Add(el.Text);
                    }
                }

                var closeDebugResult_button = driver.FindElementById("scryptoTRACE.codepub:id/camera_preview_debug_linear_layout");
                closeDebugResult_button.Click();
                return automationResult;

            }

            [TestMethod]
            [Obsolete]
            public AutomationTestResult CollectData(AndroidDriver<AppiumWebElement> driver)
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
                AutomationTestResult automationResult = new AutomationTestResult();
                automationResult.testDateTime = DateTime.Now.ToString();
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(180));
                wait.Until(ExpectedConditions.ElementIsVisible(By.Id("scryptoTRACE.codepub:id/camera_preview_buttonCLoseZoneResultsView")));
                var closeZoneResult_button = driver.FindElementById("scryptoTRACE.codepub:id/camera_preview_buttonCLoseZoneResultsView");
                closeZoneResult_button.Click();
                wait = new WebDriverWait(driver, TimeSpan.FromSeconds(180));
                wait.Until(ExpectedConditions.ElementIsVisible(By.Id("scryptoTRACE.codepub:id/camera_preview_debug_scroll_view")));
                IList<AppiumWebElement> elsZone = driver.FindElementsByClassName("android.widget.TextView");

                foreach (AppiumWebElement el in elsZone)
                {
                    if (el.Text.Contains("Zone"))
                    {
                        automationResult.listResult.Add(el.Text);
                    }
                }

                var closeDebugResult_button = driver.FindElementById("scryptoTRACE.codepub:id/camera_preview_debug_linear_layout");
                closeDebugResult_button.Click();

                return automationResult;

            }
        }


        DatabaseConnection db;
        Preset preset;
        UnitTest test = new UnitTest();
        AndroidDriver<AppiumWebElement> driver;
        List<AutomationTestResult> listResult;

        String listFullData;
        public AutomationTestDetail()
        {
            InitializeComponent();
            db = new DatabaseConnection();
        }

        [Obsolete]
        public AutomationTestDetail(Preset preset)
        {
            InitializeComponent();
            this.preset = preset;
            db = new DatabaseConnection();
            lblDevice.Text = db.GetDeviceByID(preset.deviceID).model;
            lblProduct.Text = db.GetProductByID(preset.productID).productName;
            lblQuantity.Text = preset.quantity.ToString();

        }
        public int Hwnd { get; private set; }
        [Obsolete]
        private void btnTest_Click (object sender, EventArgs e)
        {
            if (preset.platformName.Equals("Android"))
            {
                if (Int32.Parse(lblQuantity.Text) > 0)
                {
                    listResult.Add(test.CollectData(driver));
                    DataTable dt = new DataTable();
                    dt.Columns.Add("ID");
                    dt.Columns.Add("Captured Time");
                    for (int m = 0; m < listResult[0].listResult.Count; m++)
                    {
                        dt.Columns.Add("Zone " + (m + 1));
                    }
                    int column = dt.Columns.Count;
                    for (int m = 0; m < listResult.Count; m++)
                    {
                        dt.Rows.Add();
                        dt.Rows[m][0] = (m + 1);
                        dt.Rows[m][1] = listResult[m].testDateTime;
                        for (int k = 2; k < column; k++)
                        {
                            dt.Rows[m][k] = listResult[m].listResult[k - 2];
                        }
                    }
                    dgvResult.DataSource = dt;
                    for (int k = 2; k < dt.Columns.Count; k++)
                    {
                        dgvResult.Columns[k].Width = 400;
                    }
                    dgvResult.Refresh();
                    lblQuantity.Text = (Int32.Parse(lblQuantity.Text) - 1).ToString();
                    lblQuantity.Refresh();
                }
                if (Int32.Parse(lblQuantity.Text) == 0)
                {
                    btnTest.Enabled = false;
                }
            }
        }

        [Obsolete]
        private void btnReConnect_Click(object sender, EventArgs e)
        {
            if (db.GetProductByID(preset.productID).type.Equals("Manual"))
            {
                lblDevice.Text = db.GetDeviceByID(preset.deviceID).name;
                lblProduct.Text = db.GetProductByID(preset.productID).productName;
                lblQuantity.Text = preset.quantity.ToString();
                if (preset.isManual)
                {
                    btnTest.Text = "Collect Result";
                }
                else
                {
                    btnTest.Text = "Test";
                }
                driver = test.BeforeAllAndroidManual(preset);
                listResult = new List<AutomationTestResult>();
            }
            else
            {
                lblDevice.Text = db.GetDeviceByID(preset.deviceID).name;
                lblProduct.Text = db.GetProductByID(preset.productID).productName;
                lblQuantity.Text = preset.quantity.ToString();
                if (preset.isManual)
                {
                    btnTest.Text = "Collect Result";
                }
                else
                {
                    btnTest.Text = "Test";
                }
               
                if (preset.platformName.Equals("Android"))
                {
                    if (!preset.isManual)
                    {
                        List<AutomationTestResult> listResult = test.BeforeAllAndroidVG(preset, dgvResult, lblQuantity);
                        //DataTable dt = new DataTable();
                        //dt.Columns.Add("ID");
                        //dt.Columns.Add("Captured Time");
                        //for (int m = 0; m < listResult[0].listResult.Count; m++)
                        //{
                        //    dt.Columns.Add("Zone " + (m + 1));
                        //}
                        //int column = dt.Columns.Count;
                        //for (int m = 0; m < listResult.Count; m++)
                        //{
                        //    dt.Rows.Add();
                        //    dt.Rows[m][0] = (m + 1);
                        //    dt.Rows[m][1] = listResult[m].testDateTime;
                        //    for (int k = 2; k < column; k++)
                        //    {
                        //        dt.Rows[m][k] = listResult[m].listResult[k - 2];
                        //    }
                        //}
                        //dgvResult.DataSource = dt;
                        //for (int k = 2; k < dt.Columns.Count; k++)
                        //{
                        //    dgvResult.Columns[k].Width = 400;
                        //}
                        //dgvResult.Refresh();

                        //lblQuantity.Text = (Int32.Parse(lblQuantity.Text) - 1).ToString();
                        //lblQuantity.Refresh();
                    }
                    if (Int32.Parse(lblQuantity.Text) == 0)
                    {
                        btnTest.Enabled = false;
                    }
                }
                else
                {
                    if (Int32.Parse(lblQuantity.Text) > 0)
                    {


                    }

                }

            }

        }

        private void btnRetest_Click(object sender, EventArgs e)
        {
            dgvResult.DataSource = null;
            dgvResult.Refresh();
            listResult = new List<AutomationTestResult>();
            lblQuantity.Text = preset.quantity.ToString();
            btnTest.Enabled = true;
        }

        private void btnBrowseCSV_Click(object sender, EventArgs e)
        {
            CSVFileDialog.ShowDialog();
            txtCSVFile.Text = CSVFileDialog.FileName;
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            ResultFolderDialog.ShowDialog();
            txtSaveAs.Text = ResultFolderDialog.SelectedPath;
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            if (txtCSVFile.Text.Length == 0)
            {
                MessageBox.Show("Please browse to the CSV file to get result.");
            }
            else
            {
                List<string> listData = Ultility.GetRawData(txtCSVFile.Text);

                listFullData = "";
                listFullData += listData[0] + "\n";
                int lastIndex = 1;

                for (int i = listResult.Count - 1; i >= 0; i--)
                {


                    List<String> listCompared = new List<String>();
                    for (int k = 0; k < listResult[i].listResult.Count; k++)
                    {
                        listCompared.Add(GetComparedData(listResult[i].listResult[k].Replace("\r\n", "").Replace("\"\"", "\"") + '"' + ';'));
                    }
                    for (int k = lastIndex; k < listData.Count; k++)
                    {
                        int pass = 0;
                        for (int m = 0; m < listCompared.Count; m++)
                        {
                            if (listData[k].Contains(listCompared[m]))
                            {
                                pass++;
                            }
                            else
                            {
                                m = listCompared.Count;
                            }
                        }
                        if (pass == listCompared.Count)
                        {
                            listFullData += listData[k] + "\n";
                            lastIndex = k;
                            k = listData.Count;
                        }

                    }

                }

            }
        }

        private String GetComparedData(String rawData)
        {
            String data = "";
            int positionNo = Int32.Parse(rawData.Split(';')[0].Replace("Zone ", "")) - 1;
            List<Zone> listZone = db.GetZonesByProductID(preset.productID);
            if (listZone[positionNo].method.Equals("STC"))
            {
                String[] splittedData = rawData.Split(';');
                data += splittedData[0].Replace("Zone ", "Zone=") + ";";
                data += "Code=" + splittedData[1] + ";";
                if (splittedData[2].Contains('/'))
                {
                    data += "NoRL=" + splittedData[2].Split('/')[0] + ";";
                    data += "NoVL=" + splittedData[2].Split('/')[1] + ";";
                    data += "NoTL=" + splittedData[2].Split('/')[2] + ";";
                }
                else
                {
                    data += "Mov=" + splittedData[2] + ";";
                    data += "Cal=" + splittedData[3] + ";";
                    data += "StdDev=" + splittedData[4];
                }

            }
            else if (!listZone[positionNo].method.Equals("CDS"))
            {
                String[] splittedData = rawData.Split(';');
                for (int i = 0; i < splittedData.Length; i++)
                {
                    if (splittedData[i].Contains("scoreBlurred"))
                    {
                        data += splittedData[i] + ";" + splittedData[i + 1];
                        i = splittedData.Length;
                    }
                }

            }
            else if (listZone[positionNo].method.Equals("CDS"))
            {
                String[] splittedData = rawData.Split(';');
                data += splittedData[0].Replace("Zone ", "Zone=") + ";";
                if (rawData.Contains("F"))
                {
                    data += rawData.Split('F')[0].Split(';')[1] + ";";
                    data += rawData.Split('F')[1].Split(';')[0].Replace("ineSigma", "FineSigma=") + ";";
                    for (int k = 2; k < splittedData.Length - 2; k++)
                    {
                        data += splittedData[k] + ";";
                    }

                }
                else
                {
                    for (int k = 1; k < splittedData.Length - 2; k++)
                    {
                        data += splittedData[k] + ";";
                    }

                }


            }
            else
            {
                String[] splittedData = rawData.Split(';');
                data += splittedData[0].Replace("Zone ", "Zone=") + ";";
                if (rawData.Contains("F"))
                {
                    data += rawData.Split('F')[0].Split(';')[1] + ";";
                    data += rawData.Split('F')[1].Split(';')[0].Replace("ineSigma", "FineSigma=") + ";";
                    for (int k = 2; k < splittedData.Length - 2; k++)
                    {
                        data += splittedData[k] + ";";
                    }

                }
                else
                {
                    for (int k = 1; k < splittedData.Length - 2; k++)
                    {
                        data += splittedData[k] + ";";
                    }

                }
            }
            return data;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            String path = txtSaveAs.Text + "\\" + txtCSVFile.Text.Split('\\')[txtCSVFile.Text.Split('\\').Length - 1];
            StreamWriter sw = new StreamWriter(new FileStream(path, FileMode.Create), Encoding.UTF8);
            sw.Write(listFullData);
            sw.Close();
        }
    }
}
