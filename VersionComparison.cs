﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Version = MiniTool.Data.Version;

namespace MiniTool
{
    public partial class VersionComparison : MasterForm
    {
        DatabaseConnection db;
        List<Chart> listChart;
        int currentChart;
        public VersionComparison() : base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            currentChart = 0;
            LoadVersionComparison();
            ShowChart(true, true);
            btnBack.Visible = false;
            btnNext.Visible = false;
        }

        public void LoadVersionComparison()
        {
            List<Version> listVersionA = db.GetAllVersions();
            List<Version> listVersionB = db.GetAllVersions();
            var bindingVersionA = new BindingSource();
            bindingVersionA.DataSource = listVersionA;
            var bindingVersionB = new BindingSource();
            bindingVersionB.DataSource = listVersionB;
            cbbVersionA.DataSource = bindingVersionA.DataSource;
            cbbVersionA.DisplayMember = "version";
            cbbVersionA.ValueMember = "id";
            cbbVersionB.DataSource = bindingVersionB.DataSource;
            cbbVersionB.DisplayMember = "version";
            cbbVersionB.ValueMember = "id";
        }

        private void cbbVersionA_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowChart(true, true);
            List<Product> listProduct = new List<Product>();
            if (cbbVersionA.SelectedIndex != -1 && cbbVersionB.SelectedIndex != -1)
            {
                List<Product> listProductA = db.GetProductThroughVersion(cbbVersionA.Text);
                List<Product> listProductB = db.GetProductThroughVersion(cbbVersionB.Text);

                for (int i = 0; i < listProductA.Count; i++)
                {
                    for (int k = 0; k < listProductB.Count; k++)
                    {
                        if (listProductA[i].id == listProductB[k].id)
                        {
                            listProduct.Add(listProductB[k]);
                        }
                    }
                }
                var bindingProduct = new BindingSource();
                bindingProduct.DataSource = listProduct;

                cbbProductCompare.DataSource = bindingProduct.DataSource;
                cbbProductCompare.DisplayMember = "productName";
                cbbProductCompare.ValueMember = "id";
            }
        }

        private void cbbVersionB_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowChart(true, true);
            List<Product> listProduct = new List<Product>();
            if (cbbVersionA.SelectedIndex != -1 && cbbVersionB.SelectedIndex != -1)
            {
                List<Product> listProductA = db.GetProductThroughVersion(cbbVersionA.Text);
                List<Product> listProductB = db.GetProductThroughVersion(cbbVersionB.Text);

                for (int i = 0; i < listProductA.Count; i++)
                {
                    for (int k = 0; k < listProductB.Count; k++)
                    {
                        if (listProductA[i].id == listProductB[k].id)
                        {
                            listProduct.Add(listProductB[k]);
                        }
                    }
                }
                var bindingProduct = new BindingSource();
                bindingProduct.DataSource = listProduct;

                cbbProductCompare.DataSource = bindingProduct.DataSource;
                cbbProductCompare.DisplayMember = "name";
                cbbProductCompare.ValueMember = "id";
            }


        }

        public void ShowChart(bool isAll, bool isCleared)
        {
            listChart = new List<Chart>();
            listChart.Add(Chart1A);
            listChart.Add(Chart2A);
            listChart.Add(Chart3A);
            listChart.Add(Chart4A);
            listChart.Add(Chart5A);
            listChart.Add(Chart6A);
            listChart.Add(Chart7A);
            listChart.Add(Chart8A);
            listChart.Add(Chart9A);
            listChart.Add(Chart10A);
            listChart.Add(Chart11A);
            listChart.Add(Chart1B);
            listChart.Add(Chart2B);
            listChart.Add(Chart3B);
            listChart.Add(Chart4B);
            listChart.Add(Chart5B);
            listChart.Add(Chart6B);
            listChart.Add(Chart7B);
            listChart.Add(Chart8B);
            listChart.Add(Chart9B);
            listChart.Add(Chart10B);
            listChart.Add(Chart11B);
            if (isCleared)
            {
                for (int i = 0; i < listChart.Count; i++)
                {
                    listChart[i].Visible = false;
                    listChart[i].ResetAutoValues();
                    listChart[i].Series.Clear();
                }
            }
            else if (!isAll)
            {
                DrawChartComparison(GetEvaluationDetail((Version)cbbVersionA.SelectedItem, (Product)cbbProductCompare.SelectedItem, cbbDeviceCompare.Text, Int32.Parse(cbbZoneCompare.SelectedValue.ToString().Split('-')[0].Replace("Zone ", "").TrimEnd())), Chart1A, cbbEdgeCompare.SelectedIndex);
                DrawChartComparison(GetEvaluationDetail((Version)cbbVersionB.SelectedItem, (Product)cbbProductCompare.SelectedItem, cbbDeviceCompare.Text, Int32.Parse(cbbZoneCompare.SelectedValue.ToString().Split('-')[0].Replace("Zone ", "").TrimEnd())), Chart1B, cbbEdgeCompare.SelectedIndex);

            }
            else if (isAll && !isCleared)
            {
                for (int i = 0; i < cbbEdgeCompare.Items.Count - 1; i++)
                {
                    DrawChartComparison(GetEvaluationDetail((Version)cbbVersionA.SelectedItem, (Product)cbbProductCompare.SelectedItem, cbbDeviceCompare.Text, Int32.Parse(cbbZoneCompare.SelectedValue.ToString().Split('-')[0].Replace("Zone ", "").TrimEnd())), listChart[i], i);
                    DrawChartComparison(GetEvaluationDetail((Version)cbbVersionB.SelectedItem, (Product)cbbProductCompare.SelectedItem, cbbDeviceCompare.Text, Int32.Parse(cbbZoneCompare.SelectedValue.ToString().Split('-')[0].Replace("Zone ", "").TrimEnd())), listChart[i + 11], i);
                }
            }
        }

        public List<sTCEvaluationDetail> GetEvaluationDetail(Version version, Product product, String deviceModel, int zone)
        {
            List<sTCEvaluationDetail> listEvaluationDetail = new List<sTCEvaluationDetail>();
            if (cbbVersionA.SelectedItem != null && cbbVersionB.SelectedItem != null && cbbProductCompare.SelectedItem != null && cbbDeviceCompare.SelectedItem != null && cbbZoneCompare.SelectedValue.ToString().Length != 0)
            {
                List<ResultForUploading> listDrawChart = db.GetResultByFullInfo(version.id, product.id, deviceModel, zone);
                for (int i = 0; i < listDrawChart.Count; i++)
                {
                    sTCEvaluationDetail evaluationDetail = Ultility.GetsTCEvaluationDetail(listDrawChart[i].statisticID, i, listDrawChart[i].info, CountEdge(listDrawChart));
                    listEvaluationDetail.Add(evaluationDetail);

                }
                return listEvaluationDetail;
            }
            else
            {
                return null;
            }

        }
        public int CountEdge(List<ResultForUploading> listData)
        {
            int count = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                int countEdge = 0;
                String[] data = listData[i].info.Split(';');
                for (int k = 0; k < data.Length; k++)
                {
                    if (data[k].Contains("Sigma="))
                    {
                        countEdge++;
                    }
                }
                if (countEdge > count)
                {
                    count = countEdge;
                }
            }
            return count;
        }


        public void DrawChartComparison(List<sTCEvaluationDetail> listEvaluationDetail, Chart chart, int edgePosition)
        {
            chart.Visible = true;
            chart.ResetAutoValues();
            chart.Series.Clear();
            chart.Titles.Clear(); ;
            chart.Legends[0].Docking = Docking.Bottom;

            if (edgePosition != -1)
            {
                chart.Titles.Add("Edge " + (edgePosition + 1));
                chart.Titles[0].Font = new Font("Arial", 14, FontStyle.Bold);
                chart.Titles[0].ForeColor = Color.DarkBlue;

                List<DataDrawChart> listChart = new List<DataDrawChart>();
                for (int i = 0; i < listEvaluationDetail.Count; i++)
                {
                    DataDrawChart data = new DataDrawChart();
                    data.cal = listEvaluationDetail[i].cal;
                    data.center = 0;
                    if (listEvaluationDetail[i].listEdge.Count != 0)
                    {
                        data.shiftingNegative = listEvaluationDetail[i].listEdge[edgePosition].shiftingNegative;
                        data.shiftingPositive = listEvaluationDetail[i].listEdge[edgePosition].shiftingPositive;
                        data.eTriggerNegative = listEvaluationDetail[i].listEdge[edgePosition].eTriggerNegative;
                        data.eTriggerPositive = listEvaluationDetail[i].listEdge[edgePosition].eTriggerPositive;
                        data.e = listEvaluationDetail[i].listEdge[edgePosition].e;
                    }
                    else
                    {
                        data.shiftingNegative = 0;
                        data.shiftingPositive = 0;
                        data.eTriggerNegative = 0;
                        data.eTriggerPositive = 0;
                        data.e = 0;
                    }

                    listChart.Add(data);
                }
                var seriesCenter = new Series();
                seriesCenter.ChartType = SeriesChartType.Line;
                seriesCenter.Name = "Center";
                seriesCenter.BorderWidth = 3;
                seriesCenter.Color = Color.Gray;
                chart.Series.Add(seriesCenter);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chart.Series[0].Points.AddXY(0, listChart[i].center);
                }


                var seriesTrigger1 = new Series();
                seriesTrigger1.ChartType = SeriesChartType.Line;
                seriesTrigger1.Name = "Trigger1";
                seriesTrigger1.BorderWidth = 3;
                seriesTrigger1.Color = Color.Green;
                chart.Series.Add(seriesTrigger1);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chart.Series[1].Points.AddXY(0, listChart[i].eTriggerPositive);
                }



                var seriesTrigger2 = new Series();
                seriesTrigger2.ChartType = SeriesChartType.Line;
                seriesTrigger2.Name = "Trigger2";
                seriesTrigger2.BorderWidth = 3;
                seriesTrigger2.Color = Color.Green;
                chart.Series.Add(seriesTrigger2);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chart.Series[2].Points.AddXY(0, listChart[i].eTriggerNegative);
                }


                var seriesShifting1 = new Series();
                seriesShifting1.ChartType = SeriesChartType.Line;
                seriesShifting1.Name = "Shifting1";
                seriesShifting1.BorderWidth = 3;
                seriesShifting1.Color = Color.Yellow;
                chart.Series.Add(seriesShifting1);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chart.Series[3].Points.AddXY(0, listChart[i].shiftingPositive);
                }


                var seriesShifting2 = new Series();
                seriesShifting2.ChartType = SeriesChartType.Line;
                seriesShifting2.Name = "Shifting2";
                seriesShifting2.BorderWidth = 3;
                seriesShifting2.Color = Color.Yellow;
                chart.Series.Add(seriesShifting2);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chart.Series[4].Points.AddXY(0, listChart[i].shiftingNegative);
                }


                var seriesCal = new Series();
                seriesCal.ChartType = SeriesChartType.Line;
                seriesCal.Name = "Cal";
                seriesCal.BorderWidth = 3;
                seriesCal.Color = Color.Red;
                chart.Series.Add(seriesCal);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chart.Series[5].Points.AddXY(0, listChart[i].cal);
                }


                var seriesE = new Series();
                seriesE.ChartType = SeriesChartType.Line;
                seriesE.Name = "E";
                seriesE.BorderWidth = 3;
                seriesE.Color = Color.DarkBlue;
                seriesE.IsValueShownAsLabel = true;
                seriesE.SmartLabelStyle.Enabled = true;
                chart.Series.Add(seriesE);
                for (int i = 0; i < listChart.Count(); i++)
                {
                    chart.Series[6].Points.AddXY(0, listChart[i].e);
                }
            }
        }

        private void cbbProductCompare_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowChart(true, true);
            List<Product> listProduct = new List<Product>();
            if (cbbVersionA.SelectedIndex != -1 && cbbVersionB.SelectedIndex != -1 && cbbProductCompare.SelectedIndex != -1)
            {
                List<Product> listProductA = db.GetProductThroughVersion(cbbVersionA.Text);
                List<Product> listProductB = db.GetProductThroughVersion(cbbVersionB.Text);

                for (int i = 0; i < listProductA.Count; i++)
                {
                    for (int k = 0; k < listProductB.Count; k++)
                    {
                        if (listProductA[i].id == listProductB[k].id)
                        {
                            listProduct.Add(listProductB[k]);
                        }
                    }
                }
                List<String> listDeviceModel = new List<String>();
                for (int i = 0; i < listProduct.Count; i++)
                {
                    List<String> listDeviceModelA = db.GetDeviceThroughVersionAndProduct(cbbVersionA.Text, listProduct[i].id);
                    List<String> listDeviceModelB = db.GetDeviceThroughVersionAndProduct(cbbVersionB.Text, listProduct[i].id);

                    for (int k = 0; k < listDeviceModelA.Count; k++)
                    {
                        for (int m = 0; m < listDeviceModelB.Count; m++)
                        {
                            if (listDeviceModelA[k].Equals(listDeviceModelB[m]) && !listDeviceModel.Contains(listDeviceModelB[m]))
                            {
                                listDeviceModel.Add(listDeviceModelB[m]);
                            }
                        }

                    }

                }
                
                var bindingDevice = new BindingSource();
                bindingDevice.DataSource = listDeviceModel;

                cbbDeviceCompare.DataSource = bindingDevice;
            }
        }

        private void cbbDeviceCompare_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowChart(true, true);
            List<Zone> listZoneCompared = db.GetProductByID(((Product)(cbbProductCompare.SelectedItem)).id).listZone;
            List<String> listZone = new List<String>();
            for (int i = 0; i < listZoneCompared.Count; i++)
            {
                listZone.Add("Zone " + (i + 1) + " - " + listZoneCompared[i].method);
            }
            var bindingZone = new BindingSource();
            bindingZone.DataSource = listZone;

            cbbZoneCompare.DataSource = bindingZone.DataSource;
        }

        private void cbbZoneCompare_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowChart(true, true);
            if (cbbZoneCompare.SelectedIndex != -1)
            {
                List<sTCEvaluationDetail> listEvaluationDetail;
                listEvaluationDetail = GetEvaluationDetail((Version)cbbVersionA.SelectedItem, (Product)cbbProductCompare.SelectedItem, cbbDeviceCompare.Text, Int32.Parse(cbbZoneCompare.SelectedValue.ToString().Split('-')[0].Replace("Zone ", "").TrimEnd()));

                if (listEvaluationDetail.Count != 0)
                {
                    List<String> listEdge = new List<String>();
                    for (int i = 0; i < listEvaluationDetail[0].listEdge.Count; i++)
                    {
                        listEdge.Add("Edge " + (i + 1));
                    }
                    listEdge.Add("All Edges");
                    var bindingEdge = new BindingSource();
                    bindingEdge.DataSource = listEdge;

                    cbbEdgeCompare.DataSource = bindingEdge.DataSource;
                }

            }
        }

        private void cbbEdgeCompare_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowChart(false, true);
            if (cbbEdgeCompare.SelectedIndex != (cbbEdgeCompare.Items.Count - 1))
            {
                btnNext.Visible = false;
                btnBack.Visible = false;
                ShowChart(false, false);
            }
            else
            {
                ShowChart(true, false);
                btnNext.Visible = true;
                btnBack.Visible = true;
                currentChart = 0;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (currentChart >= 0 && currentChart < cbbEdgeCompare.Items.Count - 2)
            {
                currentChart++;
                panel1.VerticalScroll.Value = 0;
                panel2.VerticalScroll.Value = 0;
                panel1.VerticalScroll.Value = currentChart * 400;
                panel2.VerticalScroll.Value = currentChart * 400;

            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (currentChart > 0 && currentChart < cbbEdgeCompare.Items.Count - 1)
            {
                currentChart--;
                if (currentChart == 0)
                {
                    panel1.VerticalScroll.Value = 0;
                    panel2.VerticalScroll.Value = 0;
                }

                panel1.VerticalScroll.Value = currentChart * 400;
                panel2.VerticalScroll.Value = currentChart * 400;



            }
        }
    }
}
