﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MiniTool
{
    public partial class FilteredComparison : MasterForm
    {
        List<DataStatisticShow> listDataNoDup;
        List<DataStatistic> listDataStatistic;
        List<DataStatisticShow> listDataStatisticShow;
        Dictionary<int, String> dictionarySeries;
        DatabaseConnection db;
        public FilteredComparison() : base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            dictionarySeries = new Dictionary<int, String>();
            LoadDataStatistic();
            cbbData.SelectedIndex = 5;
        }

        public void LoadDataStatistic()
        {
            List<ResultForUploading> listResult = db.GetAllResult();
            listDataStatistic = new List<DataStatistic>();
            listDataStatisticShow = new List<DataStatisticShow>();
            for (int i = 0; i < listResult.Count; i++)
            {
                ResultForUploading result = listResult[i];
                String[] detail = result.info.Split(';');
                List<String> listEdge = new List<String>();
                int start = FindTheFirstEdge(result.info);
                if (start > 0)
                {
                    for (int j = start; j < detail.Length - 1; j += 5)
                    {
                        int pos = listEdge.Count + 1;
                        if (detail[j].Split('=')[0].Equals("E" + pos))
                        {
                            DataStatisticShow dataStatisticShow = new DataStatisticShow();
                            dataStatisticShow.deviceModel = result.deviceModel;
                            dataStatisticShow.productName = result.productName;
                            dataStatisticShow.DrawChart = false;
                            dataStatisticShow.versionName = result.version;
                            dataStatisticShow.zone = result.zone.ToString();
                            dataStatisticShow.edgeNumber = "E" + pos;
                            listDataStatisticShow.Add(dataStatisticShow);
                            listEdge.Add("E" + pos);
                        }
                    }
                }

            }

            listDataNoDup = new List<DataStatisticShow>();
            for (int i = 0; i < listDataStatisticShow.Count(); i++)
            {
                if (!isDuplicated(listDataNoDup, listDataStatisticShow[i]))
                {
                    listDataNoDup.Add(listDataStatisticShow[i]);
                }
            }

            var bindingStatistic = new BindingSource();
            bindingStatistic.DataSource = listDataNoDup;
            dgvDataStatistic.DataSource = bindingStatistic;
        }

        public int FindTheFirstEdge(String data)
        {
            String[] detail = data.Split(';');
            int position = 0;
            for (int i = 0; i < detail.Length; i++)
            {
                if (detail[i].Contains("E1"))
                {
                    position = i;
                    i = detail.Length;
                }
            }
            return position;
        }

        public bool isDuplicated(List<DataStatisticShow> listData, DataStatisticShow data)
        {
            for (int i = 0; i < listData.Count; i++)
            {
                if (listData[i].deviceModel.Equals(data.deviceModel) && listData[i].productName.Equals(data.productName) && listData[i].versionName.Equals(data.versionName) && listData[i].zone.Equals(data.zone) && listData[i].edgeNumber.Equals(data.edgeNumber))
                {
                    return true;
                }
            }
            return false;
        }

        private void txtVersion_TextChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void txtProduct_TextChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void txtDevice_TextChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void txtZone_TextChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void txtEdge_TextChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        public void FilterData()
        {
            dictionarySeries = new Dictionary<int, string>();
            chartStatistic.ResetAutoValues();
            chartStatistic.Series.Clear();
            var bindingStatistic = new BindingSource();
            bindingStatistic.DataSource = listDataNoDup;
            dgvDataStatistic.DataSource = bindingStatistic;
            DataTable dt = new DataTable();
            foreach (DataGridViewColumn column in dgvDataStatistic.Columns)
            {
                dt.Columns.Add(column.HeaderText, column.ValueType);
            }

            //Adding the Rows.
            foreach (DataGridViewRow row in dgvDataStatistic.Rows)
            {
                dt.Rows.Add();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Value != null)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value;
                    }
                }
            }

            List<String> filtered = new List<string>();
            for (int i = 0; i < 5; i++)
            {
                filtered.Add("112233");
            }
            List<String> columnNames = new List<string>();
            columnNames.Add("AppVersion");
            columnNames.Add("Product");
            columnNames.Add("Device");
            columnNames.Add("Zone");
            columnNames.Add("Edge");
            int countFilter = 0;
            int firstPosition = -1;
            String query = "";
            if (txtVersion.Text.Length > 0)
            {
                countFilter++;
                filtered[0] = txtVersion.Text;
                if (firstPosition == -1)
                {
                    firstPosition = 0;
                }
            }
            if (txtProduct.Text.Length > 0)
            {
                countFilter++;
                filtered[1] = txtProduct.Text;
                if (firstPosition == -1)
                {
                    firstPosition = 1;
                }
            }
            if (txtDevice.Text.Length > 0)
            {
                countFilter++;
                filtered[2] = txtDevice.Text;
                if (firstPosition == -1)
                {
                    firstPosition = 2;
                }
            }
            if (txtZone.Text.Length > 0)
            {
                countFilter++;
                filtered[3] = txtZone.Text;
                if (firstPosition == -1)
                {
                    firstPosition = 3;
                }
            }
            if (txtEdge.Text.Length > 0)
            {
                countFilter++;
                filtered[4] = txtEdge.Text;
                if (firstPosition == -1)
                {
                    firstPosition = 4;
                }
            }
            int countQuery = 2;
            if (firstPosition != -1)
            {
                if (countFilter == 1)
                {
                    query += String.Format(columnNames[firstPosition] + " Like '%" + filtered[firstPosition] + "%'");
                }
                else
                {
                    query += String.Format(columnNames[firstPosition] + " Like '%" + filtered[firstPosition] + "%' and ");
                    for (int i = firstPosition + 1; i < filtered.Count; i++)
                    {
                        if (countQuery != countFilter)
                        {
                            if (!filtered[i].Equals("112233"))
                            {
                                query += String.Format(columnNames[i] + " Like '%" + filtered[i] + "%' and ");
                                countQuery++;
                            }

                        }
                        else
                        {
                            if (!filtered[i].Equals("112233"))
                            {
                                query += String.Format(columnNames[i] + " Like '%" + filtered[i] + "%'");
                                i = filtered.Count;
                            }

                        }

                    }
                }
            }

            dgvDataStatistic.DataSource = dt;
            dt.DefaultView.RowFilter = query;
        }

        private void dgvDataStatistic_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            List<ResultForUploading> listDrawStatistic = new List<ResultForUploading>();
            List<sTCEvaluationDetail> listEvaluationStatisticDetail = new List<sTCEvaluationDetail>();
            Int32 selectedRowCount = dgvDataStatistic.SelectedRows[0].Index;
            if (selectedRowCount >= 0)
            {
                if (Convert.ToBoolean(dgvDataStatistic.Rows[selectedRowCount].Cells[0].Value))
                {
                    dgvDataStatistic.Rows[selectedRowCount].Cells[0].Value = false;
                    if (dictionarySeries.Count > 0)
                    {

                        for (int i = 0; i < dictionarySeries.Count; i++)
                        {
                            if (dictionarySeries.Keys.ElementAt(i) == selectedRowCount)
                            {
                                int removedPosition = 0;
                                for (int j = 0; j < chartStatistic.Series.Count; j++)
                                {
                                    if (dictionarySeries.Values.ElementAt(i).Equals(chartStatistic.Series[j].Name))
                                    {
                                        removedPosition = j;
                                    }
                                }
                                chartStatistic.Series.Remove(chartStatistic.Series[removedPosition]);
                                chartStatistic.ResetAutoValues();
                                dictionarySeries.Remove(selectedRowCount);
                                i = dictionarySeries.Count;
                            }
                        }
                    }

                }
                else
                {
                    dgvDataStatistic.Rows[selectedRowCount].Cells[0].Value = true;
                    listDrawStatistic = db.GetResultByNameInfo(dgvDataStatistic.Rows[selectedRowCount].Cells[1].Value.ToString(), dgvDataStatistic.Rows[selectedRowCount].Cells[2].Value.ToString(), dgvDataStatistic.Rows[selectedRowCount].Cells[3].Value.ToString(), Int32.Parse(dgvDataStatistic.Rows[selectedRowCount].Cells[4].Value.ToString()));
                    String seriesName = dgvDataStatistic.Rows[selectedRowCount].Cells[1].Value.ToString() + " - " + dgvDataStatistic.Rows[selectedRowCount].Cells[2].Value.ToString() + " - " + dgvDataStatistic.Rows[selectedRowCount].Cells[3].Value.ToString() + "- " + dgvDataStatistic.Rows[selectedRowCount].Cells[4].Value.ToString() + " - " + dgvDataStatistic.Rows[selectedRowCount].Cells[5].Value.ToString();


                    for (int i = 0; i < listDrawStatistic.Count; i++)
                    {
                        sTCEvaluationDetail evaluationDetail = Ultility.GetsTCEvaluationDetail(listDrawStatistic[i].statisticID, i, listDrawStatistic[i].info, CountEdge(listDrawStatistic));
                        listEvaluationStatisticDetail.Add(evaluationDetail);
                    }

                    DrawChart(seriesName, listEvaluationStatisticDetail, Int32.Parse(dgvDataStatistic.Rows[selectedRowCount].Cells[5].Value.ToString().Replace("E", "")));
                    dictionarySeries.Add(selectedRowCount, seriesName);
                }
            }
        }

        public int CountEdge(List<ResultForUploading> listData)
        {
            int count = 0;
            for (int i = 0; i < listData.Count; i++)
            {
                int countEdge = 0;
                String[] data = listData[i].info.Split(';');
                for (int k = 0; k < data.Length; k++)
                {
                    if (data[k].Contains("Sigma="))
                    {
                        countEdge++;
                    }
                }
                if (countEdge > count)
                {
                    count = countEdge;
                }
            }
            return count;
        }

        public void DrawChart(String seriesName, List<sTCEvaluationDetail> listEvaluationStatisticDetail, int edge)
        {
            chartStatistic.MinimumSize = new Size(1200, 500);
            chartStatistic.Width = listEvaluationStatisticDetail.Count() * 10;
            chartStatistic.ResetAutoValues();
            chartStatistic.Legends[0].Docking = Docking.Bottom;
            int position = edge - 1;

            List<DataDrawChart> listChart = new List<DataDrawChart>();
            for (int i = 0; i < listEvaluationStatisticDetail.Count; i++)
            {
                DataDrawChart data = new DataDrawChart();
                data.cal = listEvaluationStatisticDetail[i].cal;
                data.center = 0;
                if (listEvaluationStatisticDetail[i].listEdge.Count != 0)
                {
                    data.shiftingNegative = listEvaluationStatisticDetail[i].listEdge[position].shiftingNegative;
                    data.shiftingPositive = listEvaluationStatisticDetail[i].listEdge[position].shiftingPositive;
                    data.eTriggerNegative = listEvaluationStatisticDetail[i].listEdge[position].eTriggerNegative;
                    data.eTriggerPositive = listEvaluationStatisticDetail[i].listEdge[position].eTriggerPositive;
                    data.e = listEvaluationStatisticDetail[i].listEdge[position].e;
                }
                else
                {
                    data.shiftingNegative = 0;
                    data.shiftingPositive = 0;
                    data.eTriggerNegative = 0;
                    data.eTriggerPositive = 0;
                    data.e = 0;
                }

                listChart.Add(data);
            }
            var series = new Series();
            series.ChartType = SeriesChartType.Line;
            series.Name = seriesName;
            series.BorderWidth = 3;
            Random random = new Random();
            series.IsValueShownAsLabel = true;
            series.SmartLabelStyle.Enabled = true;
            series.Color = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            chartStatistic.Series.Add(series);
            for (int i = 0; i < listChart.Count(); i++)
            {
                if (cbbData.SelectedIndex == 0)
                {
                    chartStatistic.Series[chartStatistic.Series.Count - 1].Points.AddXY(0, listChart[i].eTriggerPositive);
                }
                else if (cbbData.SelectedIndex == 1)
                {
                    chartStatistic.Series[chartStatistic.Series.Count - 1].Points.AddXY(0, listChart[i].eTriggerNegative);
                }
                else if (cbbData.SelectedIndex == 2)
                {
                    chartStatistic.Series[chartStatistic.Series.Count - 1].Points.AddXY(0, listChart[i].shiftingPositive);
                }
                else if (cbbData.SelectedIndex == 3)
                {
                    chartStatistic.Series[chartStatistic.Series.Count - 1].Points.AddXY(0, listChart[i].shiftingNegative);
                }
                else if (cbbData.SelectedIndex == 4)
                {
                    chartStatistic.Series[chartStatistic.Series.Count - 1].Points.AddXY(0, listChart[i].cal);
                }
                else if (cbbData.SelectedIndex == 5)
                {
                    chartStatistic.Series[chartStatistic.Series.Count - 1].Points.AddXY(0, listChart[i].e);
                }

            }



        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtVersion.Text = "";
            txtProduct.Text = "";
            txtDevice.Text = "";
            txtEdge.Text = "";
            txtZone.Text = "";
            cbbData.SelectedIndex = 5;
            LoadDataStatistic();
            chartStatistic.ResetAutoValues();
            chartStatistic.Series.Clear();
            dictionarySeries.Clear();
        }
    }
}
