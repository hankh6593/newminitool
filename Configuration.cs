﻿using MiniTool.Controller;
using MiniTool.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Version = MiniTool.Data.Version;

namespace MiniTool
{
    public partial class Configuration : MasterForm
    {
        List<Device> listDevice;
        List<DeviceID> listDeviceIDs;
        List<Product> listProduct;
        List<Version> listVersion;
        DatabaseConnection db;
        public Configuration() : base()
        {
            InitializeComponent();
            db = new DatabaseConnection();
            LoadDataDevices();
            LoadDataProducts();
            LoadDataVersions();
            LoadDataDeviceModel();
            progressBarMaster.Visible = false;
        }

        private void LoadDataDeviceModel()
        {
            listDeviceIDs = db.GetAllDeviceModel();
            dgvMasterList.DataSource = listDeviceIDs;
            dgvMasterList.Columns[0].Width = 50;
            dgvMasterList.Columns[1].Width = 200;
            dgvMasterList.Columns[2].Width = 200;
            dgvMasterList.Columns[3].Width = 200;
            cbbMasterServer.SelectedIndex = 0;
            cbbThresholdServer.SelectedIndex = 0;
        }

        private void LoadDataDevices()
        {
            listDevice = db.GetAllDevices(2);
            dgvDevices.DataSource = listDevice;
            dgvDevices.Columns[0].Width = 50;
            dgvDevices.Columns[1].Width = 300;
            dgvDevices.Columns[2].Width = 300;
            dgvDevices.Columns[3].Width = 300;

        }
        private void LoadDataProducts()
        {
            listProduct = db.GetAllProducts();
            dgvProduct.DataSource = listProduct;
            dgvProduct.Columns[0].Width = 50;
            dgvProduct.Columns[1].Width = 200;
            dgvProduct.Columns[5].Width = 200;

        }

        private void LoadDataVersions()
        {
            listVersion = db.GetAllVersions();
            dgvVersion.DataSource = listVersion;
            dgvVersion.Columns[0].Width = 100;
            dgvVersion.Columns[1].Width = 200;
            dgvVersion.Columns[2].Width = 200;

        }

        private void btnResetDevice_Click(object sender, EventArgs e)
        {
            txtDeviceID.Text = "";
            txtDeviceModel.Text = "";
            txtDeviceCode.Text = "";
            txtDeviceName.Text = "";
            txtDeviceEmail.Text = "";
            txtLocation.Text = "";
            txtType.Text = "";
            txtUDID.Text = "";

        }

        private void btnResetProduct_Click(object sender, EventArgs e)
        {
            txtProductNo.Text = "";
            txtProductID.Text = "";
            txtProject.Text = "";
            txtCompanyID.Text = "";
            txtCompanyName.Text = "";
            txtProductName.Text = "";
            txtResult.Text = "";
            cbbType.SelectedIndex = 0;
        }

        private void btnResetVersion_Click(object sender, EventArgs e)
        {
            txtVersionID.Text = "";
            txtVersion.Text = "";
            txtOS.Text = "";
        }

        private void dgvDevices_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Int32 selectedRowCount = dgvDevices.SelectedRows[0].Index;
            if (selectedRowCount >= 0)
            {
                txtDeviceID.Text = (dgvDevices.Rows[selectedRowCount].Cells[0].Value.ToString());
                txtDeviceModel.Text = (dgvDevices.Rows[selectedRowCount].Cells[1].Value.ToString());
                txtDeviceCode.Text = (dgvDevices.Rows[selectedRowCount].Cells[2].Value.ToString());
                txtDeviceName.Text = (dgvDevices.Rows[selectedRowCount].Cells[3].Value.ToString());
                txtDeviceEmail.Text = (dgvDevices.Rows[selectedRowCount].Cells[4].Value.ToString());
                txtLocation.Text = (dgvDevices.Rows[selectedRowCount].Cells[5].Value.ToString());
                txtType.Text = (dgvDevices.Rows[selectedRowCount].Cells[6].Value.ToString());
                txtUDID.Text = (dgvDevices.Rows[selectedRowCount].Cells[7].Value.ToString());

            }
        }

        private void dgvProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Int32 selectedRowCount = dgvProduct.SelectedRows[0].Index;
            if (selectedRowCount >= 0)
            {
                txtProductNo.Text = (dgvProduct.Rows[selectedRowCount].Cells[0].Value.ToString());
                txtProject.Text = (dgvProduct.Rows[selectedRowCount].Cells[1].Value.ToString());
                txtCompanyID.Text = (dgvProduct.Rows[selectedRowCount].Cells[2].Value.ToString());
                txtCompanyName.Text = (dgvProduct.Rows[selectedRowCount].Cells[3].Value.ToString());
                txtProductID.Text = (dgvProduct.Rows[selectedRowCount].Cells[4].Value.ToString());
                txtProductName.Text = (dgvProduct.Rows[selectedRowCount].Cells[5].Value.ToString());
                txtResult.Text = (dgvProduct.Rows[selectedRowCount].Cells[6].Value.ToString());
                cbbType.Text = (dgvProduct.Rows[selectedRowCount].Cells[7].Value.ToString());

            }
        }

        private void dgvVersion_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Int32 selectedRowCount = dgvVersion.SelectedRows[0].Index;
            if (selectedRowCount >= 0)
            {
                txtVersionID.Text = (dgvVersion.Rows[selectedRowCount].Cells[0].Value.ToString());
                txtVersion.Text = (dgvVersion.Rows[selectedRowCount].Cells[1].Value.ToString());
                txtOS.Text = (dgvVersion.Rows[selectedRowCount].Cells[2].Value.ToString());

            }
        }

        private void btnAddDevice_Click(object sender, EventArgs e)
        {
            if (txtDeviceID.Text.Length == 0)
            {
                db.AddDevice(txtDeviceModel.Text, txtDeviceCode.Text, txtDeviceName.Text, txtDeviceEmail.Text, txtLocation.Text, txtType.Text, txtUDID.Text, false);
                LoadDataDevices();
                btnResetDevice_Click(sender, e);
                MessageBox.Show("A new device has been added successfully.");
            }
            else
            {
                MessageBox.Show("Please click 'Reset' button and fill in information again to add new device.");
            }
        }

        private void btnUpdateDevice_Click(object sender, EventArgs e)
        {
            if (txtDeviceID.Text.Length != 0)
            {
                db.UpdateDeviceByID(Int32.Parse(txtDeviceID.Text), txtDeviceModel.Text, txtDeviceCode.Text, txtDeviceName.Text, txtDeviceEmail.Text, txtLocation.Text, txtType.Text, txtUDID.Text, false);
                LoadDataDevices();
                MessageBox.Show("A device has been updated successfully.");
            }
            else
            {
                MessageBox.Show("Please select Device to update.");
            }

        }

        private void btnRemoveDevice_Click(object sender, EventArgs e)
        {
            if (txtDeviceID.Text.Length != 0)
            {
                db.RemoveDeviceByID(Int32.Parse(txtDeviceID.Text));
                LoadDataDevices();
                btnResetDevice_Click(sender, e);
                MessageBox.Show("A device has been removed successfully.");
            }
            else
            {
                MessageBox.Show("Please select Device to remove.");
            }
        }

        private void btnAddVersion_Click(object sender, EventArgs e)
        {
            if (txtVersionID.Text.Length == 0)
            {
                MessageBox.Show("A new version has been added successfully.");
                db.AddVersion(txtVersion.Text, txtOS.Text);
                LoadDataVersions();
                btnResetVersion_Click(sender, e);
            }
            else
            {
                MessageBox.Show("Please click on 'Reset' button and fill in information again to add new version.");
            }
        }

        private void btnUpdateVersion_Click(object sender, EventArgs e)
        {
            if (txtVersionID.Text.Length != 0)
            {
                MessageBox.Show("A version has been updated successfully.");
                db.UpdateVersionByID(Int32.Parse(txtVersionID.Text), txtVersion.Text, txtOS.Text);
                LoadDataVersions();
            }
            else
            {
                MessageBox.Show("Please select Version to update.");

            }
        }

        private void btnRemoveVersion_Click(object sender, EventArgs e)
        {
            if (txtVersionID.Text.Length != 0)
            {
                MessageBox.Show("A version has been removed successfully.");
                db.RemoveVersionByID(Int32.Parse(txtVersionID.Text));
                LoadDataVersions();
                btnRemoveVersion_Click(sender, e);
            }
            else
            {
                MessageBox.Show("Please select Version to remove.");

            }
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            if (txtProductNo.Text.Length == 0)
            {
                int productID = db.AddProduct(txtProject.Text, Int32.Parse(txtCompanyID.Text), txtCompanyName.Text, Int32.Parse(txtProductID.Text), txtProductName.Text, txtResult.Text, cbbType.Text);
                txtProductNo.Text = productID.ToString();
                MessageBox.Show("Product has been added successfully. Please remember to setup Zone.");
                LoadDataProducts();
                btnResetProduct_Click(sender, e);

            }
            else
            {
                MessageBox.Show("Please click on 'Reset' button and fill in information again to add new product.");
            }
        }

        private void btnUpdateProduct_Click(object sender, EventArgs e)
        {
            if (txtProductNo.Text.Length != 0)
            {
                db.UpdateProductByID(Int32.Parse(txtProductNo.Text), txtProject.Text, Int32.Parse(txtCompanyID.Text), txtCompanyName.Text, Int32.Parse(txtProductID.Text), txtProductName.Text, txtResult.Text, cbbType.Text);
                LoadDataProducts();
                MessageBox.Show("Product has been updated successfully.");
            }
            else
            {
                MessageBox.Show("Please select Product to update.");
            }
            
        }

        private void btnRemoveProduct_Click(object sender, EventArgs e)
        {
            if (txtProductNo.Text.Length != 0)
            {
                db.RemoveProductByID(Int32.Parse(txtProductNo.Text));
                LoadDataProducts();
                MessageBox.Show("Product has been removed successfully.");
                btnResetProduct_Click(sender, e);
            }
            else
            {
                MessageBox.Show("Please select Product to remove.");

            }
        }

        private void btnSetupZone_Click(object sender, EventArgs e)
        {
            if (txtProductNo.Text.Length == 0)
            {
                MessageBox.Show("Please select the product to setup zone");
            }
            else
            {
                SetupZone form = new SetupZone(Int32.Parse(txtProductNo.Text));
                form.Show();
            }
        }

        private void btnBrowseJSON_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtMasterJSONPath.Text = openFileDialog.FileName;
        }

        private void btnImportJSON_Click(object sender, EventArgs e)
        {
           
            String server = cbbMasterServer.Text.Split('-')[0].Replace(" ", "");
            db.RemoveAllDeviceModelByServer(server);
            List<DeviceID> listDeviceID = Ultility.GetListDeviceIDFromJSON(txtMasterJSONPath.Text, server);
            progressBarMaster.Visible = true;
            progressBarMaster.Maximum = listDeviceID.Count * 10;
            for (int i = 0; i<listDeviceID.Count; i++)
            {
                db.AddDeviceModel(listDeviceID[i]);
                progressBarMaster.Value = i * 10;
            }
            progressBarMaster.Visible = false;
            MessageBox.Show("The Master device list has been imported.");
            LoadDataDeviceModel();
        }

        private void btnBrowseThreshold_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            txtThresholdJSONPath.Text = openFileDialog.FileName;
        }

        private void btnBrowseSaveAs_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.ShowDialog();
            txtSaveAsThreshold.Text = folderBrowserDialog.SelectedPath;
        }

        private void btnImportThreshold_Click(object sender, EventArgs e)
        {
            String server = cbbThresholdServer.Text.Split('-')[0].Replace(" ", "");
            List<ThresholdImported> listImported = Ultility.GetListThresholdFromJSON(txtThresholdJSONPath.Text, -1, server);
            progressBarMaster.Visible = true;
            progressBarMaster.Maximum = listImported.Count * 10;
            for (int i = 0; i < listImported.Count; i++)
            {
              //  db.AddDeviceModel(listDeviceID[i]);
                progressBarMaster.Value = i * 10;
            }
            progressBarMaster.Visible = false;
            MessageBox.Show("The Master device list has been imported.");
            LoadDataDeviceModel();
        }

     
    }
}
